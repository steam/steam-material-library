---
runme:
  id: 01HP6NYCWHG50Q9ZKYZR49NBXF
  version: v2.2
---

# COMSOL

To use a steam-material-library function on COMSOL:

- [Visit the pipeline page on Gitlab.](https://gitlab.cern.ch/steam/steam-material-library/-/pipelines)
- Download the artifacts of the latest -passed- pipeline (download button on the right side of your screen) for your desired operating system.
- Right-click on "Global Definitions" button, in COMSOL Model Builder.
- Click on "Functions", then "external"
- Select the function from the DLL/SO folder and then add the arguments of the function. If the amount of arguments is unknown, it is best to check the source c file.

Below you can find an example of how the function plotting should look like:

![Comsol example](../../static/comsol_example.png)