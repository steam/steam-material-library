---
runme:
  id: 01HHFBDE7W3YAWCVKH2MYHRFY1
  version: v2.2
---

It is possible to compile the source functions into files compatible with MATLAB, allowing MATLAB-based interfaces to use them. For instance, [STEAM-LEDET](https://gitlab.cern.ch/steam/steam-ledet), is an interface that simulates electro-magnetic and thermal transients in superconducting magnets and it is based on the MATLAB programming language.

The C-models are wrapped by [generic_wrapper.cpp](https://gitlab.cern.ch/steam/steam-material-library/-/blob/master/mex_compilation/generic_wrapper.cpp) which implements the MATLAB API. Functions with up until 15 input parameters can be compiled into .mex files and can take matrice, vector and scalar inputs.

To simplify the use of the mex library, a MATLAB wrapper class is automatically generated. This class is responsible for wrapping `.mex` files, making them behave in the same way as the original steam-ledet MATLAB functions. As a result, the steam-ledet MATLAB functions can be replaced by these converted (c into mex) functions while maintaining the same format and usage.

## Example

Before running the script below, make sure you have cloned `steam-material-library` and [built the MATLAB bindings](#mex-compilation-process).

```
    % Add the paths required
    % Choose as parentDirectory the path for steam-material-library on your local machine
    addpath( genpath( fullfile(parentDirectory, 'steam-material-library\source_m') ) )
    addpath( genpath( fullfile(parentDirectory, 'steam-material-library\mex_compilation\compiled_mex') ) )
    addpath( genpath( fullfile(parentDirectory, 'steam-material-library\compile_STEAM_MatPro\compiled_STEAM_MatPro') ) 

    % Setup material properties by assigning STEAM_MatPro function handles to current functions
    functionSet='mex'; % functionSet='m'; in case we want to use the original MatLab functions
    STEAM_ML = STEAM_MatPro(functionSet);
    
    % Define T, B & RRR_Cu
    rho_el_0 = STEAM_ML.rhoCu(T, B, RRR_Cu);
```

## Mex compilation process

### Prerequisites
In order to compile C to Mex files **MinGW-w64 C/C++ Compiler** is required. To install the compiler follow
the steps below: 
1. Open MatLab -> go to HOME -> Add-Ons
2. Search for MinGW-w64 C/C++ Compiler as shown in the image below, and click **install**:

![img.png](img.png) 

3. Restart MatLab and you are ready!

### Compilation

1. Create a new Python virtual environment (`python -m venv venv`), activate it (Powershell: `.\venv\Scripts\activate.ps1`) and install the `requirements.txt` in the root directory (`pip install -r requirements.txt`)
2. Enter the `bindings/matlab/` directory and run `python build.py`
3. Done!