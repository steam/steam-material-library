---
runme:
  id: 01HP4JYVT5N76XFEQFDAPK03GY
  version: v2.2
---

# Python

```
pip install STEAM-materials==2024.2.3
```

The source functions of steam-material-library can be used as a shared library, but also integrated into Python functions. The functions are first compiled into a shared library (`.dll` on Windows, `.so` on Linux), using CMake, which that they can be employed by interfaces such as COMSOL and SIGMA. Subsequently, Python bindings allow pythonic access to the C functions through the [Pypi STEAM-material library](https://pypi.org/project/STEAM-materials/), for applications such as pyBBQ, a Quench model interface, and Ni-coils, a simulation tool for the thermo-electromagnetic behavior of no-insulation coils.

## Example

First, the package must be installed from [Pypi](https://pypi.org/project/STEAM-materials/) (for example, by using `pip`).
Then the module can be imported and used as shown below:

``` 
import os
import numpy as np
import matplotlib.pyplot as plt
from steammaterials import SteamMaterials

B = [0.0, 4.0, 8.0, 12.0]
T_ref_RRR = 295
RRR = 1000
T = np.linspace(0, 300, 1000)
for i in range(len(B)):
    b = B[i] * np.ones(T.shape)
    rrr = RRR * np.ones(T.shape)
    rrr_ref = T_ref_RRR * np.ones(T.shape)
    numpy2d = np.vstack((T, b, rrr, rrr_ref))

#1 argument function
sm_CvHast = SteamMaterials("CFUN_CvHast_v1", 1, len(T))
Cv_hast   = sm_CvHast.evaluate(T)
#4 arguments function
sm_cp_rho = SteamMaterials("CFUN_rhoCu_v1", numpy2d.shape[0], numpy2d.shape[1])
RhoCu     = sm_cp_rho.evaluate(numpy2d)

# Plots the results
plt.figure(1)
plt.plot(T,Cv_hast)
plt.title('Hastelloy-Specific Heat')
plt.figure(2)
plt.plot(T, RhoCu)
plt.title('Copper-Resistivity')
plt.show()      
```
