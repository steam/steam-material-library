---
runme:
  id: 01HP44KJVV4VVYWJM30HPQ06ZC
  version: v2.2
---
# Compiling shared libraries

## Compilation requirements
- A C compiler
    - Windows: Microsoft Visual Studio 
    - Linux-based systems: GCC
- CMake
:material-information-outline:


### Visual Studio
If you need to install Microsoft VS, make sure to add the correct package that is "Desktop development with C++" during the installation.
In case you already have Microsoft VS, you can add the package accordingly: Tools -> Get Tools and Features... -> Check the box "Desktop development with C++"

![img_1.png](img_1.png)

Make sure to include CMake if prompted.

And you are ready!

## Library compilation process
Here are the steps to compile shared libraries for the models found in `models\c\`: 

- Create a build folder, for example `build`, and enter it (`cd build`)  
- Prepare CMake with `cmake ..`  
- Compile the libraries with `cmake --build . --config Release -j4 -v` (Of course, the number of threads `-jX` can be adjusted to your machine)  
