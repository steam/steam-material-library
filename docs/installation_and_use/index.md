# Programming languages

The STEAM material library is coded in [**C**](Use/c.md).

It is possible to use the material in other programming languages, for example:

- [**Python**](Use/python.md)

- [**MatLab**](Use/matlab.md)

- [**COMSOL**](Use/comsol.md)

The diagram below illustrates the STEAM-material library's contributions in the context of simulation interfaces:

![Flow chart for the contribution of STEAM-material-library](../static/steamMaterialLib_Diagram.png)

## Platforms

| API | Windows | Linux |
|---|---|---|
| Raw C Models | :heavy_check_mark: | :heavy_check_mark: |
| Python Bindings | :heavy_check_mark: | :heavy_check_mark: |
| C++ Wrapper | :heavy_check_mark: | :heavy_check_mark: |
| MATLAB | :heavy_check_mark: | :x: |

Only Windows is currently fully supported.
Linux support is in progress.