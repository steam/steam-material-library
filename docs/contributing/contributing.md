# Contributing
If you have material properties that you would like to share with others using this platform please contact the **STEAM Team** by e-mail.

[steam-team@cern.ch](mailto:steam-team@cern.ch){ .md-button .md-button--primary }
