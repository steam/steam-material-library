# How to add new C functions

Below is the template available for creating new C functions that
can be compiled and used within STEAM modeling tools properly:

```commandline
#include <math.h>
#include <float.h>
#include <stdlib.h>
#include <string.h>
#ifdef _MSC_VER
#define EXPORT __declspec(dllexport)
#else
#define EXPORT
#endif	


static const char *error = NULL;

EXPORT int init(const char *str) {
  return 1;
}

EXPORT const char * getLastError() {
  return error;
}

EXPORT int eval(const char *func,
                              int nArgs,
                              const double **inReal,
                              const double **inImag,
                              int blockSize,
                              double *outReal,
                              double *outImag) {
							
    int i;
  
    if (strcmp("C_function_name", func) == 0) {
    
        if (nArgs != your_num_of_args) {
            error = "your_num_of_args arguments expected";
            return 0;
        }

        for (i = 0; i < blockSize; i++) {    
	        double input1        = inReal[0][i];
	        double input2        = inReal[1][i];
            
                ...
	        
	            Your Code and Calculations here
	        ...
            
                outReal[i] = //output

	        if(outReal[i]!=outReal[i]){
	        error = "Output is nan"; 
	        return 0;	  
	        }
        }
        	        
    return 1;
    }

    else {
        error = "Unknown function";
        return 0;
    }

}
```