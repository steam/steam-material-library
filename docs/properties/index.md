---
runme:
  id: 01HHKWXED379JFEEJZFVJMNSM6
  version: v2.0
---

# Material properties and other functions

The following material property types are available

- [**Resistivity**](Resistivity/index.md)
- [__Thermal Conductivity__](Thermal_conductivity/index.md)
- [__Specific Heat__](Specific_heat/index.md)
- [__Volumetric heat capacity__](Volumetric_heat_capacity/index.md)
- [__Critical Current__](Critical_current/index.md)
- [__Critical current density (HTS)__](Critical_current_density_(HTS)/index.md)
- [__Critical current density (LTS)__](Critical_current_density_(LTS)/index.md)
- [**Derivatives**](Derivatives/index.md)
