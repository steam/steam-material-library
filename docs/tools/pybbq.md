# PyBBQ

Just like Ni-coils, [PyBBQ](https://gitlab.cern.ch/steam/pybbq) is a quench modeling tool written in python. It is the updated version of BBQ (Bus-Bar-Quench), and it is able to create geometries and illustrate the thermal transfer for HTS magnet models.