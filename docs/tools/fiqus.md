# FiQuS

[FiQuS](https://gitlab.cern.ch/steam/fiqus) or else Finite Element Method Quench Simulation tool, is an open-source tool based on Gmsh, CERNGetDP and ONELAB. It is a modeling tool for both high and low temperature superconductors. FiQuS interface uses material functions through CERNGetDP, during numerical operations mostly. 