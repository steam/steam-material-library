---
runme:
  id: 01HP4PRCYCAVEHSRTVWBW30STF
  version: v3
---

# Tools

The STEAM material library has been currently integrated into many different simulation modeling tools for both LTS and HTS magnets. Specifically, some of them are:

- [**CERNGetDP**](cerngetdp.md)
- [**FiQuS**](fiqus.md)
- [**LEDET**](ledet.md)
- [**PyBBQ**](pybbq.md)
- [**Ni-Coils**](nicoils.md)
- [**COMSOL**](comsol.md)
- [**SIGMA**](sigma.md)



