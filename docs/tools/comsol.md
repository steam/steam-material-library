# COMSOL

Steam-material-library functions can also be imported in [COMSOL](https://www.comsol.com/) multiphysics software. Specifically, the DLL files of the corresponding C-functions can be straight imported into COMSOL. The procedure is quite rather for COMSOL users.