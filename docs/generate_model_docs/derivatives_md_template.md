---
runme:
  id: 01HHYB65R8K6SKNR6SGEJFZ5FK
  version: v2.0
---

# <<function_property_deriv[0]['prop_deriv']>>

The following versions and revisions are available:

{% for function in function_property_deriv %}

Version: <<function_property_deriv[0]['version_deriv']>>

Number of parameters: <<num_of_params>>

Range: <<ranges |replace("'", "")>>

Source: https://gitlab.cern.ch/steam/steam-material-library/-/blob/master/models/c/derivatives/<<function_property_deriv[0]['function_name_deriv']>>.c

{% endfor %}
