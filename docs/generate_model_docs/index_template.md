# << folder_name >>

<< folder_name >> calculated analytically from functions written in C for different types of materials.

Here, one can find the list of the functions included in STEAM-material-library:

{% for function_name in function_names %}
- [**<<function_name>>**](<< function_name >>)

{% endfor %}