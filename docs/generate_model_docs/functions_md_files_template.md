---
runme:
  id: 01HHF036FYWWA0S79QG9BSQZSW
  version: v3
---

# <<function_property[0]['prop']>>

The following versions and revisions are available:

{% for function in function_property %}

Version: <<function_property[0]['version']>>

Revision: 0

Number of parameters: <<num_of_params>>

Range: <<ranges |replace("'", "")>>

Source: https://gitlab.cern.ch/steam/steam-material-library/-/blob/master/models/c/general/<<function_property[0]['function_name']>>.c

Mex-MatLab testing plots (in case 'Error' is on the title, the corresponding Matlab function doesn't exist):

![](../../dynamic/MatLab_Mex_graphs/<<function_handle_name>>.svg)

![](../../dynamic/MatLab_Mex_graphs/Only_Mex_folder/<<function_handle_name>>.svg)

![](../../dynamic/MatLab_Mex_graphs/Only_Mex_folder/<<function_property[0]['function_name']>>.svg)

{% endfor %}
