from jinja2 import Environment, FileSystemLoader
import os
import shutil
import re
import pandas as pd
from pathlib import Path
import mkdocs


def generate_documentation():
    template_directory = Path(os.path.dirname(os.path.abspath(__file__)))
    base_directory = template_directory / '..' / '..'
    # Steam-material-library C-funcitons location
    source_directory = base_directory / 'models' / 'c' / 'general'
    # Define the subdirectory where the function names should be searched
    source_subdirectory = base_directory / 'docs' / 'properties'
    # Steam-material-library C-functions location
    source_deriv_directory = base_directory / 'models' / 'c' / 'derivatives'

    # Filter function names that start with 'CFUN' and remove the .c extension
    function_names = [f.stem for f in source_directory.glob('CFUN*.c')]
    derivative_function_names = [f.stem for f in source_deriv_directory.glob('CFUN*.c')]

    # Create a Jinja2 environment with a template directory
    loader = FileSystemLoader(template_directory)
    env = Environment(
        loader=loader,
        variable_start_string='<<',
        variable_end_string='>>',
        trim_blocks=True,
        lstrip_blocks=True
    )

    # Define excel file location
    # source_excel = os.path.join(template_directory, 'functionsNames_website.xlsx')
    source_excel             = base_directory / 'functionsNames_website_csv.csv'
    source_excel_derivatives = base_directory / 'Derivatives_website.csv'

    # Load the template
    template = env.get_template('functions_md_files_template.md')
    template_derivatives = env.get_template('derivatives_md_template.md')

    # Define the names of the output directories
    keyword_to_output_directory = {
        'Ic': 'Critical_current',
        'Cv': 'Volumetric_heat_capacity',
        'Cp': 'Specific_heat',
        'k': 'Thermal_conductivity',
        'rho': 'Resistivity',
        'HTS': 'Critical_current_density_(HTS)',
        'Jc': 'Critical_current_density_(LTS)',
        'deriv': 'Derivatives'
    }

    for keyword, output_subdirectory in keyword_to_output_directory.items():
        # Define path to the output directory
        output_dir = source_subdirectory / output_subdirectory

        # Check if the output directory exists
        if output_dir.exists():
            # If it exists, replace it with a new directory
            shutil.rmtree(output_dir)

        # Create a new output directory
        output_dir.mkdir()

    # Iterate through the function names and generate .md files
    for function_name in function_names:
        # Determine the keyword (Ic, Cv, Cp, k, rho, HTS, Jc) based on the function name
        keyword = None
        for potential_keyword in keyword_to_output_directory.keys():
            if potential_keyword in function_name:
                keyword = potential_keyword
                break


        # Define the pattern to match function names
        pattern = r'^CFUN_([A-Za-z0-9_]+)_v(\d+)'

        function_property = []
        derivatives_function_properties = []
        # Function names properties
        match = re.match(pattern, function_name)
        if match:
            prop = match.group(1)
            version = match.group(2)
            function_property.append({'prop': prop, 'version': version, 'function_name': function_name})
    
        # df = pd.read_excel(source_excel, header=0)
        df =pd.read_csv(source_excel, header=0)

        ranges = []
        num_of_params = None  # Initialize num_of_params as None
        
        name = function_property[0]['function_name']
        if name in df['C_function_prop'].values:
            # Find the first matching row and use its 'Input_parameters' value
            matching_row = df[df['C_function_prop'] == name].iloc[0]
            num_of_params = int(matching_row['Input_parameters'])
            function_handle_name = matching_row['handle_name']
            # Construct the column names (Range_1, Range_2, etc.) dynamically
            range_columns = [f'Range_{i}' for i in range(1, 13)]
            # Extract and append the values of the 'Range_X' columns to the 'ranges' list
            ranges.extend([matching_row[column] for column in range_columns if not pd.isna(matching_row[column])])
        
        ranges_string = ', '.join(ranges)
        
        # Render the template with the current function_property in the context
        rendered_content = template.render(function_property=function_property, ranges=ranges, num_of_params=num_of_params, path_exists=os.path.exists, function_handle_name=function_handle_name)

        if keyword:
            # Define the output directory based on the keyword
            output_dir = keyword_to_output_directory[keyword]


            # Define the output file path based on the function_name and keyword
            output_file_path = source_subdirectory / output_dir / f'{function_name}.md'

            # Write the rendered content to the output .md file
            with output_file_path.open('w') as output_file:
                output_file.write(rendered_content)
            # Print or indicate the completion of file generation
            #print(f'{len(function_names)} .md files generated in corresponding output folders')

        else:
            other_directory = source_subdirectory / 'others'
            other_directory.mkdir(exist_ok=True)

            output_file_path = other_directory / f'{function_name}.md'

            with output_file_path.open('w') as output_file:
                output_file.write(rendered_content)

            print(f'WARNING: {function_name} could not be associated with any of the defined property types.')

    # Same thing for the derivatives
    for function_name in derivative_function_names:
        # Determine the keyword (Ic, Cv, Cp, k, rho, HTS, Jc) based on the function name
        keyword = None
        for potential_keyword in keyword_to_output_directory.keys():
            if potential_keyword in function_name:
                keyword = potential_keyword
                break


        # Define the pattern to match deriv function names
        pattern_derivs = r'^CFUN_([A-Za-z0-9_]+)_v(\d+)_deriv'

        function_property_deriv = []

        match = re.match(pattern, function_name)
        if match:
            prop = match.group(1)
            version = match.group(2)
            function_property_deriv.append({'prop_deriv': prop, 'version_deriv': version, 'function_name_deriv': function_name})
    
        # df = pd.read_excel(source_excel, header=0)
        df_derivs = pd.read_csv(source_excel_derivatives, header=0)

        ranges = []
        num_of_params = None  # Initialize num_of_params as None
        
        name = function_property_deriv[0]['function_name_deriv']
        if name in df_derivs['Derivatives'].values:
            # Find the first matching row and use its 'Input_parameters' value
            matching_row = df_derivs[df_derivs['Derivatives'] == name].iloc[0]
            num_of_params = int(matching_row['Input_parameters'])
        
            # Construct the column names (Range_1, Range_2, etc.) dynamically
            range_columns = [f'Range_{i}' for i in range(1, 13)]
            # Extract and append the values of the 'Range_X' columns to the 'ranges' list
            ranges.extend([matching_row[column] for column in range_columns if not pd.isna(matching_row[column])])
        
        ranges_string = ', '.join(ranges)
        
        # Render the template with the current function_property in the context
        rendered_content_deriv = template_derivatives.render(function_property_deriv=function_property_deriv, ranges=ranges, num_of_params=num_of_params, path_exists=os.path.exists)

        output_dir = keyword_to_output_directory['deriv']

        # Define the output file path based on the function_name and keyword
        output_file_path = source_subdirectory / output_dir / f'{function_name}.md'

        # Write the rendered content to the output .md file
        with output_file_path.open('w') as output_file:
            output_file.write(rendered_content_deriv)

    directory_paths = []
    # List all directories within the subdirectory
    for entry in source_subdirectory.iterdir():
        if entry.is_dir():
            directory_paths.append(entry)

    for path in directory_paths:
        # Get the folder name from the path
        folder_name = path.stem
        folder_name_tempalate = folder_name.replace("_", " ")

        # Get the list of CFUN names in the current directory
        function_names = [name.stem for name in list(path.iterdir())]

        # Create a Jinja2 environment with a template directory
        loader = FileSystemLoader(template_directory)
        env = Environment(
            loader=loader,
            variable_start_string='<<',
            variable_end_string='>>',
            trim_blocks=True,
            lstrip_blocks=True
        )
        
        # Load and render the template
        index_template = env.get_template('index_template.md')
        rendered_content = index_template.render(folder_name=folder_name_tempalate, function_names=function_names, path_exists=os.path.exists)

        # Write the rendered content to 'index.md' in the current directory
        with (path / 'index.md').open('w') as output_file:
            output_file.write(rendered_content)


# Bindings to use this as a plugin in mkdocs. This allows automatic rebuilding
def on_pre_build(config):
    generate_documentation()


if __name__=='__main__':
    generate_documentation()