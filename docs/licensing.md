# Licensing

CERN provides a licence for the STEAM Framework software to interested individuals or organisations.
Licence is provided without fee to Universities, R&D laboratories, public/non-profit institutions and private individuals.

It may also be possible for commercial organisations to obtain a STEAM Framework licence.  
Please contact the knowledge transfer (KT) team (kt@cern.ch) at CERN for more information.

Terms of use of the STEAM Framework software are outlined below document.  
When using the STEAM Framework software you agree with these terms.  
   
[STEAM User agreement](https://edms.cern.ch/ui/file/2024516/1.08/STEAM_Agreement_22April2020_v1.8.pdf){ .md-button .md-button--primary }


!!! question "For questions please contact"
    [STEAM Team](mailto:steam-team@cern.ch)