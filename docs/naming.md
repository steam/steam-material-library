# Naming conventions
Each function in the library follows this naming convention:

**CFUN_PropertyTypeMaterialName_Version.c**

For example:
CFUN_[**rho**](properties/Resistivity/index.md)[**BeCu**](properties/Resistivity/CFUN_rhoBeCu_v1.md)_v1.c

All the library functions are fundamentally implemented in the [C-Language](installation_and_use/Use/c.md) or MATLAB.
More information regarding the possible properties can be found in the [Properties section](properties/index.md).
