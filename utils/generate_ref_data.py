#!/usr/bin/env python3
import itertools
import yaml
import numpy as np
from   steammaterials.STEAM_materials import STEAM_materials
from   datetime import datetime
from   pathlib import Path
import os
import sys
import pandas as pd


def create_all_ref_files(output_path=None):
    repository_root = Path(os.path.dirname(__file__)).parent
    if output_path is None:
        output_path = repository_root / 'tests' / 'ref_data'
    csv_file = pd.read_csv(repository_root / 'all_material_functions.csv')
   
    errors = 0
    for i, row in csv_file.iterrows():
        if row['make_ref']:
            try:
                create_ref_file(row, output_path)
            except Exception as e:
                print(f'Failed to generate reference file for {row["ref_name"]}')
                print(f'Failed with error: {e}')
                errors += 1
    if errors:
        print(f'!!! During generation, {errors} errors occurred !!!')


def create_ref_file(row, output_path):
    """
    Creates a file to act as reference for the expected output of the given material function.
    Expects a dictionary-like `row` containing the number of 'input_parameters', the material name
    'ref_name', author 'ref_generated_by' and inputs with 'input{i}'.
    """
    parameter_count = int(row['input_parameters'])
    ref_name = row['ref_name']
    if parameter_count < 1:
        raise ValueError('Incorrect number of input parameters for {ref_name}')

    inputs = [eval(row[f'input{i+1}']) for i in range(parameter_count)]
    input_mat = np.array(list(itertools.product(*inputs))).T
    out = STEAM_materials(ref_name, input_mat.shape[0], input_mat.shape[1]).evaluate(input_mat)

    dictionary_output = {
        'author': row['ref_generated_by'],
        'date': datetime.today().strftime('%d.%m.%Y'),
        'material_function': ref_name,
        'input': input_mat.T.tolist(),
        'output': [i.item() for i in out],
        'input_parameters': parameter_count,
    }
    try:
        with (output_path / f'{ref_name}.yaml').open("x") as f:
            yaml.dump(dictionary_output, f, default_flow_style=False)
    except FileExistsError:
        print(f'Reference file for {ref_name} already exists. You cannot overwrite it.')


if __name__ == '__main__':
    if len(sys.argv) > 1:
        create_all_ref_files(Path(sys.argv[1]))
    else:
        create_all_ref_files()
