import os
import openpyxl
import pandas as pd

if __name__ == "__main__":
    '''
    This script is used to quickly edit all instances of a list of Matlab functions with a list of equivalent STEAM_MatPro class methods.  
    '''
    # Input
    # xlsx_file = os.path.join(os.path.dirname(os.path.dirname(__file__)), 'compile_STEAM_MatPro', 'c_mat_mex_correspondance.xlsx')
    csv_file = os.path.join(os.path.dirname(os.path.dirname(__file__)), 'functionsNames_website_csv.csv')
    dict_files_to_change = {
        # LEDET
        r'F:\Dropbox\Working files\MainFolderSimulations\Useful functions\steam-ledet\main\LEDET_SimulationFastMode.m': r'F:\Dropbox\Working files\MainFolderSimulations\Useful functions\steam-ledet\main\LEDET_SimulationFastMode.m',
        r'F:\Dropbox\Working files\MainFolderSimulations\Useful functions\steam-ledet\main\processInputsLEDET.m': r'F:\Dropbox\Working files\MainFolderSimulations\Useful functions\steam-ledet\main\processInputsLEDET.m',
        r'F:\Dropbox\Working files\MainFolderSimulations\Useful functions\steam-ledet\main\acquireInputsLEDET.m': r'F:\Dropbox\Working files\MainFolderSimulations\Useful functions\steam-ledet\main\acquireInputsLEDET.m',
        r'F:\Dropbox\Working files\MainFolderSimulations\Useful functions\steam-ledet\main\acquireOptionsLEDET.m': r'F:\Dropbox\Working files\MainFolderSimulations\Useful functions\steam-ledet\main\acquireOptionsLEDET.m',
        r'F:\Dropbox\Working files\MainFolderSimulations\Useful functions\steam-ledet\main\generateReportDomLEDET.m': r'F:\Dropbox\Working files\MainFolderSimulations\Useful functions\steam-ledet\main\generateReportDomLEDET.m',
        r'F:\Dropbox\Working files\MainFolderSimulations\Useful functions\steam-ledet\main\generateReportDomLEDET3D.m': r'F:\Dropbox\Working files\MainFolderSimulations\Useful functions\steam-ledet\main\generateReportDomLEDET3D.m',
        r'F:\Dropbox\Working files\MainFolderSimulations\Useful functions\steam-ledet\main\calculateMagneticFieldMapsLEDET.m': r'F:\Dropbox\Working files\MainFolderSimulations\Useful functions\steam-ledet\main\calculateMagneticFieldMapsLEDET.m',
        r'F:\Dropbox\Working files\MainFolderSimulations\Useful functions\steam-ledet\main\initializeExternalInputsLEDET.m': r'F:\Dropbox\Working files\MainFolderSimulations\Useful functions\steam-ledet\main\initializeExternalInputsLEDET.m',
        r'F:\Dropbox\Working files\MainFolderSimulations\Useful functions\steam-ledet\main\postprocessLEDET.m': r'F:\Dropbox\Working files\MainFolderSimulations\Useful functions\steam-ledet\main\postprocessLEDET.m',
        r'F:\Dropbox\Working files\MainFolderSimulations\Useful functions\steam-ledet\main\acquireMagneticFieldLEDET.m': r'F:\Dropbox\Working files\MainFolderSimulations\Useful functions\steam-ledet\main\acquireMagneticFieldLEDET.m',
        r'F:\Dropbox\Working files\MainFolderSimulations\Useful functions\steam-ledet\main\acquireSaveTxtOptionsLEDET.m': r'F:\Dropbox\Working files\MainFolderSimulations\Useful functions\steam-ledet\main\acquireSaveTxtOptionsLEDET.m',
        r'F:\Dropbox\Working files\MainFolderSimulations\Useful functions\steam-ledet\main\acquirePlotOptionsLEDET.m': r'F:\Dropbox\Working files\MainFolderSimulations\Useful functions\steam-ledet\main\acquirePlotOptionsLEDET.m',
        # LEDET 3D class
        r'F:\Dropbox\Working files\MainFolderSimulations\Useful functions\steam-ledet\main\ParametersLEDET3D.m': r'F:\Dropbox\Working files\MainFolderSimulations\Useful functions\steam-ledet\main\ParametersLEDET3D.m',
        # LEDET auxiliary functions
        r'F:\Dropbox\Working files\MainFolderSimulations\Useful functions\steam-ledet\main\auxiliaryLEDET\calculateHotSpotTemperature.m': r'F:\Dropbox\Working files\MainFolderSimulations\Useful functions\steam-ledet\main\auxiliaryLEDET\calculateHotSpotTemperature.m',
        r'F:\Dropbox\Working files\MainFolderSimulations\Useful functions\steam-ledet\main\auxiliaryLEDET\calculateThermalCapacity_MatPro.m': r'F:\Dropbox\Working files\MainFolderSimulations\Useful functions\steam-ledet\main\auxiliaryLEDET\calculateThermalCapacity_MatPro.m',
        r'F:\Dropbox\Working files\MainFolderSimulations\Useful functions\steam-ledet\main\auxiliaryLEDET\calculateElectricalResistance_MatPro.m': r'F:\Dropbox\Working files\MainFolderSimulations\Useful functions\steam-ledet\main\auxiliaryLEDET\calculateElectricalResistance_MatPro.m',
        r'F:\Dropbox\Working files\MainFolderSimulations\Useful functions\steam-ledet\main\auxiliaryLEDET\calculate_Tc_Tcs_flagQ_MatPro.m': r'F:\Dropbox\Working files\MainFolderSimulations\Useful functions\steam-ledet\main\auxiliaryLEDET\calculate_Tc_Tcs_flagQ_MatPro.m',
        r'F:\Dropbox\Working files\MainFolderSimulations\Useful functions\steam-ledet\main\auxiliaryLEDET\calculateThermalConductivity_MatPro.m': r'F:\Dropbox\Working files\MainFolderSimulations\Useful functions\steam-ledet\main\auxiliaryLEDET\calculateThermalConductivity_MatPro.m',
        # Utils
        r'F:\Dropbox\Working files\MainFolderSimulations\Useful functions\steam-matlab-utils\quenchPropagationVelocity_MatPro.m': r'F:\Dropbox\Working files\MainFolderSimulations\Useful functions\steam-matlab-utils\quenchPropagationVelocity_MatPro.m',
        r'F:\Dropbox\Working files\MainFolderSimulations\Useful functions\steam-matlab-utils\calculateHotSpotT_MatPro.m': r'F:\Dropbox\Working files\MainFolderSimulations\Useful functions\steam-matlab-utils\calculateHotSpotT_MatPro.m',
    }
    list_functions_to_exclude = ['error']  # These functions will NOT be converted

    # Read file defining correspondances between Matlab functions and STEAM_MatPro method replacements
    # wb = openpyxl.load_workbook(xlsx_file)
    # sheet = wb.active
    # df = pd.read_excel(xlsx_file)

    # Read file defining correspondances between Matlab functions and STEAM_MatPro method replacements
    df = pd.read_csv(csv_file)

    # Edit the "Replacement" values by adding "MP." before them
    df['handle_name'] = 'MP.' + df['handle_name']
    # Exclude functions in the list list_functions_to_exclude
    df = df[~df['matlab_name'].isin(list_functions_to_exclude)]
    # Make a correspondence dictionary from the DataFrame
    correspondence = dict(zip(df['matlab_name'], df['handle_name']))

    # Manually add correspondancies to change
    correspondence['MP.CvCu'] = 'MP.CvCu_nist'
    correspondence['MP.CvHe'] = 'MP.CvHe_nist'
    correspondence['MP.CvNbTi'] = 'MP.CvNbTi_cudi'
    correspondence['MP.CvSteel'] = 'MP.CvSS316_nist'
    correspondence['MP.kAg_NIST_mat'] = 'MP.kAg_RRR30'
    correspondence['_nist_nist'] = '_nist'
    correspondence['_NIST_NIST'] = '_NIST'
    correspondence['_cudi_cudi'] = '_cudi'

    print(correspondence)

    # Change files
    for file_to_change, file_to_change_output in dict_files_to_change.items():
        # Read the text file you want to edit
        with open(file_to_change, "r") as file:
            content = file.read()

        # Replace the strings according to the correspondence
        for source, replacement in correspondence.items():
            content = content.replace(source, replacement)

        # If the entries are identical, the string "MP." is added every time the script is run. This bit of code corrects for that
        content = content.replace("MP.MP.", "MP.")

        # Write the updated content back to the text file
        with open(file_to_change_output, "w") as file:
            file.write(content)
    #
        print(f"Text file {file_to_change} edited and written to file {file_to_change_output}.")
