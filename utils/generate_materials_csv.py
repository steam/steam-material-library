import pandas as pd
import os
import pathlib as pl
from    glob import glob

# Loads all c files from the folders
path1 = pl.Path('../models/c/general/')
path2 = pl.Path('../models/c/legacy/')
result = sorted([pl.Path(y).stem for x in os.walk(path1) for y in glob(os.path.join(x[0], '*.c'))] + [pl.Path(y).stem for x in os.walk(path2) for y in glob(os.path.join(x[0], '*.c'))])

columns = ['material_function', 'testing', 'testing_ref_name', 'make_ref', 'ref_name', 'ref_generated_by', 'notes', 'input_parameters', 'input1', 'input2', 'input3', 'input4', 'input5', 'input6', 'input7', 'input8', 'input9', 'input10', 'input11', 'input12']

csv_file = pd.DataFrame(columns=columns)
csv_file['material_function'] = result

csv_file.loc[:, 'testing'] = True
csv_file['testing_ref_name'] = result
csv_file.loc[:, 'make_ref'] = True
csv_file['ref_name'] = result
csv_file.loc[:, 'ref_generated_by'] = 'Tim Mulder'

# Saving the file.
try:
    csv_file.to_csv('all_material_function.csv', mode='x')
except FileExistsError:
    print('WARNING: This file already exists!')
