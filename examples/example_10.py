# Plotting
import matplotlib.pyplot as plt
import matplotlib

# Others
import numpy as np
import os

# Material functions: you need to install STEAM-materials, 'pip install STEAM-materials'
from steammaterials import SteamMaterials

cmap = matplotlib.cm.get_cmap('Spectral')

# Example with 1 input value
T = np.linspace(2.0, 450, 1000)
numpy2d = T.reshape((1, len(T)))
sm = SteamMaterials('CFUN_rhoHast_v1', numpy2d.shape[0], numpy2d.shape[1])
out = sm.evaluate(numpy2d)

fig2 = plt.figure(figsize=(6,4))
ax2 = fig2.add_subplot(111)

B = 0.0
T = np.linspace(0.0, 300, 1000)
RRR = [0.1, 1.0, 10.0, 30.0, 100.0, 300.0, 1000.0, 3000.0]
T_ref_RRR = 273.

colors = cmap(np.linspace(0, 1, len(RRR)))

for i in range(len(RRR)):
    b = B * np.ones(T.shape)
    rrr = RRR[i] * np.ones(T.shape)
    numpy2d2 = np.vstack((T, b, rrr))
    sm2 = SteamMaterials('CFUN_rhoCu_NIST_v2', numpy2d2.shape[0], numpy2d2.shape[1])
    out2 = sm2.evaluate(numpy2d2)
    ax2.plot(T, out2, color=colors[i], ls='--', label=str(RRR[i]))

ax2.set_title('Copper, RRR = ' + str(RRR))
ax2.set_xscale("log")
ax2.set_yscale("log")
ax2.set_xlabel('Temperature, K')
ax2.set_ylabel('Resistivity, $\Omega$m')
ax2.legend()

plt.show()
