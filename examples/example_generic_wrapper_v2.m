parentDirectory = fileparts(pwd);
addpath( genpath( fullfile(parentDirectory, 'models/matlab/') ) )

mex -output CFUN_Tcs_Nb3Sn_v1.mex ..\mex_compilation\generic_wrapper_v2.cpp  ..\models\c\general\CFUN_Tcs_Nb3Sn_v1.c

J_sc = linspace(0,50e3/(40*pi/4*.0008^2/2), 20);
B = rand(size(J_sc))*30;
Jc_Nb3Sn0 = 14.2196*ones(size(J_sc))*1e10; %14.2196
Tc0_Nb3Sn = 18*ones(size(J_sc));
Bc20_Nb3Sn = 29*ones(size(J_sc));

alpha = 1.01*ones(size(J_sc));

[Jc0, ~] = CFUN_Jc_Nb3Sn_Bordini_v1('CFUN_Jc_Nb3Sn_Bordini_v1', 0*ones(size(J_sc)), B, Jc_Nb3Sn0, Tc0_Nb3Sn, Bc20_Nb3Sn, alpha);
[Tc_1, Tcs_1] = CFUN_Tcs_Nb3Sn_v1('CFUN_Tcs_Nb3Sn_v1', J_sc, B, Jc0, alpha, Tc0_Nb3Sn, Bc20_Nb3Sn);

[Tc_2, Tcs_2]=Tc_Tcs_Nb3Sn_Bordini_approx(J_sc, B, Jc_Nb3Sn0, Tc0_Nb3Sn, Bc20_Nb3Sn, alpha);

figure
hold on,box on,grid on
plot(J_sc, Tcs_1, 'ko-')
plot(J_sc, Tc_2, 'r*-')

figure
hold on,box on,grid on
plot(J_sc, Tc_1, 'ko-')
plot(J_sc, Tcs_2, 'r*-')

