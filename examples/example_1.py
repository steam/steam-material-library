# Plotting
import matplotlib.pyplot as plt
import matplotlib

# Others
import numpy as np
import os

# Material functions: you need to install STEAM-materials, 'pip install STEAM-materials'
from steammaterials import SteamMaterials

cmap = matplotlib.cm.get_cmap('Spectral')

# Example with 1 input value
T = np.linspace(2.0, 450, 1000)
numpy2d = T.reshape((1, len(T)))
sm = SteamMaterials('CFUN_rhoHast_v1', numpy2d.shape[0], numpy2d.shape[1])
out = sm.evaluate(numpy2d)

fig1 = plt.figure()
ax1 = fig1.add_subplot(111)
ax1.plot(T, out)
ax1.set_xlabel('Temperature, K')
ax1.set_ylabel('Thermal Conductivity, W/mk')

# Example with multiple input values
fig2 = plt.figure(figsize=(8,4))
ax2 = fig2.add_subplot(121)
ax3 = fig2.add_subplot(122)

B = [0.0, 1.0, 2.0, 3.0, 4.0, 10.0, 15, 40.0]
T = np.linspace(0.0, 300, 1000)
RRR = 100
T_ref_RRR = 273.

colors = cmap(np.linspace(0, 1, len(B)))

for i in range(len(B)):
    b = B[i] * np.ones(T.shape)
    rrr = RRR * np.ones(T.shape)
    rrr_ref = T_ref_RRR * np.ones(T.shape)
    numpy2d = np.vstack((T, b, rrr, rrr_ref))
    numpy2d2 = np.vstack((T, b, rrr))
    sm = SteamMaterials('CFUN_rhoCu_v1', numpy2d.shape[0], numpy2d.shape[1])
    sm2 = SteamMaterials('CFUN_rhoCu_NIST_v2', numpy2d2.shape[0], numpy2d2.shape[1])
    out = sm.evaluate(numpy2d)
    out2 = sm2.evaluate(numpy2d2)
    ax2.plot(T, out, color=colors[i], label='Magnetic Field = ' + str(B[i]) + ' T')
    ax2.plot(T, out2, color=colors[i], ls='--')
    ax3.plot(T, out/out2-1)

ax2.set_title('Copper, RRR = ' + str(RRR))
ax2.set_xscale("log")
ax2.set_yscale("log")
ax2.set_xlabel('Temperature, K')
ax2.set_ylabel('Resistivity, $\Omega$m')
ax2.legend()

plt.show()
