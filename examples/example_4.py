# Plotting
import matplotlib.pyplot as plt

# Others
import numpy as np
import os

# Material functions: you need to install STEAM-materials, 'pip install STEAM-materials'
from steammaterial import SteamMaterials

# Example with multiple input values
fig2 = plt.figure()
ax2 = fig2.add_subplot(111)

B = 0.1
T = np.linspace(1.9, 4.5, 1000)
T_ref_RRR = 295.

T = T * np.ones(T.shape)
b = B * np.ones(T.shape)
Jc = np.ones(T.shape)
Tc0 = 18.3 * np.ones(T.shape)
Bc20 = 28.7 * np.ones(T.shape)
numpy2d = np.vstack((T, b, Jc, Tc0, Bc20))
sm = SteamMaterials('CFUN_Jc_Nb3Sn_Summer_v1', numpy2d.shape[0], numpy2d.shape[1])
out = sm.evaluate(numpy2d)
ax2.plot(T, out, label='B=0T')

ax2.set_xlabel('Temperature, K')
ax2.set_ylabel('Jc')
ax2.legend()

plt.show()
