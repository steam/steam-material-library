# Plotting
import matplotlib.pyplot as plt

# Others
import numpy as np
import os

# Material functions: you need to install STEAM-materials, 'pip install STEAM-materials'
from steammaterials import SteamMaterials

# Example with multiple input values
fig2 = plt.figure()
ax2 = fig2.add_subplot(111)

T = 4.5
B = np.linspace(0, 50, 10000)
RRR = 100.0
T_ref_RRR = 295.0

T = T * np.ones(B.shape)
b = B
rrr = RRR * np.ones(B.shape)
rrr_ref = T_ref_RRR * np.ones(B.shape)
numpy2d = np.vstack((T, b, rrr, rrr_ref))
sm = SteamMaterials('CFUN_rhoCu_v1', numpy2d.shape[0], numpy2d.shape[1])
out = sm.evaluate(numpy2d)
ax2.plot(B, out)

ax2.set_title('Copper, RRR=100, T=300K')
ax2.set_xlabel('Magnetic Field, T')
ax2.set_ylabel('Resistivity, $\Omega$m')
ax2.legend()

plt.show()
