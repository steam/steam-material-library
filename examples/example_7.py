# Plotting
import matplotlib.pyplot as plt

# Others
import numpy as np
import os

# Material functions: you need to install STEAM-materials, 'pip install STEAM-materials'
from steammaterials import SteamMaterials

# Example with multiple input values
fig2 = plt.figure()
ax2 = fig2.add_subplot(111)

T = np.linspace(0.0, 10, 1000)
TheI = 4.2*np.ones_like(T)
TheII = 1.9*np.ones_like(T)
numpy2dI = np.vstack((T,TheI))
numpy2dII = np.vstack((T,TheII))

sm = SteamMaterials('CFUN_hHe_v1', numpy2dII.shape[0], numpy2dII.shape[1])
outI = sm.evaluate(numpy2dI)
outII = sm.evaluate(numpy2dII)
ax2.plot(T, outI)
ax2.plot(T, outII)

ax2.set_xlabel('Temperature, K')
ax2.set_ylabel('h, W/m$^2$k')
ax2.legend()

plt.show()
