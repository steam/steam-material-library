# Plotting
import matplotlib.pyplot as plt

# Others
import numpy as np
import os

# Material functions: you need to install STEAM-materials, 'pip install STEAM-materials'
from steammaterials import SteamMaterials

# Example with multiple input values
fig2 = plt.figure()
ax2 = fig2.add_subplot(111)

T = np.linspace(1.0, 300, 1000)
# T = T.reshape((1,len(T)))
RRR = 100
rrr = RRR * np.ones(T.shape)
numpy2d = np.vstack((T, rrr, np.ones(T.shape), np.ones(T.shape)))
sm = SteamMaterials('CFUN_kCu_v1', numpy2d.shape[0], numpy2d.shape[1])
out = sm.evaluate(numpy2d)
ax2.plot(T, out)

ax2.set_title('Copper, RRR = ' + str(RRR))
ax2.set_xscale("log")
ax2.set_yscale("log")
ax2.set_xlabel('Temperature, K')
ax2.set_ylabel('Thermal Conductivity, W/mk')
ax2.legend()

plt.show()
