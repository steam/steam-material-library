# Plotting
import matplotlib.pyplot as plt

# Others
import numpy as np
import os

# Material functions: you need to install STEAM-materials, 'pip install STEAM-materials'
from steammaterials import SteamMaterials

# Example with multiple input values
fig2 = plt.figure()
ax2 = fig2.add_subplot(111)

T = np.linspace(1, 30, 100)
B = 10 * np.ones(T.shape)
A = 0.00001 * np.ones(T.shape)
S = 1 * np.ones(T.shape)
a = 2 * np.ones(T.shape)
c0 = 1 * np.ones(T.shape)
Bc20 = 28 * np.ones(T.shape)
Smax = 2 * np.ones(T.shape)


numpy2d = np.vstack((T, B, A, S, a, c0, Bc20, Smax))
sm = SteamMaterials('CFUN_IcNb3Sn_WithStress_v1', numpy2d.shape[0], numpy2d.shape[1])
out = sm.evaluate(numpy2d)
ax2.plot(T, out/1.7313358331527338e-08, label='RRR')

ax2.set_title('Copper')
ax2.set_xlabel('Temperature, K')
ax2.set_ylabel('Resistivity, $\Omega$m')
ax2.legend()

plt.show()
