# Others
import numpy as np

# Material functions: you need to install STEAM-materials, 'pip install STEAM-materials'
from steammaterials import SteamMaterials

# Example with 1 input value

numpy2d = np.array([22, 0, 15, 273])
numpy2d2 = np.array([22, 0, 15])
sm = SteamMaterials('CFUN_rhoCu_v1', 4, 1)
sm2 = SteamMaterials('CFUN_rhoCu_NIST_v1', 3, 1)
out = sm.evaluate(numpy2d)
out2 = sm2.evaluate(numpy2d2)

print(out, out2)