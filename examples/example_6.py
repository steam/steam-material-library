# Plotting
import matplotlib.pyplot as plt

# Others
import numpy as np
import os
from math import isclose

# Material functions: you need to install STEAM-materials, 'pip install STEAM-materials'
from steammaterials import SteamMaterials

# Example with multiple input values
fig2 = plt.figure()
ax2 = fig2.add_subplot(111)

T = 1.9
Tc = 15
Tc0 = 18
Tcs = 1.9

result = []

for i in range(100000):
    numpy2d = np.vstack((T, Tc, Tc0, Tcs))
    sm = SteamMaterials('CFUN_CpNb3Sn_v1', numpy2d.shape[0], numpy2d.shape[1])
    out = sm.evaluate(numpy2d)
    if not isclose(out[0], 166.16752652):
        print(out)
    result.append(out)

print(result)
# ax2.plot(result, label='B')

# ax2.set_title('Copper, RRR=100, T=4.5K')
# ax2.set_xlabel('Magnetic Field, T')
# ax2.set_ylabel('Resistivity, $\Omega$m')
# ax2.legend()

# plt.show()
