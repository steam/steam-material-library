# STEAM Material Library

STEAM Material Library is a collection of functions written in C, that is used by various simulations at CERN to caclulate and use material properties (volumetric heat capacity, thermal conductivity, resistivity e.t.c.).


## Table of Contents
- [Documentation](#Documentation)
- [Implementation in CERN simulating interfaces](#Implementation-in-CERN-simulating-interfaces)
  - [CERNGetDP](#CERNGetDP)
  - [pyBBQ](#pyBBQ)
  - [LEDET](#LEDET)
- [Compilation](#Compilation)
- [Function Parameters](#Function-Parameters)

## Documentation

The documentation is more complete than this README, we recommend taking a look:
https://steam-material-library.docs.cern.ch/

Some of this documentation can also be accessed through the `docs` folder.

## Adding a material function:

- Add the c-function to the models/c/general folder.
- Make sure it has 'return_test_yaml' and the 'eval' function inside of it.
- Add the function name to the all_material_functions.csv file.
- Make sure that testing and make_ref is set to TRUE -> this means that a reference file will be made and the output of the function will be tested against this rerefence file.
- Run utils/generate_ref_data.py
- Run tests/test_dll.py -> this will build a local dll library and will test all c-functions, it requires you to have installed CMake and its requirements.

## Implementation in CERN simulating interfaces

### CERNGetDP

To add and use a material function on CERNGetDP, two steps are needed.
First, the user needs to add the C-function in `steam-material-library\models\c\*\` and
then, using [steam-material-adder](https://gitlab.cern.ch/steam/cerngetdp/-/tree/master/steam_material_adder)
on `cerngetdp\steam_material_adder`, one can automatically add functions from
steam-material-library just by filling out the characteristics of the function on the
excel sheet. There are precise instructions on `steam_material_adder` repository.

### pyBBQ

Hardcoded functions on pyBBQ interface have been replaced with the corresponding C-functions from
steam material library providing more accurate results.

### LEDET

LEDET interface runs in MatLab, so a mex-based wrapper around the C-code was built.
The `bindings\matlab\build.py` file compiles the C-functions from the `models\c\*\` folders into mex
files and saves them in into the `bindings\matlab\mex_compilation` directory.
It also creates a MATLAB class to wrap all the mex function calls.
The interface is like any other native MATLAB function. 

## Compilation
To compile the shared libraries, simply build the cmake project.
For example, with `cmake .. && cmake --build . --config Release -j4 -v` in a user-created build-directory.
More detailled instructions may be found in the documentation.

### Python
A pre-built python wrapper of these functions is available on [PyPI](https://pypi.org/project/STEAM-materials/).


## Function Parameters

Functions of **specificheat  & volumetric heat capacity**  in J/(kg K) and J/(K*m^3) correspondingly (plus 2 interpolation functions- BHAir and BHIron2):

| C function name     | Arguments | 1st Arg | 2nd Arg | 3d Arg | 4th Arg | 5th Arg |
|---------------------|-----------|---------|---------|--------|---------|---------|
| CFUN_BHAir_v1       | 1         | x       |         |        |         |         |
| CFUN_BHIron2_v1     | 1         | x       |         |        |         |         |
| CFUN_CpHeMass_v1    | 1         | T       |         |        |         |         |
| CFUN_CpNb3Sn_v1     | 4         | T       | Tc      | Tc0    | Tcs     |         |
| CFUN_CvAg_v1        | 1         | T       |         |        |         |         |
| CFUN_CvAl_v1        | 1         | T       |         |        |         |         |
| CFUN_CvAl5083_v1    | 1         | T       |         |        |         |         |
| CFUN_CvBeCu_v1      | 1         | T       |         |        |         |         |
| CFUN_CvBrass_v1     | 1         | T       |         |        |         |         |
| CFUN_CvBSCCO2212_v1 | 1         | T       |         |        |         |         |
| CFUN_CvCu_v1        | 1         | T       |         |        |         |         |
| CFUN_CvG10_v1       | 1         | T       |         |        |         |         |
| CFUN_CvHast_v1      | 1         | T       |         |        |         |         |
| CFUN_CvHe_v1        | 1         | T       |         |        |         |         |
| CFUN_CvHeMass_v1    | 1         | T       |         |        |         |         |
| CFUN_CvIn_v0        | 1         | T       |         |        |         |         |
| CFUN_CvKapton_v1    | 1         | T       |         |        |         |         |
| CFUN_CvNb3Sn_v1     | 2         | T       | B       |        |         |         |
| CFUN_CvNb3Sn_v2     | 4         | T       | B       | Tc0    | Bc20    |         |
| CFUN_CvNbTi_v1      | 5         | T       | B       | I      | C1      | C2      |
| CFUN_CvNbTi_v2      | 2         | T       | B       |        |         |         |
| CFUN_CvSteel_v1     | 1         | T       |         |        |         |         |
| CFUN_CvTi_v1        | 1         | T       |         |        |         |         |

Functions of **Thermal conductivity** in W/(K*m):

| C function name              | Arguments | 1st Arg | 2nd Arg | 3d Arg | 4th Arg | 5th Arg |
|------------------------------|-----------|---------|---------|--------|---------|---------|
| CFUN_kAg_v1                  | 1         | T       |         |        |         |         |
| CFUN_kAl_v1                  | 2         | T       | RRR*    |        |         |         |
| CFUN_kAl1350_v1              | 1         | T       |         |        |         |         |
| CFUN_kAl1350_v1              | 1         | T       |         |        |         |         |
| CFUN_kAl5083_v1              | 1         | T       |         |        |         |         |
| CFUN_kBeCu_WiedemannFranz_v1 | 2         | T       | rhoBeCu |        |         |         |
| CFUN_kBrass_v1               | 1         | T       |         |        |         |         |
| CFUN_kCu_v1                  | 1         | T       | RRR     | rho_B0 | rho     |         |
| CFUN_kCu_Wiedemann_v1        | 1         | T       | T       | rho    |         |         |
| CFUN_kG10_v1                 | 1         | T       |         |        |         |         |
| CFUN_kHast_v1                | 1         | T       |         |        |         |         |
| CFUN_kIn_v1                  | 1         | T       |         |        |         |         |
| CFUN_kKapton_v1              | 1         | T       |         |        |         |         |
| CFUN_kSteel_v1               | 1         | T       |         |        |         |         |
| CFUN_kStycast_v1             | 1         | T       |         |        |         |         |

Functions of **Resistivity** in Ohm*m.

| C function name | Arguments | 1st Arg | 2nd Arg   | 3d Arg | 4th Arg  | 5th Arg   | 6th Arg     | 7th Arg      | 8th Arg      | 9th Arg | 10th Arg     | 11th Arg      | 12th Arg |
|-----------------|-----------|---------|-----------|--------|----------|-----------|-------------|--------------|--------------|---------|--------------|---------------|----------|
| CFUN_rhoAg_v1   | 5         | T       | B         | RRR    | Tref_RRR | c0_scale  |             |              |              |         |              |               |          |
| CFUN_rhoAgMg_v1 | 2         | T       | B         |        |          |           |             |              |              |         |              |               |          |
| CFUN_rhoAl_v1   | 2         | T       | RRR*      |        |          |           |             |              |              |         |              |               |          |
| CFUN_rhoBeCu_v1 | 2         | T       | f_scaling |        |          |           |             |              |              |         |              |               |          |
| CFUN_rhoCu_v1   | 5         | T       | B         | RRR    | Tup_RRR  | c0_scale  |             |              |              |         |              |               |          |
| CFUN_rhoHast_v1 | 1         | T       |           |        |          |           |             |              |              |         |              |               |          |
| CFUN_rhoIn_v0   | 1         | T       |           |        |          |           |             |              |              |         |              |               |          |
| CFUN_rhoSS_v1   | 1         | T       |           |        |          |           |             |              |              |         |              |               |          |

Functions of **Critical Current**, **Critical Current density** & **Critical Temperature** in A, A/m^2 & K correspondingly.

| C function name                              | Arguments | 1st Arg | 2nd Arg  | 3d Arg            | 4th Arg   | 5th Arg    | 6th Arg     | 7th Arg      | 8th Arg      | 9th Arg | 10th Arg     | 11th Arg      | 12th Arg |
|----------------------------------------------|-----------|---------|----------|-------------------|-----------|------------|-------------|--------------|--------------|---------|--------------|---------------|----------|
| CFUN_fisc_I_Ic_n_rhom_Am_BSCCO2212           | 5         | I       | Ic       | n                 | rho_m     | A_matrix   |             |              |              |         |              |               |          |
| CFUN_HTS_JcFit_sunam_v1                      | 3         | Top_IN  | Bnorm_IN | thetaFieldTape_IN |           |            |             |              |              |         |              |               |          |
| CFUN_HTS_JcFit_SUPERPOWER_v1                 | 3         | Top_IN  | Bnorm_IN | thetaFieldTape_IN |           |            |             |              |              |         |              |               |          |
| CFUN_HTS_JcFit_THEVA_v1                      | 3         | Top_IN  | Bnorm_IN | thetaFieldTape_IN |           |            |             |              |              |         |              |               |          |
| CFUN_IcNb3Sn_HiLumi_v1                       | 3         | T       | B        | Area              |           |            |             |              |              |         |              |               |          |
| CFUN_IcNb3Sn_v1                              | 3         | T       | B        | Area              |           |            |             |              |              |         |              |               |          |
| CFUN_IcNb3Sn_WithStress_v1                   | 6         | T       | B        | Area              | S         | a          | c           | Bc20         | Smax         |         |              |               |          |
| CFUN_IcNbTi_v1                               | 3         | T       | B        | Area              |           |            |             |              |              |         |              |               |          |
| CFUN_Jc_Bordini_v1                           | 6         | T       | B        | C0                | Tc0_Nb3Sn | Bc20_Nb3Sn | alpha       |              |              |         |              |               |          |
| CFUN_Jc_Nb3Sn_Bottura_v1                     | 9         | T       | B        | Bc20              | Tc0       | CJ         | p           | q            | wireDiameter | Cu_noCu |              |               |          |
| CFUN_Jc_Nb3Sn_Summer_v1                      | 5         | T       | B        | Jc_Nb3Sn0         | Tc0_Nb3Sn | Bc20_Nb3Sn |             |              |              |         |              |               |          |
| CFUN_Jc_NbTi_Bottura_v1                      | 11        | T       | B        | Tc0               | Bc20      | Jc_ref     | C0          | alpha        | beta         | gamma   | wireDiameter | Cu_noCu       |          |
| CFUN_Jc_NbTi_Cudi_fit1_v1                    | 8         | T       | B        | Tc0               | Bc20      | C1         | C2          | wireDiameter | Cu_noCu      |         |              |               |          |
| CFUN_Jc_NbTi_Cudi_v1                         | 12        | T       | B        | Tc0               | Bc20      | c1         | c2          | c3           | c4           | c5      | c6           | wireDiameter  | Cu_noCu  |
| CFUN_Jc_T_B_BSCCO2212_block20T_new_v1        | 3         | T       | B        | f_scaling         |           |            |             |              |              |         |              |               |          |
| CFUN_Jc_T_B_BSCCO2212_block20T_v1            | 3         | T       | B        | f_scaling         |           |            |             |              |              |         |              |               |          |
| CFUN_Jc_T_B_BSCCO2212_block20T_Power_Gosh_v1 | 3         | T       | B        | f_scaling         |           |            |             |              |              |         |              |               |          |
| CFUN_Jc_T_B_BSCCO2212_Power_Jiang_Wesche_v1  | 3         | T       | B        | f_scaling         |           |            |             |              |              |         |              |               |          |
| CFUN_JcNbTiLowCurrent_v1                     | 2         | T       | B        |                   |           |            |             |              |              |         |              |               |          |
| CFUN_Tcs_Nb3Sn_v1                            | 6         | J_sc    | B        | Jc0               | Jc_Nb3Sn0 | Tc0_Nb3Sn  | Bc20_Nb3Sn  |              |              |         |              |               |          |
| CFUN_TcsNbTi_v1                              | 4         | B       | I        | C1                | C2        |            |             |              |              |         |              |               |          |

Other functions:

| C function name                      | Arguments | 1st Arg | 2nd Arg | 3d Arg  | 4th Arg | 5th Arg | 6th Arg | 7th Arg | 8th Arg | 9th Arg | 10th Arg   | 11th Arg      |
|--------------------------------------|-----------|---------|---------|---------|---------|---------|---------|---------|---------|---------|------------|---------------|
| CFUN_hHe_v1                          | 2         | T       | THe     |         |         |         |         |         |         |         |            |               |
| CFUN_n_T_B_BSCCO2212_Jiang_Wesche_v1 | 3         | T       | B       | n0      |         |         |         |         |         |         |            |               |
| CFUN_PvsT_cryocooler_SHI_SRDE_418D4  | 1         | T       |         |         |         |         |         |         |         |         |            |               |
| CFUN_QHCircuit_v1                    | 10        | t       | t_on    | T       | U_0     | C       | R_warm  | w_QH    | h_QH    | l_QH    | mode (int) |               |
| CFUN_QHCircuit_v2                    | 11        | t       | T       | rho_SS  | t_on    | U_0     | C       | R_warm  | w_QH    | h_QH    | l_QH       | mode (int)    |
| CFUN_quenchState_v1                  | 2         | Is      | Icrit   |         |         |         |         |         |         |         |            |               |

## RRR* for Aluminium types

In the matrix below one can find the values for RRR based on the aluminium type used.

| Aluminium type | RRR                                 |
|----------------|-------------------------------------|
| 6061           | 2.853 - 3.551 (k: 2.55, rho: 3.055) |
| 5083           | 1.868 - 1.954 (k: 1.89, rho: 1.943) |
| 1350           | 62.51                               |
| 1100           | 37                                  |
| 2014           | 2.789                               |
| 2024 - 0       | 5.8                                 |
| 7075 - T6      | 2.05                                |
