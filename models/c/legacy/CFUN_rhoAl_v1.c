#include <math.h>
#include <float.h>
#include <stdlib.h>
#include <string.h>
#ifdef _MSC_VER
#define EXPORT __declspec(dllexport)
#else
#define EXPORT
#endif

static const char *error = NULL;

EXPORT int init(const char *str) {
  return 1;
}

EXPORT const char * getLastError() {
  return error;
}

EXPORT const char* return_test_yaml() {
    return "CFUN_rhoAl_v1";
}

EXPORT int eval(const char *func,
                              int nArgs,
                              const double **inReal,
                              const double **inImag,
                              int blockSize,
                              double *outReal,
                              double *outImag) {
  
  	double params1[7] = {0.476050395713888, -3.299730411832043, 7.489576522510857, -5.738194411483883, 1.121748209742612, 0.207979301275814, -4.010635050869012};
    double rhoAl;
    double log_T;
	int i;

  if (strcmp("CFUN_rhoAl_v1", func) == 0) {
    if (nArgs != 1) {
      error = "One argument expected";
      return 0;
    }

    for (i = 0; i < blockSize; i++) {
		
	    double T = inReal[0][i];

      if(T < 0){
          error = "T is negative!"; return 0; 
        } 
      
      if(T > 1400){
          error = "Temperature is above 1400 K - outside of the range!";
          return 0;	  
        }

      if(T > 0 && T <= 1400) {
        log_T = log10(T);
        rhoAl = params1[0] * pow(log_T, 6) + params1[1] * pow(log_T, 5) + params1[2] * pow(log_T, 4) + params1[3] * pow(log_T, 3) + params1[4] * log_T* log_T + params1[5] * log_T + params1[6];
        }
  
      outReal[i] = 1e-8 * pow(10, rhoAl);

      if(outReal[i]!=outReal[i]){
        error = "Output is nan"; 
        return 0;	  
      }
      if (fabs(outReal[i])>DBL_MAX){
        error = "Output is inf"; 
        return 0;	  
      }
	  }
		return 1;
  }

}
