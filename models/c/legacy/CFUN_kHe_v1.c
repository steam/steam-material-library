#include <math.h>
#include <float.h>
#include <stdlib.h>
#include <string.h>
#ifdef _MSC_VER
#define EXPORT __declspec(dllexport)
#else
#define EXPORT
#endif

static const char *error = NULL;

EXPORT int init(const char *str) {
  return 1;
}

EXPORT const char * getLastError() {
  return error;
}

EXPORT const char* return_test_yaml() {
    return "";
}

EXPORT int eval(const char *func,
                              int nArgs,
                              const double **inReal,
                              const double **inImag,
                              int blockSize,
                              double *outReal,
                              double *outImag) {

  int i;
  
  double deltaT1 = 0.3;
  double deltaT2 = 8.0;
  double deltaT3 = 20.0;
  
  double T1;
  double T2;
  double T3; 
  
  double aK_in[4] = { 0.00000E+00,  2.16030E-01,  0.00000E+00,  2.01540E-11};
  double bK_in[4] = { 0.00000E+00, -6.93930E+00, -1.21100E-03, -2.10600E-08};
  double cK_in[4] = { 0.00000E+00,  8.89759E+01,  8.81300E-02,  8.53000E-06};
  double dK_in[4] = { 0.00000E+00, -5.80971E+02, -2.56290E+00, -1.62500E-03};
  double eK_in[4] = { 0.00000E+00,  2.04498E+03,  3.76464E+01,  1.22460E-01};
  double fK_in[4] = { 0.00000E+00, -3.79372E+03, -2.79119E+02,  4.80000E+00};	
  double gK_in[4] = { 2.47800E+03,  3.45053E+03,  1.03405E+03,  1.00480E+02};	 
	
  if (strcmp("CFUN_kHe_v1", func) == 0) {
    if (nArgs != 2) {
      error = "Two arguments expected";
      return 0;
    }

    for (i = 0; i < blockSize; i++) {
		
      double T        = inReal[0][i];
      double T_ExtRef = inReal[1][i];
      
      /*Input consistency check with hotfix for negative T*/
      //	  if(T < 1.9)   {T=1.9;}
      if(T < 0)        {error = "T is negative!"; return 0; } 
      if(T_ExtRef < 0) {error = "T_ExtRef is negative!"; return 0; } 	  
    
      T1 = T_ExtRef + deltaT1;
      T2 = T_ExtRef + deltaT2;
      T3 = T_ExtRef + deltaT3;
            
      if(T <= T1) {
        outReal[i] = aK_in[0]*pow(T,6) + bK_in[0]*pow(T,5) + cK_in[0]*pow(T,4) + dK_in[0]*pow(T,3) + eK_in[0]*pow(T,2) + fK_in[0]*T + gK_in[0];
        }
      else if (T > T1 && T<= T2) {
        outReal[i] = aK_in[1]*pow(T,6) + bK_in[1]*pow(T,5) + cK_in[1]*pow(T,4) + dK_in[1]*pow(T,3) + eK_in[1]*pow(T,2) + fK_in[1]*T + gK_in[1];
        }
      else if (T > T2 && T<= T3) {
        outReal[i] = aK_in[2]*pow(T,6) + bK_in[2]*pow(T,5) + cK_in[2]*pow(T,4) + dK_in[2]*pow(T,3) + eK_in[2]*pow(T,2) + fK_in[2]*T + gK_in[2];  
        }
      else {
        outReal[i] = aK_in[3]*pow(T,6) + bK_in[3]*pow(T,5) + cK_in[3]*pow(T,4) + dK_in[3]*pow(T,3) + eK_in[3]*pow(T,2) + fK_in[3]*T + gK_in[3];
        }
      
        /*Output consistency check*/
      if(outReal[i]!=outReal[i]){
        error = "Output is nan"; 
        return 0;	  
        }
      if (fabs(outReal[i])>DBL_MAX){
        error = "Output is inf"; 
        return 0;	  
        }
    }
    return 1;
  }

  else {
    error = "Unknown function";
    return 0;
  }
}