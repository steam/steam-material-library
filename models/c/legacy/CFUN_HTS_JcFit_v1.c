#include <math.h>
#include <float.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#ifdef _MSC_VER
#define EXPORT __declspec(dllexport)
#else
#define EXPORT
#endif

#define MAX(x, y) (((x) > (y)) ? (x) : (y))
#define MIN(x, y) (((x) < (y)) ? (x) : (y))
#define PI 3.14159265358979323846
#define deg2rad PI/180
#define rad2deg 180/PI

static const char *error = NULL;

EXPORT int init(const char *str) {
  return 1;
}

EXPORT const char * getLastError() {
  return error;
}

EXPORT const char* return_test_yaml() {
    return "";
}

EXPORT int eval(const char *func,
                int nArgs,
                const double **inReal,
                const double **inImag,
                int blockSize,
                double *outReal,
                double *outImag) {
 
 	
 if (nArgs != 3) {
	// Top_IN, Bnorm_IN, thetaFieldTape_IN
	error = "three arguments expected";
    return 0;
    }
	
// Fit Parameters
double n0;  
double n1;
double n2;
double a; 
double Tc0;
double Bi0_ab;
double Bi0_c;
double alpha_ab;
double p_ab;
double q_ab;
double alpha_c;
double p_c;
double q_c;
double g0;
double g1;
double g2;
double g3;
double gamma_ab;
double gamma_c;
double nu;
double thetaOff;

//For this to work we need to correct the names
if (strcmp("CFUN_HTS_JcFit_DUMMY", func) == 0) {
	g0       = 0.061764023;     //[-]
	g1       = 0.20004869;      //[-]
	g2       = 0.030574229;     //[-]
	g3       = 0.069662567;     //[-]
	Tc0      = 93;              //[K]
	p_c      = 0.530993392;     //[-]
	q_c      = 2.79847849;      //[-]
	Bi0_c    = 140;             //[T]
	gamma_c  = 2.461196439;     //[-]
	alpha_c  = 2.09687542*1e12; //[A*T/m^2]
	nu       = 1.483955279;     //[-]		
	n0       = 1.1212306;       //[-]
	n1       = 1.599578904;     //[-]
	n2       = 2.157677199;     //[-]
	p_ab     = 0.792153987;     //[-]
	q_ab     = 4.003286501;     //[-]
	Bi0_ab   = 250;             //[T]		
	a        = 0.026386799;     //[-]
	gamma_ab = 2.117501761;     //[-]
	alpha_ab = 17.186580*1e12;  //[A*T/m^2]
	thetaOff = -30.0;           //[deg]	
}
else if (strcmp("CFUN_HTS_JcFit_THEVA", func) == 0) {
	g0       = 0.061764023;     //[-]
	g1       = 0.20004869;      //[-]
	g2       = 0.030574229;     //[-]
	g3       = 0.069662567;     //[-]
	Tc0      = 93;              //[K]
	p_c      = 0.530993392;     //[-]
	q_c      = 2.79847849;      //[-]
	Bi0_c    = 140;             //[T]
	gamma_c  = 2.461196439;     //[-]
	alpha_c  = 2.09687542*1e12; //[A*T/m^2]
	nu       = 1.483955279;     //[-]		
	n0       = 1.1212306;       //[-]
	n1       = 1.599578904;     //[-]
	n2       = 2.157677199;     //[-]
	p_ab     = 0.792153987;     //[-]
	q_ab     = 4.003286501;     //[-]
	Bi0_ab   = 250;             //[T]		
	a        = 0.026386799;     //[-]
	gamma_ab = 2.117501761;     //[-]
	alpha_ab = 17.186580*1e12;  //[A*T/m^2]
	thetaOff = -32.85;          //[deg]	
}
else if(strcmp("CFUN_HTS_JcFit_SUPERPOWER", func) == 0) {
	g0       = 0.039445688; //[-]
	g1       = 0.276598888;  //[-]
	g2       = 0.047699303; //[-]
	g3       = 0.055878401; //[-]
	Tc0      = 93; //[K]
	p_c      = 0.400002709; //[-]
	q_c      = 2.940671164; //[-]
	Bi0_c    = 140; //[T]
	gamma_c  = 2.999950877; //[-]
	alpha_c  = 2.99996E+12; //[A*T/m^2]
	nu       = 1.720055527; //[-]		
	n0       = 1.169541488; //[-]
	n1       = 1.599990183; //[-]
	n2       = 1.131992777; //[-]
	p_ab     = 0.825413689; //[-]
	q_ab     = 5.495925721; //[-]
	Bi0_ab   = 250; //[T]		
	a        = 2.05E-05; //[-]
	gamma_ab = 4.998961387; //[-]
	alpha_ab = 7.10415E+13; //[A*T/m^2]
	thetaOff = -2.179824999; //[deg]	
}
else if(strcmp("CFUN_HTS_JcFit_sunam", func) == 0) {
	g0       = 0.031379048; //[-]
	g1       = 0.233191118; //[-]
	g2       = 0.155740362; //[-]
	g3       = 0.036370472; //[-]
	Tc0      = 93; //[K]
	p_c      = 0.400240526; //[-]
	q_c      = 2.984353635; //[-]
	Bi0_c    = 140; //[T]
	gamma_c  = 2.303390274; //[-]
	alpha_c  = 9.07843E+11; //[A*T/m^2]
	nu       = 2.197403244; //[-]		
	n0       = 1.120860649; //[-]
	n1       = 1.543948731; //[-]
	n2       = 4.979700712; //[-]
	p_ab     = 0.794763523; //[-]
	q_ab     = 4.009325728; //[-]
	Bi0_ab   = 250; //[T]		
	a        = 0.299243313; //[-]
	gamma_ab = 1.898633207; //[-]
	alpha_ab = 4.87979E+13; //[A*T/m^2]
	thetaOff = 0; //[deg]			
}
else if(strcmp("CFUN_HTS_JcFit_JeroenThesis", func) == 0) {
	g0       = 0.03; //[-]
	g1       = 0.25; //[-]
	g2       = 0.06; //[-]
	g3       = 0.058; //[-]
	Tc0      = 93; //[K]
	p_c      = 0.5; //[-]
	q_c      = 2.5; //[-]
	Bi0_c    = 140; //[T]
	gamma_c  = 2.44; //[-]
	alpha_c  = 1.86E+12; //[A*T/m^2]
	nu       = 1.85; //[-]		
	n0       = 1; //[-]
	n1       = 1.4; //[-]
	n2       = 4.45; //[-]
	p_ab     = 1; //[-]
	q_ab     = 5; //[-]
	Bi0_ab   = 250; //[T]		
	a        = 1.0E-01; //[-]
	gamma_ab = 1.63; //[-]
	alpha_ab = 6.83E+13; //[A*T/m^2]
	thetaOff = 0; //[deg]			
}
else{	
	error = "Unknown fit";
	return 0;
}

// Correction factor
double B_MIN = 0.01; //[T]

// Global index
int i; 

// Parfor loop
for (i = 0; i < blockSize; i++) {
	// Raw Input
	double Top_IN   = inReal[0][i];	
	double Bnorm_IN = inReal[1][i];
	double thetaFieldTape_IN = inReal[2][i];

	//Pre-allocation
	double Top;
	double Bnorm;
	double theta;
	double thetaMod;
	
	//Pre-processing
	Bnorm     = MAX(B_MIN,fabs(Bnorm_IN));
	Top       = Top_IN;	
    theta     = fabs(thetaFieldTape_IN - thetaOff)*deg2rad;	

	if (fmod(theta,PI)<=PI/2){ 
		thetaMod = fmod(theta,PI); 
		}
	else{ 
		thetaMod = PI-fmod(theta,PI);
		}

	//Pre-allocation
	double t_T;
	double t_n0;	
	double t_n1;	
	double Bi_ab;
	double Bi_c;
	double b_ab;
	double b_c;
	double Jc_ab;
	double Jc_c;
	bool TCrit_Flag;	
	bool babCrit_Flag;
	bool bcCrit_Flag;
	double g;
	double JcFit;
	
	// Temperature scaling factors
    t_T  = Top/Tc0;
	t_n0 = 1-pow(t_T,n0);
	t_n1 = 1-pow(t_T,n1);	
	
	// Irreversibility field  
	Bi_ab = Bi0_ab*(pow(t_n1,n2)+a*t_n0);
	Bi_c  = Bi0_c*t_n0;
    
	// magnetic flux density	
	b_ab  = Bnorm / Bi_ab;
	b_c   = Bnorm / Bi_c;
	
	// flags	
	TCrit_Flag   = t_T  >= 1 || t_T  < 0;
	babCrit_Flag = b_ab >= 1 || b_ab < 0;		
	bcCrit_Flag  = b_c  >= 1 || b_c  < 0;
	
	if (TCrit_Flag==false){
			
		if (babCrit_Flag==false){
		    // critical current density ab-plane	
			Jc_ab =	(alpha_ab/Bnorm*pow(b_ab,p_ab)) * pow((1-b_ab),q_ab) * pow((pow(t_n1,n2) + a*t_n0),gamma_ab);
			}
		else{
			// field over Bi
			Jc_ab = 0;
			}
		
		if (bcCrit_Flag==false){
			// critical current density c-plane
			Jc_c = (1/Bnorm)*alpha_c*pow(b_c,p_c)*pow((1-b_c),q_c)*pow((1-pow(t_T,n0)),gamma_c);
			}
		else{
			// field over Bi
			Jc_c = 0;
			}	
        
		// anisotropy factor
		g = g0 + g1*exp(-g2*Bnorm*exp(g3*Top));
	
		JcFit = MIN(Jc_ab,Jc_c)+ MAX(0,(Jc_ab-Jc_c))/(1+pow(((PI/2-thetaMod)/g),nu));
		}
		else{
		// temperature over Tc0
		JcFit=0;
		}
	
	//outReal[i] = JcFit;
	outReal[i] =JcFit;
	
	//Consistency check: output
	if(outReal[i]!=outReal[i]){
		error = "Output is nan"; 
		return 0;	  
		}
	
	if (fabs(outReal[i])>DBL_MAX){
		error = "Output is inf"; 
		return 0;	  
		}
	
}
	
return 1;
}
	
