# Material C-Library
For more information:
https://steam-material-library.docs.cern.ch/

## General Models
The `steam-material-library/models/c` folder includes C functions for the study of the LTS and HTS material properties. All the function curves in this folder, have been reproduced using fitting parameters generated from experimental measurements.

Most of the functions have a range from 0 to 1400 K. The generated fittings are basically used for a range 0-300 K. Linear approximations have been applied to almost all of the functions at temperatures above 295-400 K (it differs in every function), and in many of them at temperatures close to 0. For Temperatures above 1400 or below 0, there is no approximation used.

In this folder there are, also, functions that can calculate the critical temperatures TC & Tcs and the critical current densities Jc, using different methods.

## Derivatives
The analytical derivatives of material functions are contained in `steam-material-library\models\c\derivatives`.
They are used for example in the **Newton-Raphson algorithm** carried out by [CERNGetDP interface](https://gitlab.cern.ch/steam/cerngetdp). 
Since they are analytically calculated, these derivatives compute faster than the finite difference method that was used before.

## Materials & properties

Material properties:
* Cv  : specific heat in J/(m^3*K)
* k   : thermal conductivity in  
* rho : resistivity in Ohm*m

Materials used: 
* Silver (Ag)
* Aluminum 1350/6061/5083(Al)
* Brass
* Beryllium copper (BeCu)
* Bismuth strontium calcium copper oxide (BSCCO or Bi-2212)
* Copper (Cu)
* G10
* Hastelloy
* Helium (He)
* Kapton
* Nb3Sn
* NbTi
* Steel
* Stycast
* Stainless Steel (SS)
* Silver-Magnesium (AgMg)

## Python Bindings
A package in Pypi that includes the functions of this folder has been created so that they can be easily accessible by Python developers. You can find and install this package [here](https://pypi.org/project/STEAM-materials/#files)
