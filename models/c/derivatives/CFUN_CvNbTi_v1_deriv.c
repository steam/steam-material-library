#include <math.h>
#include <float.h>
#include <stdlib.h>
#include <string.h>
#ifdef _MSC_VER
#define EXPORT __declspec(dllexport)
#else
#define EXPORT
#endif

static const char *error = NULL;

EXPORT int init(const char *str) {
  return 1;
}

EXPORT const char * getLastError() {
  return error;
}

EXPORT int eval(const char *func,
                              int nArgs,
                              const double **inReal,
                              const double **inImag,
                              int blockSize,
                              double *outReal,
                              double *outImag) {
  double Tc0 = 9.2;
  double Bc20 = 14.5;
  
  double T1 = 28.358;
  double T2 = 50.099;
  double T3 = 165.8;
  double T4 = 496.54;
  
  int i;
  double aCp[6] = {0.00000E+00, 0.00000E+00, -2.17700E-01, -4.82000E-03, -6.29000E-05,  0.00000E+00};
  double bCp[6] = {4.91000E+01, 1.62400E+01,  1.19838E+01,  2.97600E+00,  9.29600E-02,  0.00000E+00};
  double cCp[6] = {0.00000E+00, 0.00000E+00,  5.53710E+02, -7.16300E+02, -5.16600E+01, -2.57000E-01};
  double dCp[6] = {6.40000E+01, 9.28000E+02, -7.84610E+03,  8.30220E+04,  1.37060E+04,  9.55500E+02};
  double eCp[6] = {0.00000E+00, 0.00000E+00,  4.13830E+04, -1.53000E+06,  1.24000E+06,  2.45000E+06};

  //linear approximation parameters
  double m = 526.3158;
  double n = 2604242.490531;

  if (strcmp("CFUN_CvNbTi_v1_deriv", func) == 0) {
    if (nArgs != 5) {
      error = "Five arguments expected";
      return 0;
    }

    for (i = 0; i < blockSize; i++) {
		
	  double T   = inReal[0][i];
	  double B   = inReal[1][i];
	  double I   = inReal[2][i];
	  double C1  = inReal[3][i];
      double C2  = inReal[4][i];
	  double Tcs = 0;
	  
	  /*Input consistency check with hotfix for negative T*/
		  if(T < 1.9)   {T=1.9;}
	      if(T < 0)     {error = "T is negative!"; return 0; } 
		  if(B < 0)     {error = "normB is negative!"; return 0; }
		  	  
	  	if(B <= Bc20){
			if (fabs(I)/(C1+C2*B)>=0){
				if (fabs(I)/(C1+C2*B)<=1){
					Tcs = (1-fabs(I)/(C1+C2*B))*Tc0*pow((1-B/Bc20),0.59);	
				}	
			}	
		}

	  	  
	  if(T <= Tcs) {
		outReal[i] = 4* aCp[0]*pow(T,3) + 3* bCp[0]*pow(T,2) + 2* cCp[0]*T + dCp[0]*B;
	  }
      else if (T > Tcs && T<= T1) {
		  outReal[i] = 4* aCp[1]*pow(T,3) + 3* bCp[1]*pow(T,2) + 2* cCp[1]*T + dCp[1];
	  }
	  else if (T > T1 && T<= T2) {
		  outReal[i] =  4* aCp[2]*pow(T,3) + 3* bCp[2]*pow(T,2) + 2* cCp[2]*T + dCp[2];
	  }
	  else if (T > T2 && T<= T3) {
		  outReal[i] =  4* aCp[3]*pow(T,3) + 3* bCp[3]*pow(T,2) + 2* cCp[3]*T + dCp[3];
	  }
	  else if (T > T3 && T<= T4) {
		  outReal[i] =  4* aCp[4]*pow(T,3) + 3* bCp[4]*pow(T,2) + 2* cCp[4]*T + dCp[4];
	  }
	  /*else {
		  outReal[i] = aCp[5]*pow(T,4) + bCp[5]*pow(T,3) + cCp[5]*pow(T,2) + dCp[5]*T + eCp[5];
	  }*/
	  
	  else{
		//linear approximation
		outReal[i] = m;
	  }
	  /*Output consistency check*/
	  if(outReal[i]!=outReal[i]){
		error = "Output is nan"; 
		return 0;	  
	  }
	  if (abs(outReal[i])>DBL_MAX){
		error = "Output is inf"; 
		return 0;	  
	  }
    }
    return 1;
  }

  else {
    error = "Unknown function";
    return 0;
  }
}