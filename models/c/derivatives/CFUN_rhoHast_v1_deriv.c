#include <math.h>
#include <float.h>
#include <stdlib.h>
#include <string.h>
#ifdef _MSC_VER
#define EXPORT __declspec(dllexport)
#else
#define EXPORT
#endif

static const char *error = NULL;

EXPORT int init(const char *str) {
return 1;
}

EXPORT const char * getLastError() {
return error;
}

double rhoHast(double T) {
    double rhoHast, drhoHast_dT;
    double a =  3.06570628e-08;
    double b = -1.85419904e-05;
    double c =  2.33889251e-04;
    double d = -2.87724698e-01;
    double e = -9.42567900e-08;
    double f =  1.27809896e-06;

	double a2 =  1.2293715479519699e+002;
	double b2 =  2.0894998327105480e-003;
	double c2 =  1.2582374848591569e-004;
	double d2 = -2.9269784569833794e-007;

	double g =  5.5555e-11;


    if(T <= 295 && T >= 50) {
		drhoHast_dT = e * (pow(10, a*T*T*T + b*T*T + c*T + d))* (3*a*T*T + 2*b*T + c) * log(10);
	}
	if(T < 50){
		drhoHast_dT = (3*d2*T*T + 2*c2*T + b2)*1e-8;
	}
	if(T > 295){
		//linear approximation
		drhoHast_dT = g;
	}
    return drhoHast_dT;
}

EXPORT int eval(const char *func,
                              int nArgs,
                              const double **inReal,
                              const double **inImag,
                              int blockSize,
                              double *outReal,
                              double *outImag) {

	int i;

	if (strcmp("CFUN_rhoHast_v1_deriv", func) == 0) {
		if (nArgs != 1) {
			error = "One argument expected";
			return 0;
			}

    for (i = 0; i < blockSize; i++) {
			
			double T = inReal[0][i];

			if(T < 0)     {
				error = "T is negative!"; return 0; 
			} 
	
			if(T > 1400){
				error = "Temperature is above 1400 K - outside of the range!";
				return 0;	  
			}
	
			outReal[i] = rhoHast( T ) ;
	
			/*Output consistency check*/
			if(outReal[i]!=outReal[i]){
				error = "Output is nan"; 
				return 0;	  
			}
			if (fabs(outReal[i])>DBL_MAX){
				error = "Output is inf"; 
				return 0;	  
			}
			}
	
    return 1;
}

}