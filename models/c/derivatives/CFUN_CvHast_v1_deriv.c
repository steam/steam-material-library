#include <math.h>
#include <float.h>
#include <stdlib.h>
#include <string.h>
#ifdef _MSC_VER
#define EXPORT __declspec(dllexport)
#else
#define EXPORT
#endif

static const char *error = NULL;

EXPORT int init(const char *str) {
return 1;
}

EXPORT const char * getLastError() {
return error;
}

double CvHast(double T) {
	double CvHast;
    double a =  6.24337055e-10;
    double b = -1.39265083e-07;
    double c =  1.52127960e-05;
    double d =  2.29168247e-07;
    double e = -1.87730865e-01;
    double f =  1.70166185e+01;
	double g = -2.77114255e+02;
	double h =  3.09769670e+03;
	double i =  2.95618392e+03;
	double j = -1.57110370e-12;
	double k =  2.12749836e-15;
	double l = -1.21101931e-18;

	double a2 =  3764.063571;
    double b2 =  3.4472427467879702e-001;
    double c2 = -2.5464259199394670e-002;
    double d2 =  1.5172451948324440e-003;
    double e2 = -1.2905644232172242e-005;
	
	double m = 2.8333357325708231e-001*8890;
	double n = 3036761.030182;
	
	if(T < 316 && T > 34) {
		CvHast = 11 * l * pow(T, 10) + 10 * k * pow(T, 9) + 9 * j * pow(T, 8) + 8 * a * pow(T, 7) + 7 * b * pow(T, 6) + 6 * c * pow(T, 5) + 5 * pow(d, pow(T, 4)) * log(d) + 4 * e * pow(T, 3) + 3 * f * pow(T, 2) + 2 * g * T + h;    
	}   
	
	//316 is the temperature 
	if(T >= 316) {
		 CvHast = m;
    }
	if(T < 34) {
		/*Linear appr.*/
		
		CvHast = (4*e2*pow(T,3) + 3*d2*pow(T,2) + 2*c2*T + b2)*8890;
    } 

	return CvHast;
}


EXPORT int eval(const char *func,
                              int nArgs,
                              const double **inReal,
                              const double **inImag,
                              int blockSize,
                              double *outReal,
                              double *outImag) {

	int i;

	if (strcmp("CFUN_CvHast_v1_deriv", func) == 0) {
		if (nArgs != 1) {
			error = "One argument expected";
			return 0;
			}

    for (i = 0; i < blockSize; i++) {
			
			double T = inReal[0][i];

			if(T < 0)     {
				error = "T is negative!"; return 0; 
			} 
	
			if(T > 1400){
				error = "Temperature is above 1400 K - out of range!";
				return 0;	  
			} 
	
			
			outReal[i] = CvHast( T );
	
			/*Output consistency check*/
			if(outReal[i]!=outReal[i]){
				error = "Output is nan"; 
				return 0;	  
			}
			if (fabs(outReal[i])>DBL_MAX){
				error = "Output is inf"; 
				return 0;	  
			}
			}
	
    return 1;
}

/*else {
    error = "Unknown function";
    return 0;
}*/
}