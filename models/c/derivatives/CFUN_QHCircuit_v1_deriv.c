#include <math.h>
#include <float.h>
#include <stdlib.h>
#include <string.h>
#ifdef _MSC_VER
#define EXPORT __declspec(dllexport)
#else
#define EXPORT
#endif

static const char *error = NULL;

EXPORT int init(const char *str) {
  return 1;
}

EXPORT const char * getLastError() {
  return error;
}

double rhoSS(double T) {
	double a = -6.88E-07; // Ohm*m/K^3
	double b = 3.94E-04;  // Ohm*m/K^2
	double c = 1.92E-02;  // Ohm*m/K
	double d = 6.07E+01;  // Ohm*m
	           
	return (a*pow(T,3) + b*pow(T,2) + c*T + d)*1e-8;
}

double drhoSS_dt(double T) {
	double a = -6.88E-07; // Ohm*m/K^3
	double b = 3.94E-04;  // Ohm*m/K^2
	double c = 1.92E-02;  // Ohm*m/K
	double d = 6.07E+01;  // Ohm*m
	           
	return (3*a*pow(T,2) + 2*b*T + c)*1e-8;
}

EXPORT int eval(const char *func,
                              int nArgs,
                              const double **inReal,
                              const double **inImag,
                              int blockSize,
                              double *outReal,
                              double *outImag) {
  int i;
  
  if (strcmp("CFUN_QHCircuit_v1_deriv", func) == 0) {
    if (nArgs != 10) {
      error = "Ten arguments expected";
      return 0;
    }

    for (i = 0; i < blockSize; i++) {
	  /*Input parameters read*/
	  double t    = inReal[0][i];
	  double t_on = inReal[1][i];
	  double T    = inReal[2][i];
	  
	  double U_0    = inReal[3][i];
	  double C      = inReal[4][i];
	  double R_warm = inReal[5][i];
	  double w_QH   = inReal[6][i];
	  double h_QH   = inReal[7][i];
	  double l_QH   = inReal[8][i];
	  int mode      = (int) inReal[9][i];
	  
	  /*Local variables needed for computation*/
	  double i_0, i_QH, tau, S_QH, dR_QH_dT, R_QH, R_QH_T0, dR_QH_T0_dT, di_0_dT, dtau_dT, di_QH_dT;
	  
	  /*Input consistency check*/
	  if(t<0)     {error = "Time is negative!";        return 0; }
	  if(t_on<0)  {error = "t_on should be positive!"; return 0; }
	  if(T<0)     {error = "Temperature is negative!"; return 0; }
	  if(C<0)     {error = "Capacitance is negative!"; return 0; }
	  if(R_warm<0){error = "R_warm is negative!";      return 0; }
	  if(w_QH<0)  {error = "w_QH is negative!";        return 0; }
	  if(h_QH<0)  {error = "h_QH is negative!";        return 0; }
	  if(l_QH<0)  {error = "l_QH is negative!";        return 0; }
	  
	  if(t >= t_on) {
		  t    = t - t_on;
		  S_QH = (w_QH * h_QH);
		  
		  R_QH_T0 = rhoSS(1.9) * l_QH / S_QH;
          if(T ==1.9){
            dR_QH_T0_dT    = 0;
          }
          
		  R_QH     = rhoSS( T ) * l_QH / S_QH;
          dR_QH_dT = drhoSS_dt( T ) * l_QH / S_QH;
		  
		  i_0     = - U_0 / (R_warm + R_QH_T0);
          di_0_dT =   U_0 / pow(R_warm + R_QH_T0, 2) * dR_QH_T0_dT;

		  tau     = (R_warm + R_QH) * C;
          dtau_dT = dR_QH_dT * C;

		  i_QH     = i_0 * exp( -t / tau );
          di_QH_dT = di_0_dT * exp( -t / tau ) + dtau_dT* (t/pow(tau, 2)) * exp( -t / tau ); 
		  
		  switch(mode) {
			case 1: // Power
				outReal[i] = 2 *i_QH *di_QH_dT *pow(1 / S_QH, 2) * rhoSS( T ) +  pow(i_QH / S_QH, 2) * drhoSS_dt( T );
				break;
			case 2: // Current
				outReal[i] = di_QH_dT;
				break;
			case 3: // Resistance
				outReal[i] = dR_QH_dT;
				break;
			default:
				outReal[i] = 0.0;
				break;
		  }
	  }
	  else {
		  outReal[i] = 0;
	  }
	  	  
	  /*Output consistency check*/
	  if(outReal[i]!=outReal[i]){
		error = "Output is nan"; 
		return 0;	  
	  }
	  if (fabs(outReal[i])>DBL_MAX){
		error = "Output is inf"; 
		return 0;	  
	  }	
	  
    }
    return 1;
  }

  else {
    error = "Unknown function";
    return 0;
  }
}