#include <math.h>
#include <float.h>
#include <stdlib.h>
#include <string.h>
#ifdef _MSC_VER
#define EXPORT __declspec(dllexport)
#else
#define EXPORT
#endif

static const char *error = NULL;

EXPORT int init(const char *str) {
  return 1;
}

EXPORT const char * getLastError() {
  return error;
}

EXPORT int eval(const char *func,
                              int nArgs,
                              const double **inReal,
                              const double **inImag,
                              int blockSize,
                              double *outReal,
                              double *outImag) {
  double Tc0   = 17.8;   //[K]
  double Bc20  = 27.012; //[T]

  double T1 = 20;
  double T2 = 400;
 
  int i;
  
  double a0    = 79.78547;
  double a1    = -247.44839;
  double a2    = 305.01434;
  double a3    = -186.90995;
  double a4    = 57.48133;
  double a5    = -6.3977;
  double a6    = -0.6827738;
  double a7    = 0.1662252;
  double beta  = 1.241e-3;
  double gamma = 0.138;
  double rho   = 8950; //[kg/m^3]
  double log_T;
  double f_exp;
  double df_dlog_T, df_dT, dlog_T_dT, derivative;
  
  if (strcmp("CFUN_CvNb3Sn_v1_deriv", func) == 0) {
    if (nArgs != 2) {
      error = "Two arguments expected";
      return 0;
    }

    for (i = 0; i < blockSize; i++) {		
	  double T   = inReal[0][i];
	  double B   = inReal[1][i];
	  double Tc  = 0;
	  
	  /*Input consistency check with hotfix for negative T*/
	  	  //	  if(T < 1.9)   {T=1.9;}
	  if(T < 0)     {error = "T is negative!"; return 0; } 
	  if(B < 0)     {error = "normB is negative!"; return 0; }
	  
	  if(B <= Bc20){
		Tc = Tc0*pow((1-B/Bc20),0.59);	
	   }	

	  	  
	  if(T <= Tc) {
	  outReal[i] = rho * (3 * (beta + 3 * gamma / (Tc0 * Tc0)) * pow(T, 2) + gamma * B / Bc20);
    }
      else if (T > Tc && T<= T1) {
		  outReal[i] = rho*( 3*beta*pow(T,2) + gamma );
	  }
	  else if (T > T1 && T<= T2) {
		  log_T      = log10(T);
      df_dlog_T = a1 + 2 * a2 * log_T + 3 * a3 * pow(log_T, 2) + 4 * a4 * pow(log_T, 3) + 5 * a5 * pow(log_T, 4) + 6 * a6 * pow(log_T, 5) + 7 * a7 * pow(log_T, 6);
      //Calculate the derivative of log_T with respect to T
			dlog_T_dT = 1.0 / (T * log(10));
			//Chain rule 1st part
		  df_dT = df_dlog_T * dlog_T_dT;

		  f_exp      = (a0 + a1*log_T + a2*pow(log_T,2) + a3*pow(log_T,3) + a4*pow(log_T,4) + a5*pow(log_T,5) + a6*pow(log_T,6) + a7*pow(log_T,7));
		  outReal[i] = rho*(pow(10, f_exp))*df_dT* log(10);
	  }
	  else {
		  outReal[i] = rho*(0.0425);
	  }
	  	  
	  /*Output consistency check*/
	  if(outReal[i]!=outReal[i]){
		error = "Output is nan"; 
		return 0;	  
	  }
    
	  if (fabs(outReal[i])>DBL_MAX){
		error = "Output is inf"; 
		return 0;	  
	  }
    }
    return 1;
  }

  else {
    error = "Unknown function";
    return 0;
  }
}
