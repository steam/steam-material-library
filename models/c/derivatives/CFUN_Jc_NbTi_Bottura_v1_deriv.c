#include <math.h>
#include <float.h>
#include <stdlib.h>
#include <string.h>

#ifdef _MSC_VER
#define EXPORT __declspec(dllexport)
#else
#define EXPORT
#endif

static const char *error = NULL;

EXPORT int init(const char *str) {
    return 1;
}

EXPORT const char *getLastError() {
    return error;
}

double maximum(double a, double b) {
    if (a > b) {
        return a;
    }
    return b;
}

EXPORT int eval(const char *func,
                int nArgs,
                const double **inReal,
                const double **inImag,
                int blockSize,
                double *outReal,
                double *outImag) {
    
    double Tc0, Bc20, CJ, p, q;
    double exp_factor = 1.52;
    int i;
    double PI_4       = 3.14159265358979323846/4 ;
    double t, b, Jc_sc, Ic_sc;
    double Bc2, dBc2_dT, dJc_dT;
    double minB = 1e-6;

    
  
    if (strcmp("CFUN_Jc_NbTi_Bottura_v1_deriv", func) == 0) {
        if (nArgs != 11) {
            error = "Eleven arguments expected";
            return 0;
        }

        for (i = 0; i < blockSize; i++) {
            // parameters 
            double T        = inReal[0][i];
            double B        = inReal[1][i];
            double Tc0      = inReal[2][i];
            double Bc20     = inReal[3][i];
            double Jc_ref   = inReal[4][i];
            double C0       = inReal[5][i];
            double alpha    = inReal[6][i];
            double beta     = inReal[7][i];
            double gamma    = inReal[8][i];
            //Wire parameters
            double wireDiameter = inReal[9][i];
            double Cu_noCu      = inReal[10][i];

            double f_sc = 1 / (1 + Cu_noCu);

            
            B = maximum(minB, fabs(B));
            Bc2 = maximum(1e-9, Bc20 * (1 - pow(T / Tc0, 1.7)));

            if(Bc2 ==1e-9){
                dBc2_dT = 0;
            }else{
                dBc2_dT = Bc20 * (-1/Tc0) * (1.7 * pow((T/Tc0),0.7));
            }

            Jc_sc = Jc_ref * C0 * pow(B, alpha-1) / pow(Bc2, alpha) * pow(1-B/Bc2, beta) * pow(1 - pow(T / Tc0, 1.7), gamma);
            
            
            dJc_dT = Jc_ref * C0 * pow(B, alpha-1) / pow(Bc2, alpha) * pow(1-B/Bc2, beta) *(-1/Tc0)*(pow(T / Tc0, 0.7)*1.7)*gamma* pow(1 - pow(T / Tc0, 1.7), gamma-1) + Jc_ref * C0 * pow(B, alpha-1) / pow(Bc2, alpha) *dBc2_dT*(B/pow(Bc2,2))*beta* pow(1-B/Bc2, beta-1) * pow(1 - pow(T / Tc0, 1.7), gamma) + Jc_ref * C0 * pow(B, alpha-1)*dBc2_dT*(-alpha) / pow(Bc2, alpha+1) * pow(1-B/Bc2, beta) * pow(1 - pow(T / Tc0, 1.7), gamma);
            
            if(T >= Tc0){
                Jc_sc = 0;
                dJc_dT = 0;
            }
            if(B >= Bc20){
                Jc_sc = 0;
                dJc_dT = 0;
            }
            if(Jc_sc < 0){
                Jc_sc = 0;
                dJc_dT = 0;
            }
            
            outReal[i] = dJc_dT;
            
        }
        return 1;
    }

    else {
      error = "Unknown function";
      return 0;
    }
}
