#include <math.h>
#include <float.h>
#include <stdlib.h>
#include <string.h>
#ifdef _MSC_VER
#define EXPORT __declspec(dllexport)
#else
#define EXPORT
#endif

static const char *error = NULL;

EXPORT int init(const char *str) {
  return 1;
}

EXPORT const char * getLastError() {
  return error;
}

EXPORT int eval(const char *func,
                              int nArgs,
                              const double **inReal,
                              const double **inImag,
                              int blockSize,
                              double *outReal,
                              double *outImag) {
  
  	double rho = 0;
    double a = -1.23892787e-15;
    double b = 6.67134053e-13;
    double c = -4.98835279e-12;
    double d = 8.19454022e-08;
 
	int i;

  if (strcmp("CFUN_rhoBeCu_v1_deriv", func) == 0) {
    if (nArgs != 2) {
      error = "Two argument expected";
      return 0;
    }

    for (i = 0; i < blockSize; i++) {
		
	    double T          = inReal[0][i];
      double f_scaling  = inReal[1][i];
	    
        if (f_scaling == 0) {
            f_scaling = 1;
        }

        if(T < 19.6 && T >=0) {
            rho = 0;
        }

		    if (T >= 19.6 && T <= 300) {
            rho = 3*a*T*T + b *2*T + c;
        }
        else {
            error = "Temperature is out of range [4, 300] K";
        }

        outReal[i] = rho * f_scaling;
		
		/*Output consistency check*/
		if(outReal[i]!=outReal[i]){
			error = "Output is nan"; 
			return 0;	  
			}
		if (fabs(outReal[i])>DBL_MAX){
		error = "Output is inf"; 
		return 0;	  
		}
		}
		return 1;
  }

}
