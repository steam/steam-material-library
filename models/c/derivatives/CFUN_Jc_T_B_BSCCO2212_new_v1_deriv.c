#include <math.h>
#include <float.h>
#include <stdlib.h>
#include <string.h>
#ifdef _MSC_VER
#define EXPORT __declspec(dllexport)
#else

#define EXPORT
#endif

static const char *error = NULL;

EXPORT int init(const char *str) {
  return 1;
}

EXPORT const char * getLastError() {
  return error;
}

EXPORT int eval(const char *func,
                              int nArgs,
                              const double **inReal,
                              const double **inImag,
                              int blockSize,
                              double *outReal,
                              double *outImag) {
								  
    double a1       = 2463.4;
    double a2       = 2.05;

    double Tc_B, T0 = 4.2; //[K]
    double PI_4     = 3.14159265358979323846/4.0;

    double b1 = 816.11 / (PI_4 * 0.0012*0.0012 * 0.25);
    double b2 = 147.9 / (PI_4 * 0.0012*0.0012 * 0.25);
    
    double Jc_T0_B; //[A/m^2]
    double Jc_T_B;  //[A/m^2]
    double dJc_T_B_dT;
    int i;

    // renamed from CFUN_Jc_T_B_BSCCO2212_block20T_new_v1
    if (strcmp("CFUN_Jc_T_B_BSCCO2212_new_v1_deriv", func) == 0) {
        if (nArgs != 3) {
          error = "Three arguments expected";
          return 0;
        }

        for (i = 0; i < blockSize; i++) {  	 
        
            double T           = inReal[0][i];
	        double B           = inReal[1][i];
	        double f_scaling   = inReal[2][i];

    	        /*Input consistency check*/
	        if(T < 0)              {error = "T is negative!"; return 0; } 
            if(B < 0)      {error = "Jc_Nb3Sn0 is negative!"; return 0; }

            // Modify the input magnetic field
            if(B<0){
                B=-B; // direction of the magnetic field is unimportant
            }
            if(fabs(B)<0.001){
                B=0.001; // very small magnetic field causes numerical problems
            }
            if (T == 0) {
                T =0.001;
                dJc_T_B_dT = 0; // temperature equal to zero causes numerical problems
            }

            Tc_B = exp((log(a1) - log(B)) / a2);
            Jc_T0_B = b1 - b2 * log(B);

            Jc_T_B = Jc_T0_B * (Tc_B - T) / (Tc_B - T0);
            dJc_T_B_dT =  Jc_T0_B * (- 1) / (Tc_B - T0);

            if (Jc_T_B < 0) {
                Jc_T_B =0;
                dJc_T_B_dT = 0; // temperature equal to zero causes numerical problems
            }

            outReal[i] = dJc_T_B_dT * f_scaling;
    
	        if(outReal[i]!=outReal[i]){
	        error = "Output is nan"; 
	        return 0;	  
	    }
    	}
     return 1;

    }

    else {
      error = "Unknown function";
      return 0;
    }
}