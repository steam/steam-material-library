#include <math.h>
#include <float.h>
#include <stdlib.h>
#include <string.h>
#ifdef _MSC_VER
#define EXPORT __declspec(dllexport)
#else
#define EXPORT
#endif

static const char *error = NULL;

EXPORT int init(const char *str) {
  return 1;
}

EXPORT const char * getLastError() {
  return error;
}

EXPORT int eval(const char *func,
                              int nArgs,
                              const double **inReal,
                              const double **inImag,
                              int blockSize,
                              double *outReal,
                              double *outImag) {
								  
    double Bc2, f_T_T0, f_B_Bc2, C;
    double Jc_T_B ;
    int i;
  
    if (strcmp("CFUN_Jc_Bordini_v1_deriv", func) == 0) {
        if (nArgs != 6) {
          error = "Six arguments expected";
          return 0;
        }

        for (i = 0; i < blockSize; i++) {  	 
        
      double T           = inReal[0][i];
	    double B           = inReal[1][i];
	    double C0          = inReal[2][i];
	    double Tc0_Nb3Sn   = inReal[3][i];
	    double Bc20_Nb3Sn  = inReal[4][i];
	    double alpha       = inReal[5][i];

      double df_T_T0_dT, dBc2_dT, d_f_B_Bc2_dT, dC_dT;

    	    /*Input consistency check*/
	    if(T < 0)              {error = "T is negative!"; return 0; } 
        if(Tc0_Nb3Sn < 0)      {error = "Tc0_Nb3Sn is negative!"; return 0; }
        if(Bc20_Nb3Sn < 0)     {error = "Bc20_Nb3Sn is negative!"; return 0; }

        // Modify the input magnetic field
        if(B<0){
            B=-B; // direction of the magnetic field is unimportant
        }
        if(fabs(B)<0.001){
            B=0.001; // very small magnetic field causes numerical problems
        }

        df_T_T0_dT = 1.0 / Tc0_Nb3Sn;
        // Calculate f_T_T0
        f_T_T0 = T / Tc0_Nb3Sn;

        if (f_T_T0 > 1) {
            f_T_T0 = 1;
            df_T_T0_dT = 0; // avoid values higher than 1
        }

        // Calculate Bc2
        Bc2 = Bc20_Nb3Sn *pow(1-pow(f_T_T0,1.52),-1);
        dBc2_dT = Bc20_Nb3Sn *1.52* pow(f_T_T0, 0.52) * pow(1-pow(f_T_T0, 1.52), -2)*df_T_T0_dT;

        // Calculate f_B_Bc2
        f_B_Bc2 = B / Bc2;
        d_f_B_Bc2_dT = - B/(pow(Bc2, 2)) * dBc2_dT;

        if (f_B_Bc2 > 1) {
            f_B_Bc2 = 1;
            d_f_B_Bc2_dT = 0; // avoid values higher than 1
        }

        // Calculate C
        C = C0 * pow(1 - pow(f_T_T0, 1.52), alpha) * pow(1 - pow(f_T_T0, 2), alpha);
        // Calculate derivative of C
        dC_dT = C0 * (alpha *(-1.52)*pow(f_T_T0, 0.52)* pow(1 - pow(f_T_T0, 1.52), alpha -1) * pow(1 - pow(f_T_T0, 2), alpha) + pow(1 - pow(f_T_T0, 1.52), alpha) *alpha *(-2*f_T_T0)* pow(1 - pow(f_T_T0, 2), alpha -1)) *df_T_T0_dT;

        // Calculate Jc_T_B
        Jc_T_B = dC_dT / B * pow(f_B_Bc2, 0.5) * pow(1 - f_B_Bc2, 2) + C/ B *d_f_B_Bc2_dT* (0.5* pow(f_B_Bc2, -0.5) * pow(1 - f_B_Bc2, 2) - 2* pow(f_B_Bc2, 0.5) * (1 - f_B_Bc2));

        // Assign Jc_T_B to outReal[i]
        outReal[i] = Jc_T_B;
 
	    if(outReal[i]!=outReal[i]){
	    error = "Output is nan"; 
	    return 0;	  
	    }
    	}
     return 1;

    }

    else {
      error = "Unknown function";
      return 0;
    }
}
