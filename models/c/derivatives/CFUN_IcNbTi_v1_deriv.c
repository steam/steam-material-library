#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <float.h>
#ifdef _MSC_VER
#define EXPORT __declspec(dllexport)
#else
#define EXPORT
#endif

#define EPS .000000001

static const char *error = NULL;

double maximum(double a, double b) {
    return (a > b) ? a : b;
}

double minimum(double a, double b) {
    return (a < b) ? a : b;
}

EXPORT int init(const char *str) {
    return 1;
}

EXPORT const char * getLastError() {
    return error;
}

EXPORT int eval(const char *func,
                int nArgs,
                const double **inReal,
                const double **inImag,
                int blockSize,
                double *outReal,
                double *outImag) {

    double Tc0 = 9.2;             //[K]
    double Bc20 = 14.5;           //[T]
    double Jc0 = 1750 * pow(10, 6); //[A/m^2]
    double C0 = 27;
    double alpha = 0.57;
    double beta = 0.9;
    double gamma = 2.32;
    double n = 1.7;              //[-] taken from Lubell

    double Tmin = 1e-3;           // Min allowed T
    double Bmin = 1e-1;           // Min allowed B

    double Bc2;
    double Jc;
    double Ti;
    double ti;
    double Bi;
    double bi;
    int i;

    double dti_dTi, dJc_dti, dJc_dT, dBc2_dT, dbi_dT;

    if (strcmp("CFUN_IcNbTi_v1_deriv", func) == 0) {
        if (nArgs != 3) {
            error = "Three arguments expected";
            return 0;
        }

        for (i = 0; i < blockSize; i++) {
            double T = inReal[0][i];
            double B = inReal[1][i];
            double Area = inReal[2][i];

            /*Input consistency check with hotfix for negative T*/
            if (T < 0) { error = "T is negative!"; return 0; }
            if (B < 0) { error = "normB is negative!"; return 0; }
            if (Area < 0) { error = "Area is negative!"; return 0; }

            Ti = maximum(minimum(T, Tc0), Tmin);
            ti = Ti / Tc0;

            if(T < Tc0 && T > Tmin){
                dti_dTi = 1 / Tc0;
            }
            else{
                dti_dTi = 0;
            }

            Bc2 = Bc20 * (1 - pow(ti, n));
            dBc2_dT = Bc20 * (-n* pow(ti, n-1))*dti_dTi;

            Bi = maximum(B, Bmin);
            bi = minimum(Bi / (Bc2 + EPS), 1);

            if(Bi / (Bc2 + EPS) < 1 ){
                dbi_dT = - dBc2_dT * Bi / (pow((Bc2 + EPS), 2));
            }else{
                dbi_dT = 0;
            }

            Jc = Jc0 * (C0 / Bi * pow(bi, alpha) * pow((1 - bi), beta) * pow((1 - pow(ti, n)), gamma));

            dJc_dT = Jc0 * C0 / Bi* (pow(bi, alpha) * pow((1 - bi), beta) * gamma * pow((1 - pow(ti, n)), gamma - 1) * n *dti_dTi* (- pow(ti, n - 1)) + alpha*pow(bi, alpha-1)*dbi_dT * pow((1 - bi), beta) * pow((1 - pow(ti, n)), gamma) + pow(bi, alpha) * pow((1 - bi), beta-1)*beta* (-dbi_dT) * pow((1 - pow(ti, n)), gamma));

            outReal[i] = dJc_dT * Area;

            /*Output consistency check*/
            if (outReal[i] != outReal[i]) {
                error = "Output is nan";
                return 0;
            }
            
        }
        return 1;
    }

    else {
        error = "Unknown function";
        return 0;
    }
}
