#include <math.h>
#include <float.h>
#include <stdlib.h>
#include <string.h>
#ifdef _MSC_VER
#define EXPORT __declspec(dllexport)
#else
#define EXPORT
#endif

static const char *error = NULL;

EXPORT int init(const char *str) {
  return 1;
}

EXPORT const char * getLastError() {
  return error;
}

EXPORT int eval(const char *func,
                              int nArgs,
                              const double **inReal,
                              const double **inImag,
                              int blockSize,
                              double *outReal,
                              double *outImag) {
  
    double L0 = 2.45E-8;
    double kBeCu = 0;
    
    int i;
    
    if (strcmp("CFUN_kBeCu_WiedemannFranz_v1_deriv", func) == 0) {
        if (nArgs < 1 || nArgs > 2) {
            error = "One or two arguments expected";
            return 0;
        }
        
        for (i = 0; i < blockSize; i++) {
            double rhoBeCu     = inReal[0][i]; // use CFUN_rhoBeCu_v1 mex file to generate this input array
            double f_scaling   = 1;
            if (nArgs == 2) {
                f_scaling      = inReal[1][i];
            }
            
            kBeCu = L0 / rhoBeCu;
            outReal[i] = kBeCu * f_scaling;
            
            /* Output consistency check */
            if (outReal[i] != outReal[i]) {
                error = "Output is nan";
                return 0;
            }
            if (fabs(outReal[i]) > DBL_MAX) {
                error = "Output is inf";
                return 0;
            }
        }
        
        return 1;
    }
    
    return 0;
}
