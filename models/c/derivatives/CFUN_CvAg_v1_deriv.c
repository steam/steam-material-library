#include <math.h>
#include <float.h>
#include <stdlib.h>
#include <string.h>
#ifdef _MSC_VER
#define EXPORT __declspec(dllexport)
#else
#define EXPORT
#endif

static const char *error = NULL;

EXPORT int init(const char *str) {
  return 1;
}

EXPORT const char * getLastError() {
  return error;
}

EXPORT int eval(const char *func,
                              int nArgs,
                              const double **inReal,
                              const double **inImag,
                              int blockSize,
                              double *outReal,
                              double *outImag) {
  
  	double params[9] =  {0.191049066537241, -2.34359159400347, 11.5766999155046, -29.3520406103732,
     40.4479932778170, -30.2548599606873, 12.2576978613138, -0.170606365568724, -2.11971321961447};
    
    double density = 10470; // kg/m^3
    double log_T;
    double f_exp; 
	int i;
	double df_dlog_T, df_dT, dlog_T_dT;

  if (strcmp("CFUN_CvAg_v1_deriv", func) == 0) {
    if (nArgs != 1) {
      error = "One argument expected";
      return 0;
    }

    for (i = 0; i < blockSize; i++) {
		
	    double T = inReal[0][i];

		if(T < 0){
				error = "T is negative!"; return 0; 
			} 
		
		if(T > 1400){
				error = "Temperature is above 1400 K - out of range!"; 
				return 0;	  
			}
		if(T <= 1 && T >= 0){
			outReal[i] = 0;
		}

		if (T > 1 && T <= 300) {
		    log_T = log10(T);
			//Calculate the derivative of f_exp with respect to log_T
		    df_dlog_T = 8 * params[0] * pow(log_T, 7) + 7 * params[1] * pow(log_T, 6) + 6 * params[2] * pow(log_T, 5) + 5 * params[3] * pow(log_T, 4) + 4 * params[4] * pow(log_T, 3) + 3 * params[5] * pow(log_T, 2) + 2 * params[6] * log_T + params[7];
		    //Calculate the derivative of log_T with respect to T
			dlog_T_dT = 1.0 / (T * log(10));
			//Chain rule 1st part
		    df_dT = df_dlog_T * dlog_T_dT;

		    f_exp = params[0] * pow(log_T, 8) + params[1] * pow(log_T, 7) + params[2] * pow(log_T, 6) + params[3] * pow(log_T, 5) + params[4] * pow(log_T, 4) + params[5] * pow(log_T, 3) + params[6] * pow(log_T, 2) + params[7] * log_T + params[8];
		    outReal[i] = density * log(10) * pow(10, f_exp) * df_dT;
		}

		if(T > 300 ) {
			outReal[i] = 0;
		}
		
		if(outReal[i]!=outReal[i]){
			error = "Output is nan"; 
			return 0;	  
			}
		if (fabs(outReal[i])>DBL_MAX){
		error = "Output is inf"; 
		return 0;	  
		}
		}
		return 1;
  }

}