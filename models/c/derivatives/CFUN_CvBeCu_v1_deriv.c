#include <math.h>
#include <float.h>
#include <stdlib.h>
#include <string.h>
#ifdef _MSC_VER
#define EXPORT __declspec(dllexport)
#else
#define EXPORT
#endif

static const char *error = NULL;

EXPORT int init(const char *str) {
  return 1;
}

EXPORT const char * getLastError() {
  return error;
}

EXPORT int eval(const char *func,
                              int nArgs,
                              const double **inReal,
                              const double **inImag,
                              int blockSize,
                              double *outReal,
                              double *outImag) {
  
  	double params1[4] =  {-30.35, 4.2376, -0.0147, 1.77285978322629E-05};
    double params2[2] =  {346.5 , 0.1667};

    //linear approximation parameters
    double m = 1375;
    double n = 2878925;
    // BeCu density: 8.25-8.36 g/cm^3
    double density = 8300.0; // kg/m^3
    double log_T;
    double poly; 
    double CvBeCu;
	  int i;

  if (strcmp("CFUN_CvBeCu_v1_deriv", func) == 0) {
    if (nArgs != 1) {
      error = "One argument expected";
      return 0;
    }

    for (i = 0; i < blockSize; i++) {
		
	    double T = inReal[0][i];

      if(T < 0){
          error = "T is negative!"; return 0; 
        } 
      
      if(T > 1400){
          error = "Temperature is above 1400 K - out of range!"; 
          return 0;	  
        }
      if(T <= 10){
        //keep constant value
          CvBeCu = 0;
        }
          
      if(T > 10 && T <= 297) {

        poly = params1[1] + params1[2] * 2 * T + 3 * params1[3] * T*T;
        CvBeCu = density*poly;
      }

      if(T > 297 && T <= 345) {
        poly = params2[1];
        CvBeCu = density*poly;
      }

      if(T > 345){
        //linear approximation
          CvBeCu = m;
       }

      outReal[i] = CvBeCu;

      if(outReal[i]!=outReal[i]){
        error = "Output is nan"; 
        return 0;	  
      }
      if (fabs(outReal[i])>DBL_MAX){
        error = "Output is inf"; 
        return 0;	  
      }
	  }
		return 1;
  }

}

