#include <math.h>
#include <float.h>
#include <stdlib.h>
#include <string.h>
#ifdef _MSC_VER
#define EXPORT __declspec(dllexport)
#else
#define EXPORT
#endif

static const char *error = NULL;

EXPORT int init(const char *str) {
return 1;
}

EXPORT const char * getLastError() {
return error;
}

double kIn(double T) {
	double kIn;
    double logT;
    double dlogT_dT, dkIn_dT;
	double a = -6.0687972076932786e-05;
	double b = 0.0006060452148140404;
	double c = -0.0017081286086864747;
	double d = 2.291682473087e-07;
	double e = 2.136638975542853e-06;
	double f = 0.04211294520109297;
	double g = -0.1413157118208483;
	double h = 0.1787110642091033;
	double i = 1.20438393685077;
	double j = 47855.261199244625;
	double k = -2471.1581298451724;

    logT = log10(T);
	dlogT_dT = 1/(T*log(10));

    double f_exp = a*pow(logT,8) + b*pow(logT,7) + c*pow(logT,6) + pow(d,pow(logT,5)) + e*pow(logT,4) + f*pow(logT,3) + g*pow(logT,2) + h*logT + i; 

    double df_exp_dT = (8*a*pow(logT,7) + 7*b*pow(logT,6) + 6*c*pow(logT,5) + pow(d,pow(logT,5)) * log(d) * 5*pow(logT,4) + 4*e*pow(logT,3) + 3*f*pow(logT,2) + 2*g*logT + h)*dlogT_dT;

	kIn = k * pow(10, f_exp) + j; 
    dkIn_dT = k * log(10)* pow(10, f_exp)*df_exp_dT; 

	return dkIn_dT;
}


EXPORT int eval(const char *func,
                              int nArgs,
                              const double **inReal,
                              const double **inImag,
                              int blockSize,
                              double *outReal,
                              double *outImag) {

	int i;

	if (strcmp("CFUN_kIn_v0_deriv", func) == 0) {
		if (nArgs != 1) {
			error = "One argument expected";
			return 0;
			}

    for (i = 0; i < blockSize; i++) {
			
			double T = inReal[0][i];

			if(T < 15)     {
				error = "Temperature is below 15 K - out of range!"; 
                return 0; 
			} 
	
			if(T > 300){
				error = "Temperature is above 300 K - out of range!";
				return 0;	  
			}
	
			
			outReal[i] = kIn( T );
	
			/*Output consistency check*/
			if(outReal[i]!=outReal[i]){
				error = "Output is nan"; 
				return 0;	  
			}
			if (fabs(outReal[i])>DBL_MAX){
				error = "Output is inf"; 
				return 0;	  
			}
			}
	
    return 1;
}

/*else {
    error = "Unknown function";
    return 0;
}*/
}