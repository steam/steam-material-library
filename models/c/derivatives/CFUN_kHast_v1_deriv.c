#include <math.h>
#include <float.h>
#include <stdlib.h>
#include <string.h>
#ifdef _MSC_VER
#define EXPORT __declspec(dllexport)
#else
#define EXPORT
#endif

static const char *error = NULL;

EXPORT int init(const char *str) {
  return 1;
}

EXPORT const char * getLastError() {
  return error;
}

EXPORT int eval(const char *func,
                              int nArgs,
                              const double **inReal,
                              const double **inImag,
                              int blockSize,
                              double *outReal,
                              double *outImag) {
  
  	double a = -1.44774526e-15;
    double b = -3.69656787e-01;  
    double c =  3.70142581e+00;
    double d = -1.51395823e+01;
    double e =  3.26240850e+01;
    double f = -3.97002234e+01;
	double g =  2.66580222e+01;
	double h = -7.85571122e+00;
	double k =  5.03186469e-01;
	
	//linear regression T > 200 & T < 2.7
	double l[2] = {10.5211605, 0.2665405};
	double m[2] = {-13.5869, -0.0685};

    double log_T, df_exp_dT, derivative, dlogT_dT;
    double f_exp;
	int i;

  if (strcmp("CFUN_kHast_v1_deriv", func) == 0) {
    if (nArgs != 1) {
      error = "One argument expected";
      return 0;
    }

    for (i = 0; i < blockSize; i++) {
		
	  double T = inReal[0][i];
	  
	  
	  /*Input consistency check with hotfix for negative T*/
		if (T > 2.5 && T < 200) {
      log_T = log10(T);
      dlogT_dT = 1/(T * log(10));

    	f_exp = a * pow(log_T, 8) + b * pow(log_T, 7) + c * pow(log_T, 6) + d * pow(log_T, 5) +
                   e * pow(log_T, 4) + f * pow(log_T, 3) + g * pow(log_T, 2) + h * log_T + k;
      
      df_exp_dT = (a * 8.0 * pow(log_T, 7) + b * 7.0 * pow(log_T, 6) + c * 6.0 * pow(log_T, 5) +
             d * 5.0 * pow(log_T, 4) + e * 4.0 * pow(log_T, 3) + f * 3.0 * pow(log_T, 2) +
             g * 2.0 * log_T + h) * dlogT_dT;

      // Calculate the derivative of outReal
      derivative = pow(10, f_exp) * df_exp_dT * log(10);

    	// Assign the derivative to outReal[i]
    	outReal[i] = derivative;
	}
    
    if (T >= 200) {
        log_T = log10(T);
        dlogT_dT = 1/T;
        outReal[i] = l[0]*dlogT_dT/log(10);
    }
    
    if (T <= 2.5) {
        outReal[i] = l[1];
    }
    
	  }

	  /*Output consistency check*/
	  if(outReal[i]!=outReal[i]){
		error = "Output is nan"; 
		return 0;	  
	  }
	  if (fabs(outReal[i])>DBL_MAX){
		error = "Output is inf"; 
		return 0;	  
	  }
    }
    return 1;
  }

  /*else {
    error = "Unknown function";
    return 0;
  }*/
