#include <math.h>
#include <float.h>
#include <stdlib.h>
#include <string.h>
#ifdef _MSC_VER
#define EXPORT __declspec(dllexport)
#else
#define EXPORT
#endif

static const char *error = NULL;

EXPORT int init(const char *str) {
  return 1;
}

EXPORT const char * getLastError() {
  return error;
}

EXPORT int eval(const char *func,
                              int nArgs,
                              const double **inReal,
                              const double **inImag,
                              int blockSize,
                              double *outReal,
                              double *outImag) {
  
  int i;

  if (strcmp("CFUN_kCu_v1_deriv", func) == 0) {
    if (nArgs != 6) {
      error = "Six arguments expected";
      return 0;
    }

    for (i = 0; i < blockSize; i++) {
		
	  double T   	     = inReal[0][i];
	  double RRR 	     = inReal[1][i];
	  double rho_B0      = inReal[2][i];
	  double rho         = inReal[3][i];	
	  double drho_B0_dT  = inReal[4][i]; // derivative of rhoCu at 0 magnetic field calculated from CFUN_rhoCu_v1_deriv.c using B = 0.
	  double drho_dT     = inReal[4][i]; // derivative of rhoCu calculated from CFUN_rhoCu_v1_deriv.c  
	  
	  /*Input consistency check with hotfix for negative T*/
	  //	  if(T < 1.9)   {T=1.9;}
	  if (T < 0) {
	      error = "T is negative!";
	      return 0;
	  }
	  if (RRR < 0) {
	      error = "RRR is negative!";
	      return 0;
	  }
	  if (rho_B0 < 0) {
	      error = "rhoCu is negative!";
	      return 0;
	  }
	  if (rho < 0) {
	      error = "rhoCu is negative!";
	      return 0;
	  }
	  
	  double beta = 0.634 / RRR;
	  double beta_r = beta / 3e-4;
	  
	  double P1 = 1.754e-8;
	  double P2 = 2.763;
	  double P3 = 1102;
	  double P4 = -0.165;
	  double P5 = 70;
	  double P6 = 1.756;
	  double P7 = 0.838 / (pow(beta_r, 0.1661));
	  double dk_n_dT, dW_i,dW_i0, dW_0, deriv_denominator;
	  double W_0, W_i,  W_i0, denominator, k_n;

	  W_0 = beta / T;
	  dW_0 = - beta / pow(T,2);

	  denominator = (1 + P1 * P3 * pow(T, (P2 + P4)) * exp(-pow((P5 / T), P6)));
	  deriv_denominator = P1 * P3 * (P2 + P4) * pow(T, P2 + P4 - 1) * exp(-pow((P5 / T), P6)) + P1 * P3 * pow(T, (P2 + P4)) * (P6*pow(P5,P6)/pow(T,P6+1))* exp(-pow((P5 / T), P6));
	
	  W_i = P1 * pow(T, P2) / (1 + P1 * P3 * pow(T, (P2 + P4)) * exp(-pow((P5 / T), P6)));
	  dW_i = (P1 * P2 * pow(T, P2-1) *denominator -  P1 * pow(T, P2)*deriv_denominator)/(pow(denominator,2));

	  W_i0 = P7 * (W_i*W_0)/(W_i+W_0);
	  dW_i0 = P7 * W_0 * (W_0 * dW_i) / pow(W_i + W_0, 2) + P7 * W_i * (W_i * dW_0) / pow(W_i + W_0, 2);

	  k_n = 1/( W_0 + W_i + W_i0);
	  
	  dk_n_dT= -(dW_0 + dW_i + dW_i0) / pow(W_0 + W_i + W_i0, 2);
	  
	  // Derivative calculation
	  double derivative = k_n * (drho_B0_dT / rho) - k_n * drho_dT* (rho_B0 / pow(rho,2)) + dk_n_dT * (rho_B0 / rho);
	  
	  outReal[i] = derivative;
	  /*Output consistency check*/
	  if(outReal[i]!=outReal[i]){
		error = "Output is nan"; 
		return 0;	  
	  }
	  if (fabs(outReal[i])>DBL_MAX){
		error = "Output is inf"; 
		return 0;	  
	  }
    }
    return 1;
  }

  else {
    error = "Unknown function";
    return 0;
  }
}