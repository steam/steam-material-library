#include <math.h>
#include <float.h>
#include <stdlib.h>
#include <string.h>
#ifdef _MSC_VER
#define EXPORT __declspec(dllexport)
#else
#define EXPORT
#endif

static const char *error = NULL;

EXPORT int init(const char *str) {
return 1;
}

EXPORT const char * getLastError() {
return error;
}

// AL 5083 & 6061-T6
EXPORT int eval(const char *func,
                              int nArgs,
                              const double **inReal,
                              const double **inImag,
                              int blockSize,
                              double *outReal,
                              double *outImag) {
    double a = -1.44774526e-15;
    double b =  1.78431409e-12;
    double c =  8.35360982e-01;
    double d = -6.26469313e+00;
    double e =  1.57494558e+01;
    double f =  2.80976267e-01;
	double g =  9.25595833e-08;
	double h =  3.55289401e+03;

    double a1 = -4.4565240386873484e-010;
    double a2 =  5.4212798397466395e+002;
    double a3 = -1.2035245144233102e+002;
    double a4 =  1.6666051362179942e+001;

    double m[2] = {177.08330, 1.4830999999999999e+003};
    double n[2] = {2617.180842, 2848459.283410};

    double log_T;
    double f_exp;
	int k;
    double df_dlog_T, df_dT, dlog_T_dT;
    
	if (strcmp("CFUN_CvBrass_v1_deriv", func) == 0) {
		if (nArgs != 1) {
			error = "One argument expected";
			return 0;
			}

        for (k = 0; k < blockSize; k++) {
            
		    double T = inReal[0][k];
            
			if(T < 0)     {
				    error = "T is negative!"; return 0; 
			    } 
	
			if(T > 1400){
				    error = "Temperature is above 1400 K - out of range!"; 
				    return 0;	  
			    } 
            
            if(T >= 300) {
                outReal[k] = m[1];
                }
            
            if(T > 6 && T < 300) {
                log_T = log10(T);
                df_dlog_T = 5 * a * pow(log_T, 4) + 4 * b * pow(log_T, 3) + 3 * c * pow(log_T, 2) + 2 * d * log_T + e;
				//Calculate the derivative of log_T with respect to T
				dlog_T_dT = 1.0 / (T * log(10));
				//Chain rule 1st part
		    	df_dT = df_dlog_T * dlog_T_dT;

                f_exp = a*pow(log_T,5) + b*pow(log_T,4) + c*pow(log_T,3) + d*pow(log_T,2) + e*log_T + f;
                outReal[k] = g*pow(10,f_exp)*df_dT* log(10);
                }

            if(T <= 6) {
                outReal[k] = m[0];
                }
            
			
			/*Output consistency check*/
			if(outReal[k]!=outReal[k]){
				error = "Output is nan"; 
				return 0;	  
			}
			if (fabs(outReal[k])>DBL_MAX){
				error = "Output is inf"; 
				return 0;	  
			}
			}
	
    return 1;
}

else {
    error = "Unknown function";
    return 0;
}
}