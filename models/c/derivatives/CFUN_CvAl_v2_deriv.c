#include <math.h>
#include <float.h>
#include <stdlib.h>
#include <string.h>
#ifdef _MSC_VER
#define EXPORT __declspec(dllexport)
#else
#define EXPORT
#endif

static const char *error = NULL;

EXPORT int init(const char *str) {
return 1;
}

EXPORT const char * getLastError() {
return error;
}

//Aluminum 5083 (95% alu), 6061 (98% alu), 1350 (99.7% alu)
EXPORT int eval(const char *func,
                              int nArgs,
                              const double **inReal,
                              const double **inImag,
                              int blockSize,
                              double *outReal,
    						  double *outImag) {
    double a = 46.6467;
    double b = -314.292;
    double c = 866.662;
    double d = -1298.3;
    double e = 1162.27;
    double f = -637.795;
	double g = 210.351;
	double h = -38.3094;
	double i = 2.96344;

    double log_T;
    double f_exp; 
	int k;
	double df_dlog_T, df_dT, dlog_T_dT;


	//0.38475e3 or 0.5130e3
	
	if (strcmp("CFUN_CvAl_v2_deriv", func) == 0) {
		if (nArgs != 2) {
			error = "Two argument expected";
			return 0;
			}

        for (k = 0; k < blockSize; k++) {
			
		    double T = inReal[0][k];
            double density = inReal[1][k];
			

			if(T < 4)     {
				    error = "T is negative!"; return 0; 
			    } 
	
			if(T > 300){
				    error = "Temperature is above 1400 K - out of range!"; 
				    return 0;	  
			    } 
			
			
            if(T > 4 && T <= 300) {
				log_T = log10(T);

				df_dlog_T = b + 2 * c * log_T + 3 * d * pow(log_T, 2) + 4 * e * pow(log_T, 3) + 5 * f * pow(log_T, 4) + 6 * g * pow(log_T, 5) + 7 * h * pow(log_T, 6) + 8 * i * pow(log_T, 7);
				//Calculate the derivative of log_T with respect to T
				dlog_T_dT = 1.0 / (T * log(10));
				//Chain rule 1st part
		    	df_dT = df_dlog_T * dlog_T_dT;

                f_exp =  a + b*(log_T) + c*pow(log_T, 2) + d*pow(log_T,3) + e*pow(log_T,4) + f*pow(log_T,5) + g*pow(log_T,6) + h*pow(log_T,7) + i*pow(log_T,8) ;
                outReal[k] = density*pow(10,f_exp)*df_dT* log(10);
            } 
			
			
			/*Output consistency check*/
			if(outReal[k]!=outReal[k]){
				error = "Output is nan"; 
				return 0;	  
			}
			if (fabs(outReal[k])>DBL_MAX){
				error = "Output is inf"; 
				return 0;	  
			}
			}
	
    return 1;
}

}