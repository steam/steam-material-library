#include <math.h>
#include <float.h>
#include <stdlib.h>
#include <string.h>

#ifdef _MSC_VER
#define EXPORT __declspec(dllexport)
#else
#define EXPORT
#endif

static const char *error = NULL;

EXPORT int init(const char *str) {
    return 1;
}

EXPORT const char *getLastError() {
    return error;
}

double maximum(double a, double b) {
    if (a > b) {
        return a;
    }
    return b;
}

EXPORT int eval(const char *func,
                int nArgs,
                const double **inReal,
                const double **inImag,
                int blockSize,
                double *outReal,
                double *outImag) {
    
    double Tc0, Bc20, CJ, p, q;
    double exp_factor = 1.52;
    int i;
    double PI_4       = 3.14159265358979323846/4 ;
    double t, b, Jc_sc, Ic_sc;
    double Bc2, dJc_dT, dJc_db, dIc_dt;
    double minB = 1e-6;

    
  
    if (strcmp("CFUN_Jc_Nb3Sn_Bottura_v1_deriv", func) == 0) {
        if (nArgs != 9) {
            error = "Nine arguments expected";
            return 0;
        }

        for (i = 0; i < blockSize; i++) {
            // parameters 
            double T        = inReal[0][i];
            double B        = inReal[1][i];
            double Tc0      = inReal[2][i];
            double Bc20     = inReal[3][i];
            double CJ       = inReal[4][i];
            double p        = inReal[5][i];
            double q        = inReal[6][i];
            //Wire parameters
            double wireDiameter = inReal[7][i];
            double Cu_noCu      = inReal[8][i];

            double dt_dT, dJc_sc_dt, dBc2_dT, db_dT;

            double f_sc = 1 / (1 + Cu_noCu);

            t = T / Tc0;
            dt_dT = 1/ Tc0;
            B = maximum(minB, fabs(B));
            Bc2 = maximum(1e-9, Bc20* (1 - pow(t,exp_factor)));
            b = B / Bc2;
            
            if (t > 1) {
                t = 1;
                dt_dT = 0;
            }

            if(Bc2 ==1e-9){
                dBc2_dT = 0;
            }else{
                dBc2_dT = Bc20 * dt_dT * (-exp_factor * pow(t,exp_factor-1));
            }

            db_dT = - B / (pow(Bc2, 2)) * dBc2_dT;

            

            if (b > 1) {
                b = 1;
                db_dT = 0;
            }
            

            Jc_sc = CJ / B * pow(b, p) * (1 - t * t) * (1 - pow(t, exp_factor)) * pow((1 - b), q);

            // Derivative calculation
            dJc_dT = CJ /B * pow(b, p) * (-2 * t * (1 - pow(t, exp_factor))  + (1 - t * t) * (-exp_factor * pow(t, exp_factor - 1)) ) * pow((1 - b), q) * dt_dT + CJ / B * (1 - t * t) * (1 - pow(t, exp_factor)) *db_dT * (p* pow(b, p-1) * pow((1 - b), q) - q * pow(b, p) * pow((1 - b), q-1)); 

            if(T >= Tc0){
                Jc_sc = 0;
                dJc_dT = 0;
            }
            if(B >= Bc20){
                Jc_sc = 0;
                dJc_dT = 0;
            }
            if(Jc_sc < 0){
                Jc_sc = 0;
                dJc_dT = 0;
            }

            Ic_sc = Jc_sc * (PI_4 * wireDiameter * wireDiameter * f_sc);

            // Derivative of Ic_sc w.r.t T
            dIc_dt = dJc_dT * (PI_4 * wireDiameter * wireDiameter * f_sc);

            // Assign result to outReal[i]
            outReal[i] = dJc_dT;
            
        }
        return 1;
    }

    else {
      error = "Unknown function";
      return 0;
    }
}
