#include <math.h>
#include <float.h>
#include <stdlib.h>
#include <string.h>
#ifdef _MSC_VER
#define EXPORT __declspec(dllexport)
#else
#define EXPORT
#endif

#define EPS .000000001

static const char *error = NULL;

double maximum(double a, double b) {
    return (a > b) ? a : b;
}

double minimum(double a, double b) {
    return (a < b) ? a : b;
}

EXPORT int init(const char *str) {
  return 1;
}

EXPORT const char * getLastError() {
  return error;
}

EXPORT int eval(const char *func,
                              int nArgs,
                              const double **inReal,
                              const double **inImag,
                              int blockSize,
                              double *outReal,
                              double *outImag) {
								  
  double Tc0   = 16.0;             //[K]
  double Bc20  = 29.38;            //[T]
  double c0    = 276000*pow(10,6); //[-]
  double degr  = 0.03;             // [-]
  double p     = 1.52;             // [-]
  double alpha = 0.96;             // [-] 
  double tmin  = 1e-2;             // Min allowed T
  double Bmin  = 1e-2;             // Min allowed B
  
  double Bc2;
  double Jc;
  double ti;
  double Bi;
  double bi;
  double c;
  int i;
  double dti_dT, dc_dT, dJc_dT, dbi_dT, dBc2_dT;

  
  if (strcmp("CFUN_IcNb3Sn_v1_deriv", func) == 0) {
    if (nArgs != 3) {
      error = "Three arguments expected";
      return 0;
    }

    for (i = 0; i < blockSize; i++) {  	 
	
	  double T    = inReal[0][i];
	  double B    = inReal[1][i];
	  double Area = inReal[2][i];
	
	  /*Input consistency check*/
	  if(T < 0)     {error = "T is negative!"; return 0; } 
	  if(B < 0)     {error = "normB is negative!"; return 0; }
	  if(Area < 0)  {error = "Area is negative!"; return 0; }	
	  
    /* Mandatory constraint: 0.01 <= t <= 1 */
    ti=T/Tc0;
       dti_dT = 1/Tc0;
	   
	   if(ti>=1) {
		   Jc = 0;
       dJc_dT = 0;
	   }	   
	   else {
			   
		   ti=maximum(ti,tmin);
		   
       Bc2 = Bc20 * (1-pow(ti,p));

       if(ti > tmin){
        dBc2_dT = Bc20 * (-p*pow(ti,p-1)*dti_dT);
       }else{
        dBc2_dT = 0;
       }
		    

		   /* Mandatory constraint: 0.01 <= b <= 1 */
		   bi=B/Bc2;
		   bi=minimum(maximum(bi,Bmin),1);       
       if(bi > Bmin && bi < 1){
        dbi_dT = - B/pow(Bc2,2) * dBc2_dT;
       }else{
        dbi_dT = 0;
       }

		   /* Bordini's original fit: */
       c = (1-degr) * c0 * pow((1-pow(ti,p)),alpha) * pow((1-pow(ti,2)),alpha);

		   dc_dT = (1-degr) * c0 * alpha* ( pow((1-pow(ti,p)),alpha-1) *(-p *pow(ti,p-1)) * pow((1-pow(ti,2)),alpha) + pow((1-pow(ti,p)),alpha) * pow((1-pow(ti,2)),alpha -1) *(-2*ti)) * dti_dT;

		  dJc_dT = dc_dT / (Bc2*bi) * sqrt(bi)*pow((1-bi),2) - c / (pow(Bc2,2)*bi)*dBc2_dT * sqrt(bi)*pow((1-bi),2) - dbi_dT* c / (Bc2*pow(bi,2)) * sqrt(bi)*pow((1-bi),2) + c / (Bc2*bi) * 1/(2*sqrt(bi))*dbi_dT*pow((1-bi),2) + c / (Bc2*bi) * sqrt(bi)*(-dbi_dT)*2*(1-bi);	
  
	  }
    outReal[i] = dJc_dT*Area;
	   
	  /*Output consistency check*/
	  if(outReal[i]!=outReal[i]){
		error = "Output is nan"; 
		return 0;	  
	  }
	   
	}
   return 1;

  }

  else {
    error = "Unknown function";
    return 0;
  }
}
