#include <math.h>
#include <float.h>
#include <stdlib.h>
#include <string.h>

#ifdef _MSC_VER
#define EXPORT __declspec(dllexport)
#else
#define EXPORT
#endif

static const char *error = NULL;

EXPORT int init(const char *str) {
    return 1;
}

EXPORT const char *getLastError() {
    return error;
}


EXPORT int eval(const char *func,
                int nArgs,
                const double **inReal,
                const double **inImag,
                int blockSize,
                double *outReal,
                double *outImag) {

    double Tc, f_T_Tc, Ic_sc, Jc_sc, f_B_Bc20, f_sc, df_T_Tc_dT, dIc_dT, dJc_sc_dT;
    int i;
    double PI_4 = 3.14159265358979323846 / 4;

    if (strcmp("CFUN_Jc_NbTi_Cudi_fit1_v1_deriv", func) == 0) {
        if (nArgs != 8) {
            error = "Eight arguments expected";
            return 0;
        }

        for ( i = 0; i < blockSize; i++) {
            // unpack parameters
            double T = inReal[0][i];
            double B = inReal[1][i];
            double Tc0 = inReal[2][i];
            double Bc20 = inReal[3][i];
            double C1 = inReal[4][i];
            double C2 = inReal[5][i];
            double wireDiameter = inReal[6][i];
            double Cu_noCu = inReal[7][i];
            
            f_sc = 1 / (1 + Cu_noCu);

            // calculate absolute magnetic field
            B = fabs(B);

            // calculate critical temperature
            f_B_Bc20 = B / Bc20;

            if (f_B_Bc20 > 1) {
                f_B_Bc20 = 1;
            }
            Tc = Tc0 * pow((1 - f_B_Bc20), 0.59);

            // calculate critical current
            f_T_Tc = T / Tc;
            df_T_Tc_dT = 1 / Tc;

            if (f_T_Tc > 1) {
                f_T_Tc = 1;
                df_T_Tc_dT = 0;
            }
            Ic_sc = (C1 + C2 * B) * (1 - f_T_Tc);
            dIc_dT = (C1 + C2 * B) * (- df_T_Tc_dT);

            // set critical current to 0 if T >= Tc0
            //if (T >= Tc0) {
            //    Ic_sc = 0;
            //}

            // set critical current to 0 if B >= Bc20
            //if (B >= Bc20) {
            //    Ic_sc = 0;
            //}

            // set critical current to 0 if negative
            if (Ic_sc < 0) {
                Ic_sc = 0;
                dIc_dT = 0;
            }

            // calculate critical current density
            //Jc_sc = Ic_sc / (PI_4 * pow(wireDiameter, 2) * f_sc);
            dJc_sc_dT = dIc_dT / (PI_4 * pow(wireDiameter, 2) * f_sc);

            outReal[i] = dJc_sc_dT; // Ic_sc
        }

        return 1;
    }

    else {
        error = "Unknown function";
        return 0;
    }
}
