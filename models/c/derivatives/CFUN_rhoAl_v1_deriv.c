#include <math.h>
#include <float.h>
#include <stdlib.h>
#include <string.h>
#ifdef _MSC_VER
#define EXPORT __declspec(dllexport)
#else
#define EXPORT
#endif

static const char *error = NULL;

EXPORT int init(const char *str) {
  return 1;
}

EXPORT const char * getLastError() {
  return error;
}

EXPORT int eval(const char *func,
                              int nArgs,
                              const double **inReal,
                              const double **inImag,
                              int blockSize,
                              double *outReal,
                              double *outImag) {
   
    double d_rhoAl_dT;
    double x;
    double dres2, dres3, dres;
	int i;

  if (strcmp("CFUN_rhoAl_v1_deriv", func) == 0) {
    if (nArgs != 2) {
      error = "Two argument expected";
      return 0;
    }

    for (i = 0; i < blockSize; i++) {
		
	    double T   = inReal[0][i];
      double RRR = inReal[1][i];

      if(T < 0){
          error = "T is negative!"; 
          d_rhoAl_dT = 0;
          return 0;
        } 

      if(T > 300){
          error = "Temperature is above 1400 K - outside of the range!";
          d_rhoAl_dT = 0;
          return 0;	  
        }

      if(T > 0 && T <= 300) {
        x = 1.0 / RRR;

        dres2 = (1.034377E-07 * pow(x, 3) - 3.454618E-09 * pow(x, 2) + 3.312862E-08 * x - 5.895681E-09);
        dres3 = (2.275988E-04 * pow(x, 6) - 8.699217E-04 * pow(x, 5) + 1.382615E-03 * pow(x, 4) - 1.167455E-03 * pow(x, 3) + 5.519664E-04 * pow(x, 2) - 1.383929E-04 * x + 1.437485E-05);

        dres = (RRR < 2.1147) * dres3 + (RRR >= 2.1147) * dres2;

        d_rhoAl_dT = -1.295161E-15 *3* pow(T, 2) + 7.531087E-13 * 2*T - 1.385185E-11;
        }
  
      outReal[i] = d_rhoAl_dT;

      if(outReal[i]!=outReal[i]){
        error = "Output is nan"; 
        return 0;	  
      }
      if (fabs(outReal[i])>DBL_MAX){
        error = "Output is inf"; 
        return 0;	  
      }
	  }
		return 1;
  }

}