#include <math.h>
#include <float.h>
#include <stdlib.h>
#include <string.h>
#ifdef _MSC_VER
#define EXPORT __declspec(dllexport)
#else
#define EXPORT
#endif

static const char *error = NULL;

EXPORT int init(const char *str) {
  return 1;
}

EXPORT const char * getLastError() {
  return error;
}

EXPORT int eval(const char *func,
                              int nArgs,
                              const double **inReal,
                              const double **inImag,
                              int blockSize,
                              double *outReal,
                              double *outImag) {
  
  	double params1[8] = {-0.576099035551066, 6.118441661578747, -26.439181574747003, 59.954465708087419, -77.256966475288834, 56.803684410219617, -19.144696871373529, 0.552095810764055};
    //double params2[2] =  {0.4493506493507, 403.8787878787871};
    double params2[2] = { 0.2517, 459.947856};
    
    double density = 4540; // [kg/m^3]
    double log_T;
    double poly; 
    double CvTi;
    double df_dlog_T, df_dT, dlog_T_dT;

	int i;

  if (strcmp("CFUN_CvTi_v1_deriv", func) == 0) {
    if (nArgs != 1) {
      error = "One argument expected";
      return 0;
    }

    for (i = 0; i < blockSize; i++) {
		
	    double T = inReal[0][i];

      if(T < 0){
          error = "T is negative!"; return 0; 
        } 
      
      if(T > 1400){
          error = "Temperature is above 1400 K - out of range!"; 
          return 0;	  
        }
      if(T >=0 && T <= 4){
        CvTi = 0.0;
        }
          
      if(T > 4 && T <= 300) {
        log_T =log10(T);
        df_dlog_T = 7 * params1[0] * pow(log_T, 6) + 6 * params1[1] * pow(log_T, 5) + 5 * params1[2] * pow(log_T, 4) + 4 * params1[3] * pow(log_T, 3) + 3 * params1[4] * pow(log_T, 2) + 2 * params1[5] * log_T + params1[6];
        //Calculate the derivative of log_T with respect to T
			  dlog_T_dT = 1.0 / (T * log(10));
			  //Chain rule 1st part
		    df_dT = df_dlog_T * dlog_T_dT;
        poly = params1[0] * pow(log_T, 7) + params1[1] * pow(log_T, 6) + params1[2] * pow(log_T, 5) + params1[3] * pow(log_T, 4) + params1[4] * pow(log_T, 3) + params1[5] * pow(log_T, 2) + params1[6] * log_T+ params1[7];
        CvTi = pow(10,poly)* df_dT* log(10);
        }

      if(T > 300){
        CvTi = params2[0];
       }

      outReal[i] = density * CvTi;

      if(outReal[i]!=outReal[i]){
        error = "Output is nan"; 
        return 0;	  
      }
      if (fabs(outReal[i])>DBL_MAX){
        error = "Output is inf"; 
        return 0;	  
      }
	  }
		return 1;
  }

}

