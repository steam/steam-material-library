#include <math.h>
#include <float.h>
#include <stdlib.h>
#include <string.h>
#ifdef _MSC_VER
#define EXPORT __declspec(dllexport)
#else
#define EXPORT
#endif

static const char *error = NULL;

EXPORT int init(const char *str) {
  return 1;
}

EXPORT const char * getLastError() {
  return error;
}

EXPORT int eval(const char *func,
                              int nArgs,
                              const double **inReal,
                              const double **inImag,
                              int blockSize,
                              double *outReal,
                              double *outImag) {
  int i;
  double a = 5.73101;
  double b = -39.5199;
  double c = 79.9313;
  double d = -83.8572;
  double e = 50.9157;
  double f = -17.9835;
  double g = 3.42413;
  double h = -0.27133;

  double m = 1.6367e-04;
  double n = 0.142771;
  double log_T; 
  double f_exp, derivative; 
  
  if (strcmp("CFUN_kKapton_v1_deriv", func) == 0) {
    if (nArgs != 1) {
      error = "One argument expected";
      return 0;
    }

    for (i = 0; i < blockSize; i++) {
	  double T = inReal[0][i];
	  
	  /*Input consistency check with hotfix for negative T*/
	  if(T < 1.9)   {T=1.9; outReal[i] =0;}
	  if(T < 0)     {error = "T is negative!"; return 0; } 
	  
	  if(T < 4.3) {
		outReal[i] = 1.61*1e-3;
	  }

	  else if (T > 0 && T <= 300) {
		log_T = log10(T);
		f_exp = (a + b*log_T + c*pow(log_T,2) + d*pow(log_T,3) + e*pow(log_T,4) + f*pow(log_T,5) + g*pow(log_T,6) + h*pow(log_T,7));

        derivative = (7 * h * pow(log_T, 6) + 6 * g * pow(log_T, 5)
                        + 5 * f * pow(log_T, 4) + 4 * e * pow(log_T, 3) + 3 * d * pow(log_T, 2)
                        + 2 * c * log_T + b) / (T * log(10));
        if(T==0){
		         T = 0.001;
                 derivative = 0;
	        }
		outReal[i] = derivative * log(10) *pow(10,f_exp);  
	  }
    
    if(T > 300){
      //linear approximation
      outReal[i] = m;
    }
	  
	  /*Output consistency check*/
	  if(outReal[i]!=outReal[i]){
		error = "Output is nan"; 
		return 0;	  
	  }
	  if (fabs(outReal[i])>DBL_MAX){
		error = "Output is inf"; 
		return 0;	  
	  }	  
    }
    return 1;
  }

  else {
    error = "Unknown function";
    return 0;
  }
}