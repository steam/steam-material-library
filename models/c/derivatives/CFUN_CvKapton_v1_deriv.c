#include <math.h>
#include <float.h>
#include <stdlib.h>
#include <string.h>
#ifdef _MSC_VER
#define EXPORT __declspec(dllexport)
#else
#define EXPORT
#endif

static const char *error = NULL;

EXPORT int init(const char *str) {
  return 1;
}

EXPORT const char * getLastError() {
  return error;
}

EXPORT int eval(const char *func,
                              int nArgs,
                              const double **inReal,
                              const double **inImag,
                              int blockSize,
                              double *outReal,
                              double *outImag) {
  int i;
  double a = -1.3684;
  double b = 0.65892;
  double c = 2.8719;
  double d = 0.42651;
  double e = -3.0088;
  double f = 1.9558;
  double g = -0.51998;
  double h = 0.051574;

  double log_T;
  double log10_300 = log10(300);
  double f_exp, df_exp_dT; 
  double Cp_lowT = 0;
  double dlog_T_dT;

  //linear approximation parameters
  double m = 1.6364e+03;
  double n = 1420*pow(10,(a + b*log10_300 + c*pow(log10_300,2) + d*pow(log10_300,3) + e*pow(log10_300,4) + f*pow(log10_300,5) + g*pow(log10_300,6) + h*pow(log10_300,7))) + Cp_lowT - m*300;

  
  
  if (strcmp("CFUN_CvKapton_v1_deriv", func) == 0) {
    if (nArgs != 1) {
      error = "One argument expected";
      return 0;
    }

    for (i = 0; i < blockSize; i++) {
	  double T = inReal[0][i];
	  
	  /*Input consistency check */
	  //Patch for NegT
	  if(T < 1.9)   {T=1.9; outReal[i] =0;}
	  if(T < 0)     {error = "T is negative!"; return 0; } 
	  
	  
	  //Cp_lowT = 1000 / (pow((T+1),6) *  log10(T+1));

	  if(T >= 0 && T < 300){
      log_T = log10(T);
      dlog_T_dT = 1/(T);

      f_exp = (a + b*log_T + c*pow(log_T,2) + d*pow(log_T,3) + e*pow(log_T,4) + f*pow(log_T,5) + g*pow(log_T,6) + h*pow(log_T,7));	  	    	
      df_exp_dT = (b + 2*c*log_T + 3*d*pow(log_T,2) + 4*e*pow(log_T,3) + 5*f*pow(log_T,4) + 6*g*pow(log_T,5) + 7*h*pow(log_T,6))*dlog_T_dT;
        
      outReal[i] = 1420*pow(10,f_exp)*df_exp_dT;
    }  
	  
    if(T >= 300){
      outReal[i] = m;
    }
	  /*Output consistency check*/
	  if(outReal[i]!=outReal[i]){
		error = "Output is nan"; 
		return 0;	  
	  }
	  if (fabs(outReal[i])>DBL_MAX){
		error = "Output is inf"; 
		return 0;	  
	  }
	
	}
    return 1;
  }

  else {
    error = "Unknown function";
    return 0;
  }
}