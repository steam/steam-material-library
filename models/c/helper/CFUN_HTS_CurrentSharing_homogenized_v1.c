#include <math.h>
#include <float.h>
#include <stdlib.h>
#include <string.h>
#ifdef _MSC_VER
#define EXPORT __declspec(dllexport)
#else
#define EXPORT
#endif

static const char *error = NULL;

EXPORT int init(const char *str) {
  return 1;
}

EXPORT const char * getLastError() {
  return error;
}

EXPORT int eval(const char *func,
                              int nArgs,
                              const double **inReal,
                              const double **inImag,
                              int blockSize,
                              double *outReal,
                              double *outImag) {
  
// Global index
int i; 
int iter_i; 

// Binary search algorithm
double alpha;
double lambda;
double lambda_MIN; 
double lambda_MAX;
double fun;
double err; 
double ABS_TOL = 1e-5;  
int    NO_OF_MAX_ITER = 20;  

// Consistency check: header
if (strcmp("CFUN_HTS_CurrentSharing_homogenized_v1", func) != 0) {
	error = "Unknown function";
	return 0;
	} 
// if (nArgs != 1) {
//	error = "one argument expected";
//    return 0;
//    }
	
 if (nArgs != 7) {
	error = "seven arguments expected";
    return 0;
    }
 
// Parfor loop
for (i = 0; i < blockSize; i++) {
	
	
	double J_hom_cc = inReal[0][i]; // Current density in the homogenized coated conductor
	double Ec    = inReal[1][i]; // 1e-4 [V/m]
	double Jc    = inReal[2][i]; // from the Ic fit
	double n     = inReal[3][i]; // Exponent of the power law
	double rhoNc = inReal[4][i]; // Normal conducting part of the tape as f(T,B)
	double hSc   = inReal[5][i]; // Thickness of HTS layer (1um)
	double hNc   = inReal[6][i]; // Thickness of the remaining layers (d_tape-1um)
	
	// Consistency check: arguments
	if(Jc    <  0){error = "Jc    is <  0!"; return 0; }
	if(Ec    <= 0){error = "Ec    is <= 0!"; return 0; }
	if(n     <  0){error = "n     is <  0!"; return 0; }
	if(hSc   <= 0){error = "hSc   is <= 0!"; return 0; }
	if(hNc   <= 0){error = "hNc   is <= 0!"; return 0; }
	if(rhoNc <= 0){error = "rhoNc is <= 0!"; return 0; }

	// Quench
	if (Jc==0){
		lambda = 0;
		}		
	// Current Sharing	
	else {
		// Binary search algorithm 
		lambda_MIN = 0; 	
		lambda_MAX = 1;	
		alpha      = (hNc/rhoNc)*((1/hSc)*(Ec/Jc)*pow((fabs(J_hom_cc)/Jc * (hNc + hSc)/hSc),(n-1)));
	
		// Inner iterations
		for (iter_i = 0; iter_i < NO_OF_MAX_ITER; iter_i++) {
	
			lambda = (lambda_MIN+lambda_MAX)*0.5;
			fun    = alpha*pow(lambda,n)+lambda-1;
			err    = lambda_MAX-lambda_MIN;
	
	
			if (err>ABS_TOL){ // Update boundaries
				if (fun>=0){ // too much current in HTS
					lambda_MAX = lambda;
					}				
				else if (fun<0){ // not enough current in HTS
					lambda_MIN = lambda;
					}
				}
			else{ // Converged
				break;	
				}			
			}
	
	    // Consistency check: number of iterations	
		if (iter_i==NO_OF_MAX_ITER){
			error = "Maximum number of bisection iterations reached"; 
			return 0;	
			}
		}
	
	outReal[i] = lambda;
	
	//Consistency check: output
	if(outReal[i]!=outReal[i]){
		error = "Output is nan"; 
		return 0;	  
		}
	
	if (fabs(outReal[i])>DBL_MAX){
		error = "Output is inf"; 
		return 0;	  
		}
	
}
	
return 1;
}