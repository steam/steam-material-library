#include <math.h>
#include <float.h>
#include <stdlib.h>
#include <string.h>
#ifdef _MSC_VER
#define EXPORT __declspec(dllexport)
#else
#define EXPORT
#endif

static const char *error = NULL;

EXPORT int init(const char *str) {
  return 1;
}

EXPORT const char * getLastError() {
  return error;
}

EXPORT int eval(const char *func,
                              int nArgs,
                              const double **inReal,
                              const double **inImag,
                              int blockSize,
                              double *outReal,
                              double *outImag) {
  
// Global index
int i; 
int iter_i; 

// Binary search algorithm
double alpha;
double lambda;
double lambda_MIN; 
double lambda_MAX;
double fun;
double err; 
double ABS_TOL = 1e-5;  
int    NO_OF_MAX_ITER = 20;  

// Consistency check: header
if (strcmp("CFUN_HTS_CurrentSharing_fK_v1", func) != 0) {
	error = "Unknown function";
	return 0;
	} 
// if (nArgs != 1) {
//	error = "one argument expected";
//    return 0;
//    }
	
 if (nArgs != 5) {
	error = "five arguments expected";
    return 0;
    }
 
// Parfor loop
for (i = 0; i < blockSize; i++) {
	
	double Kz        = inReal[0][i]; // Equivalent surface current density in the tape
	double Ec        = inReal[1][i]; // 1e-4 [V/m]
	double Kc        = inReal[2][i]; // from the Ic fit
	double n         = inReal[3][i]; // 20
	double rhoSurfC  = inReal[4][i]; // Equivalent surface resistivity of the normal conducting layer

	double rhoSurfS;	
	
	// SKIP FOR SPEED-UP
	// // Consistency check: arguments
	// if(Kc       <  0){error = "Kc    is <  0!"; return 0; }
	// if(Ec       <= 0){error = "Ec    is <= 0!"; return 0; }
	// if(n        <  0){error = "n     is <  0!"; return 0; }
	// if(rhoSurfC <= 0){error = "rhoSurfC   is <= 0!"; return 0; }


	// Quench
	if (Kc==0){
		lambda = 0;
		}		
	// Current Sharing Regime	
	else {
		// Binary search algorithm 
		lambda_MIN = 0; 	
		lambda_MAX = 1;	

		rhoSurfS = (Ec/Kc)*pow((fabs(Kz)/Kc),(n-1));				
		alpha    = rhoSurfS/rhoSurfC;
	
		// Inner iterations
		for (iter_i = 0; iter_i < NO_OF_MAX_ITER; iter_i++) {
	
			lambda = (lambda_MIN+lambda_MAX)*0.5;
			fun    = alpha*pow(lambda,n)+lambda-1;
			err    = lambda_MAX-lambda_MIN;
	
	
			if (err>ABS_TOL){     // Update boundaries
				if (fun>=0){      // too much current in HTS
					lambda_MAX = lambda;
					}				
				else if (fun<0){  // not enough current in HTS
					lambda_MIN = lambda;
					}
				}
			else{ // Converged
				break;	
				}			
			}
	
	    // Consistency check: number of iterations	
		if (iter_i==NO_OF_MAX_ITER){
			error = "Maximum number of bisection iterations reached"; 
			return 0;	
			}
		}
	
	outReal[i] = lambda;
	
	//Consistency check: output
	if(outReal[i]!=outReal[i]){
		error = "Output is nan"; 
		return 0;	  
		}
	
	if (fabs(outReal[i])>DBL_MAX){
		error = "Output is inf"; 
		return 0;	  
		}
	
}
	
return 1;
}
	
