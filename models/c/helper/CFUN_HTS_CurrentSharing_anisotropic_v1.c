#include <math.h>
#include <float.h>
#include <stdlib.h>
#include <string.h>
#ifdef _MSC_VER
#define EXPORT __declspec(dllexport)
#else
#define EXPORT
#endif

static const char *error = NULL;

EXPORT int init(const char *str) {
  return 1;
}

EXPORT const char * getLastError() {
  return error;
}

EXPORT int eval(const char *func,
                              int nArgs,
                              const double **inReal,
                              const double **inImag,
                              int blockSize,
                              double *outReal,
                              double *outImag) {
  
// Global index
int i; 
int iter_i; 

// Binary search algorithm
double alpha;
double lambda;
double lambda_MIN; 
double lambda_MAX;
double fun;
double err; 

// Consistency check: header
if (strcmp("CFUN_HTS_CurrentSharing_anisotropic_v1", func) != 0) {
	error = "Unknown function";
	return 0;
	} 
// if (nArgs != 1) {
//	error = "one argument expected";
//    return 0;
//    }
	
 if (nArgs != 12) {
	error = "twelve arguments expected";
    return 0;
    }
 
// Parfor loop
for (i = 0; i < blockSize; i++) {
	
	
	double J_hom_cc_r = inReal[0][i]; // Current density in the homogenized coated conductor, radial component
	double J_hom_cc_phi = inReal[1][i]; // Current density in the homogenized coated conductor, azimuthal component
	double J_hom_cc_z = inReal[2][i]; // Current density in the homogenized coated conductor, axial component

	double Ec    = inReal[3][i]; // 1e-4 [V/m]
	double Jc    = inReal[4][i]; // from the Ic fit
	double n     = inReal[5][i]; // Exponent of the power law
	double rhoNc = inReal[6][i]; // Normal conducting part of the tape as f(T,B)
	double hSc   = inReal[7][i]; // Thickness of HTS layer (1um)
	double hNc   = inReal[8][i]; // Thickness of the remaining layers (d_tape-1um)
	double scalingTSA   = inReal[9][i]; // Thickness of the remaining layers (d_tape-1um)
	double NO_OF_MAX_ITER = inReal[10][i]; 
	double ABS_TOL = inReal[11][i];


	// Consistency check: arguments
	if(Jc    <  0){error = "Jc    is <  0!"; return 0; }
	if(Ec    <= 0){error = "Ec    is <= 0!"; return 0; }
	if(n     <  0){error = "n     is <  0!"; return 0; }
	if(hSc   <= 0){error = "hSc   is <= 0!"; return 0; }
	if(hNc   <= 0){error = "hNc   is <= 0!"; return 0; }
	if(rhoNc <= 0){error = "rhoNc is <= 0!"; return 0; }

	// Quench
	if (Jc==0){
		lambda = 0;
		}		
	// Current Sharing	
	else {
		// Binary search algorithm 
		lambda_MIN = 0; 	
		lambda_MAX = 1;	
        double fSC = hSc/(hNc + hSc);
		alpha      = (hNc/rhoNc)*((1/hSc)*(Ec/Jc));
	
		// Inner iterations
		for (iter_i = 0; iter_i < NO_OF_MAX_ITER; iter_i++) {
	
			lambda = (lambda_MIN+lambda_MAX)*0.5;
			fun    = alpha*lambda*pow(sqrt(pow(J_hom_cc_r, 2) + pow(1.0/scalingTSA * J_hom_cc_phi * lambda/fSC, 2) + pow(1.0/scalingTSA * J_hom_cc_z * lambda/fSC, 2))/Jc, n - 1)+lambda-1;

			err    = lambda_MAX-lambda_MIN;
	
	
			if (err>ABS_TOL){ // Update boundaries
				if (fun>=0){ // too much current in HTS
					lambda_MAX = lambda;
					}				
				else if (fun<0){ // not enough current in HTS
					lambda_MIN = lambda;
					}
				}
			else{ // Converged
				break;	
				}			
			}
	
	    // Consistency check: number of iterations	
		if (iter_i==NO_OF_MAX_ITER){
			error = "Maximum number of bisection iterations reached"; 
			return 0;	
			}
		}
	
	outReal[i] = lambda;
	
	//Consistency check: output
	if(outReal[i]!=outReal[i]){
		error = "Output is nan"; 
		return 0;	  
		}
	
	if (fabs(outReal[i])>DBL_MAX){
		error = "Output is inf"; 
		return 0;	  
		}
	
}
	
return 1;
}