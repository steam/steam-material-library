#include <math.h>
#include <float.h>
#include <stdlib.h>
#include <string.h>
#ifdef _MSC_VER
#define EXPORT __declspec(dllexport)
#else
#define EXPORT
#endif

static const char *error = NULL;

EXPORT int init(const char *str) {
  return 1;
}

EXPORT const char * getLastError() {
  return error;
}

EXPORT const char* return_test_yaml() {
    return "CFUN_kAlAlloy_v1";
}

EXPORT int eval(const char *func,
                              int nArgs,
                              const double **inReal,
                              const double **inImag,
                              int blockSize,
                              double *outReal,
                              double *outImag) {
   
    double kAl;
    double x;
    double dres2, dres3, dres;
	int i;
    double L = 2.45e-8; // W*Ohm/K^2 Lorentz constant

  if (strcmp("CFUN_kAlAlloy_v1", func) == 0) {
    if (nArgs != 2) {
      error = "Two argument expected";
      return 0;
    }

    for (i = 0; i < blockSize; i++) {
		
	    double T   = inReal[0][i];
      double RRR = inReal[1][i];

      if(T < 0){
          error = "T is negative!"; return 0; 
        } 

      if(T > 300){
          error = "Temperature is above 300 K - outside of the range!";
          return 0;	  
        }

      if(T > 0 && T <= 300) {
        x = 1.0 / RRR;

        dres2 = (1.034377E-07 * pow(x, 3) - 3.454618E-09 * pow(x, 2) + 3.312862E-08 * x - 5.895681E-09);
        dres3 = (2.275988E-04 * pow(x, 6) - 8.699217E-04 * pow(x, 5) + 1.382615E-03 * pow(x, 4) - 1.167455E-03 * pow(x, 3) + 5.519664E-04 * pow(x, 2) - 1.383929E-04 * x + 1.437485E-05);

        dres = (RRR < 2.1147) * dres3 + (RRR >= 2.1147) * dres2;

        kAl = -1.295161E-15 * pow(T, 3) + 7.531087E-13 * pow(T, 2) - 1.385185E-11 * T + 5.791351E-09 + dres;
        }
  
      outReal[i] =  L*T / kAl;

      if(outReal[i]!=outReal[i]){
        error = "Output is nan"; 
        return 0;	  
      }
      if (fabs(outReal[i])>DBL_MAX){
        error = "Output is inf"; 
        return 0;	  
      }
	  }
		return 1;
  }

}