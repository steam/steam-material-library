#include <math.h>
#include <float.h>
#include <stdlib.h>
#include <string.h>
#ifdef _MSC_VER
#define EXPORT __declspec(dllexport)
#else
#define EXPORT
#endif

static const char *error = NULL;

EXPORT int init(const char *str) {
  return 1;
}

EXPORT const char * getLastError() {
  return error;
}

EXPORT const char* return_test_yaml() {
    return "CFUN_kCu_v1";
}

EXPORT int eval(const char *func,
                              int nArgs,
                              const double **inReal,
                              const double **inImag,
                              int blockSize,
                              double *outReal,
                              double *outImag) {
  
  int i;
  long double P1, P2, P3, P4, P5, P6, P7, P, W_0, W_i, W_i0, k_n;
  long double beta, beta_r;

  if (strcmp("CFUN_kCu_v1", func) == 0) {
    if (nArgs != 4) {
      error = "Four arguments expected";
      return 0;
    }

    for (i = 0; i < blockSize; i++) {
		
	  double T   	= inReal[0][i];
	  double RRR 	= inReal[1][i];
	  double rho_B0 = inReal[2][i];
	  double rho    = inReal[3][i];	  
	  
	  /*Input consistency check with hotfix for negative T*/
	  //	  if(T < 1.9)   {T=1.9;}
	  //if(T < 0)     {error = "T is negative!"; return 0; } 
	  //if(RRR < 0)   {error = "RRR is negative!"; return 0; } 
	  //if(rho_B0 < 0)   {error = "rhoCu is negative!"; return 0; }
	  //if(rho < 0)   {error = "rhoCu is negative!"; return 0; }	  
	  
	  beta = 0.634/ RRR;
	  beta_r = beta/0.0003;
	  	  
	   P1 =  1.754e-8;
	   P2 =  2.763;
	   P3 =  1102;
	   P4 = -0.165;
	   P5 =  70;
	   P6 =  1.756;
	   P7 =  0.838/(pow(beta_r,0.1661));
	
	   W_0 = beta / T;
	   W_i = P1*pow(T,P2)/(1+P1*P3*pow(T,(P2+P4))*exp(-pow((P5/T),P6)));
       W_i0 = P7 * (W_i*W_0)/(W_i+W_0);
	
	   k_n = 1/( W_0 + W_i + W_i0);
		
	  outReal[i] = k_n * (rho_B0 / rho);
	  
	  /*Output consistency check*/
	  if(outReal[i]!=outReal[i]){
		error = "Output is nan"; 
		return 0;	  
	  }
	  if (fabs(outReal[i])>DBL_MAX){
		error = "Output is inf"; 
		return 0;	  
	  }
    }
    return 1;
  }

  else {
    error = "Unknown function";
    return 0;
  }
}