#include <math.h>
#include <float.h>
#include <stdlib.h>
#include <string.h>
#ifdef _MSC_VER
#define EXPORT __declspec(dllexport)
#else
#define EXPORT
#endif

static const char *error = NULL;

EXPORT int init(const char *str) {
  return 1;
}

EXPORT const char * getLastError() {
  return error;
}

EXPORT const char* return_test_yaml() {
    return "CFUN_kBrass_v1";
}

EXPORT int eval(const char *func,
                              int nArgs,
                              const double **inReal,
                              const double **inImag,
                              int blockSize,
                              double *outReal,
                              double *outImag) {
  
  	double a =  7.12047624e-14;
    double b = -2.12404631e-11;  
    double c =  4.10801471e-09;
    double d = -5.13648945e-07;
    double e =  3.98269360e-05;
    double f = -1.71995629e-03;
    double g =  2.89759779e-02;
    double h =  8.51105299e-01;
    double s =  2.53106884e-01;
    double j = -1.49466938e-16;
    double k =  1.78424277e-19;
    double l = -9.23319109e-23;


    double r = 0;
    double m = 0.16981;

    double log_T;
    double f_exp; 
	int i;

  if (strcmp("CFUN_kBrass_v1", func) == 0) {
    if (nArgs != 1) {
      error = "One argument expected";
      return 0;
    }

    for (i = 0; i < blockSize; i++) {
		
	    double T = inReal[0][i];
	    

		if(T < 0){
			error = "T is negative!"; return 0; 
		} 
		
		if(T > 1400){
			error = "Temperature is above 1400 K - out of range!";
			return 0;	  
		}
				
		if(T > 0 && T < 298) {
			outReal[i] = l*pow(T, 11) + k*pow(T, 10) + j*pow(T, 9)+ a*pow(T, 8) + b*pow(T,7) + c*pow(T,6) + d*pow(T,5) + e*pow(T,4) + f*pow(T,3) + g*pow(T,2) + h*T + s;
		}

        if(T >= 298) {
            double Temp = 298;
			      r = l*pow(Temp, 11) + k*pow(Temp, 10) + j*pow(Temp, 9)+ a*pow(Temp, 8) + b*pow(Temp,7) + c*pow(Temp,6) + d*pow(Temp,5) + e*pow(Temp,4) + f*pow(Temp,3) + g*pow(Temp,2) + h*Temp + s - m*298;
            outReal[i] = m*T + r;
        }

		
		/*Output consistency check*/
		if(outReal[i]!=outReal[i]){
			error = "Output is nan"; 
			return 0;	  
			}
		if (fabs(outReal[i])>DBL_MAX){
		error = "Output is inf"; 
		return 0;	  
		}
		}
		return 1;
  }

}