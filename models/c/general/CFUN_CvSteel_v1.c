#include <math.h>
#include <float.h>
#include <stdlib.h>
#include <string.h>
#ifdef _MSC_VER
#define EXPORT __declspec(dllexport)
#else
#define EXPORT
#endif

static const char *error = NULL;

EXPORT int init(const char *str) {
  return 1;
}

EXPORT const char * getLastError() {
  return error;
}

EXPORT const char* return_test_yaml() {
    return "CFUN_CvSteel_v1";
}

double CpSS(double T) {
    double a = -1.447745258023E-15; // 1/K^8
    double b = 1.784314094610E-12;  // 1/K^7
    double c = -8.919290916541E-10; // 1/K^6
    double d = 2.291682473087E-07;  // 1/K^5
    double e = -3.105459645700E-05; // 1/K^4
    double f = 1.892366229382E-03;  // 1/K^3
    double g = -1.205241709463E-02; // 1/K^2
    double h = 3.836440125785E-01;  // 1/K
    double i = 1.114809112848E+00;  // 1
	
	
	return a*pow(T,8) + b*pow(T,7)  + c*pow(T,6) + d*pow(T,5) + e*pow(T,4) + f*pow(T,3) + g*pow(T,2) + h*T + i;
}

EXPORT int eval(const char *func,
                              int nArgs,
                              const double **inReal,
                              const double **inImag,
                              int blockSize,
                              double *outReal,
                              double *outImag) {
  int i;
  double densitySS = 8000;


  if (strcmp("CFUN_CvSteel_v1", func) == 0) {
    if (nArgs != 1) {
      error = "One argument expected";
      return 0;
    }

    for (i = 0; i < blockSize; i++) {
		
	  double T = inReal[0][i];
	  
	  /*Input consistency check with hotfix for negative T*/
	  	  //	  if(T < 1.9)   {T=1.9;}
	  if(T < 0)     {error = "T is negative!"; return 0; }   
	  if(T > 1400) { error = "Temperature is above 1400 K - out of range!"; return 0;}
	  
    if(T >= 0 && T <= 300){
      outReal[i] = CpSS( T ) * densitySS;
    }

    if(T > 300){
      outReal[i] = 3.909e+06;
    }
	  
	  /*Output consistency check*/
	  if(outReal[i]!=outReal[i]){
		error = "Output is nan"; 
		return 0;	  
	  }
	  if (fabs(outReal[i])>DBL_MAX){
		error = "Output is inf"; 
		return 0;	  
	  }
    }
    return 1;
  }

  else {
    error = "Unknown function";
    return 0;
  }
}