#include <math.h>
#include <float.h>
#include <stdlib.h>
#include <string.h>
#ifdef _MSC_VER
#define EXPORT __declspec(dllexport)
#else
#define EXPORT
#endif

static const char *error = NULL;

EXPORT int init(const char *str) {
  return 1;
}

EXPORT const char * getLastError() {
  return error;
}

EXPORT const char* return_test_yaml() {
    return "CFUN_CvG10_v1";
}

EXPORT int eval(const char *func,
                              int nArgs,
                              const double **inReal,
                              const double **inImag,
                              int blockSize,
                              double *outReal,
                              double *outImag) {
  int i;
  double a0  = -2.4083;
  double a1  = 7.6006;
  double a2  = -8.2982;
  double a3  = 7.3301;
  double a4  = -4.2386;
  double a5  = 1.4294;
  double a6  = -0.24396;
  double a7  = 0.015236;
  double rho = 1.9e3; //[kg/m^3]
  double m   = 3203.492116;//2.690278965; 
  double log_500 = log10(500);
  double f_exp_500 = (a0 + a1*log_500 + a2*pow(log_500,2) + a3*pow(log_500,3) + a4*pow(log_500,4) + a5*pow(log_500,5) + a6*pow(log_500,6) + a7*pow(log_500,7));  
  double n = rho*pow(10,f_exp_500) - m*500;
  double log_T; 
  double f_exp; 
  
  if (strcmp("CFUN_CvG10_v1", func) == 0) {
    if (nArgs != 1) {
      error = "One argument expected";
      return 0;
    }

    for (i = 0; i < blockSize; i++) {
	  double T = inReal[0][i];
	  
	  if(T < 1.8)   {
      outReal[i] = 2.373291558813988e+02;
      }
	  
    if(T >= 1.8 && T <= 500) {
      log_T = log10(T);
      f_exp = (a0 + a1*log_T + a2*pow(log_T,2) + a3*pow(log_T,3) + a4*pow(log_T,4) + a5*pow(log_T,5) + a6*pow(log_T,6) + a7*pow(log_T,7));
      outReal[i] = rho*pow(10,f_exp);
    }

	  if(T > 500){ 
      outReal[i] = m*T + n;
    }
		
    if(T > 1400){
		error = "Temperature is above 1400 K - out of range!"; 
		return 0;	  
	  } 
    
	  /*Output consistency check*/
	  if(outReal[i]!=outReal[i]){
		error = "Output is nan"; 
		return 0;	  
	  }
	  if (fabs(outReal[i])>DBL_MAX){
		error = "Output is inf"; 
		return 0;	  
	  }
	
	}
    return 1;
  }

  else {
    error = "Unknown function";
    return 0;
  }
}
