#include <math.h>
#include <float.h>
#include <stdlib.h>
#include <string.h>
#ifdef _MSC_VER
#define EXPORT __declspec(dllexport)
#else
#define EXPORT
#endif

static const char *error = NULL;

EXPORT int init(const char *str) {
  return 1;
}

EXPORT const char * getLastError() {
  return error;
}

EXPORT const char* return_test_yaml() {
    return "";
}

EXPORT int eval(const char *func,
                              int nArgs,
                              const double **inReal,
                              const double **inImag,
                              int blockSize,
                              double *outReal,
                              double *outImag) {
  
  double T1 = 20;
  double T2 = 400;
 
  int i;
  
  double a0    = 79.78547;
  double a1    = -247.44839;
  double a2    = 305.01434;
  double a3    = -186.90995;
  double a4    = 57.48133;
  double a5    = -6.3977;
  double a6    = -0.6827738;
  double a7    = 0.1662252;
  double beta  = 1.241e-3;
  double gamma = 0.138;
  double rho   = 8950; //[kg/m^3]

  double m = 4.004004004004004e+02;
  double log_T_400      = log10(400);
	double f_exp_400      = (a0 + a1*log_T_400 + a2*pow(log_T_400,2) + a3*pow(log_T_400,3) + a4*pow(log_T_400,4) + a5*pow(log_T_400,5) + a6*pow(log_T_400,6) + a7*pow(log_T_400,7));
	double n = rho*(pow(10, f_exp_400)) - m*400;
  
  double log_T;
  double f_exp;
  
  if (strcmp("CFUN_CvNb3Sn_v2", func) == 0) {
    if (nArgs != 4) {
      error = "Four arguments expected";
      return 0;
    }

    for (i = 0; i < blockSize; i++) {		
	  double T     = inReal[0][i];
	  double B     = inReal[1][i];
    double Tc0   = inReal[2][i];  //[K]
    double Bc20  = inReal[3][i];  //[T]
	  double Tc;
	  
	  /*Input consistency check with hotfix for negative T*/
	  	  //	  if(T < 1.9)   {T=1.9;}
	  if(T < 0)     {error = "T is negative!"; return 0; } 
	  if(B < 0)     {error = "normB is negative!"; return 0; }
	  B = fabs(B);
    if (B < 0.001){
        B = 0.001;
    }

	  if(B <= Bc20){
		Tc = Tc0*pow((1-B/Bc20),0.59);	
	  }
	  
	  if(T <= Tc) {
		outReal[i] = rho * ((beta + 3 * gamma / pow(Tc0, 2)) * pow(T, 3) + gamma * B / Bc20 * T);
	  }
    else if (T > Tc && T<= T1) {
		  outReal[i] = rho*( beta*pow(T,3) + gamma*T );
	  }
	  else if (T > T1 && T<= T2) {
		  log_T      = log10(T);
		  f_exp      = (a0 + a1*log_T + a2*pow(log_T,2) + a3*pow(log_T,3) + a4*pow(log_T,4) + a5*pow(log_T,5) + a6*pow(log_T,6) + a7*pow(log_T,7));
		  outReal[i] = rho*(pow(10, f_exp));
	  }
	  else {
		  outReal[i] = n+m*T;//rho*(234.89+0.0425*T);
	  }
	  	  
	  /*Output consistency check*/
	  if(outReal[i]!=outReal[i]){
		error = "Output is nan"; 
		return 0;	  
	  }
    
	  if (fabs(outReal[i])>DBL_MAX){
		error = "Output is inf"; 
		return 0;	  
	  }
    }
    return 1;
  }

  else {
    error = "Unknown function";
    return 0;
  }
}
