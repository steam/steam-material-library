#include <math.h>
#include <float.h>
#include <stdlib.h>
#include <string.h>
#ifdef _MSC_VER
#define EXPORT __declspec(dllexport)
#else
#define EXPORT
#endif

static const char *error = NULL;

EXPORT int init(const char *str) {
  return 1;
}

EXPORT const char * getLastError() {
  return error;
}

EXPORT const char* return_test_yaml() {
    return "CFUN_CvNbTi_CUDI_v1";
}

EXPORT int eval(const char *func,
                              int nArgs,
                              const double **inReal,
                              const double **inImag,
                              int blockSize,
                              double *outReal,
                              double *outImag) {     
    int i;
    //linear approximation parameters
    double m = 526.3158;
    double n = 2604242.490531;
    double Tc0 = 9.2;
    double Bc20 = 14.5;
    double alpha = 0.59;
    double Tc;

    if (strcmp("CFUN_CvNbTi_CUDI_v1", func) == 0) {
      if (nArgs != 2) {
        error = "Two arguments expected";
        return 0;
        }

    for (i = 0; i < blockSize; i++) {
		
	  double T   = inReal[0][i];
	  double B   = inReal[1][i];
      
	  
	  /*Input consistency check with hotfix for negative T*/
		  //if(T < 1.9)   {T=1.9;}
	    if(T < 0)     {error = "T is negative!"; return 0; } 
		  if(B < 0)     {error = "normB is negative!"; return 0; } 
		  	  
	  	// Calculate the critical temperature Tc
            if (B >= 0) {
                Tc = Tc0 * pow(1 - B / Bc20, alpha);
            } else {
                Tc = 6.0; // Default critical temperature
            }
	  	  
	  if (T <= Tc) {
                outReal[i] = 0.00000E+00 * pow(T, 4) + 4.91000E+01 * pow(T, 3) + 0.00000E+00 * pow(T, 2) + 6.40000E+01 * T + 0.00000E+00;
            } else if (T > Tc && T <= 20) {
                outReal[i] = 0.00000E+00 * pow(T, 4) + 1.62400E+01 * pow(T, 3) + 0.00000E+00 * pow(T, 2) + 9.28000E+02 * T + 0.00000E+00;
            } else if (T > 20 && T <= 50) {
                outReal[i] = -2.17700E-01 * pow(T, 4) + 1.19838E+01 * pow(T, 3) + 5.53710E+02 * pow(T, 2) - 7.84610E+03 * T + 4.13830E+04;
            } else if (T > 50 && T <= 175) {
                outReal[i] = -4.82000E-03 * pow(T, 4) + 2.97600E+00 * pow(T, 3) - 7.16300E+02 * pow(T, 2) + 8.30220E+04 * T - 1.53000E+06;
            } else if (T > 175 && T <= 500) {
                outReal[i] = -6.29000E-05 * pow(T, 4) + 9.29600E-02 * pow(T, 3) - 5.16600E+01 * pow(T, 2) + 1.37060E+04 * T + 1.24000E+06;
            }else{
		        //linear approximation
		        outReal[i] = m*T + n;
	        }

	  /*Output consistency check*/
	  if(outReal[i]!=outReal[i]){
		error = "Output is nan"; 
		return 0;	  
	  }
	  if (abs(outReal[i])>DBL_MAX){
		error = "Output is inf"; 
		return 0;	  
	  }
    }
    return 1;
  }

  else {
    error = "Unknown function";
    return 0;
  }
}