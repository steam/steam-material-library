#include <math.h>
#include <float.h>
#include <stdlib.h>
#include <string.h>
#ifdef _MSC_VER
#define EXPORT __declspec(dllexport)
#else
#define EXPORT
#endif

static const char *error = NULL;

EXPORT int init(const char *str) {
  return 1;
}

EXPORT const char * getLastError() {
  return error;
}

EXPORT const char* return_test_yaml() {
    return "CFUN_CvSS316_NIST_v1";
}

EXPORT int eval(const char *func,
                              int nArgs,
                              const double **inReal,
                              const double **inImag,
                              int blockSize,
                              double *outReal,
                              double *outImag) {
  int i, j;
  double polyFit1[9] = {-1.91368, 8.44996, 3.15315, -89.9982, 239.5296, -308.854, 218.743, -80.6422, 12.2486};
  double polyFit2[9] = {17.05262, -237.22704, 1382.4627, -4305.7217, 7437.6247, -6176.028, 76.70125, 3643.198, -1879.464};

  double m = 0.617209868500000;
  double n = 3.050748366965554E2;

  double density = 7990;
  double T1 = 4;
  double T2 = 50;
  double T3 = 300;

  if (strcmp("CFUN_CvSS316_NIST_v1", func) == 0) {
    if (nArgs != 1) {
      error = "One argument expected";
      return 0;
    }

    for (i = 0; i < blockSize; i++) {
		
	  double T = inReal[0][i];
	  double log_T;
	  /*Input consistency check with hotfix for negative T*/
	  if (T < 4) {
        outReal[i] = 1.579936887499283e+04;
      }else if (T >= 4 && T <= 50) {
        log_T = log10(T);
        outReal[i] = density*pow(10, polyFit1[8] + polyFit1[7] * log_T + polyFit1[6] * pow(log_T, 2) +
                        polyFit1[5] * pow(log_T, 3) + polyFit1[4] * pow(log_T, 4) +
                        polyFit1[3] * pow(log_T, 5) + polyFit1[2] * pow(log_T, 6) +
                        polyFit1[1] * pow(log_T, 7) + polyFit1[0] * pow(log_T, 8));
      } else if (T > 50 && T <= 300) {
        log_T = log10(T);
        outReal[i] = density*pow(10, polyFit2[8] + polyFit2[7] * log_T + polyFit2[6] * pow(log_T, 2) +
                        polyFit2[5] * pow(log_T, 3) + polyFit2[4] * pow(log_T, 4) +
                        polyFit2[3] * pow(log_T, 5) + polyFit2[2] * pow(log_T, 6) +
                        polyFit2[1] * pow(log_T, 7) + polyFit2[0] * pow(log_T, 8));
      }
    else{
      //linear approximation
      outReal[i] = density*(m*T + n);
    }
	  /*Output consistency check*/
	  if(outReal[i]!=outReal[i]){
		error = "Output is nan"; 
		return 0;	  
	  }
	  if (fabs(outReal[i])>DBL_MAX){
		error = "Output is inf"; 
		return 0;	  
	  }
    }
    return 1;
  }

  else {
    error = "Unknown function";
    return 0;
  }
}
