#include <math.h>
#include <float.h>
#include <stdlib.h>
#include <string.h>
#ifdef _MSC_VER
#define EXPORT __declspec(dllexport)
#else
#define EXPORT
#endif

static const char *error = NULL;

EXPORT int init(const char *str) {
  return 1;
}

EXPORT const char * getLastError() {
  return error;
}

EXPORT const char* return_test_yaml() {
    return "CFUN_Jc_NbTi_Cudi_v1";
}


EXPORT int eval(const char *func,
                              int nArgs,
                              const double **inReal,
                              const double **inImag,
                              int blockSize,
                              double *outReal,
                              double *outImag) {
    
    double Tc0, Bc20, c1, c2, c3, c4, c5, c6;
    double wireDiameter, Cu_noCu, f_sc;
    int i;
    double PI_4 = 3.14159265358979323846 / 4;
    double Tc, f_T_Tc, Ic_sc;
    double f_B_Bc20;
    double minB = 1e-6;

    if (strcmp("CFUN_Jc_NbTi_Cudi_v1", func) == 0) {
        if (nArgs != 12) {
            error = "Twelve arguments expected";
            return 0;
        }

        for (i = 0; i < blockSize; i++) {
            // parameters 
            double T        = inReal[0][i];
            double B        = inReal[1][i];
            double Tc0      = inReal[2][i];
            double Bc20     = inReal[3][i];
            double c1       = inReal[4][i];
            double c2       = inReal[5][i];
            double c3       = inReal[6][i];
            double c4       = inReal[7][i];
            double c5       = inReal[8][i];
            double c6       = inReal[9][i];

            //Wire parameters
            double wireDiameter = inReal[10][i];
            double Cu_noCu      = inReal[11][i];

            f_sc = 1 / (1 + Cu_noCu);

            B =  fabs(B);
            f_B_Bc20 = B / Bc20;

            if (f_B_Bc20 > 1) {
                f_B_Bc20 = 1;
            }

            Tc = Tc0 * pow((1 - f_B_Bc20), 0.59);

            f_T_Tc = T / Tc;

            if (f_T_Tc > 1) {
                f_T_Tc = 1;
            }

            Ic_sc = (c1 + c2 * B + c3 * exp(-c4 * B) + c5 * exp(-c6 * B)) * (1 - f_T_Tc);

            if (Ic_sc < 0) {
                Ic_sc = 0;
            }

            outReal[i] = Ic_sc / (PI_4 * pow(wireDiameter, 2) * f_sc); //  == Jc
            
            /*Output consistency check*/
	        if(outReal[i]!=outReal[i]){
		    error = "Output is nan"; 
		    return 0;	  
	        }
	        if (fabs(outReal[i])>DBL_MAX){
		    error = "Output is inf"; 
		    return 0;	  
	        }	
	  
        }
        return 1;
    }
    else {
        error = "Unknown function";
        return 0;
    }
}
