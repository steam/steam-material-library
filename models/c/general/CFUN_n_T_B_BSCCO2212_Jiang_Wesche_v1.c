#include <math.h>
#include <float.h>
#include <stdlib.h>
#include <string.h>
#ifdef _MSC_VER
#define EXPORT __declspec(dllexport)
#else

#define EXPORT
#endif

static const char *error = NULL;

EXPORT int init(const char *str) {
  return 1;
}

EXPORT const char * getLastError() {
  return error;
}

EXPORT const char* return_test_yaml() {
    return "CFUN_n_T_B_BSCCO2212_Jiang_Wesche_v1";
}

EXPORT int eval(const char *func,
                              int nArgs,
                              const double **inReal,
                              const double **inImag,
                              int blockSize,
                              double *outReal,
                              double *outImag) {
								  
    double a1       = 448.31;
    double a2       = 0.1154;

    double Tc_B, T0 = 4.2;
    double PI_4     = 3.14159265358979323846/4.0;

    double b1 = 852.002 / (PI_4 * 0.0012*0.0012 * 0.25 );
    double b2 = 0.191301;
    
    double n_T_B;
    int i;
  
    if (strcmp("CFUN_n_T_B_BSCCO2212_Jiang_Wesche_v1", func) == 0) {
        if (nArgs != 3) {
          error = "Three arguments expected";
          return 0;
        }

        for (i = 0; i < blockSize; i++) {  	 
        
            double T   = inReal[0][i];
	          double B   = inReal[1][i];
	          double n0  = inReal[2][i];

    	        /*Input consistency check*/
	          if(T < 0)              {error = "T is negative!"; return 0; } 
            if(B < 0)      {error = "Jc_Nb3Sn0 is negative!"; return 0; }

            // Modify the input magnetic field
            if(B<0){
                B=-B; // direction of the magnetic field is unimportant
            }
            if(fabs(B)<0.2){
                B=0.2; // very small magnetic field causes numerical problems
            }
            
            Tc_B = (log(a1) - log(B)) / a2; // [K]
            
            n_T_B = 1+((n0-1) * (Tc_B - T)/Tc_B);

            if(n_T_B < 1){
                n_T_B = 1; // very small magnetic field causes numerical problems
            }
            
            outReal[i] = n_T_B;
            
            if (isnan(outReal[i])) {
              error = "Output is NaN";
              return 0;
            }
        }
     return 1;

    }

    else {
      error = "Unknown function";
      return 0;
    }
}
