#include <math.h>
#include <float.h>
#include <stdlib.h>
#include <string.h>
#ifdef _MSC_VER
#define EXPORT __declspec(dllexport)
#else
#define EXPORT
#endif

static const char *error = NULL;

EXPORT int init(const char *str) {
  return 1;
}

EXPORT const char * getLastError() {
  return error;
}

EXPORT const char* return_test_yaml() {
    return "CFUN_kG10_v2";
}

EXPORT int eval(const char *func,
                              int nArgs,
                              const double **inReal,
                              const double **inImag,
                              int blockSize,
                              double *outReal,
                              double *outImag) {
  int i;
  double a0  = -4.1236;
  double a1  =  13.788;
  double a2  = -26.068;
  double a3  =  26.272;
  double a4  = -14.663;
  double a5  =  4.4954;
  double a6  = -0.6905;
  double a7  =  0.0397;
  
  //linear approximation parameters
  double m =  0.0017;
  double n =  0.097983;
  double log_T; 
  double f_exp; 

  if (strcmp("CFUN_kG10_v2", func) == 0) {
    if (nArgs != 1) {
      error = "One argument expected";
      return 0;
    }

    for (i = 0; i < blockSize; i++) {
	  double T = inReal[0][i];
	  
	  // Enforce minimum temperature of 1.9 K
    // The value of kG10 is not computed in this case but can be directly returned 
    // for increased performance.
    // BEWARE: the value of outReal needs to be changed if the function is changed
	  if(T < 1.9)  {
      outReal[i] = 0.01520715036820004;
      continue;
    }
	  
    if(T < 300){
      log_T = log10(T);
      f_exp = (a0 + a1*log_T + a2*pow(log_T,2) + a3*pow(log_T,3) + a4*pow(log_T,4) + a5*pow(log_T,5) + a6*pow(log_T,6) + a7*pow(log_T,7));
      outReal[i] = pow(10,f_exp);  
    }

    if(T >= 300){
      //linear approximation
      outReal[i] = m*T + n;
    }
	  
	  /*Output consistency check*/
	  if(outReal[i]!=outReal[i]){
		error = "Output is nan"; 
		return 0;	  
	  }
	  if (fabs(outReal[i])>DBL_MAX){
		error = "Output is inf"; 
		return 0;	  
	  }	  
    }
    return 1;
  }

  else {
    error = "Unknown function";
    return 0;
  }
}
