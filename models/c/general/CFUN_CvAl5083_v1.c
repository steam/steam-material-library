#include <math.h>
#include <float.h>
#include <stdlib.h>
#include <string.h>
#ifdef _MSC_VER
#define EXPORT __declspec(dllexport)
#else
#define EXPORT
#endif

static const char *error = NULL;

EXPORT int init(const char *str) {
  return 1;
}

EXPORT const char * getLastError() {
  return error;
}

EXPORT const char* return_test_yaml() {
    return "CFUN_CvAl5083_v1";
}

EXPORT int eval(const char *func,
                              int nArgs,
                              const double **inReal,
                              const double **inImag,
                              int blockSize,
                              double *outReal,
                              double *outImag) {
  
  double density = 2699; // kg/m^3
  
  double params[9] =  {2.96344, -38.3094, 210.351, -637.795,
     1162.27, -1298.3, 866.662, -314.292, 46.6467};
  
  //linear approximation parameters (T>300)
  double m = 3175.31605;
  double log300 = log10(300);
  double f_exp_300 = params[0] * pow(log300, 8) + params[1] * pow(log300, 7) + params[2] * pow(log300, 6) + params[3] * pow(log300, 5) + params[4] * pow(log300, 4) + params[5] * pow(log300, 3) + params[6] * log300*log300 + params[7] * log300 + params[8];
	double n = density*pow(10,f_exp_300) - m*300;

  double log_T;
  double f_exp; 
	int i;

  if (strcmp("CFUN_CvAl5083_v1", func) == 0) {
    if (nArgs != 1) {
      error = "One argument expected";
      return 0;
    }

    for (i = 0; i < blockSize; i++) {
		
	    double T = inReal[0][i];
	    log_T = log10(T);

		if(T < 0){
				error = "T is negative!"; return 0; 
			} 
		
		if(T > 1400){
				error = "Temperature is above 1400 K - out of range!"; 
				return 0;	  
			}
    if(T < 4){
      //keep constant value
        outReal[i] = 788.044541;
      }

    if(T > 300){
        //linear approximation
        outReal[i] = m* T + n;
      }
        
		if(T >= 4 && T <= 300) {
        f_exp = params[0] * pow(log_T, 8) + params[1] * pow(log_T, 7) + params[2] * pow(log_T, 6) + params[3] * pow(log_T, 5) + params[4] * pow(log_T, 4) + params[5] * pow(log_T, 3) + params[6] * log_T*log_T + params[7] * log_T + params[8];
			  outReal[i] = density*pow(10,f_exp);
			}
       
		if(outReal[i]!=outReal[i]){
			error = "Output is nan"; 
			return 0;	  
			}
		if (fabs(outReal[i])>DBL_MAX){
      error = "Output is inf"; 
      return 0;	  
      }
		}
		return 1;
  }

}