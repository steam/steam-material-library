#include <math.h>
#include <float.h>
#include <stdlib.h>
#include <string.h>
#ifdef _MSC_VER
#define EXPORT __declspec(dllexport)
#else
#define EXPORT
#endif

static const char *error = NULL;

EXPORT int init(const char *str) {
  return 1;
}

EXPORT const char * getLastError() {
  return error;
}

EXPORT const char* return_test_yaml() {
    return "CFUN_CpHeMass_v1";
}

EXPORT int eval(const char *func,
                              int nArgs,
                              const double **inReal,
                              const double **inImag,
                              int blockSize,
                              double *outReal,
                              double *outImag) {
  
  	double params[4] =  {-274.45, 1961.6, -4673.2, 3712.9};
    
    double cpHe;

	int i;

  if (strcmp("CFUN_CpHeMass_v1", func) == 0) {
    if (nArgs != 1) {
      error = "One argument expected";
      return 0;
    }

    for (i = 0; i < blockSize; i++) {
		
	    double T = inReal[0][i];

		if(T <= 0){
				error = "T is negative!"; return 0; 
		} 
		
		if(T > 1400){
				error = "Temperature is above 1400 K - out of range!"; 
				return 0;	  
		}
        
		if(T <= 2.17 && T > 0){
			cpHe = 2.12 + 0.000678*pow(T, 12.159);
		}

		if(T > 2.17 && T <= 2.5) {
            cpHe= params[0] * pow(T, 3) + params[1] * T*T + params[2] * T + params[3];
		}
            
        if(T > 2.5 && T <= 4.3) {
			cpHe = 0.9163 * T*T - 4.484 * T + 7.68;
		}

		if(T > 4.3 && T <= 15) {
			cpHe = 5.2 + 1489.4 * pow(T, -4.06);
		}

        if(T > 15){
            cpHe = 5.2;
        }

        outReal[i] = cpHe * pow(10, 3);
		
		if(outReal[i]!=outReal[i]){
			error = "Output is nan"; 
			return 0;	  
			}
		if (fabs(outReal[i])>DBL_MAX){
		error = "Output is inf"; 
		return 0;	  
		}
		}
		return 1;
  }

}