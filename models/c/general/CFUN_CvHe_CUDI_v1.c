#include <math.h>
#include <float.h>
#include <stdlib.h>
#include <string.h>
#ifdef _MSC_VER
#define EXPORT __declspec(dllexport)
#else
#define EXPORT
#endif


static const char *error = NULL;

EXPORT int init(const char *str) {
  return 1;
}

EXPORT const char * getLastError() {
  return error;
}

EXPORT const char* return_test_yaml() {
    return "CFUN_CvHe_CUDI_v1";
}

double CpHe(double T) {
	double CpHe;
	
	if(T <= 2.17) {
		CpHe = 2.12 + 0.000678 * pow(T, 12.159);
	}
    else if (T <= 2.5) {
		CpHe = -274.45 * pow(T,3) + 1961.6 * pow(T,2) -4673.2 * T + 3712.9;
	}
	else if (T <= 4.3) {
		CpHe = 0.9163 * pow(T,2) -4.484 * T + 7.68;
	}
	else if (T <= 15) {
		CpHe = 5.2 +  1489.4 * pow(T,-4.06);
	}
	else {
		CpHe = 5.2;
	}
	           
	return CpHe;
}

double rhoThHe(double T) {
	double rhoThHe;	
	if(T <= 4.3) {
	   double a5 = -1.06;
       double a4 =  15.14; 
       double a3 = -84.21;  
       double a2 =  222.5;  
       double a1 = -277.5;  
       double a0 =  278.2;  
	   rhoThHe = a5 *pow(T, 5) + a4 *pow(T, 4) + a3 *pow(T, 3) + a2 *pow(T, 2) + a1 * T + a0;
	}
	else {
	   double p2 =  0.04925;
       double p1 =  45.09;
       double p0 = -194.2;
       double q1 = -5.978;
       double q0 =  7.222;
	   rhoThHe = ( p2 *pow(T, 2) + p1 * T + p0 ) / ( pow(T, 2) + q1 * T + q0 );
	}
	           
	return rhoThHe;
}

EXPORT int eval(const char *func,
                              int nArgs,
                              const double **inReal,
                              const double **inImag,
                              int blockSize,
                              double *outReal,
                              double *outImag) {
  int i;
 
  if (strcmp("CFUN_CvHe_CUDI_v1", func) == 0) {
    if (nArgs != 1) {
      error = "One argument expected";
      return 0;
    }

    for (i = 0; i < blockSize; i++) {
		
	  double T = inReal[0][i];
	  
	  /*Input consistency check with hotfix for negative T*/
	  	  //	  if(T < 1.9)   {T=1.9;}
	  if(T < 0)     {error = "T is negative!"; return 0; } 
	  
/* 	  if(T > 300){
		error = "Temperature is above 300 K - outside of the validated range!"; 
		return 0;	  
	  } */
	  
	  /*CpHe(T) gives back [J/g/K], that's why it is multiplied by 1e3*/
	  /*Output is in [J/m^3/K], just to be suitable for COMSOL input*/
	  
	  outReal[i] = 1e3 * CpHe( T ) * rhoThHe( T ) ;
	  
	  /*Output consistency check*/
	  if(outReal[i]!=outReal[i]){
		error = "Output is nan"; 
		return 0;	  
	  }
	  if (fabs(outReal[i])>DBL_MAX){
		error = "Output is inf"; 
		return 0;	  
	  }
    }
    return 1;
  }

  else {
    error = "Unknown function";
    return 0;
  }
}