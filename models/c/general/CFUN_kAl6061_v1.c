#include <math.h>
#include <float.h>
#include <stdlib.h>
#include <string.h>
#ifdef _MSC_VER
#define EXPORT __declspec(dllexport)
#else
#define EXPORT
#endif

static const char *error = NULL;

EXPORT int init(const char *str) {
return 1;
}

EXPORT const char * getLastError() {
return error;
}

EXPORT const char* return_test_yaml() {
    return "CFUN_kAl6061_v1";
}

// Al6061 98% alu
EXPORT int eval(const char *func,
                              int nArgs,
                              const double **inReal,
                              const double **inImag,
                              int blockSize,
                              double *outReal,
                              double *outImag) {
    double a = -1.44774526e-15;
    double b =  1.78431409e-12;
    double c = -1.28954990e-01;
    double d =  1.29163439e+00;
    double e = -5.17633937e+00;
    double f =  1.04953215e+01;
	double g = -1.14211908e+01;
	double h =  7.36717944e+00;
	double i = -1.27905989e+00;
    
    double m[2] = {1.32, 0.14445};
    double n[2] = {0.0028, 112.403};
    
    double log_T;
    double f_exp;
	int k;
    
	if (strcmp("CFUN_kAl6061_v1", func) == 0) {
		if (nArgs != 1) {
			error = "One argument expected";
			return 0;
			}

        for (k = 0; k < blockSize; k++) {
			
            
		    double T = inReal[0][k];
            
			if(T < 0)     {
				    error = "T is negative!"; return 0; 
			    } 
	
			if(T > 1400){
				    error = "Temperature is above 1400 K - out of range!";
				    return 0;	  
			    } 
            
            if(T < 4) {
                log_T = log10(4);
                f_exp = a*pow(log_T,8) + b*pow(log_T,7) + c*pow(log_T,6) + d*pow(log_T,5) + e*pow(log_T,4) + f*pow(log_T,3) + g*pow(log_T,2) + h*log_T + i;
                n[0] = pow(10,f_exp) - m[0]*4;
                outReal[k] = m[0]*T + n[0] ;
                }
            
            if(T >= 4 && T < 295) {
                log_T = log10(T);
                f_exp = a*pow(log_T,8) + b*pow(log_T,7) + c*pow(log_T,6) + d*pow(log_T,5) + e*pow(log_T,4) + f*pow(log_T,3) + g*pow(log_T,2) + h*log_T + i;
                outReal[k] = pow(10,f_exp);
                } 
            
            if(T >= 295) {
                log_T = log10(295);
                f_exp = a*pow(log_T,8) + b*pow(log_T,7) + c*pow(log_T,6) + d*pow(log_T,5) + e*pow(log_T,4) + f*pow(log_T,3) + g*pow(log_T,2) + h*log_T + i;
                n[1] = pow(10,f_exp) - m[1]*295;
                outReal[k] = m[1]*T + n[1];
                }
			
			/*Output consistency check*/
			if(outReal[k]!=outReal[k]){
				error = "Output is nan"; 
				return 0;	  
			}
			if (fabs(outReal[k])>DBL_MAX){
				error = "Output is inf"; 
				return 0;	  
			}
			}
	
    return 1;
}

else {
    error = "Unknown function";
    return 0;
}
}