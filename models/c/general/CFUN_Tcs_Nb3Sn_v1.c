#include <math.h>
#include <float.h>
#include <stdlib.h>
#include <string.h>

#ifdef _MSC_VER
#define EXPORT __declspec(dllexport)
#else
#define EXPORT
#endif

static const char *error = NULL;

EXPORT int init(const char *str) {
    return 1;
}

EXPORT const char *getLastError() {
    return error;
}

EXPORT const char* return_test_yaml() {
    return "CFUN_Tcs_Nb3Sn_v1";
}

EXPORT int eval(const char *func,
                int nArgs,
                const double **inReal,
                const double **inImag,
                int blockSize,
                double *outReal,
                double *outImag) {

    double f_J_Jc0, Tc, Tcs;
    int i;
    
    if (strcmp("CFUN_Tcs_Nb3Sn_v1", func) == 0) {
        if (nArgs != 6) {
            error = "Six arguments expected";
            return 0;
        }

        for ( i = 0; i < blockSize; i++) {
            // unpack parameters
            double J_sc       = inReal[0][i];
            double B          = inReal[1][i];
            double Jc0        = inReal[2][i]; // Jc0 = Jc_Nb3Sn_Summer(0, B, Jc_Nb3Sn0, Tc0_Nb3Sn, Bc20_Nb3Sn);
            double Jc_Nb3Sn0  = inReal[3][i];
            double Tc0_Nb3Sn  = inReal[4][i];
            double Bc20_Nb3Sn = inReal[5][i];

            // make J_sc and B positive (direction is unimportant)
            J_sc = fabs(J_sc);
            B    = fabs(B);

            // simplified Tc calculation
            double f_B_Bc2 = B / Bc20_Nb3Sn;
            if (f_B_Bc2 > 1) {
                f_B_Bc2 = 1;
            }
            Tc = Tc0_Nb3Sn * pow((1 - f_B_Bc2), 0.59);

            // simplified Tcs calculation
            f_J_Jc0 = J_sc / Jc0;
            if (f_J_Jc0 > 1) {
                f_J_Jc0 = 1;
            }
            Tcs = (1 - f_J_Jc0) * Tc;

            outReal[i] = Tcs; // Only Tcs
        }

        return 1;
    }

    else {
        error = "Unknown function";
        return 0;
    }
}

// 1) Define an array for Jc0 using CFUN_Jc_Nb3Sn_Summer_v1 mex file. The array should have the same dimensions as the 
//    rest of the parameters of the function
// 2) Run the function using as Jc0 (3d parameter) the array you defined.

//This is a general function that can use Jc computed with different methods (Bordini, Summers, Bottura)
