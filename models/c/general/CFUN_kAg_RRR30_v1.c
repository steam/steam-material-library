#include <math.h>
#include <float.h>
#include <stdlib.h>
#include <string.h>
#ifdef _MSC_VER
#define EXPORT __declspec(dllexport)
#else
#define EXPORT
#endif

static const char *error = NULL;

EXPORT int init(const char *str) {
  return 1;
}

EXPORT const char * getLastError() {
  return error;
}

EXPORT const char* return_test_yaml() {
    return "CFUN_kAg_RRR30_v1";
}

EXPORT int eval(const char *func,
                              int nArgs,
                              const double **inReal,
                              const double **inImag,
                              int blockSize,
                              double *outReal,
                              double *outImag) {
  
  	double params1[5] = {-0.326314237655585,0.551121278546028,-0.316726715333209,1.04530563240476,1.82603764562617};
    double params2[9] =  {87.6603250260841,-1304.32557090996,8449.57931641046,-31116.4147264840,71220.8138734661,-103708.203166996,93783.5641105213,-48134.2889933188,10734.3798030775};
    
    double log67 = log10(67);
    double log421 = log10(421);
    double c1 = pow(10,log67);
    double c2 = pow(10, log421);
    double kAg, logT, f_exp;
	int i;

  if (strcmp("CFUN_kAg_RRR30_v1", func) == 0) {
    if (nArgs != 1) {
      error = "One argument expected";
      return 0;
    }

    for (i = 0; i < blockSize; i++) {
		
	    double T = inReal[0][i];

      if(T < 0){
          error = "T is negative!"; return 0; 
        } 
      
      if(T > 1400){
          error = "Temperature is above 1400 K - outside of the range!"; 
          return 0;	  
        }

      if(T <= 1 ){
            kAg = c1;
        }
      else if(T >= 1 && T <= 20) {
        logT = log10(T);
        f_exp = params1[0] * pow(logT, 4) + params1[1] * pow(logT, 3) + params1[2] * pow(logT, 2) + params1[3] * logT + params1[4];
        kAg = pow(10, f_exp);
        }
      else if(T >= 20 && T <= 140) {
        logT = log10(T);
        f_exp = params2[0] * pow(logT, 8) + params2[1] * pow(logT, 7) + params2[2] * pow(logT, 6) + params2[3] * pow(logT, 5) + params2[4] * pow(logT, 4)+ params2[5]* pow(logT, 3) + params2[6]* pow(logT, 2) + params2[7]* logT + params2[8];
        kAg = pow(10, f_exp);
        }

      else if (T > 140){
        kAg = c2;
      }

      outReal[i] = kAg;

      if(outReal[i]!=outReal[i]){
        error = "Output is nan"; 
        return 0;	  
      }
      if (fabs(outReal[i])>DBL_MAX){
        error = "Output is inf"; 
        return 0;	  
      }
	  }
		return 1;
  }

}
