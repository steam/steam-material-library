#include <math.h>
#include <float.h>
#include <stdlib.h>
#include <string.h>
#ifdef _MSC_VER
#define EXPORT __declspec(dllexport)
#else
#define EXPORT
#endif

#define MAX(x, y) (((x) > (y)) ? (x) : (y))
#define MIN(x, y) (((x) < (y)) ? (x) : (y))
#define EPS .000000001

static const char *error = NULL;

EXPORT int init(const char *str) {
  return 1;
}

EXPORT const char * getLastError() {
  return error;
}

EXPORT const char* return_test_yaml() {
    return "CFUN_JcNbTiLowCurrent_v1";
}

EXPORT int eval(const char *func,
                              int nArgs,
                              const double **inReal,
                              const double **inImag,
                              int blockSize,
                              double *outReal,
                              double *outImag) {
								  
  double Tc0   = 9.2;             //[K]
  double Bc20  = 14.5;            //[T]
  
  
  double Tmin  = 1e-3;             // Min allowed T
  double Bmin  = 1e-1;             // Min allowed B
  
  double Jc;
  double Tc;
  int i;
  
  if (strcmp("CFUN_JcNbTiLowCurrent_v1", func) == 0) {
    if (nArgs != 2) {
      error = "Two arguments expected";
      return 0;
    }

    for (i = 0; i < blockSize; i++) {		
	  double T    = inReal[0][i];
	  double B    = inReal[1][i];
	  
	  /*Input consistency check with hotfix for negative T*/
	  if(T < 1.9)   {T=1.9;}
	  if(T < 0)     {error = "T is negative!"; return 0; } 
	  if(B < 0)     {error = "normB is negative!"; return 0; }
	  
	  Tc = Tc0 * pow((1 - MAX(MIN(B, 14.499),0)/Bc20), 0.59);
	  
	  Jc = MAX(0, (1.004e10 - 7.619e8*B + 2.797e10*exp(-1.4*B) + 1.112e11*exp(-12*B))*(1-T/Tc));
	  		
	outReal[i] = Jc;
	
	  /*Output consistency check*/
	  if(outReal[i]!=outReal[i]){
		error = "Output is nan"; 
		return 0;	  
	  }
	  if (fabs(outReal[i])>DBL_MAX){
		error = "Output is inf"; 
		return 0;	  
	  }
    }
    return 1;
  }

  else {
    error = "Unknown function";
    return 0;
  }
}
