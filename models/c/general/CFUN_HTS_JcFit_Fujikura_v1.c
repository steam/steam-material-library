#include <math.h>
#include <float.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#ifdef _MSC_VER
#define EXPORT __declspec(dllexport)
#else
#define EXPORT
#endif

#define MAX(x, y) (((x) > (y)) ? (x) : (y))
#define MIN(x, y) (((x) < (y)) ? (x) : (y))
#define PI 3.14159265358979323846
#define deg2rad PI/180
#define rad2deg 180/PI

static const char *error = NULL;

EXPORT int init(const char *str) {
  return 1;
}

EXPORT const char * getLastError() {
  return error;
}

EXPORT const char* return_test_yaml() {
    return "CFUN_HTS_JcFit_Fujikura_v1";
}

EXPORT int eval(const char *func,
                int nArgs,
                const double **inReal,
                const double **inImag,
                int blockSize,
                double *outReal,
                double *outImag) {
 
 	
 if (nArgs != 3) {
	// Top_IN, Bnorm_IN, thetaFieldTape_IN
	error = "three arguments expected";
    return 0;
    }
	
// Fit Parameters
double n0;  
double n1;
double n2;
double a; 
double Tc0;
double Bi0_ab;
double Bi0_c;
double alpha_ab;
double p_ab;
double q_ab;
double alpha_c;
double p_c;
double q_c;
double g0;
double g1;
double g2;
double g3;
double gamma_ab;
double gamma_c;
double nu;
double thetaOff;

// fit from J. Fleiter and A. Ballarion, "Parameterization of the critical surface of REBCO conductors
// from Fujikura", September 2014, Internal Note 2014-24, EDMS Nr: 1426239
// https://edms.cern.ch/ui/#!master/navigator/document?D:101491516:101491516:subDocs
if(strcmp("CFUN_HTS_JcFit_Fujikura_v1", func) == 0) {
	g0       = 0.03; //[-]
	g1       = 0.25; //[-]
	g2       = 0.06; //[-]
	g3       = 0.058; //[-]
	Tc0      = 93; //[K]
	p_c      = 0.5; //[-]
	q_c      = 2.5; //[-]
	Bi0_c    = 140; //[T]
	gamma_c  = 2.44; //[-]
	alpha_c  = 1.86E+12; //[A*T/m^2]
	nu       = 1.85; //[-]		
	n0       = 1; //[-]
	n1       = 1.4; //[-]
	n2       = 4.45; //[-]
	p_ab     = 1; //[-]
	q_ab     = 5; //[-]
	Bi0_ab   = 250; //[T]		
	a        = 1.0E-01; //[-]
	gamma_ab = 1.63; //[-]
	alpha_ab = 6.83E+13; //[A*T/m^2]
	thetaOff = 0; //[deg]			
}

else{	
	error = "Unknown fit";
	return 0;
}

// Correction factor
double B_MIN = 0.01; //[T]

// Global index
int i; 

// Parfor loop
for (i = 0; i < blockSize; i++) {
	// Raw Input
	double Top_IN   = inReal[0][i];	
	double Bnorm_IN = inReal[1][i];
	double thetaFieldTape_IN = inReal[2][i];

	//Pre-allocation
	double Top;
	double Bnorm;
	double theta;
	double thetaMod;
	
	//Pre-processing
	Bnorm     = MAX(B_MIN,fabs(Bnorm_IN));
	Top       = Top_IN;	
    theta     = fabs(thetaFieldTape_IN - thetaOff)*deg2rad;	

	if (fmod(theta,PI)<=PI/2){ 
		thetaMod = fmod(theta,PI); 
		}
	else{ 
		thetaMod = PI-fmod(theta,PI);
		}

	//Pre-allocation
	double t_T;
	double t_n0;	
	double t_n1;	
	double Bi_ab;
	double Bi_c;
	double b_ab;
	double b_c;
	double Jc_ab;
	double Jc_c;
	bool TCrit_Flag;	
	bool babCrit_Flag;
	bool bcCrit_Flag;
	double g;
	double JcFit;
	
	// Temperature scaling factors
    t_T  = Top/Tc0;
	t_n0 = 1-pow(t_T,n0);
	t_n1 = 1-pow(t_T,n1);	
	
	// Irreversibility field  
	Bi_ab = Bi0_ab*(pow(t_n1,n2)+a*t_n0);
	Bi_c  = Bi0_c*t_n0;
    
	// magnetic flux density	
	b_ab  = Bnorm / Bi_ab;
	b_c   = Bnorm / Bi_c;
	
	// flags	
	TCrit_Flag   = t_T  >= 1 || t_T  < 0;
	babCrit_Flag = b_ab >= 1 || b_ab < 0;		
	bcCrit_Flag  = b_c  >= 1 || b_c  < 0;
	
	if (TCrit_Flag==false){
			
		if (babCrit_Flag==false){
		    // critical current density ab-plane	
			Jc_ab =	(alpha_ab/Bnorm*pow(b_ab,p_ab)) * pow((1-b_ab),q_ab) * pow((pow(t_n1,n2) + a*t_n0),gamma_ab);
			}
		else{
			// field over Bi
			Jc_ab = 0;
			}
		
		if (bcCrit_Flag==false){
			// critical current density c-plane
			Jc_c = (1/Bnorm)*alpha_c*pow(b_c,p_c)*pow((1-b_c),q_c)*pow((1-pow(t_T,n0)),gamma_c);
			}
		else{
			// field over Bi
			Jc_c = 0;
			}	
        
		// anisotropy factor
		g = g0 + g1*exp(-g2*Bnorm*exp(g3*Top));
	
		JcFit = MIN(Jc_ab,Jc_c)+ MAX(0,(Jc_ab-Jc_c))/(1+pow(((PI/2-thetaMod)/g),nu));
		}
		else{
		// temperature over Tc0
		JcFit=0;
		}
	
	//outReal[i] = JcFit;
	outReal[i] =JcFit;
	
	//Consistency check: output
	if(outReal[i]!=outReal[i]){
		error = "Output is nan"; 
		return 0;	  
		}
	
	if (fabs(outReal[i])>DBL_MAX){
		error = "Output is inf"; 
		return 0;	  
		}
	
}
	
return 1;
}
	
