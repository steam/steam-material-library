#include <math.h>
#include <float.h>
#include <stdlib.h>
#include <string.h>
#ifdef _MSC_VER
#define EXPORT __declspec(dllexport)
#else
#define EXPORT
#endif


static const char *error = NULL;

EXPORT int init(const char *str) {
  return 1;
}

EXPORT const char * getLastError() {
  return error;
}

EXPORT const char* return_test_yaml() {
    return "CFUN_CvHe_NIST_v1";
}


EXPORT int eval(const char *func,
                              int nArgs,
                              const double **inReal,
                              const double **inImag,
                              int blockSize,
                              double *outReal,
                              double *outImag) {
    int i;
    double CvHe, log_T;

    double polyFit1[5] = {252.885047589130,-100.373791096815,1.18394466523168,7.66174537467967,4.14183161911798};
    double polyFit2[6] = {-817703.751104569,1609997.44015698,-1267260.35714448,498489.254701282,-98000.1183227793,7709.29932356629};
    double polyFit3[3] = {3.46892446633873,-2.09165480174069,5.76373838168698};
    double polyFit4[4] = {-16.5666756239721, 44.0958304892635, -40.2962974183403, 17.1939023689494};
    double polyFit5[3] = {0.00966352468250771, -1.04744413875913, 5.45949141403381};

    if (strcmp("CFUN_CvHe_NIST_v1", func) == 0) {
        if (nArgs != 1) {
            error = "One argument expected";
            return 0;
        }

        for (i = 0; i < blockSize; i++) {
             double T = inReal[0][i];
	    
            if (T <= 0.8) {
                outReal[i] = 3.29E+03;
            } else if (T > 0.8 && T <= 2.167) {
                log_T = log10(T);
                outReal[i] = pow(10, polyFit1[4] + polyFit1[3] * log_T + polyFit1[2] * pow(log_T, 2) + polyFit1[1] * pow(log_T, 3) + polyFit1[0] * pow(log_T, 4));
            } else if (T > 2.167 && T <= 2.7) {
                log_T = log10(T);
                outReal[i] = pow(10, polyFit2[5] + polyFit2[4] * log_T + polyFit2[3] * pow(log_T, 2) + polyFit2[2] * pow(log_T, 3) + polyFit2[1] * pow(log_T, 4) + polyFit2[0] * pow(log_T, 5));
            } else if (T > 2.7 && T <= 4.222) {
                log_T = log10(T);
                outReal[i] = pow(10, polyFit3[2] + polyFit3[1] * log_T + polyFit3[0] * pow(log_T, 2));
            } else if (T > 4.222 && T <= 10) {
                log_T = log10(T);
                outReal[i] = pow(10, polyFit4[3] + polyFit4[2] * log_T + polyFit4[1] * pow(log_T, 2) + polyFit4[0] * pow(log_T, 3));
            } else if (T > 10 && T <= 1500) {
                log_T = log10(T);
                outReal[i] = pow(10, polyFit5[2] + polyFit5[1] * log_T + polyFit5[0] * log_T*log_T);
            } else {
                outReal[i] = 1.6887636E+02;
            }


    /*Output consistency check*/
	  if(outReal[i]!=outReal[i]){
		error = "Output is nan"; 
		return 0;	  
	  }
	  if (fabs(outReal[i])>DBL_MAX){
		error = "Output is inf"; 
		return 0;	  
	  }
    }
    return 1;
  }

  else {
    error = "Unknown function";
    return 0;
  }
}
