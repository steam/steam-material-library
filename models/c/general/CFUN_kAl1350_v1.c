#include <math.h>
#include <float.h>
#include <stdlib.h>
#include <string.h>
#ifdef _MSC_VER
#define EXPORT __declspec(dllexport)
#else
#define EXPORT
#endif

static const char *error = NULL;

EXPORT int init(const char *str) {
return 1;
}

EXPORT const char * getLastError() {
return error;
}

EXPORT const char* return_test_yaml() {
    return "CFUN_kAl1350_v1";
}

EXPORT int eval(const char *func,
                              int nArgs,
                              const double **inReal,
                              const double **inImag,
                              int blockSize,
                              double *outReal,
                              double *outImag) {
    double a = -1.44774526e-15;
    double b =  1.05081850e+00;
    double c = -1.47337383e+01;
    double d =  8.26212776e+01;
    double e = -2.41381211e+02;
    double f =  3.97901096e+02;
	double g = -3.71866748e+02;
	double h =  1.84274235e+02;
	double i = -3.51882861e+01;
    
    double m[2] = {52.5, 0.08558};
    double n[2] = {0.01, 0};

    double log_T;
    double f_exp;
	int k;
    
	if (strcmp("CFUN_kAl1350_v1", func) == 0) {
		if (nArgs != 1) {
			error = "One argument expected";
			return 0;
			}

        for (k = 0; k < blockSize; k++) {
			
            
		    double T = inReal[0][k];
            
			if(T < 0)     {
				    error = "T is negative!"; return 0; 
			    } 
	
			if(T > 1400){
				    error = "Temperature is above 1400 K - out of range!";
				    return 0;	  
			    } 
            if(T > 233) {
                log_T = log10(233);
                f_exp = a*pow(log_T,8) + b*pow(log_T,7) + c*pow(log_T,6) + d*pow(log_T,5) + e*pow(log_T,4) + f*pow(log_T,3) + g*pow(log_T,2) + h*log_T + i;
                n[1] = pow(10,f_exp) - m[1]*233;
                outReal[k] = m[1]*T + n[1];
                }
            //T >= 10.8 && T < 228.73
            if(T > 6 && T < 233) {
                log_T = log10(T);
                f_exp = a*pow(log_T,8) + b*pow(log_T,7) + c*pow(log_T,6) + d*pow(log_T,5) + e*pow(log_T,4) + f*pow(log_T,3) + g*pow(log_T,2) + h*log_T + i;
                outReal[k] = pow(10,f_exp);
                } 

            if(T <= 6) {
                outReal[k] = m[0]*T + n[0];
            } 
			
			/*Output consistency check*/
			if(outReal[k]!=outReal[k]){
				error = "Output is nan"; 
				return 0;	  
			}
			if (fabs(outReal[k])>DBL_MAX){
				error = "Output is inf"; 
				return 0;	  
			}
			}
	
    return 1;
}

else {
    error = "Unknown function";
    return 0;
}
}