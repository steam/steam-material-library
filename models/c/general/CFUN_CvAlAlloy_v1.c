#include <math.h>
#include <float.h>
#include <stdlib.h>
#include <string.h>
#ifdef _MSC_VER
#define EXPORT __declspec(dllexport)
#else
#define EXPORT
#endif

static const char *error = NULL;

EXPORT int init(const char *str) {
return 1;
}

EXPORT const char * getLastError() {
return error;
}

EXPORT const char* return_test_yaml() {
    return "CFUN_CvAlAlloy_v1";
}

//Aluminum 5083 (95% alu), 6061 (98% alu), 1350 (99.7% alu)
EXPORT int eval(const char *func,
                              int nArgs,
                              const double **inReal,
                              const double **inImag,
                              int blockSize,
                              double *outReal,
    						  double *outImag) {
    double a = -1.44774526e-15;
    double b = -1.33966804e-01;
    double c =  9.24860132e-01;
    double d = -1.33191996e+00;
    double e = -3.79862940e+00;
    double f =  1.19928528e+01;
	double g = -7.96716523e+00;
	double h = -3.48358700e-01;
	double i =  6.80308457e+00;
	double j =  1.94439759e-03;

	/*
	  params of Al5083 = [-1.44774526e-15 -2.19696955e-02 -4.92771400e-01  6.22685769e+00
	 -2.57418791e+01  4.92983577e+01 -4.48910912e+01  1.91732252e+01
 	  2.79009273e+00  1.27168991e-03]
	*/

	double m[2] = {101.83, 1214.2857};
	double n[2] = {528.682276, 2071127.271018};
    double log_T;
    double f_exp;
	int k;

	//0.38475e3 or 0.5130e3

	if (strcmp("CFUN_CvAlAlloy_v1", func) == 0) {
		if (nArgs != 1) {
			error = "One argument expected";
			return 0;
			}

        for (k = 0; k < blockSize; k++) {

		    double T = inReal[0][k];


			if(T < 0)     {
				    error = "T is negative!"; return 0;
			    }

			if(T > 1400){
				    error = "Temperature is above 1400 K - out of range!";
				    return 0;
			    }


            if(T > 6 && T < 300) {
				log_T = log10(T);
                f_exp =  a*pow(log_T,8) + b*pow(log_T,7) + c*pow(log_T,6) + d*pow(log_T,5) + e*pow(log_T,4) + f*pow(log_T,3) + g*pow(log_T,2) + h*log_T +i;
                outReal[k] = j*pow(10,f_exp);
            }

			if(T <= 6 && T > 4) {
				//linear approximation
				outReal[k] = m[0]*T + n[0];
	  		}
			if(T <= 4 ) {
				//keep constant value
				outReal[k] = 936.002276;
	  		}
			if(T >= 300) {
				//linear approximation
				outReal[k] = m[1]*T + n[1];
	  		}


			/*Output consistency check*/
			if(outReal[k]!=outReal[k]){
				error = "Output is nan";
				return 0;
			}
			if (fabs(outReal[k])>DBL_MAX){
				error = "Output is inf";
				return 0;
			}
			}

    return 1;
}

}