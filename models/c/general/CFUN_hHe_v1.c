#include <math.h>
#include <float.h>
#include <stdlib.h>
#include <string.h>
#ifdef _MSC_VER
#define EXPORT __declspec(dllexport)
#else
#define EXPORT
#endif

static const char *error = NULL;

EXPORT int init(const char *str) {
  return 1;
}

EXPORT const char * getLastError() {
  return error;
}

EXPORT const char* return_test_yaml() {
    return "CFUN_hHe_v1";
}


EXPORT int eval(const char *func,
                              int nArgs,
                              const double **inReal,
                              const double **inImag,
                              int blockSize,
                              double *outReal,
                              double *outImag) {
    
    int i;
    double c1       = 500;   
    double c2       = 5e4;   
    double c3       = 250;
    double p        = 3.4; 
    double h1s      = 10 ;   
    double h2s      = 1e4;
    double c5       = 600;   
    double c6       = 250;
    double h5       = 10 ;   
    double h6       = 1e4;  
    double Pmax     = 35e3; 

    if (strcmp("CFUN_hHe_v1", func) == 0) {
        if (nArgs != 2) {
        error = "Two argument expected";
        return 0;}
        }
    //It returns heat transfer coefficient of He in units [W/(K*m^2)]
    // Source: https://www.sciencedirect.com/science/article/pii/S0011227506000154
    for (i = 0; i < blockSize; i++) {
      double T        = inReal[0][i]; //External Temperature
      double THe      = inReal[1][i];

      if(T < 0)                {error = "T is negative!"; return 0; }
      if(THe < 0)                {error = "THe is negative!"; return 0; }
      if(THe > 5)                {error = "THe is >5K!"; return 0; }
      if(T <= THe){ outReal[i] = 0.0; }
      else {
          if(THe > 2.2 && THe < 5){
            double dThelium = T - THe;
            double T_star_2 = pow(h2s / c2, 0.4) + THe;

            // Helium I
            double h1 = c1 * (dThelium);
            double h2 = c2 * pow(dThelium, 2.5) * (1 - (1 / (1 + exp(-(T - T_star_2) * 100))));
            double h3 = c3 * (dThelium) * (1 / (1 + exp(-(T - T_star_2) * 100)));
            outReal[i] = (h1 + h2 + h3) / dThelium;
          }
          if(THe <= 2.2){

            double Tstar    = ((Pmax / c5) + pow(THe, p));
            Tstar    = pow(Tstar, 1 / p) - THe;

            // Helium II
            double dThelium = T - THe;
            double dTstar = dThelium - Tstar;

            double sig_Tstar = 1 / (1 + exp(-dTstar * 100));
            double h5 = (c5 * (pow(T, p) - pow(THe, p))) * (1 - sig_Tstar);
            double h6 = (c6 * dThelium) * sig_Tstar;
            outReal[i] =  (h5+h6) / dThelium;
            }
        }
      

        /*Output consistency check*/
      if(outReal[i]!=outReal[i]){
        error = "Output is nan"; 
        return 0;	  
        }
      if (fabs(outReal[i])>DBL_MAX){
        error = "Output is inf"; 
        return 0;	  
        }
    }

    return 1;

}

