#include <math.h>
#include <float.h>
#include <stdlib.h>
#include <string.h>
#ifdef _MSC_VER
#define EXPORT __declspec(dllexport)
#else
#define EXPORT
#endif

static const char *error = NULL;

EXPORT int init(const char *str) {
  return 1;
}

EXPORT const char * getLastError() {
  return error;
}

EXPORT const char* return_test_yaml() {
    return "CFUN_CvCu_NIST_v1";
}

EXPORT int eval(const char *func,
                              int nArgs,
                              const double **inReal,
                              const double **inImag,
                              int blockSize,
                              double *outReal,
                              double *outImag) {
  int i, j;
  double dc_a = -1.91844;
  double dc_b = -0.15973;
  double dc_c = 8.61013;
  double dc_d =-18.996;
  double dc_e = 21.9661;
  double dc_f =-12.7328;
  double dc_g =  3.54322;
  double dc_h = -0.3797;
  double log_T, exp, CvCu;
  double density = 8960;
  
  if (strcmp("CFUN_CvCu_NIST_v1", func) == 0) {
    if (nArgs != 1) {
      error = "One argument expected";
      return 0;
    }

    for (i = 0; i < blockSize; i++) {
		
	  double T = inReal[0][i];
	  
	  /*Input consistency check with hotfix for negative T*/
	  if(T < 0)     {error = "T is negative!"; return 0; } 

	  if(T <= 1) { CvCu = 1.081105332928963e+02; }
	  
    else if(T > 1 && T <= 300) {

        log_T = log10(T);
        exp = dc_h*pow(log_T,7) + dc_g*pow(log_T,6) + dc_f*pow(log_T,5) + dc_e*pow(log_T,4) + dc_d*pow(log_T,3) + dc_c*pow(log_T,2) + dc_b*log_T + dc_a;
        CvCu = density*pow(10, exp);

      } 
      else if (T > 300) {

        CvCu = density*(361.5 + 0.093*T);

      } 

      outReal[i] = CvCu;

	  /*Output consistency check*/
	  if(outReal[i]!=outReal[i]){
		error = "Output is nan"; 
		return 0;	  
	  }
	  if (fabs(outReal[i])>DBL_MAX){
		error = "Output is inf"; 
		return 0;	  
	  }
    }
    return 1;
  }

  else {
    error = "Unknown function";
    return 0;
  }
}