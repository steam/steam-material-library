#include <math.h>
#include <float.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#ifdef _MSC_VER
#define EXPORT __declspec(dllexport)
#else
#define EXPORT
#endif

/* Computes the critical current density of a Faraday Factory Japan 2G HTS tape in A/m^2.

This fit follows the equations presented in
G. Succi, A. Ballarino, S. C. Hopkins, C. Barth and Y. Yang, "Magnetic Field and Temperature Scaling of the Critical Current of REBCO Tapes," 
in IEEE Transactions on Applied Superconductivity, vol. 34, no. 3, pp. 1-7, May 2024, Art no. 8001007, doi: 10.1109/TASC.2024.3370137.

Some modifications have been made by M. Wozniak, CERN as marked in the source code below.

The data is fit against critical current data in A from https://htsdb.wimbush.eu/dataset/23536374 from the 
'The Robinson Research Institute High-Temperature Superconducting Wire Critical Current Database' for a Faraday Factory Japan YBCO 2G HTS. The fitting parameters have been computed by curve fitting in python, the code is found at https://gitlab.cern.ch/steam/analyses/asc24-hts-pancake3d.

The Jc data in A/m^2 is computed from the Ic data by dividing by the cross-section of the tape that is given by
the width of 4mm reported by the the Robinson Research Institute data set and assuming a thickness of the YBCO of 2.4mum following Molodyk, A., Samoilenkov, S., Markelov, A. et al. "Development and large volume production of extremely high current density YBa2Cu3O7 superconducting wires for fusion". Sci Rep 11, 2084 (2021). https://doi.org/10.1038/s41598-021-81559-z.

The fit is not a function of angle but rather an angle of 0 deg of the field to the tape normal, i.e., applied field normal to the tape, is used.
*/


// Note: this temperature scaling is not explicitly mentioned in above literature reference. This is an extension by M. Wozniak.
double B0_T(double B0, double Tc0, double T) {
    return B0 * (1 - T / Tc0);
}

// Note: birr_pow is not in the original scaling. This is an extension by M. Wozniak.
double Birr_T(double Birr, double Tc0, double T, double birr_pow) {
    return Birr * pow(1 - T / Tc0, birr_pow);
}

double Ic0_T(double Ic0, double Tc0, double gamma, double T) {
    return Ic0 * pow(1 - T / Tc0, gamma);
}

static const char *error = NULL;


EXPORT int init(const char *str) {
  return 1;
}

EXPORT const char * getLastError() {
  return error;
}

EXPORT const char* return_test_yaml() {
    return "CFUN_HTS_JcFit_Succi_v2";
}

EXPORT int eval(const char *func,
                int nArgs,
                const double **inReal,
                const double **inImag,
                int blockSize,
                double *outReal,
                double *outImag) {
 
    if (nArgs != 2) {
        error = "CFUN_HTS_JcFit_Succi expects two arguments";
        return 0;
    }

    if(strcmp("CFUN_HTS_JcFit_Succi_v2", func) != 0) {
        error = "Unknown function";
        return 0;
    }
	
    // Fit Parameters
    double Ic0 = 6098.704849288037;
    double B0 = 0.5769453001599469;
    double Birr = 199.99999999999997;
    double Tc0 = 85.00000000000001;
    double alpha = 0.4817498734548764;
    double q = 2.0;
    double gamma = 1.45;
    double birr_pow = 1.4;

    // Tape cross-section parameters
    double tape_width = 4E-3;
    double YBCO_thickness = 2.4E-6;

    // Global index
    int i; 

    for (i = 0; i < blockSize; i++) {
        // Raw Input
        double Top_IN   = inReal[0][i];	
        double Bnorm_IN = inReal[1][i];

        // temperature above Tc0
        if (Top_IN >= Tc0) {
            outReal[i] = 0;
            continue;
        }

        double Birr_T_val = Birr_T(Birr, Tc0, Top_IN, birr_pow);

        // B above Birr_T
        if (Bnorm_IN >= Birr_T_val) {
            outReal[i] = 0;
            continue;
        }

        double Ic0_T_val = Ic0_T(Ic0, Tc0, gamma, Top_IN);
        double B0_T_val = B0_T(B0, Tc0, Top_IN);

        double Ic = Ic0_T_val * pow(1 + Bnorm_IN / B0_T_val, -alpha) * pow(1 - Bnorm_IN / Birr_T_val, q);
        
        double Jc = Ic/(tape_width * YBCO_thickness);

        outReal[i] = Jc;
        
        //Consistency check: output
        if(outReal[i]!=outReal[i]){
            error = "Output is nan"; 
            return 0;	  
        }
        
        if (fabs(outReal[i])>DBL_MAX){
            error = "Output is inf"; 
            return 0;	  
        }
    }
	
    return 1;
}