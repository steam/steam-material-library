#include <math.h>
#include <float.h>
#include <stdlib.h>
#include <string.h>
#ifdef _MSC_VER
#define EXPORT __declspec(dllexport)
#else
#define EXPORT
#endif

static const char *error = NULL;

EXPORT int init(const char *str) {
  return 1;
}

EXPORT const char * getLastError() {
  return error;
}

EXPORT const char* return_test_yaml() {
    return "CFUN_kSteel_v1";
}

EXPORT int eval(const char *func,
                              int nArgs,
                              const double **inReal,
                              const double **inImag,
                              int blockSize,
                              double *outReal,
                              double *outImag) {
    int i;
    double a = -1.4087; // 1/log(K)^8
    double b = 1.3982;  // 1/log(K)^7
    double c = 0.2543;  // 1/log(K)^6
    double d = -0.6260; // 1/log(K)^5
    double e = 0.2334;  // 1/log(K)^4
    double f = 0.4256;  // 1/log(K)^3
    double g = -0.4658; // 1/log(K)^2
    double h = 0.1650;  // 1/log(K)
    double l = -0.0199; // 1
    double m = 0.0265;
    double n = 7.358654;
    double log10y, log10_T;

  if (strcmp("CFUN_kSteel_v2", func) == 0) {
    if (nArgs != 1) {
      error = "One argument expected";
      return 0;
    }

    for (i = 0; i < blockSize; i++) {
		
	  double T = inReal[0][i];
	  
	  /*Input consistency check with hotfix for negative T*/
	  if(T < 0)     {error = "T is negative!"; return 0; } 	  
	  if(T > 1400) { error = "Temperature is above 1400 K - out of range!"; return 0;}
	  
	  if(T > 0 && T < 300){
        log10_T = log10(T);

        log10y = a + b * log10_T + c * pow(log10_T, 2) + d * pow(log10_T, 3) + e * pow(log10_T, 4) + f * pow(log10_T, 5) + g * pow(log10_T, 6) + h * pow(log10_T, 7) + l * pow(log10_T, 8);
	  	outReal[i] = pow(10, log10y);
	  }

	  if(T >= 300){
		outReal[i] = m*T + n;
	  }

	  /*Output consistency check*/
	  if(outReal[i]!=outReal[i]){
		error = "Output is nan"; 
		return 0;	  
	  }
	  if (fabs(outReal[i])>DBL_MAX){
		error = "Output is inf"; 
		return 0;	  
	  }
    }
    return 1;
  }

  else {
    error = "Unknown function";
    return 0;
  }
}