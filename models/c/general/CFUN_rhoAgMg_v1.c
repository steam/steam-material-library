#include <math.h>
#include <float.h>
#include <stdlib.h>
#include <string.h>
#ifdef _MSC_VER
#define EXPORT __declspec(dllexport)
#else
#define EXPORT
#endif

static const char *error = NULL;

EXPORT int init(const char *str) {
  return 1;
}

EXPORT const char * getLastError() {
  return error;
}

EXPORT const char* return_test_yaml() {
    return "CFUN_rhoAgMg_v1";
}

EXPORT int eval(const char *func,
                              int nArgs,
                              const double **inReal,
                              const double **inImag,
                              int blockSize,
                              double *outReal,
                              double *outImag) {
  
  int i;
  double rho_n = 3.84e-9;;
  double rho_0;
  double rho_i;
  double rho_i0;
  double x;
  double log_x; 
  double f_exp;
  double corr;
  
  //Constants
  double c0 =  1.97e-8;  // Silver resistivity at 273 K (Smith/Fickett)
  double Tref_RRR = 273.0;

  double P1 = 3.51121e-15;	
  double P2 = 3.40152;
  double P3 = 2.04974e10;    
  double P4 = 1.01777;    
  double P5 = 40.0;  
  double P6 = 3.16452;   
  double P7 = 0;	  

  double a0 = -4.36736;
  double a1 =  2.31218;
  double a2 = -0.0966798;
  double a3 = -0.0931658;
  double a4 =  0.0151959;

  
	
  if (strcmp("CFUN_rhoAgMg_v1", func) == 0) {
    if (nArgs != 2) {
      error = "Four arguments expected";
      return 0;
    }

    for (i = 0; i < blockSize; i++) {
	  double T       = inReal[0][i];
	  double B       = inReal[1][i];
	  // double RRR     = inReal[2][i];
	  // double Tup_RRR = inReal[3][i];
	  // double c0_scale = Tup_RRR/Tref_RRR;
	  
	  /*Input consistency check with hotfix for negative T*/
		// if(B < 0)       {error = "normB is negative!"; return 0; }
		// if(RRR < 0)     {error = "RRR is negative!"; return 0; }
		// if(Tup_RRR < 0) {error = "Tup_RRR is negative!"; return 0; }  
	
    double b = fabs(B);
	  
	  // rho_0 = c0_scale * c0 / RRR;		
    rho_0 = 3.84e-9;  
	     	  		
	  rho_i = P1 * pow(T,P2) / ( 1 + P1*P3*pow(T,(P2-P4)) * exp( -pow(P5/T,P6)) );
	  
	  rho_i0 = P7 * rho_i * rho_0 / (rho_i + rho_0);

	  rho_n = rho_0 + rho_i + rho_i0;
	  
	  if (b > 0.01){
		  x = c0 * b / rho_n;
	    log_x = log10(x);	  
	    f_exp = a0 + a1*log_x + a2*pow(log_x,2) + a3*pow(log_x,3) + a4*pow(log_x,4);
		  corr = pow(10,f_exp);
	  }
	  else{
		  corr=0;
		}
 
	  outReal[i] = rho_n * (1+corr);
	  

	  /*Output consistency check*/
	  if(outReal[i]!=outReal[i]){
		error = "Output is nan"; 
		return 0;	  
	  }
	  if (fabs(outReal[i])>DBL_MAX){
		error = "Output is inf"; 
		return 0;	  
	  }	
	  
    }
    return 1;
  }

  else {
    error = "Unknown function";
    return 0;
  }
}