#include <math.h>
#include <float.h>
#include <stdlib.h>
#include <string.h>

#ifdef _MSC_VER
#define EXPORT __declspec(dllexport)
#else
#define EXPORT
#endif

static const char *error = NULL;

EXPORT int init(const char *str) {
    return 1;
}

EXPORT const char *getLastError() {
    return error;
}

EXPORT const char* return_test_yaml() {
    return "CFUN_fisc_I_Ic_n_rhom_Am_BSCCO2212_v1";
}

EXPORT int eval(const char *func,
               int nArgs,
               const double **inReal,
               const double **inImag,
               int blockSize,
               double *outReal,
               double *outImag) {

    double alpha;
    double lambda;
    double lambda_MIN; 
    double lambda_MAX;
    double fun;
    double err;
    int    NO_OF_MAX_ITER = 100; 
    int i;
    double f_isc_I_Ic_n;
    double Ec0 = 0.0001; // [V/m]

    if (strcmp("CFUN_fisc_I_Ic_n_rhom_Am_BSCCO2212_v1", func) == 0) {
        if (nArgs != 5) {
            error = "Five arguments expected";
            return 0;
        }

        for (i = 0; i < blockSize; i++) {
            double I = inReal[0][i];
            double Ic = inReal[1][i];
            double n = inReal[2][i];
            double rho_m = inReal[3][i];
            double A_matrix = inReal[4][i];

            // Modify the inputs
            if (I < 0)
                I = -I; // Direction of the current is unimportant
            if (Ic < 0)
                Ic = 0; // Critical current must be positive

            // Calculate f_isc_I_Ic_n
            if (I == 0) {
                f_isc_I_Ic_n = 1;
            } else if (Ic <= 0.001 * I) {
                f_isc_I_Ic_n = 0;
            } else {
                // Binary search algorithm
                lambda_MIN = 0;
                lambda_MAX = 1;

                // Inner iterations
                for (int iter_i = 0; iter_i < NO_OF_MAX_ITER; iter_i++) {
                lambda = (lambda_MIN + lambda_MAX) * 0.5;
                fun    = I * (lambda - 1) + pow(I * lambda / Ic, n) * Ec0 * A_matrix / rho_m ;
                err    = lambda_MAX - lambda_MIN;

                if (err > 1e-20) {
                    if (fun >= 0) {
                        lambda_MAX = lambda;
                    }
                    else if (fun < 0) {
                        lambda_MIN = lambda;
                    }
                }
                else {
                    break;
                }
            }

            f_isc_I_Ic_n = lambda;
        }    
        outReal[i] = f_isc_I_Ic_n;

        }

    

        return 1;
    } else {
        error = "Unknown function";
        return 0;
    }
}

