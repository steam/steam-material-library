#include <math.h>
#include <float.h>
#include <stdlib.h>
#include <string.h>
#ifdef _MSC_VER
#define EXPORT __declspec(dllexport)
#else

#define EXPORT
#endif

static const char *error = NULL;

EXPORT int init(const char *str) {
  return 1;
}

EXPORT const char * getLastError() {
  return error;
}

EXPORT const char* return_test_yaml() {
    return "CFUN_Jc_T_B_BSCCO2212_Power_Jiang_Wesche_v1";
}

EXPORT int eval(const char *func,
                              int nArgs,
                              const double **inReal,
                              const double **inImag,
                              int blockSize,
                              double *outReal,
                              double *outImag) {
								  
    double a1       = 448.31;
    double a2       = 0.1154;

    double Tc_B, T0 = 4.2;
    double PI_4     = 3.14159265358979323846/4.0;

    double b1 = 852.002 / (PI_4 * 0.0012*0.0012 * 0.25 );
    double b2 = 0.191301;
    
    double Jc_T0_B;
    double Jc_T_B;
    int i;
  
    if (strcmp("CFUN_Jc_T_B_BSCCO2212_Power_Jiang_Wesche_v1", func) == 0) {
        if (nArgs != 3) {
          error = "Three arguments expected";
          return 0;
        }

        for (i = 0; i < blockSize; i++) {  	 
        
            double T           = inReal[0][i];
	        double B           = inReal[1][i];
	        double f_scaling   = inReal[2][i];

    	        /*Input consistency check*/
	        if(T < 0)              {error = "T is negative!"; return 0; } 
            if(B < 0)      {error = "Jc_Nb3Sn0 is negative!"; return 0; }

            // Modify the input magnetic field
            if(B<0){
                B=-B; // direction of the magnetic field is unimportant
            }
            if(fabs(B)<0.2){
                B=0.2; // very small magnetic field causes numerical problems
            }
            if (T == 0) {
                T =0.001; // temperature equal to zero causes numerical problems
            }

            Tc_B = (log(a1) - log(B)) / a2; // [K]
            Jc_T0_B = b1 * pow(B, -b2);
            
            if (T >= Tc_B) {
              Jc_T_B = 0.0;
            } 
            else {
              Jc_T_B = Jc_T0_B * (Tc_B - T) / Tc_B; // [A/m^2]
            }
            
            // Nonreal negative values
            if (Jc_T_B < 0) {
              Jc_T_B = 0.0;
            }
            // Scale this fit
            outReal[i] = Jc_T_B * f_scaling; // [A/m^2]
            if (isnan(outReal[i])) {
              error = "Output is NaN";
              return 0;
            }
        }
     return 1;

    }

    else {
      error = "Unknown function";
      return 0;
    }
}