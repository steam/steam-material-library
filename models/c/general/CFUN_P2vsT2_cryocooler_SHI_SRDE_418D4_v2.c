#include <math.h>
#include <float.h>
#include <stdlib.h>
#include <string.h>
#ifdef _MSC_VER
#define EXPORT __declspec(dllexport)
#else
#define EXPORT
#endif

static const char *error = NULL;

EXPORT int init(const char *str) {
return 1;
}

EXPORT const char * getLastError() {
return error;
}

EXPORT const char* return_test_yaml() {
    return "CFUN_P2vsT2_cryocooler_SHI_SRDE_418D4_v2";
}

// This function computes the 2nd stage cooling power of the 2 stage crocooler RDE-418D4 4K with a second-stage capacity of 1.8 W @ 4.2 K (50 Hz).
// Output is 2nd stage power in Watts. Argument is 2nd stage temperature in K.
// Values are for 50 Hz.
// Fit of capacity map from https://www.shicryogenics.com/product/rde-418d4-4k-cryocooler-series/.
// Extrapolation to reach 72 W at 300 K.

double sigmoid_mid_T(double x) {
    double alpha = 0.5;  // 1 / width
    double midpoint = 25.0;
    return 1.0 / (1.0 + exp(-alpha * (x - midpoint)));
}

double power_function(double x) {
    const double poly1_coeffs[] = { -5.5723402028, 2.0408162794, -0.067973562, 0.0009917441 };  // Coefficients of first polynomial (3rd order)
    const double poly2_coeffs[] = { 8.7255984496, 0.4201904795, -0.0007020974 };           // Coefficients of second polynomial (1st order)
    int o1 = 4;  // Order of first polynomial
    int o2 = 3;  // Order of second polynomial
    double zero_power = 3.15;

    double sigmoid_blend_mid_T = sigmoid_mid_T(x);
    double poly1_power = 0.0;
    double poly2_power = 0.0;
    double power = 0.0;

    for (int i = 0; i < o1; ++i) {
        poly1_power += poly1_coeffs[i] * pow(x, i);
    }

    for (int i = 0; i < o2; ++i) {
        poly2_power += poly2_coeffs[i] * pow(x, i);
    }

    power = (1 - sigmoid_blend_mid_T) * poly1_power + sigmoid_blend_mid_T * poly2_power;
    
    if (power > 0) {
        // No operation, keep the power as is
    }
    else {
        power = 0.0;
    }

    return power;
}


EXPORT int eval(const char *func,
                              int nArgs,
                              const double **inReal,
                              const double **inImag,
                              int blockSize,
                              double *outReal,
                              double *outImag) {

	int i;

	if (strcmp("CFUN_P2vsT2_cryocooler_SHI_SRDE_418D4_v2", func) == 0) {
		if (nArgs != 1) {
			error = "One argument expected";
			return 0;
			}

    for (i = 0; i < blockSize; i++) {
			
			double T = inReal[0][i];

			outReal[i] = power_function( T );
	
			/*Output consistency check*/
			if(outReal[i]!=outReal[i]){
				error = "Output is nan"; 
				return 0;	  
			}
			if (fabs(outReal[i])>DBL_MAX){
				error = "Output is inf"; 
				return 0;	  
			}
			}
	
    return 1;
	}
}