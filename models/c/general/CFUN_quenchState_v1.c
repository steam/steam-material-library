#include <math.h>
#include <float.h>
#include <stdlib.h>
#include <string.h>
#ifdef _MSC_VER
#define EXPORT __declspec(dllexport)
#else
#define EXPORT
#endif

static const char *error = NULL;

EXPORT int init(const char *str) {
  return 1;
}

EXPORT const char * getLastError() {
  return error;
}

EXPORT const char* return_test_yaml() {
    return "CFUN_quenchState_v1";
}

EXPORT int eval(const char *func,
                              int nArgs,
                              const double **inReal,
                              const double **inImag,
                              int blockSize,
                              double *outReal,
                              double *outImag) {
  
  double x;
  int i;  
  
  double noQuench = 0.0;
  double allQuench = 1.0;
   
  if (strcmp("CFUN_quenchState_v1", func) == 0) {
    if (nArgs != 2) {
      error = "Two arguments expected";
      return 0;
    }

    for (i = 0; i < blockSize; i++) {	  
	//Correction for negative values
	double Is    = fabs(inReal[0][i]); 
	double Icrit = fabs(inReal[1][i]);
	
	if(Is>0){    	
		// Icrit>Is           => x<0    noQuench
		// Icrit<Is & Icrit>0 => 0<x<1  currentShare
		// Icrit=0            => x=1    allQuench
		x = 1 - Icrit/Is;                           

				
		if (x <= noQuench){
			// noQuench
			outReal[i] = 0;	
		}	
		else if (x >= allQuench){
			// allQuench
			outReal[i] = 1;
		}
		else{
			//currentShare
			outReal[i]=x*x*(-2*x+3);
			}				
	}		
	else if (Is==0&&Icrit>0){
		// No Is, No Quench		
		outReal[i] = 0;
	}
	else if (Is==0&&Icrit==0){  
		// No Is, Quench	
		outReal[i] = 1;
		}
				
	/*Output consistency check*/
	if(outReal[i]!=outReal[i]){
    error = "Output is nan"; 
	return 0;	  
	}
	if (fabs(outReal[i])>DBL_MAX){
    error = "Output is inf"; 
	return 0;	  
	}
	
    }	
    return 1;
  }

  else {
    error = "Unknown function";
    return 0;
  }
}
