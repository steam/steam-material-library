#include <math.h>
#include <float.h>
#include <stdlib.h>
#include <string.h>
#ifdef _MSC_VER
#define EXPORT __declspec(dllexport)
#else
#define EXPORT
#endif

static const char *error = NULL;

EXPORT int init(const char *str) {
return 1;
}

EXPORT const char * getLastError() {
return error;
}

EXPORT const char* return_test_yaml() {
    return "CFUN_CvIn_v0";
}

double CvIn(double T) {
	double CvIn;
	double a = -1.447745258023e-15;
	double b = 0.07300632793020582;
	double c = -0.42974638696637013;
	double d = 2.291682473087e-07;
	double e = 4.520345939117079;
	double f = -8.02873414414318;
	double g = -6.521067548904495;
	double h = 27.822617601882705;
	double i = 3.286498601155173e-05;
	double j = 171849.4628353198;
	double k = 4.1952196502328035e-14;

	T = log10(T);

    double f_exp = a*pow(T,8) + b*pow(T,7) + c*pow(T,6) + pow(d,pow(T,5)) + e*pow(T,4) + f*pow(T,3) + g*pow(T,2) + h*T + i; 

	CvIn = k * pow(10, f_exp) + j; 

	return CvIn;
}


EXPORT int eval(const char *func,
                              int nArgs,
                              const double **inReal,
                              const double **inImag,
                              int blockSize,
                              double *outReal,
                              double *outImag) {

	int i;

	if (strcmp("CFUN_CvIn_v0", func) == 0) {
		if (nArgs != 1) {
			error = "One argument expected";
			return 0;
			}

    for (i = 0; i < blockSize; i++) {
			
			double T = inReal[0][i];

			if(T < 15)     {
				error = "Temperature is below 15 K - out of range!"; 
                return 0; 
			} 
	
			if(T > 300){
				error = "Temperature is above 300 K - out of range!";
				return 0;	  
			}
	
			
			outReal[i] = CvIn( T );
	
			/*Output consistency check*/
			if(outReal[i]!=outReal[i]){
				error = "Output is nan"; 
				return 0;	  
			}
			if (fabs(outReal[i])>DBL_MAX){
				error = "Output is inf"; 
				return 0;	  
			}
			}
	
    return 1;
}

/*else {
    error = "Unknown function";
    return 0;
}*/
}