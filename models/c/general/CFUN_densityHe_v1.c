#include <math.h>
#include <float.h>
#include <stdlib.h>
#include <string.h>
#ifdef _MSC_VER
#define EXPORT __declspec(dllexport)
#else
#define EXPORT
#endif

static const char *error = NULL;

EXPORT int init(const char *str) {
  return 1;
}

EXPORT const char * getLastError() {
  return error;
}

EXPORT const char* return_test_yaml() {
    return "CFUN_densityHe_v1";
}

EXPORT int eval(const char *func,
                              int nArgs,
                              const double **inReal,
                              const double **inImag,
                              int blockSize,
                              double *outReal,
                              double *outImag) {
  
  	
    double polyFit1[4] = {2.11416939375024,-1.52598215218988,0.376561004541185,2.13605330409407};
    double polyFit2[5] = {-13.2534629276111,22.3402757369487,-14.6074099333166,4.25768883178274,1.71041439003585};
    double polyFit3[6] = {-11.3703467424725,57.8805160101170,-117.249581388477,118.438559118732,-60.9051212476063,13.9060268302894};
    double polyFit4[3] = {-0.000343946768435402,-0.997784222538021,1.68461132135252};
    double densityHe;
	int i;

  if (strcmp("CFUN_densityHe_v1", func) == 0) {
    if (nArgs != 1) {
      error = "One argument expected";
      return 0;
    }

    for (i = 0; i < blockSize; i++) {
		
	    double T          = inReal[0][i];
	    
        if (T <= 1.6) {
            densityHe = 146.9;
        } else if (T > 1.6 && T <= 2.167) {
            densityHe = pow(10, polyFit1[3] + polyFit1[2] * log10(T) + polyFit1[1] * pow(log10(T), 2) + polyFit1[0] * pow(log10(T), 3) );
        } else if (T > 2.167 && T <= 4.222) {
            densityHe = pow(10, polyFit2[4] + polyFit2[3] * log10(T) + polyFit2[2] * pow(log10(T), 2) + polyFit2[1] * pow(log10(T), 3) + polyFit2[0] * pow(log10(T), 4));
        } else if (T > 4.222 && T <= 20) {
            densityHe = pow(10, polyFit3[5] + polyFit3[4] * log10(T) + polyFit3[3] * pow(log10(T), 2) + polyFit3[2] * pow(log10(T), 3) + polyFit3[1] * pow(log10(T), 4) + polyFit3[0] * pow(log10(T), 5));
        } else if (T > 20 && T <= 1500) {
            densityHe = pow(10, polyFit4[2] + polyFit4[1] * log10(T) + polyFit4[0] * pow(log10(T), 2));
        } else {
            densityHe = 3.25e-02;
        }

        outReal[i] = densityHe;
		
		/*Output consistency check*/
		if(outReal[i]!=outReal[i]){
			error = "Output is nan"; 
			return 0;	  
			}
		if (fabs(outReal[i])>DBL_MAX){
		error = "Output is inf"; 
		return 0;	  
		}
		}
		return 1;
  }

}