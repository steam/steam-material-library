#include <math.h>
#include <float.h>
#include <stdlib.h>
#include <string.h>
#ifdef _MSC_VER
#define EXPORT __declspec(dllexport)
#else
#define EXPORT
#endif

static const char *error = NULL;

EXPORT int init(const char *str) {
  return 1;
}

EXPORT const char * getLastError() {
  return error;
}

EXPORT const char* return_test_yaml() {
    return "CFUN_rhoSS_v1";
}

EXPORT int eval(const char *func,
                              int nArgs,
                              const double **inReal,
                              const double **inImag,
                              int blockSize,
                              double *outReal,
                              double *outImag) {
  
  	double params1[4] = {-6.16E-15/ 1.0867, 3.52E-12/ 1.0867, 1.72E-10/ 1.0867, 5.43E-07/ 1.0867};
    double params2[2] = {7.24E-10/ 1.0867,  5.2887E-7/ 1.0867};
    double rhoSS;
	int i;

  if (strcmp("CFUN_rhoSS_v1", func) == 0) {
    if (nArgs != 1) {
      error = "One argument expected";
      return 0;
    }

    for (i = 0; i < blockSize; i++) {
		
	    double T = inReal[0][i];

      if(T < 0){return 0;}

      if(T >= 0 && T <= 300) {
          rhoSS = params1[0] * pow(T, 3) + params1[1] * T * T + params1[2] * T + params1[3];
        }

      if(T > 300) {
          rhoSS = params2[0] * T + params2[1];
        }

      outReal[i] = rhoSS;

      if(T > 1400){
          error = "Temperature is above 1400 K - outside of the range!";
          return 0;	  
        }
    
      if(outReal[i]!=outReal[i]){
        error = "Output is nan"; 
        return 0;	  
      }
      if (fabs(outReal[i])>DBL_MAX){
        error = "Output is inf"; 
        return 0;	  
      }
	  }
		return 1;
  }

}