#include <math.h>
#include <float.h>
#include <stdlib.h>
#include <string.h>
#ifdef _MSC_VER
#define EXPORT __declspec(dllexport)
#else
#define EXPORT
#endif

static const char *error = NULL;

EXPORT int init(const char *str) {
  return 1;
}

EXPORT const char * getLastError() {
  return error;
}

EXPORT const char* return_test_yaml() {
    return "CFUN_TcsNbTi_v1";
}

EXPORT int eval(const char *func,
                              int nArgs,
                              const double **inReal,
                              const double **inImag,
                              int blockSize,
                              double *outReal,
                              double *outImag) {

  double Bc20 = 14.5;
  double Tc0  = 9.2;
  int i;

  if (strcmp("CFUN_TcsNbTi_v1", func) == 0) {
    if (nArgs != 4) {
      error = "Four arguments expected";
      return 0;
    }

    for (i = 0; i < blockSize; i++) {
		
	  double B   = inReal[0][i];
	  double I   = inReal[1][i];
	  double C1  = inReal[2][i];
    double C2  = inReal[3][i];
	  double Tcs = 0;
	  
	  /*Input consistency check*/
	  if(B<0){error = "normB is negative!"; return 0;}
	  
	  	if(B <= Bc20){
			if (fabs(I)/(C1+C2*B)>=0){
				if (fabs(I)/(C1+C2*B)<=1){
					Tcs = (1-fabs(I)/(C1+C2*B))*Tc0*pow((1-B/Bc20),0.59);
				}	
			}	
		}
		outReal[i]=Tcs;
		
	  /*Output consistency check*/
	  if(outReal[i]!=outReal[i]){
		error = "Output is nan"; 
		return 0;	  
	  }
	  // if (fabs(outReal[i])>DBL_MAX){
		// error = "Output is inf"; 
		// return 0;	  
	  // }	
    }
    return 1;
  }

  else {
    error = "Unknown function";
    return 0;
  }
}
