#include <math.h>
#include <float.h>
#include <stdlib.h>
#include <string.h>
#ifdef _MSC_VER
#define EXPORT __declspec(dllexport)
#else
#define EXPORT
#endif

static const char *error = NULL;

EXPORT int init(const char *str) {
  return 1;
}

EXPORT const char * getLastError() {
  return error;
}

EXPORT const char* return_test_yaml() {
    return "CFUN_rhoAl6061_v1";
}

EXPORT int eval(const char *func,
                              int nArgs,
                              const double **inReal,
                              const double **inImag,
                              int blockSize,
                              double *outReal,
                              double *outImag) {
  
  	double a = -1.44774526e-15;
    double b = -2.84892945e-02;  
    double c =  3.84011935e-04;
    double d =  1.05167127e+00;
    double e = -3.97794420e+00;
    double f =  6.07489999e+00;
	double g = -4.23491790e+00;
	double h = -6.31876879e-01;
	double k =  7.69246458e-07;

	//linear regression T > 300
	double l = 0.01219e-8;
	double m = 0.5785e-8;

    double p = 1.3913*1e-8;
	
    double log_T;
    double f_exp; 
	int i;

  if (strcmp("CFUN_rhoAl6061_v1", func) == 0) {
    if (nArgs != 1) {
      error = "One argument expected";
      return 0;
    }

    for (i = 0; i < blockSize; i++) {
		
	    double T = inReal[0][i];
	    log_T = log10(T);

		if(T < 0){
				error = "T is negative!"; return 0; 
			} 
		
		if(T > 1400){
				error = "Temperature is above 1400 K - outside of the range!";
				return 0;	  
			}
		if( T <= 20 && T >= 0 ) {
				p = log10(20);
				f_exp = a*pow(p,7) + b*pow(p,6) + c*pow(p,5) + d*pow(p,4) + e*pow(p,3) + f*pow(p,2) + g*p + h;
				outReal[i] = k*pow(10,f_exp);
			}
				
		if(T > 20 && T < 300) {
				f_exp = a*pow(log_T,7) + b*pow(log_T,6) + c*pow(log_T,5) + d*pow(log_T,4) + e*pow(log_T,3) + f*pow(log_T,2) + g*log_T + h;
				outReal[i] = k*pow(10,f_exp);
			}
		if(T >= 300) {
			p = log10(300);
			f_exp = a*pow(p,7) + b*pow(p,6) + c*pow(p,5) + d*pow(p,4) + e*pow(p,3) + f*pow(p,2) + g*p + h;
			m = k*pow(10,f_exp) - l*300;
			outReal[i] = l*T + m;
		}
			/*Output consistency check*/
		if(outReal[i]!=outReal[i]){
			error = "Output is nan"; 
			return 0;	  
			}
		if (fabs(outReal[i])>DBL_MAX){
		error = "Output is inf"; 
		return 0;	  
		}
		}
		return 1;
  }

}