#include <math.h>
#include <float.h>
#include <stdlib.h>
#include <string.h>
#ifdef _MSC_VER
#define EXPORT __declspec(dllexport)
#else
#define EXPORT
#endif

static const char *error = NULL;

EXPORT int init(const char *str) {
  return 1;
}

EXPORT const char * getLastError() {
  return error;
}

EXPORT const char* return_test_yaml() {
    return "CFUN_kAg_v1";
}

EXPORT int eval(const char *func,
                              int nArgs,
                              const double **inReal,
                              const double **inImag,
                              int blockSize,
                              double *outReal,
                              double *outImag) {
  
  	double params1[6] = {-8.085202766065000e-04, 0.084173230577580, -2.898387926986296, 30.127572196852682, 1.323932096483007e+02, 1.031106730600989e+02};
    double params2[6] =  {-3.358195377766776e-07,  1.869961406456805e-04, -0.041125881979616, 4.473201297735611, -2.413050714859127e+02, 5.659032445727915e+03};
    double params3[6] =  {8.147381572319895e-11, -1.042739292996614e-07,  5.409027188706845e-05, -0.013972777872369, 1.569131427472265,  4.105421708582979e+02};
    
    double kAg;
	int i;

  if (strcmp("CFUN_kAg_v1", func) == 0) {
    if (nArgs != 1) {
      error = "One argument expected";
      return 0;
    }

    for (i = 0; i < blockSize; i++) {
		
	    double T = inReal[0][i];

      if(T < 0){
          error = "T is negative!"; return 0; 
        } 
      
      if(T > 1400){
          error = "Temperature is above 1400 K - outside of the range!"; 
          return 0;	  
        }

      if(T <= 33){
        kAg = params1[0] * pow(T, 5) + params1[1] * pow(T, 4) + params1[2] * pow(T, 3) + params1[3] * pow(T, 2) + params1[4] * T+ params1[5];
        }
          
      if(T > 33 && T <= 100) {
        kAg = params2[0] * pow(T, 5) + params2[1] * pow(T, 4) + params2[2] * pow(T, 3) + params2[3] * pow(T, 2) + params2[4] * T+ params2[5];
        }

      if(T > 100 && T < 300){
        kAg = params3[0] * pow(T, 5) + params3[1] * pow(T, 4) + params3[2] * pow(T, 3) + params3[3] * pow(T, 2) + params3[4] * T+ params3[5];
       }

      if(T >= 300){
        kAg = 437.5;
      }

      outReal[i] = kAg;

      if(outReal[i]!=outReal[i]){
        error = "Output is nan"; 
        return 0;	  
      }
      if (fabs(outReal[i])>DBL_MAX){
        error = "Output is inf"; 
        return 0;	  
      }
	  }
		return 1;
  }

}
