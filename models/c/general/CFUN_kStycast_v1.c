#include <math.h>
#include <float.h>
#include <stdlib.h>
#include <string.h>
#ifdef _MSC_VER
#define EXPORT __declspec(dllexport)
#else
#define EXPORT
#endif

static const char *error = NULL;

EXPORT int init(const char *str) {
return 1;
}

EXPORT const char * getLastError() {
return error;
}

EXPORT const char* return_test_yaml() {
    return "CFUN_kStycast_v1";
}

EXPORT int eval(const char *func,
                              int nArgs,
                              const double **inReal,
                              const double **inImag,
                              int blockSize,
                              double *outReal,
                              double *outImag) {
    double a = -1.44774526e-15;
    double b =  1.34960504e-01;
    double c = -1.22800949e+00;
    double d =  3.74296526e+00;
    double e = -1.44079527e+00;
    double f = -1.78900656e+01;
	double g =  4.74103670e+01;
	double h = -5.51555008e+01;
	double i =  3.31100291e+01;
    double j =  1.00000000e-18;
    double s =  3.19701063e-10;
    double l =  2.45943131e-02;

    double a2 =  5.1837183226009016e-005;
    double b2 =  4.2713736725689781e-005;
    double c2 =  5.2561006822686353e-003;
    double d2 = -5.2495995997016074e-004;
    double e2 =  1.7724192489926551e-005;
    
    double m[2] = {0.01794, 0.0002309};
    double n[2] = {0, 0};

    double log_T;
    double f_exp;
	int k;
    
	if (strcmp("CFUN_kStycast_v1", func) == 0) {
		if (nArgs != 1) {
			error = "One argument expected";
			return 0;
			}

        for (k = 0; k < blockSize; k++) {
            
		    double T = inReal[0][k];
            
			if(T < 0)     {
				    error = "T is negative!"; return 0; 
			    } 
	
			if(T > 1400){
				    error = "Temperature is above 1400 K - out of range!"; 
				    return 0;	  
			    } 
            
            if(T > 290) {
                log_T = log10(290);
                f_exp = a*pow(log_T,9) + b*pow(log_T,8) + c*pow(log_T,7) + d*pow(log_T,6) + e*pow(log_T,5) + f*pow(log_T,4) + g*pow(log_T,3) + h*log_T*log_T + i*log_T + j;
                n[1] = s*pow(10,f_exp) + l - m[1]*290;
                outReal[k] = m[1]*T + n[1];
                }
            
            //T >= 10.8 && T < 228.73
            if(T > 10 && T <= 290) {
                log_T = log10(T);
                f_exp = a*pow(log_T,9) + b*pow(log_T,8) + c*pow(log_T,7) + d*pow(log_T,6) + e*pow(log_T,5) + f*pow(log_T,4) + g*pow(log_T,3) + h*log_T*log_T + i*log_T + j;       
                outReal[k] = s*pow(10,f_exp) + l;
                }

            if(T <= 10) {
                log_T = log10(10);
                f_exp = a*pow(log_T,9) + b*pow(log_T,8) + c*pow(log_T,7) + d*pow(log_T,6) + e*pow(log_T,5) + f*pow(log_T,4) + g*pow(log_T,3) + h*log_T*log_T + i*log_T + j;
                a2 = s*pow(10,f_exp) + l - (e2*pow(10,4) + d2*pow(10,3) + c2*10*10 + b2*10);
                outReal[k] = e2*pow(T,4) + d2*pow(T,3) + c2*T*T + b2*T + a2;
                
                /*n[0] = s*pow(10,f_exp) + l - m[0]*2.8;
                outReal[k] = m[0]*T + n[0];
                */
                } 
            
			
			/*Output consistency check*/
			if(outReal[k]!=outReal[k]){
				error = "Output is nan"; 
				return 0;	  
			}
			if (fabs(outReal[k])>DBL_MAX){
				error = "Output is inf"; 
				return 0;	  
			}
			}
	
    return 1;
}

else {
    error = "Unknown function";
    return 0;
}
}