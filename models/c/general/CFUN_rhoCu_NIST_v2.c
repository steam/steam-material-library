#include <math.h>
#include <float.h>
#include <stdlib.h>
#include <string.h>

#ifdef _MSC_VER
#define EXPORT __declspec(dllexport)
#else
#define EXPORT
#endif

// ##################################################################
// 18.11.2024 TM
// Source: NIST
// ##################################################################

static const char *error = NULL;

EXPORT int init(const char *str) {
    return 1;
}

EXPORT const char *getLastError() {
    return error;
}

EXPORT const char* return_test_yaml() {
    return "CFUN_rhoCu_NIST_v2";
}

EXPORT int eval(const char *func,
                int nArgs,
                const double **inReal,
                const double **inImag,
                int blockSize,
                double *outReal,
                double *outImag) {

    int i;
    double rho_n;
    double rho_0;
    double rho_i;
    double rho_i0;
    double x, b;
    double log_x;
    double f_exp;
    double corr;

    // Constants
    double c0 = 1.553e-8;  // (for RRR=100=R_273/R_4)
    double Tref_RRR = 273; //(NIST) 

    double P1 = 1.171e-17;
    double P2 = 4.49;
    double P3 = 4.497811e-7; // 3.841e10;
    double P4 = 3.35;    // 1.14;
    double P5 = 50;
    double P6 = 6.428;
    double P7 = 0.4531;

    double a0 = -2.662;
    double a1 = 0.3168;
    double a2 = 0.6229;
    double a3 = -0.1839;
    double a4 = 0.01827;

    if (strcmp("CFUN_rhoCu_NIST_v2", func) == 0) {
        if (nArgs != 3) {
            error = "Three arguments expected";
            return 0;
        }

        for (i = 0; i < blockSize; i++) {
            double T = inReal[0][i];
            double B = inReal[1][i];
            double RRR = inReal[2][i];

            /* Input consistency check with hotfix for negative T */
            if (B < 0) {
                error = "normB is negative!";
                return 0;
            }
            if (RRR < 0) {
                error = "RRR is negative!";
                return 0;
            }

            b = fabs(B);
            rho_0 = c0 / RRR;
            rho_i = P1 * pow(T, P2) / (1 + P3 * pow(T, P4) * exp(-pow(P5 / T, P6)));
            rho_i0 = P7 * rho_i * rho_0 / (rho_i + rho_0);

            rho_n = rho_0 + rho_i + rho_i0;
            if (b >= 0.1) {
                x = 0.00000001553 * b / rho_n;
                log_x = 0.43429 * log(x);
                f_exp = a0 + a1 * log_x + a2 * pow(log_x, 2) + a3 * pow(log_x, 3) + a4 * pow(log_x, 4);
                corr = pow(10, f_exp);
            } else {
                corr = 0;
            }

            outReal[i] = rho_n * (1 + corr);

            /* Output consistency check */
            if (isnan(outReal[i])) {
                error = "Output is nan";
                return 0;
            }
            if (fabs(outReal[i]) > DBL_MAX) {
                error = "Output is inf";
                return 0;
            }
        }
        return 1;
    } else {
        error = "Unknown function";
        return 0;
    }
}
