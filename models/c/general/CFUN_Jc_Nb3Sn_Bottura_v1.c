#include <math.h>
#include <float.h>
#include <stdlib.h>
#include <string.h>

#ifdef _MSC_VER
#define EXPORT __declspec(dllexport)
#else
#define EXPORT
#endif

static const char *error = NULL;

EXPORT int init(const char *str) {
    return 1;
}

EXPORT const char *getLastError() {
    return error;
}

double maximum(double a, double b) {
    if (a > b) {
        return a;
    }
    return b;
}

EXPORT const char* return_test_yaml() {
    return "CFUN_Jc_Nb3Sn_Bottura_v1";
}

EXPORT int eval(const char *func,
                int nArgs,
                const double **inReal,
                const double **inImag,
                int blockSize,
                double *outReal,
                double *outImag) {
    
    double Tc0, Bc20, CJ, p, q;
    double exp_factor = 1.52;
    int i;
    double PI_4       = 3.14159265358979323846/4 ;
    double t, b, Jc_sc, Ic_sc;
    double Bc2;
    double minB = 1e-6;

    
  
    if (strcmp("CFUN_Jc_Nb3Sn_Bottura_v1", func) == 0) {
        if (nArgs != 9) {
            error = "Nine arguments expected";
            return 0;
        }

        for (i = 0; i < blockSize; i++) {
            // parameters 
            double T        = inReal[0][i];
            double B        = inReal[1][i];
            double Tc0      = inReal[2][i];
            double Bc20     = inReal[3][i];
            double CJ       = inReal[4][i];
            double p        = inReal[5][i];
            double q        = inReal[6][i];
            //Wire parameters
            double wireDiameter = inReal[7][i];
            double Cu_noCu      = inReal[8][i];

            double f_sc = 1 / (1 + Cu_noCu);

            B = maximum(minB, fabs(B));
            t = T / Tc0;

            if(t > 1){
                t = 1;
            }

            Bc2 = maximum(1e-9, Bc20* (1 - pow(t,exp_factor)));
            
            b = B/Bc2;


            if(b > 1){
                b = 1;
            }

            Jc_sc = CJ /B * pow(b,p) * (1-t*t) * (1-pow(t,exp_factor)) * pow((1 - b),q);

            if(T >= Tc0){
                Jc_sc = 0;
            }
            if(B >= Bc20){
                Jc_sc = 0;
            }
            if(Jc_sc < 0){
                Jc_sc = 0;
            }

            // Ic_sc = Jc_sc * (PI_4 * wireDiameter*wireDiameter * f_sc);

            outReal[i] = Jc_sc; //Ic_sc
            
        }
        return 1;
    }

    else {
      error = "Unknown function";
      return 0;
    }
}
