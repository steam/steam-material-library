#include <math.h>
#include <float.h>
#include <stdlib.h>
#include <string.h>
#ifdef _MSC_VER
#define EXPORT __declspec(dllexport)
#else
#define EXPORT
#endif	


static const char *error = NULL;

EXPORT int init(const char *str) {
  return 1;
}

EXPORT const char * getLastError() {
  return error;
}

EXPORT const char* return_test_yaml() {
    return "";
}

double CpNb3Sn_NormalState(double T) {
    double a  =   38.2226876;
    double b  =   -848.36422;
    double c  =   1415.13807;
    double d  =   -346.83796;
    double e   =  6.804586085;
    double f   =  59.92091818;
    double g   =  25.82863336;
    double h   =  8.779183354;
    double na  =   1;
    double nb  =   2;
    double nc  =   3;
    double nd  =   4;

    //Normal state of Nb3Sn
    double CpNb3Sn;
    if(T <= 10) {
        CpNb3Sn = 7.5475e-3 * pow(T, 2);
    }
    else if(T > 10 && T <= 20){
        CpNb3Sn = (-0.3 + 0.00375 * pow(T, 2) )/ 0.09937;
    }
    if(T > 20 && T <= 400){
        CpNb3Sn = a* T /pow((e + T), na) + b*pow(T,2) /pow(f + T, nb) + c*pow(T, 3)/pow(g + T, nc)+ d* pow(T, 4) / pow(h + T, nd);
    }
    if(T > 400){
        CpNb3Sn = 250.8246;
    }

    return CpNb3Sn;
}

EXPORT int eval(const char *func,
                              int nArgs,
                              const double **inReal,
                              const double **inImag,
                              int blockSize,
                              double *outReal,
                              double *outImag) {
								  
    double density = 8950;
    int i;

    double CpNb3Sn;
    double cpntc;
  
    if (strcmp("CFUN_CvNb3Sn_Arp_Knapp_v1", func) == 0) {
    
        if (nArgs != 4) {
            error = "Four arguments expected";
            return 0;
        }

        for (i = 0; i < blockSize; i++) {    
	        double T         = inReal[0][i];
	        double Tc        = inReal[1][i];
	        double Tc0       = inReal[2][i];
	        double Tcs       = inReal[3][i];

	        if(T < 0)    {error = "T is negative!"; return 0; } 
	        if(Tc < 0)   {error = "Tc is negative!"; return 0; }
            if(Tc0 < 0)  {error = "Tc0 is negative!"; return 0; }
	        if(Tcs < 0)  {error = "Tcs is negative!"; return 0; }

            double TT = Tc / Tc0;
            double dBc2Dt = -0.46306-0.067830*Tc;
            double delcp  = 1500 * (pow(dBc2Dt, 2))/(2*pow(27.2/(1+0.34*TT), 2)-1);

            if(Tc <= 10){
                cpntc = 7.5475e-3*pow(Tc,2);
            }
            if(Tc > 10 && Tc <= 20){
                cpntc=(-0.3+0.00375*pow(Tc, 2))/0.09937;
            }
            if(T < Tc) {
                double F = (T-Tcs)/(Tc-Tcs);
                CpNb3Sn = F * CpNb3Sn_NormalState(T) + (1-F) * (cpntc+delcp) * pow(T/Tc, 3);
            }
            if(T < Tcs){
                CpNb3Sn = (cpntc+delcp) * pow(T/Tcs, 3);
            }
            if(T >= Tc){
                CpNb3Sn = CpNb3Sn_NormalState(T);
            }
            
            outReal[i] = CpNb3Sn*density;

	        if(outReal[i]!=outReal[i]){
	        error = "Output is nan"; 
	        return 0;	  
	        }
        }
        	        
    return 1;
    }

    else {
        error = "Unknown function";
        return 0;
    }

}
