#include <math.h>
#include <float.h>
#include <stdlib.h>
#include <string.h>
#ifdef _MSC_VER
#define EXPORT __declspec(dllexport)
#else
#define EXPORT
#endif

static const char *error = NULL;

EXPORT int init(const char *str) {
return 1;
}

EXPORT const char * getLastError() {
return error;
}

EXPORT const char* return_test_yaml() {
    return "CFUN_kAl5083_v1";
}

// Al5083 95% alu
EXPORT int eval(const char *func,
                              int nArgs,
                              const double **inReal,
                              const double **inImag,
                              int blockSize,
                              double *outReal,
                              double *outImag) {
    double a = -1.44774526e-15;
    double b =  1.78431409e-12;
    double c = -1.77776121e-03;
    double d = -3.31306518e-02;
    double e =  3.90166241e-01;
    double f = -1.51911637e+00;
	double g =  2.57843844e+00;
	double h = -9.14891718e-01;
	double i =  4.20018897e-01;

    double m = 0.8500;
    double n = -0.110;
    double p = 0;

    double log_T;
    double f_exp; 
	int k;
    
	if (strcmp("CFUN_kAl5083_v1", func) == 0) {
		if (nArgs != 1) {
			error = "One argument expected";
			return 0;
			}

        for (k = 0; k < blockSize; k++) {
			
            
		    double T = inReal[0][k];
            log_T = log10(T);
			if(T < 0)     {
				    error = "T is negative!"; return 0; 
			    } 
	
			if(T > 1400){
				    error = "Temperature is above 1400 K - out of range!";
				    return 0;	  
			    } 
            if(T < 4) {
                log_T = log10(4);
                f_exp = a*pow(log_T,8) + b*pow(log_T,7) + c*pow(log_T,6) + d*pow(log_T,5) + e*pow(log_T,4) + f*pow(log_T,3) + g*pow(log_T,2) + h*log_T + i;
                n =  pow(10,f_exp) - m*4;
                outReal[k] = m*T + n;
                }
            
            if(T >= 4 && T < 650) {
                f_exp = a*pow(log_T,8) + b*pow(log_T,7) + c*pow(log_T,6) + d*pow(log_T,5) + e*pow(log_T,4) + f*pow(log_T,3) + g*pow(log_T,2) + h*log_T + i;
                outReal[k] = pow(10,f_exp);
                } 

            if(T >= 650) {
                log_T = log10(650);
                f_exp = a*pow(log_T,8) + b*pow(log_T,7) + c*pow(log_T,6) + d*pow(log_T,5) + e*pow(log_T,4) + f*pow(log_T,3) + g*pow(log_T,2) + h*log_T + i;
                p =  pow(10,f_exp);
                outReal[k] = p;
            }
			
			/*Output consistency check*/
			if(outReal[k]!=outReal[k]){
				error = "Output is nan"; 
				return 0;	  
			}
			if (fabs(outReal[k])>DBL_MAX){
				error = "Output is inf"; 
				return 0;	  
			}
			}
	
    return 1;
}

else {
    error = "Unknown function";
    return 0;
}
}