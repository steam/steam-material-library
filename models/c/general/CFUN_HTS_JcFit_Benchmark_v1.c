#include <math.h>
#include <float.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#ifdef _MSC_VER
#define EXPORT __declspec(dllexport)
#else
#define EXPORT
#endif

static const double C0 = 2.5527e+12;   // original value from Davide 6.35E+10 divided by fraction of YBCO, which is 0.0249   // A T / m^2
static const double Birr0 = 274.84;    // T
static const double Tirr0 = 91.317;    // K
static const double m = 0.7008;
static const double p = 0.75;
static const double q = 5.69;
static const double nu = 0.3323;
static const double T = 4.2; // K
static const double T_red = 0.045993627;
static const double minB = 1E-6; // T

static const char *error = NULL;

EXPORT int init(const char *str) {
  return 1;
}

EXPORT const char * getLastError() {
  return error;
}

EXPORT const char* return_test_yaml() {
    return "";
}

double Bc(double T_red_value) {
    return ((T_red_value > 0 && T_red_value < 1) * (1 - pow(T_red_value, nu)) +
            (T_red_value <= 0) * 1) * Birr0 +
           (T_red_value >= 1) * 0.001;
}

double Jc(double B, double T_red_value) {
    double Bc_Tred = Bc(T_red_value);

    if (T_red_value < 1 && B < Bc_Tred) {
        double factor1 = (1 - pow(T_red_value, nu)) * (1 - pow(T_red_value, m));
        double factor2 = pow((p + q), (p + q)) / (pow(p, p) * pow(q, q));
        double factor3 = pow(B / Bc_Tred, p) * pow(1 - (B / Bc_Tred), q);

        return (C0 / B) * factor1 * factor2 * factor3;
    } else {
        return pow(10, -3);
    }
}

double maximum(double a, double b) {
    if (a > b) {
        return a;
    }
    return b;
}

EXPORT int eval(const char *func,
                int nArgs,
                const double **inReal,
                const double **inImag,
                int blockSize,
                double *outReal,
                double *outImag) {
 
	if (nArgs != 1) {
        // Bnorm_IN
        error = "one argument expected";
        return 0;
	}
	

	// Parfor loop
	for (int i = 0; i < blockSize; i++) {

		double Bnorm_IN = inReal[0][i];

        double B = maximum(minB, fabs(Bnorm_IN));

		outReal[i] = Jc(B, T_red);

		//Consistency check: output
		if(outReal[i] != outReal[i]){
			error = "Output is nan"; 
			return 0;	  
		}
		
		if (fabs(outReal[i])>DBL_MAX){
			error = "Output is inf"; 
			return 0;	  
		}
	}
	
	return 1;
}