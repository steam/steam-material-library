#include <math.h>
#include <float.h>
#include <stdlib.h>
#include <string.h>

#ifdef _MSC_VER
#define EXPORT __declspec(dllexport)
#else
#define EXPORT
#endif

static const char *error = NULL;

EXPORT int init(const char *str) {
    return 1;
}

EXPORT const char *getLastError() {
    return error;
}

EXPORT const char* return_test_yaml() {
    return "CFUN_rhoCu_CUDI_v1";
}

EXPORT int eval(const char *func,
                int nArgs,
                const double **inReal,
                const double **inImag,
                int blockSize,
                double *outReal,
                double *outImag) {

    int i;
    double rhoCu;
    double c0=1.7;
    double c1=2.3E9;
    double c2=9.57E5;
    double c3=163;

    if (strcmp("CFUN_rhoCu_CUDI_v1", func) == 0) {
        if (nArgs != 4) {
            error = "Four arguments expected";
            return 0;
        }

        for (i = 0; i < blockSize; i++) {
            double T = inReal[0][i];
            double B = inReal[1][i];
            double RRR = inReal[2][i];
            double MR = inReal[3][i]; 

            /* Input consistency check with hotfix for negative T */
            if (B < 0) {
                error = "normB is negative!";
                return 0;
            }
            if (RRR < 0) {
                error = "RRR is negative!";
                return 0;
            }
            

            outReal[i] = (c0/RRR+1/(c1/pow(T,5)+c2/pow(T,3)+c3/T))/pow(10,8)+MR*B;

            /* Output consistency check */
            if (isnan(outReal[i])) {
                error = "Output is nan";
                return 0;
            }
            if (fabs(outReal[i]) > DBL_MAX) {
                error = "Output is inf";
                return 0;
            }
        }
        return 1;
    } else {
        error = "Unknown function";
        return 0;
    }
}
