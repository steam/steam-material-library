#include <math.h>
#include <float.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#ifdef _MSC_VER
#define EXPORT __declspec(dllexport)
#else
#define EXPORT
#endif
#define LENGTH 0

static const char* error = NULL;

EXPORT int init(const char* str) {
	return 1;
}

EXPORT const char* getLastError() {
	return error;
}

EXPORT const char* return_test_yaml() {
    return "";
}

double interpolate(double array_h[], double array_b[], double h){
	double pi = 3.14159265359;
	double u0 = 4.0*pi*pow(10,-7);
	if(LENGTH != 0 || array_h != NULL){
		for(int i=0; i<LENGTH; i++){
			if(array_h[i]==h){
				return array_b[i];
			}
			else if(h>array_h[i] & h<array_h[i+1]){
				double deltah = h-array_h[i];
				double deltab = array_b[i+1]-array_b[i];
				return array_b[i]+(array_b[i+1]-array_b[i])/(array_h[i+1]-array_h[i])*deltah;

			}
			else if(h>array_h[LENGTH-1]){
				return array_b[LENGTH-1]+(h-array_h[LENGTH-1])*u0;
			}

		}

	}else{
		return h*u0;
	}


}

EXPORT int eval(const char* func,
	int nArgs,
	const double** inReal,
	const double** inImag,
	int blockSize,
	double* outReal,
	double* outImag) {

	if (strcmp("BHAir_v1", func) == 0) {
		if (nArgs != 4) {
			error = "Four arguments expected";
			return 0;
		}
        
		for (int i = 0; i < blockSize; i++) { // For all points to evaluate
			double x = inReal[0][i];

			outReal[i] = interpolate(NULL, NULL, x);
			
			/*Output consistency check*/
			if (outReal[i] != outReal[i]) {
				error = "Output is nan";
				return 0;
			}
			if (fabs(outReal[i]) > DBL_MAX) {
				error = "Output is inf";
				return 0;
			}

		}
		return 1;
	}

	else {
		error = "Unknown function";
		return 0;
	}
}
