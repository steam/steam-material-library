#include <math.h>
#include <float.h>
#include <stdlib.h>
#include <string.h>
#ifdef _MSC_VER
#define EXPORT __declspec(dllexport)
#else
#define EXPORT
#endif

static const char *error = NULL;

EXPORT int init(const char *str) {
  return 1;
}

EXPORT const char * getLastError() {
  return error;
}

EXPORT const char* return_test_yaml() {
    return "CFUN_rhoAl_v2";
}

EXPORT int eval(const char *func,
                              int nArgs,
                              const double **inReal,
                              const double **inImag,
                              int blockSize,
                              double *outReal,
                              double *outImag) {
   
    double log_T;
    double a=0.476050395713888;
    double b=-3.299730411832043;
    double c=7.489576522510857;
    double d=-5.738194411483883;
    double e=1.121748209742612;
    double f=0.207979301275814;
    double g=-4.010635050869012;
    double logRho; 
    double rhoAl;
    double m = 5.44098441e-11;
    double n = 2.452835175035380e-08;

	int i;

  if (strcmp("CFUN_rhoAl_v2", func) == 0) {
    if (nArgs != 1) {
      error = "One argument expected";
      return 0;
    }

    for (i = 0; i < blockSize; i++) {
		
	double T   = inReal[0][i];

      if(T < 0){
          error = "T is negative!"; return 0; 
        } 

      if(T > 1400){
          error = "Temperature is above 500 K - outside of the range!";
          return 0;	 
        }

      if(T >= 0 && T <= 300) {
        // From NIST, 1984, Temperature range: 1-500 K
        log_T = log10(T);
        
        logRho = a * pow(log_T, 6) + b * pow(log_T, 5) + c * pow(log_T, 4) + d * pow(log_T, 3) + e * pow(log_T, 2) + f * log_T + g;
        rhoAl =  pow(10, logRho)*1e-8;

        }
        if( T > 300) {
          rhoAl =  m*T + n - m*300;
        }
  
      outReal[i] = rhoAl;

      if(outReal[i]!=outReal[i]){
        error = "Output is nan"; 
        return 0;	  
      }
      if (fabs(outReal[i])>DBL_MAX){
        error = "Output is inf"; 
        return 0;	  
      }
	  }
		return 1;
  }

}