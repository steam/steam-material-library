#include <math.h>
#include <float.h>
#include <stdlib.h>
#include <string.h>
#ifdef _MSC_VER
#define EXPORT __declspec(dllexport)
#else
#define EXPORT
#endif

static const char *error = NULL;

EXPORT int init(const char *str) {
  return 1;
}

EXPORT const char * getLastError() {
  return error;
}

EXPORT const char* return_test_yaml() {
    return "CFUN_CvAg_v1";
}

EXPORT int eval(const char *func,
                              int nArgs,
                              const double **inReal,
                              const double **inImag,
                              int blockSize,
                              double *outReal,
                              double *outImag) {
  
  	double params[9] =  {0.191049066537241, -2.34359159400347, 11.5766999155046, -29.3520406103732,
     40.4479932778170, -30.2548599606873, 12.2576978613138, -0.170606365568724, -2.11971321961447};
    
    double density = 10470; // kg/m^3
	double m = 317.531605;
	double log_300 = log10(300);
    double f_exp_300 = params[0] * pow(log_300, 8) + params[1] * pow(log_300, 7) + params[2] * pow(log_300, 6) + params[3] * pow(log_300, 5) + params[4] * pow(log_300, 4) + params[5] * pow(log_300, 3) + params[6] * log_300*log_300 + params[7] * log_300 + params[8];
				
	double n = density*pow(10,f_exp_300)- m*300;
    double log_T;
    double f_exp; 
	int i;

  if (strcmp("CFUN_CvAg_v1", func) == 0) {
    if (nArgs != 1) {
      error = "One argument expected";
      return 0;
    }

    for (i = 0; i < blockSize; i++) {
		
	    double T = inReal[0][i];

		if(T < 0){
				error = "T is negative!"; return 0; 
			} 
		
		if(T > 1400){
				error = "Temperature is above 1400 K - out of range!"; 
				return 0;	  
			}
		if(T <= 1 && T >= 0){
			outReal[i] = 79.475535358200474;
		}

		if(T > 1 && T <= 300) {
				log_T = log10(T);
                f_exp = params[0] * pow(log_T, 8) + params[1] * pow(log_T, 7) + params[2] * pow(log_T, 6) + params[3] * pow(log_T, 5) + params[4] * pow(log_T, 4) + params[5] * pow(log_T, 3) + params[6] * log_T*log_T + params[7] * log_T + params[8];
				outReal[i] = density*pow(10,f_exp);
			}
		if(T > 300 ) {
			outReal[i] = m*T + n;//2.444995403700760e+06;
		}
		
		if(outReal[i]!=outReal[i]){
			error = "Output is nan"; 
			return 0;	  
			}
		if (fabs(outReal[i])>DBL_MAX){
		error = "Output is inf"; 
		return 0;	  
		}
		}
		return 1;
  }

}