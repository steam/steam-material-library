#include <math.h>
#include <float.h>
#include <stdlib.h>
#include <string.h>
#ifdef _MSC_VER
#define EXPORT __declspec(dllexport)
#else
#define EXPORT
#endif

static const char *error = NULL;

EXPORT int init(const char *str) {
  return 1;
}

EXPORT const char * getLastError() {
  return error;
}

EXPORT const char* return_test_yaml() {
    return "CFUN_rhoAl1350_v1";
}

EXPORT int eval(const char *func,
                              int nArgs,
                              const double **inReal,
                              const double **inImag,
                              int blockSize,
                              double *outReal,
                              double *outImag) {
  
  	double a = -1.44774526e-15;
    double b =  1.02243231e-01;  
    double c =  1.27482227e-01;
    double d =  6.12413693e-08;
    double e =  4.92004990e-02;
    double f =  2.05543059e-02;
	double g = -8.33415647e-03;
	double h =  3.82962692e-01;
    double s =  1.11480913e+00;
    double j = -1.61852038e-01;
	double k =  8.87569968e-02;
	double l = -1.34991833e-02;
	double m =  3.29123884e-13;
    double n =  5.17066836e-10;

	double a2 = 5.0092594044213295e-010;
	double b2 = -2.8275040974679632e-012;
	double c2 =  3.6851971243233049e-013;
	double d2 = -1.2598537943805338e-014;
	double e2 =  5.6433946446808698e-016;
	double f2 = -4.4088151579667404e-018;

    double r[2] = {0.007018e-8, 1.038e-8};

    double log_T;
    double f_exp; 
	int i;

  if (strcmp("CFUN_rhoAl1350_v1", func) == 0) {
    if (nArgs != 1) {
      error = "One argument expected";
      return 0;
    }

    for (i = 0; i < blockSize; i++) {
		
	    double T = inReal[0][i];
	    

		if(T < 0){
			error = "T is negative!"; return 0; 
		} 
		
		if(T > 1400){
			error = "Temperature is above 1400 K - outside of the range!";
			return 0;	  
		}
				
		if( T <= 55){
			log_T = log10(55);
			f_exp = l*pow(log_T, 11) + k*pow(log_T, 10) + j*pow(log_T, 9)+ a*pow(log_T, 8) + b*pow(log_T,7) + c*pow(log_T,6) + d*pow(log_T,5) + e*pow(log_T,4) + f*pow(log_T,3) + g*pow(log_T,2) + h*log_T + s;
			a2 = m*pow(10,f_exp) + n - (f2*pow(55,5) + e2*pow(55,4) + d2*pow(55,3) + c2*pow(55,2) + b2*55);
			outReal[i]  = f2*pow(T,5) + e2*pow(T,4) + d2*pow(T,3) + c2*pow(T,2) + b2*T + a2;
		}
		if(T > 55 && T < 300) {
            log_T = log10(T);
			f_exp = l*pow(log_T, 11) + k*pow(log_T, 10) + j*pow(log_T, 9)+ a*pow(log_T, 8) + b*pow(log_T,7) + c*pow(log_T,6) + d*pow(log_T,5) + e*pow(log_T,4) + f*pow(log_T,3) + g*pow(log_T,2) + h*log_T + s;
			outReal[i] = m*pow(10,f_exp) + n;
		}

        if(T >= 300) {
            log_T = log10(300);
			f_exp = l*pow(log_T, 11) + k*pow(log_T, 10) + j*pow(log_T, 9)+ a*pow(log_T, 8) + b*pow(log_T,7) + c*pow(log_T,6) + d*pow(log_T,5) + e*pow(log_T,4) + f*pow(log_T,3) + g*pow(log_T,2) + h*log_T + s;
			r[1]  = m*pow(10,f_exp) + n - r[0]*300;
            outReal[i] = r[0]*T + r[1];
        }    

		
		/*Output consistency check*/
		if(outReal[i]!=outReal[i]){
			error = "Output is nan"; 
			return 0;	  
			}
		if (fabs(outReal[i])>DBL_MAX){
		error = "Output is inf"; 
		return 0;	  
		}
		}
		return 1;
  }

}