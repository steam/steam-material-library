#include <math.h>
#include <float.h>
#include <stdlib.h>
#include <string.h>
#ifdef _MSC_VER
#define EXPORT __declspec(dllexport)
#else
#define EXPORT
#endif

static const char *error = NULL;

EXPORT int init(const char *str) {
  return 1;
}

EXPORT const char * getLastError() {
  return error;
}

EXPORT const char* return_test_yaml() {
    return "CFUN_CvBSCCO2212_v1";
}

EXPORT int eval(const char *func,
                              int nArgs,
                              const double **inReal,
                              const double **inImag,
                              int blockSize,
                              double *outReal,
                              double *outImag) {
  
 	double params[8] =  {-2.42416626823659,28.4237015006178,-137.032430914329,
  347.713024194822,-489.818611373570,361.755938829780,-103.662828238944,-6.00000115633633};
  
  double m[2]  = {0.578520351476380, 1.207941928414329};

  //density=7850  [kg/m^3]
  double density = 6600; // kg/m^3
  double log_T;
  double f_exp; 
	int i;

  if (strcmp("CFUN_CvBSCCO2212_v1", func) == 0) {
    if (nArgs != 1) {
      error = "One argument expected";
      return 0;
  }

  for (i = 0; i < blockSize; i++) {
		
	  double T = inReal[0][i];
		if(T < 0){
				error = "T is negative!"; return 0; 
			} 
		
		if(T > 1400){
				error = "Temperature is above 1400 K - out of range!"; 
				return 0;	  
			}
    if(T <= 1 && T > 0){
      outReal[i] = 0.006599982427109;
    }

		if(T > 1 && T <= 333) {
      log_T = log10(T);
      f_exp = params[0] * pow(log_T, 7) + params[1] * pow(log_T, 6) + params[2] * pow(log_T, 5) + params[3] * pow(log_T, 4) + params[4] * pow(log_T, 3) + params[5] * pow(log_T, 2) + params[6] * log_T + params[7];
		  outReal[i] =density * pow(10,f_exp);
		}

    if(T > 333 && T < 1400){
      log_T = log10(T);
      f_exp = m[0] * log_T + m[1];
      outReal[i] = density * pow(10,f_exp);
    }

    if(outReal[i]!=outReal[i]){
      error = "Output is nan"; 
      return 0;	  
    }
    if (fabs(outReal[i])>DBL_MAX){
      error = "Output is inf"; 
      return 0;	  
    }
		}
		return 1;
  }

}