#include <math.h>
#include <float.h>
#include <stdlib.h>
#include <string.h>
#ifdef _MSC_VER
#define EXPORT __declspec(dllexport)
#else
#define EXPORT
#endif

static const char *error = NULL;

EXPORT int init(const char *str) {
return 1;
}

EXPORT const char * getLastError() {
return error;
}

EXPORT const char* return_test_yaml() {
    return "";
}

// This function computes the 2nd stage cooling power of the 2 stage crocooler RDE-418D4 4K with a second-stage capacity of 1.8 W @ 4.2 K (50 Hz).
// Output is 2nd stage power in Watts. Argument is 2nd stage temperature in K.
// Values are for 50 Hz.
// Fit of capacity map from https://www.shicryogenics.com/product/rde-418d4-4k-cryocooler-series/.
// Extrapolation to reach 70 W at 300 K.

double power_cryocooler_SHI_SRDE_418D4(double T) {
	double a = 4E-06;
	double b = -0.0031;
	double c = 0.814;
	double d = -1;

    double power = a*pow(T,3) + b*pow(T,2) + c*T + d; 

	return power;
}


EXPORT int eval(const char *func,
                              int nArgs,
                              const double **inReal,
                              const double **inImag,
                              int blockSize,
                              double *outReal,
                              double *outImag) {

	int i;

	if (strcmp("CFUN_PvsT_cryocooler_SHI_SRDE_418D4_v1", func) == 0) {
		if (nArgs != 1) {
			error = "One argument expected";
			return 0;
			}

    for (i = 0; i < blockSize; i++) {
			
			double T = inReal[0][i];

			double T_cutoff_lower = 1.23429;
			double T_cutoff_higher = 300;

			// no cooling power
			if(T < T_cutoff_lower)     {
			    outReal[i] = 0;
				return 1;
			} 
	
			if(T > T_cutoff_higher){
				error = "Temperature is above 300 K - out of range!";
				return 0;	  
			}
	
			outReal[i] = power_cryocooler_SHI_SRDE_418D4( T );
	
			/*Output consistency check*/
			if(outReal[i]!=outReal[i]){
				error = "Output is nan"; 
				return 0;	  
			}
			if (fabs(outReal[i])>DBL_MAX){
				error = "Output is inf"; 
				return 0;	  
			}
			}
	
    return 1;
	}
}