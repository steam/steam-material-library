#include <math.h>
#include <float.h>
#include <stdlib.h>
#include <string.h>
#ifdef _MSC_VER
#define EXPORT __declspec(dllexport)
#else
#define EXPORT
#endif

static const char *error = NULL;

EXPORT int init(const char *str) {
  return 1;
}

EXPORT const char * getLastError() {
  return error;
}

EXPORT const char* return_test_yaml() {
    return "CFUN_CvHeMass_v1";
}

EXPORT int eval(const char *func,
                              int nArgs,
                              const double **inReal,
                              const double **inImag,
                              int blockSize,
                              double *outReal,
                              double *outImag) {
  
  	
    double polyFit1[5] = {241.448012111007, -95.5201593998280, 0.874630242276258, 7.60977238688473, 1.97693963530308};
    double polyFit2[6] = {-191037.978607428, 401872.287717891, -337475.272209836, 141422.767309501, -29577.3097166323, 2473.32506486582};
    double polyFit3[3] = {-2.58146202246642, 3.56109372768921, 2.18906733625052};
    double polyFit4[4] = {-0.766746819633252, 2.12000865414934, -1.94508411823656, 4.08521093984522};
    double CvHe;
	int i;

  if (strcmp("CFUN_CvHeMass_v1", func) == 0) {
    if (nArgs != 1) {
      error = "One argument expected";
      return 0;
    }

    for (i = 0; i < blockSize; i++) {
		
	    double T          = inReal[0][i];
	    
        if (T <= 0.8) {
            CvHe = 22.40;
        } else if (T > 0.8 && T <= 2.167) {
            CvHe = pow(10, polyFit1[4] + polyFit1[3] * log10(T) + polyFit1[2] * pow(log10(T), 2) + polyFit1[1] * pow(log10(T), 3) + polyFit1[0] * pow(log10(T), 4));
        } else if (T > 2.167 && T <= 3) {
            CvHe = pow(10, polyFit2[5] + polyFit2[4] * log10(T) + polyFit2[3] * pow(log10(T), 2) + polyFit2[2] * pow(log10(T), 3) + polyFit2[1] * pow(log10(T), 4) + polyFit2[0] * pow(log10(T), 5));
        } else if (T > 3 && T <= 4.222) {
            CvHe = pow(10, polyFit3[2] + polyFit3[1] * log10(T) + polyFit3[0] * pow(log10(T), 2));
        } else if (T > 4.222 && T <= 10) {
            CvHe = pow(10, polyFit4[3] + polyFit4[2] * log10(T) + polyFit4[1] * pow(log10(T), 2) + polyFit4[0] * pow(log10(T), 3));
        } else {
            CvHe = 3116;
        }

        outReal[i] = CvHe;
		
		/*Output consistency check*/
		if(outReal[i]!=outReal[i]){
			error = "Output is nan"; 
			return 0;	  
			}
		if (fabs(outReal[i])>DBL_MAX){
		error = "Output is inf"; 
		return 0;	  
		}
		}
		return 1;
  }

}
