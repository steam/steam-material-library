#include <math.h>
#include <float.h>
#include <stdlib.h>
#include <string.h>
#ifdef _MSC_VER
#define EXPORT __declspec(dllexport)
#else
#define EXPORT
#endif

static const char *error = NULL;

EXPORT int init(const char *str) {
return 1;
}

EXPORT const char * getLastError() {
return error;
}

EXPORT const char* return_test_yaml() {
    return "CFUN_rhoIn_v0";
}

double rhoIn(double T) {
	double rhoIn;
    double a = -2.2420979178027047e-23;
    double b = 4.44307430128052e-21;
    double c = -4.345285961448319e-19;
    double d = 2.291682473087e-07;
    double e = 4.763099193031951e-15;
    double f = -5.068277655612249e-13;
    double g = 2.435652267775081e-11;
    double h = -2.934928562314791e-10;
    double i = 1.1602016764429902e-09;
    double j = 6.366772071048326e-26;
    double k = -9.732887632074973e-29;
    double l = 6.25174813939631e-32;

    rhoIn = l*pow(T,11) + k*pow(T,10) + j*pow(T,9) + a*pow(T,8) + b*pow(T,7) + c*pow(T,6) + pow(d,pow(T,5)) + e*pow(T,4) + f*pow(T,3) + g*pow(T,2) + h*T + i; 

	return rhoIn;
}


EXPORT int eval(const char *func,
                              int nArgs,
                              const double **inReal,
                              const double **inImag,
                              int blockSize,
                              double *outReal,
                              double *outImag) {

	int i;

	if (strcmp("CFUN_rhoIn_v0", func) == 0) {
		if (nArgs != 1) {
			error = "One argument expected";
			return 0;
			}

    for (i = 0; i < blockSize; i++) {
			
			double T = inReal[0][i];

			if(T < 15)     {
				error = "Temperature is below 15 K - out of range!"; 
                return 0; 
			} 
	
			if(T > 300){
				error = "Temperature is above 300 K - out of range!";
				return 0;	  
			}
	
			
			outReal[i] = rhoIn( T );
	
			/*Output consistency check*/
			if(outReal[i]!=outReal[i]){
				error = "Output is nan"; 
				return 0;	  
			}
			if (fabs(outReal[i])>DBL_MAX){
				error = "Output is inf"; 
				return 0;	  
			}
			}
	
    return 1;
}

/*else {
    error = "Unknown function";
    return 0;
}*/
}