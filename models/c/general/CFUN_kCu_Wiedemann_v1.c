#include <math.h>
#include <float.h>
#include <stdlib.h>
#include <string.h>
#ifdef _MSC_VER
#define EXPORT __declspec(dllexport)
#else
#define EXPORT
#endif

static const char *error = NULL;

EXPORT int init(const char *str) {
  return 1;
}

EXPORT const char * getLastError() {
  return error;
}

EXPORT const char* return_test_yaml() {
    return "CFUN_kCu_Wiedemann_v1";
}

EXPORT int eval(const char *func,
                              int nArgs,
                              const double **inReal,
                              const double **inImag,
                              int blockSize,
                              double *outReal,
                              double *outImag) {
  int i;

  if (strcmp("CFUN_kCu_Wiedemann_v1", func) == 0) {
    if (nArgs != 2) {
      error = "Two arguments expected";
      return 0;
    }

    for (i = 0; i < blockSize; i++) {
		
	  double T   = inReal[0][i];
	  //double B   = inReal[1][i];
	  double rho = inReal[1][i];
	  
	  /*Input consistency check with hotfix for negative T*/
	  //	  if(T < 1.9)   {T=1.9;}
	  if(T < 0)     {error = "T is negative!"; return 0; } 
	  if(rho < 0)     {error = "rhoCu is negative!"; return 0; }
	  
	  
	  outReal[i] = 2.45e-8*T / rho;
	  
	  /*Output consistency check*/
	  if(outReal[i]!=outReal[i]){
		error = "Output is nan"; 
		return 0;	  
	  }
	  if (fabs(outReal[i])>DBL_MAX){
		error = "Output is inf"; 
		return 0;	  
	  }
    }
    return 1;
  }

  else {
    error = "Unknown function";
    return 0;
  }
}