#include <math.h>
#include <float.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#ifdef _MSC_VER
#define EXPORT __declspec(dllexport)
#else
#define EXPORT
#endif
#define LENGTH 33

static const char* error = NULL;

EXPORT int init(const char* str) {
	return 1;
}

EXPORT const char* getLastError() {
	return error;
}

EXPORT const char* return_test_yaml() {
    return "";
}

double interpolate(double array_h[LENGTH], double array_b[LENGTH], double h){
	double pi = 3.14159265359;
	double u0 = 4.0*pi*pow(10,-7);
	if(LENGTH != 0){
		for(int i=0; i<LENGTH; i++){
			if(array_h[i]==h){
				return array_b[i];
			}
			else if(h>array_h[i] & h<array_h[i+1]){
				double deltah = h-array_h[i];
				double deltab = array_b[i+1]-array_b[i];
				return array_b[i]+(array_b[i+1]-array_b[i])/(array_h[i+1]-array_h[i])*deltah;

			}
			else if(h>array_h[LENGTH-1]){
				return array_b[LENGTH-1]+(h-array_h[LENGTH-1])*u0;
			}

		}

	}else{
		return h*u0;
	}


}

EXPORT int eval(const char* func,
	int nArgs,
	const double** inReal,
	const double** inImag,
	int blockSize,
	double* outReal,
	double* outImag) {

	if (strcmp("BHIron2_v1", func) == 0) {
		if (nArgs != 4) {
			error = "Four arguments expected";
			return 0;
		}

		double array_h[LENGTH];
		double array_b[LENGTH];
		array_b[0] = 0.000000;
		array_b[1] = 0.800000;
		array_b[2] = 1.200000;
		array_b[3] = 1.400000;
		array_b[4] = 1.500000;

		array_b[5] = 1.550000;
		array_b[6] = 1.600000;
		array_b[7] = 1.650000;
		array_b[8] = 1.700000;
		array_b[9] = 1.750000;

		array_b[10] = 1.800000;
		array_b[11] = 1.850000;
		array_b[12] = 1.900000;
		array_b[13] = 2.000000;
		array_b[14] = 2.050000;

		array_b[15] = 2.100000;
		array_b[16] = 2.150000;
		array_b[17] = 2.200000;
		array_b[18] = 2.249000;
		array_b[19] = 2.275000;

		array_b[20] = 2.300000;
		array_b[21] = 2.344300;
		array_b[22] = 2.399600;
		array_b[23] = 2.490500;
		array_b[24] = 2.562700;

		array_b[25] = 2.670600;
		array_b[26] = 2.849800;
		array_b[27] = 3.207400;
		array_b[28] = 4.278200;
		array_b[29] = 4.813400;

		array_b[30] = 5.705200;
		array_b[31] = 6.418600;
		array_b[32] = 7.488700;

		array_h[0] = 0.000000;
		array_h[1] = 159.200000;
		array_h[2] = 294.400000;
		array_h[3] = 501.300000;
		array_h[4] = 795.800000;

		array_h[5] = 1154.000000;
		array_h[6] = 1795.000000;
		array_h[7] = 2862.000000;
		array_h[8] = 4383.000000;
		array_h[9] = 6044.000000;

		array_h[10] = 8122.000000;
		array_h[11] = 10590.000000;
		array_h[12] = 13160.000000;
		array_h[13] = 21170.000000;
		array_h[14] = 26750.000000;

		array_h[15] = 33760.000000;
		array_h[16] = 43800.000000;
		array_h[17] = 66000.000000;
		array_h[18] = 99740.000000;
		array_h[19] = 120960.000000;

		array_h[20] = 141210.000000;
		array_h[21] = 169600.000000;
		array_h[22] = 212170.000000;
		array_h[23] = 283130.000000;
		array_h[24] = 339890.000000;

		array_h[25] = 425040.000000;
		array_h[26] = 566950.000000;
		array_h[27] = 850760.000000;
		array_h[28] = 1702300.000000;
		array_h[29] = 2128000.000000;

		array_h[30] = 2837700.000000;
		array_h[31] = 3405100.000000;
		array_h[32] = 4256700.000000;


		for (int i = 0; i < blockSize; i++) { // For all points to evaluate
			double x = inReal[0][i];

			outReal[i] = interpolate(array_h, array_b, x);


			/*Output consistency check*/
			if (outReal[i] != outReal[i]) {
				error = "Output is nan";
				return 0;
			}
			if (fabs(outReal[i]) > DBL_MAX) {
				error = "Output is inf";
				return 0;
			}

		}
		return 1;
	}

	else {
		error = "Unknown function";
		return 0;
	}
}
