function [Ic_sc, Jc_sc] = Jc_NbTi_Bottura(T, B, fitParameters, wireParameters)
% Critical current density in a Nb-Ti wire/strand
% Calculation follows the fit proposed by L. Bottura
% Reference: L. Bottura, "A practical fit for the critical surface of NbTi," in IEEE Transactions on Applied Superconductivity, vol. 10, no. 1, pp. 1054-1057, March 2000, doi: 10.1109/77.828413.
% Function written by E. Ravaioli
% July 2020
% (C) Emmanuele Ravaioli, STEAM, TE-MPE-PE, CERN, Geneva, CH
% 
% The function calculates the critical current and critical current density
% in the superconductor either for one wire/strands or more than one
% simulateneously.
% 
% Note: The minimum accepted value of B is 1 uT: lower values will be set to 1 uT.
% 
% Fit:
% Jc_sc = Jc_ref * C0 * B^(alpha-1) / Bc2(T)^alpha * (1 - B/Bc2(T))^beta * (1 - (T/Tc0)^1.7)^gamma
% where Bc2(T) = Bc20 * (1 - (T/Tc0)^1.7)
% Inputs:
% - T: Temperature (scalar or vector) [K]
% - B: Magnetic field (scalar or vector) [T]
% - Structure defining fit parameters (scalars or vectors), OR matrix with parameters
% following this order (columns are different strands, rows are different parameters):
% -- Tc0: Critical temperature at B=0, I=0 [K]
% -- Bc20: Critical magnetic field at T=0, I=0 [T]
% -- Jc_ref: Parameter for the fit [A/m^2]
% -- C0: Parameter for the fit [-]
% -- alpha: Parameter for the fit [-]
% -- beta: Parameter for the fit [-]
% -- gamma: Parameter for the fit [-]
% - Structure defining wire/strand parameters (scalars or vectors), OR matrix with parameters
% following this order (columns are different strands, rows are different parameters):
% -- wireDiameter: Wire/strand diameter [m]
% -- Cu_noCu: Copper to non-Copper ration in the wire/strand [-]
% Outputs:
% - Ic_sc: Critical current in the wire/strand (scalars or vectors) [A]
% - Jc_sc: Critical current density in the superconductor (scalars or
% vectors) [A/m^2]


% % % Unpack parameters
if isstruct(fitParameters)==1
    Tc0    = fitParameters.Tc0;
    Bc20   = fitParameters.Bc20;
    Jc_ref = fitParameters.Jc_ref;
    C0     = fitParameters.C0;
    alpha  = fitParameters.alpha;
    beta   = fitParameters.beta;
    gamma  = fitParameters.gamma;
else
    Tc0    = fitParameters(1,:);
    Bc20   = fitParameters(2,:);
    Jc_ref = fitParameters(3,:);
    C0     = fitParameters(4,:);
    alpha  = fitParameters(5,:);
    beta   = fitParameters(6,:);
    gamma  = fitParameters(7,:);
end
if isstruct(wireParameters)==1
    wireDiameter = wireParameters.wireDiameter;
    Cu_noCu = wireParameters.Cu_noCu;
else
    wireDiameter = wireParameters(1,:);
    Cu_noCu = wireParameters(2,:);
end
f_sc = 1 ./ (1+Cu_noCu);

% % % Check all inputs are scalars or vectors
if ( isvector(T) && isvector(B) && isvector(Tc0) && isvector(Bc20) && isvector(Jc_ref) && isvector(C0) && isvector(alpha) && isvector(beta) && isvector(gamma) )~=1
    error('All inputs must be scalars or vectors with the same number of elements.')
end

nElements=max([ numel(T), numel(B), numel(Tc0), numel(Bc20), numel(Jc_ref), numel(C0), numel(alpha), numel(beta), numel(gamma) ]);

% % % Check all inputs are scalars or vectors with the same number of elements
if (...
        numel(T)>1  && numel(T)~=nElements  ||...
        numel(B)>1  && numel(B)~=nElements  ||...
        numel(Tc0)>1   && numel(Tc0)~=nElements   ||...
        numel(Bc20)>1  && numel(Bc20)~=nElements  ||...
        numel(Jc_ref)>1 && numel(Jc_ref)~=nElements ||...
        numel(C0)>1 && numel(C0)~=nElements ||...
        numel(alpha)>1 && numel(alpha)~=nElements ||...
        numel(beta)>1 && numel(beta)~=nElements ||...
        numel(gamma)>1 && numel(gamma)~=nElements)
    error('All inputs must be scalars or vectors with the same number of elements.')
end

% % % Find whether the inputs are given as vertical or horizontal vectors
% Note: iscolumn returns 1 in case of a scalar
if iscolumn(T)~=0||iscolumn(B)~=0||iscolumn(Tc0)~=0||iscolumn(Bc20)~=0
    flagColumn=0;
else
    flagColumn=1;
end

% % % Make vectors if T, B, Tc0, Bc20 are scalar
if flagColumn==1
    if numel(T)<nElements
        T = T*ones(nElements,1);
    end
    if numel(B)<nElements
        B = B*ones(nElements,1);
    end
    if numel(Tc0)<nElements
        Tc0 = Tc0*ones(nElements,1);
    end
    if numel(Bc20)<nElements
        Bc20 = Bc20*ones(nElements,1);
    end
elseif flagColumn==0
    if numel(T)<nElements
        T = T*ones(1,nElements);
    end
    if numel(B)<nElements
        B = B*ones(1,nElements);
    end
    if numel(Tc0)<nElements
        Tc0 = Tc0*ones(1,nElements);
    end
    if numel(Bc20)<nElements
        Bc20 = Bc20*ones(1,nElements);
    end
end

% % % Set minimum B
minB = 1e-6; % [T]

% % % Calculate absolute magnetic field [field polarity not important)
B = max(minB, abs(B));

% % % Calculate critical magnetic field [corrected to avoid Bc2=0]
Bc2 = max(1e-9, Bc20 .* (1 - (T./Tc0).^1.7));

% % % Calculate critical current density
Jc_sc = Jc_ref .* C0 .* B.^(alpha-1) ./ Bc2.^alpha .* (1 - B./Bc2).^beta .* (1 - (T./Tc0).^1.7).^gamma;

% % % Set to 0 the critical current density if T>=Tc0
Jc_sc(T>=Tc0) = 0;

% % % Set to 0 the critical current density if B>=Bc20
Jc_sc(B>=Bc20) = 0;

% % % Set to 0 the critical current if calculation yields a  negative value
Jc_sc(Jc_sc<0) = 0;

% % % Calculate critical current
Ic_sc = Jc_sc .* (pi/4 * wireDiameter.^2 .* f_sc);

end