function cpNb3Sn = cpNb3Sn_cryocomp_mat(T,Tc,Tc0,Tcs)
% C #  0.. 20 K: V.D. Arp, Stability and Thermal Quenches in Force-Cooled
% C #            Superconducting Cables, Superconducting MHD Magnet Design
% C #            Conference, MIT, pp 142-157, 1980.
% C # 20..400 K: G.S.Knapp,S.D.Bader,Z.Fisk, Phonon properties of A-15
% C #            superconductors obtained from heat capacity measurements,
% C #            Phys.Rev B, 13, no.9, pp 3783-3789, 1976.

pn=zeros(size(T));
ps=zeros(size(T));
p=zeros(size(T));

density = 8950.0;

% Normal component
idx1=find(T<=10);
idx2=find(T>10&T<=20);
idx3=find(T>20&T<=400);
idx4=find(T>400);

AA  =   38.2226876;
BB  =   -848.36422;
CC  =   1415.13807;
DD  =   -346.83796;
a   =  6.804586085;
b   =  59.92091818;
c   =  25.82863336;
d   =  8.779183354;
na  =   1;
nb  =   2;
nc  =   3;
nd  =   4;

pn(idx1)=(7.5475e-3)*T(idx1).^2;
pn(idx2)=(-0.3+0.00375*T(idx2).^2)/0.09937;
pn(idx3)=AA*T(idx3)./(a+T(idx3)).^na+BB*T(idx3).^2./(b+T(idx3)).^nb + CC*T(idx3).^3./(c+T(idx3)).^nc+DD*T(idx3).^4./(d+T(idx3)).^nd;
pn(idx4)=250.8246;


% Superconducting component
idxBelowTc=find(T<Tc);
idxBelowTcs=find(T<Tcs);
idxAboveTc=find(T>=Tc);

TT=Tc/Tc0;
dBc2Dt=-0.46306-0.067830*Tc;
delcp=1500*(dBc2Dt^2)/(2*(27.2/(1+0.34*TT))^2-1);
if Tc<=10
    cpntc=7.5475e-3*Tc^2;
elseif Tc<=20
    cpntc=(-0.3+0.00375*Tc^2)/0.09937;
end

ps(idxBelowTc)=(cpntc+delcp)*(T(idxBelowTc)/Tc).^3;
ps(idxAboveTc)=0;

p(idxAboveTc)=pn(idxAboveTc);
F=(T(idxBelowTc)-Tcs)/(Tc-Tcs);
p(idxBelowTc)=F.*pn(idxBelowTc)+(1-F).*ps(idxBelowTc);
p(idxBelowTcs)=ps(idxBelowTcs);


cpNb3Sn=density*p;

end