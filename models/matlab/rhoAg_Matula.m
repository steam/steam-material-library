function [rho] = rhoAg_Matula(T)

%Calculation of specific resistivity of Silver depending on temperature
%Input: Symbol / Description / Unit
% T / Temperature / K
%Output: Symbol / Description / Unit
% rho / Specific Resistivity / Ohm*m
%Source: Electrical Resistivity of Copper, Gold, Palladium and Silver, R.A.
%Matula, 1979
if T<4
    rho=1.18438607E-14;
else
    e = 2.718281828459;
    M = 107.868;
    Theta = 220.9;
    C = 0.0011650123;
    lowlim = 0;
    uplim = Theta/T;
    fun = @(x) (x.^5) .* e.^x ./ (e.^x- 1).^2 ;
    J_5 = quadl( fun, lowlim, uplim);
    rho = (C/(M*Theta)) * (T/Theta)^5 * J_5;
end
