function cpHe = cpHe_mat(T)
% Helium volumetric heat capacity
% Data from NIST
% https://nvlpubs.nist.gov/nistpubs/Legacy/TN/nbstechnicalnote1334.pdf
% Page 34
% Temperature range: 1-1500 K
% 
% Fit and function by E. Ravaioli, CERN, Geneva, Switzerland


cpHe=zeros(size(T));

idxT0=find(T<=0.8);
idxT1=find(T>0.8&T<=2.167);
idxT2=find(T>2.167&T<=2.7);
idxT3=find(T>2.7&T<=4.222);
idxT4=find(T>4.222&T<=10);
idxT5=find(T>10&T<=1500);
idxT6=find(T>1500);

% Polinomial fits
polyFit1=[252.885047589130,-100.373791096815,1.18394466523168,7.66174537467967,4.14183161911798];
polyFit2=[-817703.751104569,1609997.44015698,-1267260.35714448,498489.254701282,-98000.1183227793,7709.29932356629];
polyFit3=[3.46892446633873,-2.09165480174069,5.76373838168698];
polyFit4=[-16.5666756239721,44.0958304892635,-40.2962974183403,17.1939023689494];
polyFit5=[0.00966352468250771,-1.04744413875913,5.45949141403381];

cpHe(idxT0) = 3.29E+03;
cpHe(idxT1) = 10.^( polyval(polyFit1,log10(T(idxT1))) );
cpHe(idxT2) = 10.^( polyval(polyFit2,log10(T(idxT2))) );
cpHe(idxT3) = 10.^( polyval(polyFit3,log10(T(idxT3))) );
cpHe(idxT4) = 10.^( polyval(polyFit4,log10(T(idxT4))) );
cpHe(idxT5) = 10.^( polyval(polyFit5,log10(T(idxT5))) );
cpHe(idxT6) = 1.6887636E+02;

end