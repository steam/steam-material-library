function rhoAl = rhoAl_nist(T)
    % From NIST, 1984, Temperature range: 1-300 K
    % Ensure T and f_scaling are of the same size or scalar
    idxT1 = (T >= 0) & (T <= 300);
    idxT2 = (T > 300);

    a = 0.476050395713888;
    b = -3.299730411832043;
    c = 7.489576522510857;
    d = -5.738194411483883;
    e = 1.121748209742612;
    f = 0.207979301275814;
    g = -4.010635050869012;
    
    m = 5.44098441e-11;
    n = 2.452835175035380e-08;
    
    rhoAl = zeros(size(T));
    
    logT = log10(T(idxT1));
    logRho = polyval([a b c d e f g], logT);
    rhoAl(idxT1) = 10.^logRho*1e-8;
    
    rhoAl(idxT2) = m*T(idxT2) + n - m*300;
    
end
