function [Ic_sc, Jc_sc] = Jc_NbTi_Cudi(T, B, fitParameters, wireParameters)
% Critical current density in a Nb-Ti wire/strand
% Calculation follows the "Fit 3" described in the CUDI manual by A.
% Verweij
% Function written by E. Ravaioli
% July 2020
% (C) Emmanuele Ravaioli, STEAM, TE-MPE-PE, CERN, Geneva, CH
% 
% The function calculates the critical current and critical current density
% in the superconductor either for one wire/strands or more than one
% simulateneously.
% 
% Fit:
% Jc_sc = (c1+c2*B +c3*exp(-c4*B) +c5*exp(-c6*B)) *(1-T/Tc);
% where Tc = Tc0 * (1-B/Bc20)^.59
% Inputs:
% - T: Temperature (scalar or vector) [K]
% - B: Magnetic field (scalar or vector) [T]
% - Structure defining fit parameters (scalars or vectors), OR matrix with parameters
% following this order (columns are different strands, rows are different parameters):
% -- Tc0: Critical temperature at B=0, I=0 [K]
% -- Bc20: Critical magnetic field at T=0, I=0 [T]
% -- c1: Parameter for the fit [A]
% -- c2: Parameter for the fit [A/T]
% -- c3: Parameter for the fit [A]
% -- c4: Parameter for the fit [1/T]
% -- c5: Parameter for the fit [A]
% -- c6: Parameter for the fit [1/T]
% - Structure defining wire/strand parameters (scalars or vectors), OR matrix with 
% parameters following this order  (columns are different strands, rows are different 
% parameters):
% -- wireDiameter: Wire/strand diameter [m]
% -- Cu_noCu: Copper to non-Copper ration in the wire/strand [-]
% Outputs:
% - Ic_sc: Critical current in the wire/strand (scalars or vectors) [A]
% - Jc_sc: Critical current density in the superconductor (scalars or
% vectors) [A/m^2]


% % % Unpack parameters
if isstruct(fitParameters)==1
    Tc0  = fitParameters.Tc0;
    Bc20 = fitParameters.Bc20;
    c1   = fitParameters.c1;
    c2   = fitParameters.c2;
    c3   = fitParameters.c3;
    c4   = fitParameters.c4;
    c5   = fitParameters.c5;
    c6   = fitParameters.c6;
else
    Tc0  = fitParameters(1,:);
    Bc20 = fitParameters(2,:);
    c1   = fitParameters(3,:);
    c2   = fitParameters(4,:);
    c3   = fitParameters(5,:);
    c4   = fitParameters(6,:);
    c5   = fitParameters(7,:);
    c6   = fitParameters(8,:);
end
if isstruct(wireParameters)==1
    wireDiameter = wireParameters.wireDiameter;
    Cu_noCu = wireParameters.Cu_noCu;
else
    wireDiameter = wireParameters(1,:);
    Cu_noCu = wireParameters(2,:);
end
f_sc = 1 ./ (1+Cu_noCu);

% % % Check all inputs are scalars or vectors
if ( isvector(T) && isvector(B) && isvector(Tc0) && isvector(Bc20) && isvector(c1) && isvector(c2) && isvector(c3) && isvector(c4) && isvector(c5) && isvector(c6) )~=1
    error('All inputs must be scalars or vectors with the same number of elements.')
end

nElements=max([ numel(T), numel(B), numel(Tc0), numel(Bc20), numel(c1), numel(c2), numel(c3), numel(c4), numel(c5), numel(c6)  ]);

% % % Check all inputs are scalars or vectors with the same number of elements
if (...
        numel(T)>1  && numel(T)~=nElements  ||...
        numel(B)>1  && numel(B)~=nElements  ||...
        numel(Tc0)>1   && numel(Tc0)~=nElements   ||...
        numel(Bc20)>1  && numel(Bc20)~=nElements  ||...
        numel(c1)>1 && numel(c1)~=nElements ||...
        numel(c2)>1 && numel(c2)~=nElements ||...
        numel(c3)>1 && numel(c3)~=nElements ||...
        numel(c4)>1 && numel(c4)~=nElements ||...
        numel(c5)>1 && numel(c5)~=nElements ||...
        numel(c6)>1 && numel(c6)~=nElements)
    error('All inputs must be scalars or vectors with the same number of elements.')
end

% % % Find whether the inputs are given as vertical or horizontal vectors
% Note: iscolumn returns 1 in case of a scalar
if iscolumn(T)~=0||iscolumn(B)~=0||iscolumn(Tc0)~=0||iscolumn(Bc20)~=0
    flagColumn=0;
else
    flagColumn=1;
end

% % % Make vectors if T, B, Tc0, Bc20 are scalar
if flagColumn==1
    if numel(T)<nElements
        T = T*ones(nElements,1);
    end
    if numel(B)<nElements
        B = B*ones(nElements,1);
    end
    if numel(Tc0)<nElements
        Tc0 = Tc0*ones(nElements,1);
    end
    if numel(Bc20)<nElements
        Bc20 = Bc20*ones(nElements,1);
    end
elseif flagColumn==0
    if numel(T)<nElements
        T = T*ones(1,nElements);
    end
    if numel(B)<nElements
        B = B*ones(1,nElements);
    end
    if numel(Tc0)<nElements
        Tc0 = Tc0*ones(1,nElements);
    end
    if numel(Bc20)<nElements
        Bc20 = Bc20*ones(1,nElements);
    end
end

% % % Calculate absolute magnetic field [field polarity not important)
B = abs(B);

% % % Calculate critical temperature
f_B_Bc20 = B ./ Bc20;
f_B_Bc20(f_B_Bc20>1)=1; % avoid values higher than 1
Tc = Tc0 .* (1 - f_B_Bc20).^.59;

% % % Calculate critical current
f_T_Tc = T ./ Tc;
f_T_Tc(f_T_Tc>1)=1; % avoid values higher than 1
Ic_sc = (c1 + c2 .* B + c3 .* exp(-c4 .* B) + c5 .* exp(-c6 .* B)) .* (1 - f_T_Tc);

% % % Set to 0 the critical current if T>=Tc0
% Ic_sc(T>=Tc0) = 0;

% % % Set to 0 the critical current if B>=Bc20
% Ic_sc(B>=Bc20) = 0;

% % % Set to 0 the critical current if calculation yields a  negative value
Ic_sc(Ic_sc<0) = 0;

% % % Calculate critical current density
Jc_sc = Ic_sc ./ (pi/4 * wireDiameter.^2 .* f_sc);

end