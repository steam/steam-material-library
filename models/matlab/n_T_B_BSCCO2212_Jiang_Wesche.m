function n_T_B = n_T_B_BSCCO2212_Jiang_Wesche(T,B,n0)
% V-I Power Law Index n-value for a BSCCO2112 strand
% Function by D.S. Davis
% (C) Daniel Davis
% NHMFL, Tallahassee, FL
% Modified from function by E. Ravaioli

if size(T,1)==size(B,1)&size(T,2)==size(B,2)
    n_T_B=zeros(size(T));
elseif size(T,1)==size(B,2)&size(T,2)==size(B,1)
    n_T_B=zeros(size(T));
    B=B';
else
    if size(B)==[1,1]
        B=ones(size(T))*B;
        n_T_B=zeros(size(T));
    elseif size(T)==[1,1]
        T=ones(size(B))*T;
        n_T_B=zeros(size(B));
    else
        error('T and B must be scalars or vectors with the same number of elements.')
    end
end

% % % Modify the input magnetic field
B(B<0)=-B(B<0); % direction of the magnetic field is unimportant
B(abs(B)<.2)=.2; % very small magnetic field causes numerical problems

% % % Critical temperature, with B here defined as the field at which Jc is nearly zero (Using Jiang & Wesche Data)
a1=448.31;
a2=0.1154;
Tc_B=(log(a1)-log(B))/a2; % [K]


% Fit based on the observations of A. Francis in varying temperature Ic measurements
% Bottoms out at proportional Ohmic I-V relationship

%n0=17.75; %sample ID: 181210-pmm170725-1-4

% if T>=Tc_B
%   n_T_B=1;
% else
%   n_T_B=(n0-1).*(Tc_B-T)./(Tc_B); %
% end

% idxHigherThanTc=find(T>=Tc_B);
% idxLowerThanTc=find(T<Tc_B);
% n_T_B(idxHigherThanTc)=1;
% n_T_B(idxLowerThanTc)=(n0-1).*(Tc_B((idxLowerThanTc))-T((idxLowerThanTc)))./(Tc_B((idxLowerThanTc))); %

n_T_B=1+((n0-1).*(Tc_B-T)./Tc_B);
n_T_B(n_T_B<1)=1;


%% % % Scale this fit 
%n_T_B=n_T_B*n_scaling; % 

end