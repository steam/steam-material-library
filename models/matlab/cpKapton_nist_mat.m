function cpKapton = cpKapton_nist_mat(T)
%     Source http://cryogenics.nist.gov/NewFiles/Polyimide_kapton.html
%     Valid from 4 to 300 K
%     log Cp = a + b(logT) + c(logT)2 + d(logT)3 + e(logT)4 + f(logT)5 + g(logT)6 + h(logT)7
%
%     Function by E. Ravaioli, CERN, Geneva, CH
%     May 2019

    density=1420; % [kg/m^3]
    LimitValidityLow=1.9; % [K]

    HighLimitValidity=300;
    
    
    a0=-1.3684;
    a1=0.65892;
    a2=2.8719;
    a3=0.42651;
    a4=-3.0088;
    a5=1.9558;
    a6=-0.51998;
    a7=0.051574;
    fit_cp_kapton=[a7 a6 a5 a4 a3 a2 a1 a0];
    m = 1.6364e+03;
    n =  1.0e+05 *5.805408395545019;


    p=zeros(size(T));
    idxT1=find(T<=LimitValidityLow);
    idxT2=find((T>LimitValidityLow)&(T<HighLimitValidity));
    idxT3=find(T>=HighLimitValidity);

    logT=log10(LimitValidityLow);
    p(idxT1)=density*(10.^(a7*((logT).^7)+a6*((logT).^6)+a5*((logT).^5)+a4*((logT).^4)+a3*((logT).^3)+a2*((logT).^2)+a1*((logT))+a0));
    
    logT=log10(T(idxT2));
    logCp2=polyval(fit_cp_kapton,logT);
    p(idxT2)=density*(10.^logCp2);

    HighlogLimitValidity=T(idxT3);
    HighlogLimitValidity=polyval([m n],HighlogLimitValidity);
    p(idxT3)=HighlogLimitValidity;
    
    cpKapton=p;
end