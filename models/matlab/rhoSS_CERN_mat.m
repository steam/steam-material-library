function rhoSS = rhoSS_CERN_mat(T)
% range (K)	0-300
% obtained by scaling down by 0.824 an existing relationship to match data measured at CERN
% Emmanuele Ravaioli

    rhoSS=zeros(size(T));

    LimitValidityLow=0; % K
    LimitValidityHigh=300; % K
    
    idxT1=find(T<LimitValidityLow);
    idxT2=find(T>=LimitValidityLow&T<=LimitValidityHigh);
	idxT3=find(T>LimitValidityHigh);
    
    fit_rho_SS_CERN=[-6.16E-15 3.52E-12 1.72E-10 5.43E-07]/1.0867;

    fit_rho_SS_CERN_linearExtrapolation=[7.24E-10 5.2887E-7]/1.0867;
    
    rhoSS(idxT1)=polyval(fit_rho_SS_CERN,LimitValidityLow);
    
    rhoSS(idxT2)=polyval(fit_rho_SS_CERN,T(idxT2));
    
    rhoSS(idxT3)=polyval(fit_rho_SS_CERN_linearExtrapolation,T(idxT3));
    
end