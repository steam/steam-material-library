function cpNb3Sn = cpNb3Sn_CUDI_mat(T, B, Tc0_Nb3Sn, Bc20_Nb3Sn, fScale)
% Based on the CUDI fit as described in the document:
% G. Manfreda, "Review of ROXIE's Material Properties Database for Quench Simulation",
% CERN Internal Note 2011-24, EDMS Nr: 1178007, 2011.
% 
% Temperature range: 1-300 K
% 
% The non-linear jump in the heat capacity occurs at the critical temperature, which
% depends on the magnetic field B
% 
% fScale : Correction factor (due to different density assumed in the fit)

nElements=max([ length(T(:)), length(B(:)) ]);

% % % Check all inputs are scalars or vectors with the same number of elements
if (length(T(:))>1 && length(T(:))~=nElements) || (length(B(:))>1 && length(B(:))~=nElements)
    error('All inputs must be scalars or vectors with the same number of elements.')
end

% % % Modify B
B(B<0)=-B(B<0); % direction of the current is unimportant
B(B<.001)=.001; % avoid B=0 to avoid numerical issues

cpNb3Sn=zeros(size(T));

% Calculate critical temperature Tc
if nargin<4
    Bc20_Nb3Sn=29; % T
end
if nargin<3
    Tc0_Nb3Sn=18; % K
end    
alpha=.59;

% f_T_T0=min(1,T./Tc0_Nb3Sn);
% Bc2=Bc20_Nb3Sn .* (1-f_T_T0.^2) .* (1-.31*f_T_T0.^2.*(1-1.77.*log( f_T_T0 )));
% Tc=Tc0_Nb3Sn.*(1-B./Bc2).^alpha;
Tc=Tc0_Nb3Sn.*(1-B./Bc20_Nb3Sn).^alpha;

% Density of Nb3Sn
density = 8950.0; % [kg/m^3]


% Calculate cp
idxT0=find(T<=Tc);
idxT1=find(T>Tc&T<=26.113);
idxT2=find(T>26.113&T<=169.416);
idxT3=find(T>169.416&T<=300);
idxT4=find(T>300);

% Calculation if T<=Tc
a1 = 207 -3.83*B +2.86*B.^2;
a2 = -110*exp(-.434*B);
a3 = 38.8 -1.8*B +.0634*B.^2;
if length(B)>1
    cpNb3Sn(idxT0) = a3(idxT0).*T(idxT0).^3 +a2(idxT0).*T(idxT0).^2 +a1(idxT0).*T(idxT0);
else
    cpNb3Sn(idxT0) = a3*T(idxT0).^3 +a2*T(idxT0).^2 +a1*T(idxT0);
end

% Calculation if T>Tc
polyFit1=[7.42 0 1522 0];
polyFit2=[-61.635 19902 -305807];
polyFit3=[-7.4636 4411 763801];
polyFit4=[1415377];

cpNb3Sn(idxT0) = cpNb3Sn(idxT0) *fScale;
cpNb3Sn(idxT1) = polyval(polyFit1,T(idxT1)) *fScale;
cpNb3Sn(idxT2) = polyval(polyFit2,T(idxT2)) *fScale;
cpNb3Sn(idxT3) = polyval(polyFit3,T(idxT3)) *fScale;
cpNb3Sn(idxT4) = polyval(polyFit4,T(idxT4)) *fScale;

