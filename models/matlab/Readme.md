## **STEAM library of Matlab material-property functions**
## Collection of Matlab functions to model material properties
(Copyright © 2021, CERN, Switzerland. All rights reserved.)

## Description
This is a collection of Matlab functions developed by different colleagues from different institutions.
The STEAM team is not directly responsible for the development of all these functions, but they perform sanity checks on the functions and their output.
The software STEAM-LEDET relies on some of these Matlab functions.
These functions used to be part of the Gitlab project https://gitlab.cern.ch/steam/steam-ledet-material-library, and are now incorporated in this project.

## Links
STEAM website: https://espace.cern.ch/steam/_layouts/15/start.aspx#/SitePages/Material%20Properties.aspx

## Contact
steam-team@cern.ch

## STEAM User Agreement
By using any software of the STEAM framework, users agree with this document:
https://edms.cern.ch/document/2024516 .

In particular, by using these functions users agree on these points:
- The functions are free of charge, but come with no guarantee on the results.
- The functions can not be re-distributed to anyone.
