function kAg = kAg_RRR30(T)
% range (K)	1-300

    kAg=zeros(size(T));
    
    T1=1; % K
    T2=20; % K
    T3=140; % K
    
    idxT1=find(T<T1);
    idxT2=find(T>=T1&T<=T2);
	idxT3=find(T>=T2&T<=T3);
    idxT4=find(T>=T3);
    
    fit1=[log10(67)];
    fit2=[-0.326314237655585,0.551121278546028,-0.316726715333209,1.04530563240476,1.82603764562617];
    fit3=[87.6603250260841,-1304.32557090996,8449.57931641046,-31116.4147264840,71220.8138734661,-103708.203166996,93783.5641105213,-48134.2889933188,10734.3798030775];
    fit4=[log10(421)];
    
    logT1=log10(T(idxT1));
    logFit1=polyval(fit1,logT1);
    kAg(idxT1)=10.^logFit1;
    
    logT2=log10(T(idxT2));
    logFit2=polyval(fit2,logT2);
    kAg(idxT2)=10.^logFit2;
    
    logT3=log10(T(idxT3));
    logFit3=polyval(fit3,logT3);
    kAg(idxT3)=10.^logFit3;
    
    logT4=log10(T(idxT4));
    logFit4=polyval(fit4,logT4);
    kAg(idxT4)=10.^logFit4;
    
end