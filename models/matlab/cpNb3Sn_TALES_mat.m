function cpNb3Sn = cpNb3Sn_TALES_mat(T,B,Tc,Tc0,Bc20)
% Appears in TALES, not sure where it comes from

p=zeros(size(T));

if size(B)==[1,1]
    B=ones(size(T))*B;
end

% Coefficients for Cp of Nb3Sn
density = 8950.0; % [kg/m^3]

beta = 1.241e-3; % [J/K^4/kg]
gamma = 0.138; % [J/K^2/kg]
dca=79.78547;
dcb=-247.44839;
dcc=305.01434;
dcd=-186.90995;
dce=57.48133;
dcf=-6.3977;
dcg=-0.6827738;
dch=0.1662252;

idx1=find(T<Tc);
idx2=find(T<20);
idx3=find(T<400);
idx4=find(T>=400);

p(idx1)=(beta+3*gamma/Tc0^2)*T(idx1).^3 + gamma*B(idx1)/Bc20.*T(idx1);
p(idx2)=beta*T(idx2).^3 + gamma*T(idx2);
logT3=log10(T(idx3));
p(idx3)=10.^(dca+dcb*logT3+dcc*logT3.^2+dcd*logT3.^3+dce*logT3.^4+dcf*logT3.^5+dcg*logT3.^6+dch*logT3.^7);
p(idx4)=234.89+0.0425*T(idx4);

cpNb3Sn=density*p;

end