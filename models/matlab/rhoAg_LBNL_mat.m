function rhoAg = rhoAg_LBNL_mat(T,B,RRR,Tref)
% Function Written by Lucas Brouwer
% Lawrence Berkeley National Laboratory
% Modified by Daniel Davis
% National High Magnetic Field Laboratory
% Further modified by Emmanuale Ravaioli
% CERN, Geneva, CH
% May 2020

% The resistivity of silver is fit as function of temperature, RRR, and magnetic field. 
% The data used for the fit (both vs. temperature and field) was extracted
% from the Smith and Fickett paper [2], and the functional form for the fit follows the 
% NIST resistivity fit for copper. The fit focuses on accuracy in a RRR range of 100-200, 
% which is most applicable to the Bi2212 wires.
% 
% Note: Tref is the upper temperature at which the RRR is defined. By
% default, it should be set to 273 K (Smith/Fickett).

    % Default value for Tref
    if nargin<4
        Tref=273;
    end

    % % % Check all inputs are scalars or vectors
    if ( isvector(T) && isvector(B) && isvector(RRR) && isvector(Tref) )~=1
        error('All inputs must be scalars or vectors with the same number of elements.')
    end

    nElements=max([ length(T), length(B), length(RRR), length(Tref) ]);

    % % % Check all inputs are scalars or vectors with the same number of elements and shape
    if (length(T)>1    && all(size(T)~=size(B))) ||...
       (length(T)>1    && all(size(T)~=size(RRR))) ||...
       (length(T)>1    && all(size(T)~=size(Tref))) ||...
       (length(B)>1    && all(size(B)~=size(T))) ||...
       (length(B)>1    && all(size(B)~=size(RRR))) ||...
       (length(B)>1    && all(size(B)~=size(Tref))) ||...
       (length(RRR)>1  && all(size(RRR)~=size(T))) ||...
       (length(RRR)>1  && all(size(RRR)~=size(B))) ||...
       (length(RRR)>1  && all(size(RRR)~=size(Tref))) ||...
       (length(Tref)>1 && all(size(RRR)~=size(T))) ||...
       (length(Tref)>1 && all(size(RRR)~=size(B))) ||...
       (length(Tref)>1 && all(size(RRR)~=size(RRR)))
        error('All inputs must be scalars or vectors with the same number of elements and shape.')
    end
    
    % % % If one of T, B, and RRR is given as a vector and one of two of the others are not, make them vectors with as many elements
    if length(T)>=length(B) && length(T)>=length(RRR)
        if size(B)==[1,1]
            B=ones(size(T))*B;
        end
        if size(RRR)==[1,1]
            RRR=ones(size(T))*RRR;
        end
    elseif length(B)>=length(T) && length(B)>=length(RRR)
        if size(RRR)==[1,1]
            RRR=ones(size(B))*RRR;
        end
        if size(T)==[1,1]
            T=ones(size(B))*T;
        end
    elseif length(RRR)>=length(T) && length(RRR)>=length(B)
        if size(T)==[1,1]
            T=ones(size(RRR))*T;
        end
        if size(B)==[1,1]
            B=ones(size(RRR))*B;
        end
    end

    tref = 273.0;
    
    % Constants
    c0 = 1.467e-8;    % Silver resistivity at 273 K (Smith/Fickett)
    tref_RRR = Tref;
 
    P1 = 1.18777e-15;   
    P2 = 3.56474;
    P3 = 3.18825e10;    
    P4 = 1.09259;    
    P5 = 49.7615;   
    P6 = 3.01754;   
    P7 = -0.557109;   
 
    a0 = -4.36736;
    a1 = 2.31218;
    a2 = -0.0966798;
    a3 = -0.0931658;
    a4 = 0.0151959;
    
    
    b=abs(B);
 
    c0_scale = tref./tref_RRR;
    rho_0 = c0_scale*c0./RRR;
    rho_i = P1.*(T.^P2)./(1+P1.*P3.*(T.^(P2-P4)).*exp(-((P5./T).^P6))); 
    rho_i0 = P7*rho_i.*rho_0./(rho_i+rho_0);
    rho_n = rho_0+rho_i+rho_i0;
    
    % Find indices with small magnetic field
    idxSmallB    = b<=0.01d0;
    idxNotSmallB = b >0.01d0;
    corr=zeros(size(rho_n));
    
    % If magnetic field is small
    corr(idxSmallB)=0.0d0;
    
    % If magnetic field is not small
    if sum(idxNotSmallB)>0
        x = c0 * b(idxNotSmallB) ./ rho_n(idxNotSmallB);
        log_x = log10(x);
        f_exp = a0 + a1*log_x + a2*(log_x.^2) + a3*(log_x.^3) + a4*(log_x.^4);
        corr(idxNotSmallB)= 10.0d0.^f_exp;
    end

    % Calculate resistivity
    rhoAg = rho_n.*(1+corr);
            
end
