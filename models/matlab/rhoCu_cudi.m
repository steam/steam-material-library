function rhoCu = rhoCu_cudi(T,B,RRR,MR)

rhoCu=zeros(size(T));

if size(RRR)==[1,1]
    RRR=ones(size(T))*RRR;
end
if size(B)==[1,1]
    B=ones(size(T))*B;
end
if size(MR)==[1,1]
    MR=ones(size(T))*MR;
end

c0=1.7;
c1=2.3E9;
c2=9.57E5;
c3=163;

rhoCu=(c0./RRR+1./(c1./T.^5+c2./T.^3+c3./T))/10^8+MR.*B;

end