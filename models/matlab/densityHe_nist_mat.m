function densityHe = densityHe_nist_mat(T)
% % % From NIST, https://nvlpubs.nist.gov/nistpubs/Legacy/TN/nbstechnicalnote1334.pdf
% % % Temperature range: 0.8-1000 K
% % % The formula gives the density in [kg/m^3]

densityHe=zeros(size(T));

idxT0=find(T<=1.6);
idxT1=find(T>1.6&T<=2.167);
idxT2=find(T>2.167&T<=4.222);
idxT3=find(T>4.222&T<=20);
idxT4=find(T>20&T<=1500);
idxT5=find(T>1500);

% Polinomial fits
polyFit1=[2.11416939375024,-1.52598215218988,0.376561004541185,2.13605330409407];
polyFit2=[-13.2534629276111,22.3402757369487,-14.6074099333166,4.25768883178274,1.71041439003585];
polyFit3=[-11.3703467424725,57.8805160101170,-117.249581388477,118.438559118732,-60.9051212476063,13.9060268302894];
polyFit4=[-0.000343946768435402,-0.997784222538021,1.68461132135252];

densityHe(idxT0) = 146.9;
densityHe(idxT1) = 10.^( polyval(polyFit1,log10(T(idxT1))) );
densityHe(idxT2) = 10.^( polyval(polyFit2,log10(T(idxT2))) );
densityHe(idxT3) = 10.^( polyval(polyFit3,log10(T(idxT3))) );
densityHe(idxT4) = 10.^( polyval(polyFit4,log10(T(idxT4))) );
densityHe(idxT5) = 3.25E-02;


end