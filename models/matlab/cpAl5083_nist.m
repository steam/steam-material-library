function cpAl = cpAl5083_nist(T)
%  5083 Aluminum (UNS A95083)
% Temperature range: 4-300 K

   cpAl=zeros(size(T));

   density=2699;
   a=2.96344;
   b=-38.3094;
   c=210.351;
   d=-637.795;
   e=1162.27;
   f=-1298.3;
   g=866.662;
   h=-314.292;
   i=46.6467;

   m = 3175.31605;
   logRho_300=polyval([a b c d e f g h i],log10(300));
   n = (10^logRho_300)*density - m*300;

   HighLimitValidity=300;
   idxT1=find(T>=HighLimitValidity);

   LowLimitValidity=4;
   idxT2=find(T<=LowLimitValidity);

   idxT3=find((T>LowLimitValidity)&(T<HighLimitValidity));
   
    logT=log10(T(idxT3));
    logcpAl=polyval([a b c d e f g h i],logT);
    cpAl(idxT3)=density*10.^logcpAl;

    LowlogLimitValidity=log10(LowLimitValidity);
    LowlogLimitValidity=polyval([a b c d e f g h i],LowlogLimitValidity);
    cpAl(idxT2)=density*10.^LowlogLimitValidity;

    fit_cp_Ag_HIgh = [m n];
    cpAl(idxT1)=polyval(fit_cp_Ag_HIgh,T(idxT1));

end