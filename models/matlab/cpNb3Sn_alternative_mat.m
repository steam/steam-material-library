function cpNb3Sn = cpNb3Sn_alternative_mat(T, B, Tc0_Nb3Sn, Bc20_Nb3Sn)
% Based on the alternative fit as described in the document:
% G. Manfreda, "Review of ROXIE's Material Properties Database for Quench Simulation",
% CERN Internal Note 2011-24, EDMS Nr: 1178007, 2011.
% 
% Temperature range: 1-400 K
% 
% The non-linear jump in the heat capacity occurs at the critical temperature, which
% depends on the magnetic field B

nElements=max([ length(T(:)), length(B(:)) ]);

% % % Check all inputs are scalars or vectors with the same number of elements
if (length(T(:))>1 && length(T(:))~=nElements) || (length(B(:))>1 && length(B(:))~=nElements) || (length(Tc0_Nb3Sn(:))>1 && length(Tc0_Nb3Sn(:))~=nElements) || (length(Bc20_Nb3Sn(:))>1 && length(Bc20_Nb3Sn(:))~=nElements)
    error('All inputs must be scalars or vectors with the same number of elements.')
end
if (length(Tc0_Nb3Sn(:))>1 & length(Bc20_Nb3Sn(:))==1) || (length(Tc0_Nb3Sn(:))==1 & length(Bc20_Nb3Sn(:))>1)
    error('Variables Tc0_Nb3Sn and Bc20_Nb3Sn must have the same number of elements.')
end

% % % Modify B
B(B<0)=-B(B<0); % direction of the current is unimportant
B(B<.001)=.001; % avoid B=0 to avoid numerical issues

cpNb3Sn=zeros(size(T));

% Calculate critical temperature Tc
if nargin<4
    Bc20_Nb3Sn=29; % T
end
if nargin<3
    Tc0_Nb3Sn=18; % K
end    
alpha=.59;

Tc=Tc0_Nb3Sn.*(1-B./Bc20_Nb3Sn).^alpha;

% Density of Nb3Sn
density = 8950.0; % [kg/m^3]


% Calculate cp
idxT0=find(T<=Tc);
idxT1=find(T>Tc&T<=20);
idxT2=find(T>20&T<=400);
idxT3=find(T>400);

% Calculation if T<=Tc
betaT=1.241E-3; % [J/K^4/kg]
gammaT=.138; % [J/K^2/kg] Sommerfeld constant
if length(B(:))>1 && length(Tc0_Nb3Sn(:))>1
    cpNb3Sn(idxT0)=density.*((betaT+3*gammaT./Tc0_Nb3Sn(idxT0).^2).*T(idxT0).^3+gammaT.*B(idxT0)./Bc20_Nb3Sn(idxT0).*T(idxT0));
elseif length(B(:))>1
    cpNb3Sn(idxT0)=density.*((betaT+3*gammaT/Tc0_Nb3Sn^2).*T(idxT0).^3+gammaT.*B(idxT0)/Bc20_Nb3Sn.*T(idxT0));
elseif length(Tc0_Nb3Sn(:))>1
    cpNb3Sn(idxT0)=density.*((betaT+3*gammaT./Tc0_Nb3Sn(idxT0).^2).*T(idxT0).^3+gammaT*B./Bc20_Nb3Sn(idxT0).*T(idxT0));
elseif length(B(:))==1 && length(Tc0_Nb3Sn(:))==1
    cpNb3Sn(idxT0)=density.*((betaT+3*gammaT/Tc0_Nb3Sn^2).*T(idxT0).^3+gammaT*B/Bc20_Nb3Sn.*T(idxT0));
end

% Calculation if Tc<T<=20 K
cpNb3Sn(idxT1)=(betaT*T(idxT1).^3+gammaT*T(idxT1))*density;

% Calculation if 20 K<T<=400 K
polyFit_20K_400K=[0.1662252 -0.6827738 -6.3977 57.48133 -186.90995 305.01434 -247.44839 79.78547];
logT=log10(T(idxT2));
logCp2=polyval(polyFit_20K_400K,logT);
cpNb3Sn(idxT2)=(10.^logCp2)*density;

% Calculation if T>400 K
m = 4.004004004004004e+02;
log400=log10(400);
logCp400=polyval(polyFit_20K_400K,log400);
n =(10.^logCp400)*density - m*400;

logCp400K=polyval([m n],T(idxT3));
cpNb3Sn(idxT3)=logCp400K;

cpNb3Sn=cpNb3Sn;
