function Jc_T_B = Jc_Nb3Sn_Bordini(T, B, C0, Tc0_Nb3Sn, Bc20_Nb3Sn, alpha)
% Critical current density in a Nb3Sn strand, Bordini fit
% Function by E. Ravaioli
% (C) Emmanuele Ravaioli
% LBNL, Berkeley, CA

% % % Check all inputs are scalars or vectors
if ( isvector(T) && isvector(B) && isvector(C0) && isvector(Tc0_Nb3Sn) && isvector(Bc20_Nb3Sn) && isvector(alpha) )~=1
    error('All inputs must be scalars or vectors with the same number of elements.')
end

nElements=max([ length(T), length(B), length(C0), length(Tc0_Nb3Sn), length(Bc20_Nb3Sn), length(alpha) ]);

% % % Check all inputs are scalars or vectors with the same number of elements
if (length(T)>1 && length(T)~=nElements) || (length(B)>1 && length(B)~=nElements) ||...
        (length(C0)>1 && length(C0)~=nElements) ||...
        (length(Tc0_Nb3Sn)>1 && length(Tc0_Nb3Sn)~=nElements) ||...
        (length(Bc20_Nb3Sn)>1 && length(Bc20_Nb3Sn)~=nElements)||...
        (length(alpha)>1 && length(alpha)~=nElements)
    error('All inputs must be scalars or vectors with the same number of elements.')
end

% % % Modify the input magnetic field
B(B<0)=-B(B<0); % direction of the magnetic field is unimportant
B(abs(B)<.001)=.001; % very small magnetic field causes numerical problems

f_T_T0=T./Tc0_Nb3Sn;
f_T_T0(f_T_T0>1)=1; % avoid values higher than 1
Bc2=Bc20_Nb3Sn.*(1-f_T_T0.^1.52);
f_B_Bc2=B./Bc2;
f_B_Bc2(f_B_Bc2>1)=1; % avoid values higher than 1
C=C0.*(1-f_T_T0.^1.52).^alpha.*(1-f_T_T0.^2).^alpha;

Jc_T_B = C./B.*f_B_Bc2.^0.5.*(1-f_B_Bc2).^2;  % [A/m^2]

end