function rhoCu = rhoCu_nist_cable2(T,B,RRR,sparseStrandToCable,f_MR,nS,StrandToCable)
% This function calculates the resistivity of copper based on NIST
% properties.
% Inputs:
% T : Vector of temperatures in each cable
% B : Vector of magnetic field in each strand of each cables
% RRR : Vector of RRR of each cable
% StrandToCable : Vector indicating in which cable each strand is located
% f_MR : (optional) Factor to scale the magneto-resistivity (default=1)
% See also the function rhoCu_nist, which works with scalars or same-size
% vectors for T, B, RRR.
% 
% E. Ravaioli, CERN, Geneva, CH

% rhoCu=zeros(size(T));

% nCables=length(rhoCu);
% nStrands=length(StrandToCable);
% sparseStrandToCable=sparse(1:nStrands,StrandToCable,1);
% nS=sum(sparseStrandToCable);
nS=sum(sparseStrandToCable)';

if nargin<5
    f_MR=1; % default, no change of the magneto-ressitivity
end

if size(B)==[1,1]
    B=ones(size(T))*B;
end

% % % Take absolute magnetic field
B=abs(B);

% Avoid passing too little magnetic field to the code below
% (the function diverges for low B)
B(B<.1)=.1;

rho0=1.553e-8./RRR;
rhoi=1.171e-17*(T.^4.49)./(1+4.48e-7*(T.^3.35).*exp(-(50./T).^6.428));
rhoiref=0.4531*rho0.*rhoi./(rho0+rhoi);
rhoCu=rho0+rhoi+rhoiref;

lgs=0.43429*log(1.553D-8*B./rhoCu(StrandToCable));
lgs=lgs*sparseStrandToCable./nS; % calculate average (this fancy way makes it faster)
poly=-2.662+lgs.*(0.3168+lgs.*(0.6229+lgs.*(-0.1839+lgs*0.01827)));
corrs=(10.^poly);

rhoCu=(1.+corrs*f_MR).*rhoCu;

end
