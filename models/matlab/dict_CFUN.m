function functionName_C = dict_CFUN(functionName_M)
    % This function acts as a dictionary between STEAM Matlab-based and
    % C-based  material property functions. The C-based functions are
    % comiled as .mex files.

    C = struct(...
    'kSS_mat', 'CFUN_kSteel',...
    'bb','Hey'...
    );
    
    functionName_C = C.(functionName_M);
end