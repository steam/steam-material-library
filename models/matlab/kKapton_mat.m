function kKapton = k_Kapton_mat(T)
% Function to calculate kapton thermal conductivity
% T in [K]
% k in [W/K/m]
% Range	of validity:  1-500 K
% E. Ravaioli, TE-MPE, CERN

    kKapton=zeros(size(T));

    T_4K=4.3; % K
    LimitValidity=500; % K
    
    idxT1=find(T<=T_4K);
    idxT2=find((T>T_4K)&(T<=LimitValidity));
	idxT3=find(T>LimitValidity);


	kKapton(idxT1)=.010703-.00161*(4.3-T(idxT1));
   
    a=5.73101;
    b=-39.5199;
    c=79.9313;
    d=-83.8572;
    e=50.9157;
    f=-17.9835;
    g=3.42413;
    h=-0.27133;
    m = 1.6367e-04;

    log_500=log10(500);
    exp_500=a+b*log_500+c*log_500.^2+d*log_500.^3+e*log_500.^4+f*log_500.^5+g*log_500.^6+h*log_500.^7;
    n = 10.^exp_500 - m*500;

    logT=log10(T(idxT2));
    logk=a+b*logT+c*logT.^2+d*logT.^3+e*logT.^4+f*logT.^5+g*logT.^6+h*logT.^7;
    kKapton(idxT2)=10.^logk;
    
    HighlogLimitValidity=T(idxT3);
    HighlogLimitValidity=polyval([m n],HighlogLimitValidity);
    kKapton(idxT3)=HighlogLimitValidity;

end