function rhoBeCu = rhoBeCu_nist_mat(T, f_scaling)
    % Electrical Resistivity Dependence of C17200 Beryllium Copper on Test Temperature (4-300 K).
    % Source: N. J. Simon, �. S. Drexler, and R. P. Reed, "Properties of Copper and Copper Alloys at Cryogenic Temperatures", NIST Monograph 177, 1992
    % https://nvlpubs.nist.gov/nistpubs/Legacy/MONO/nistmonograph177.pdf
    % 
    % Function by E. Ravaioli, TE-MPE-PE, CERN, Geneva, CH
    % August 2021
    a = -1.23892787e-15;
    b = 6.67134053e-13;
    c = -4.98835279e-12;
    d = 8.19454022e-08;

    m = 6.94283755e-11;
    n = 0.862113959930000*1.0e-07;
    
    % Ensure T and f_scaling are of the same size or scalar
    if ~isscalar(T) && ~isscalar(f_scaling) && ~isequal(size(T), size(f_scaling))
        error('T and f_scaling must be of the same size or scalar.');
    end
    
    % Scaling factor (optional)
    if nargin < 2
        f_scaling = 1;
    end

    idxT1 = (T >= 0) & (T < 19.6);
    idxT2 = (T >= 19.6) & (T <= 300);
    idxT3 = (T >= 300);

    rhoBeCu = zeros(size(T));
    rhoBeCu(idxT1) = 8.2095e-08;
    if length(f_scaling)==1
        rhoBeCu(idxT2) = (a * T(idxT2).^3 + b * T(idxT2).^2 + c * T(idxT2) + d) .* f_scaling;
        rhoBeCu(idxT3) = (n + m * T(idxT3)) .* f_scaling;
    else
        rhoBeCu(idxT2) = (a * T(idxT2).^3 + b * T(idxT2).^2 + c * T(idxT2) + d) .* f_scaling(idxT2);
        rhoBeCu(idxT3) = (n + m * T(idxT3)) .* f_scaling(idxT3);
    end


    % Old approximation
    % T_meas = [4 19.6 75.8 192 273];
    % rho_el_meas = [ 82.1 81.8 84.9 96.8 105.1]/1e9; % original

    % % Interpolate resistivity for T <= 300
    % rhoBeCu = interp1(T_meas, rho_el_meas, T, 'linear', 'extrap');

    % rhoBeCu = f_scaling * rhoBeCu;
end
