function Jc_T_B = Jc_T_B_BSCCO2212_Power_Jiang_Wesche(T,B,f_scaling)
% Critical current density in a BSCCO2112 strand
% Fit investigated by M. Brown
% Experimental data from J. Jiang and Wesche
% Function by D.S. Davis
% (C) Daniel Davis
% NHMFL, Tallahassee, FL
% Modified from function by E. Ravaioli

if size(T,1)==size(B,1)&size(T,2)==size(B,2)
    Jc_T_B=zeros(size(T));
elseif size(T,1)==size(B,2)&size(T,2)==size(B,1)
    Jc_T_B=zeros(size(T));
    B=B';
else
    if size(B)==[1,1]
        B=ones(size(T))*B;
        Jc_T_B=zeros(size(T));
    elseif size(T)==[1,1]
        T=ones(size(B))*T;
        Jc_T_B=zeros(size(B));
    else
        error('T and B must be scalars or vectors with the same number of elements.')
    end
end

% % % Modify the input magnetic field
B(B<0)=-B(B<0); % direction of the magnetic field is unimportant
B(abs(B)<.2)=.2; % very small magnetic field causes unreal Tc[B]

% % % Critical temperature, with B here defined as the field at which Jc is nearly zero (Using Jiang & Wesche Data)
a1=448.31;
a2=0.1154;
Tc_B=(log(a1)-log(B))/a2; % [K]

% B0=[3 5 8 10 12 14];
% Jc0=[618.2	568	500.67	482.3	458.2	438.11];
% Fit based on the data above (round wire, 1.2 mm diameter, 25% of superconductor)
b1=852.002/(pi*0.0012^2*0.25/4);
b2=0.191301;
Jc_T0_B=b1.*B.^-b2; % [A/m^2]

if T>=Tc_B
  Jc_T_B=zeros(size(Jc_T_B));
else
  Jc_T_B=Jc_T0_B.*(Tc_B-T)./(Tc_B); % [A/m^2]
end

% Nonreal negative values
Jc_T_B(Jc_T_B<0)=0;

% % % Scale this fit 
Jc_T_B=Jc_T_B.*f_scaling; % [A/m^2]

end