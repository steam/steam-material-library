function kSS = kSS_mat(T)
% 304 SS
% Source: E.D. Marquardt, J.P. Le, and Ray Radebaugh, Cryogenic Material Properties Database
% range (K)	4-300 (EXTENDED HERE ARBITRARILY TO 500 K)
% Emmanuele Ravaioli, LBNL, Berkeley, CA
% June 8 2017

    kSS=zeros(size(T));
    
    LimitValidity=300; % K
    
    idxT1=find(T<=LimitValidity);
	idxT2=find(T>LimitValidity);

    a=-1.4087;
    b=1.3982;
    c=0.2543;
    d=-0.626;
    e=0.2334;
    f=0.4256;
    g=-0.4658;
    h=0.165;
    ii=-0.0199;
    m = 0.0265;
    n = 7.358654;

    logT=log10(T(idxT1));
    logk=a+b*logT+c*logT.^2+d*logT.^3+e*logT.^4+f*logT.^5+g*logT.^6+h*logT.^7+ii*logT.^8;
    kSS(idxT1)=10.^logk;
    
    HighlogLimitValidity=T(idxT2);
    HighlogLimitValidity=polyval([m n],HighlogLimitValidity);
    kSS(idxT2)=HighlogLimitValidity;
end