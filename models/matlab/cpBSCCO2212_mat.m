function cpBSCCO2212 = cpBSCCO2212_mat(T)
% (C) Emmanuele Ravaioli
% LBNL, Berkeley, CA
% range (K)	1-333
% For T>333 K, a linear approximation is arbitrarily chosen

    cpBSCCO2212=zeros(size(T));
    
%     density=7850; % [kg/m^3]
    density=6600; % [kg/m^3]
    
    LimitValidityLow=1; % K
    LimitValidityHigh=333; % K
    
    idxT1=find(T<LimitValidityLow);
    idxT2=find(T>=LimitValidityLow&T<=LimitValidityHigh);
	idxT3=find(T>LimitValidityHigh);
    
    fit_cp_BSCCO2212=[-2.42416626823659,28.4237015006178,-137.032430914329,347.713024194822,-489.818611373570,361.755938829780,-103.662828238944,-6.00000115633633];
    fit_cp_BSCCO2212_AboveValidityHigh=[0.578520351476380   1.207941928414329];

    logLimitValidityLow=log10(LimitValidityLow);
    logkLimitValidityLow=polyval(fit_cp_BSCCO2212,logLimitValidityLow);
    cpBSCCO2212(idxT1)=10^logkLimitValidityLow;
    
    logT=log10(T(idxT2));
    logCp=polyval(fit_cp_BSCCO2212,logT);
    cpBSCCO2212(idxT2)=10.^logCp;
    
    logLimitValidityHigh=log10(T(idxT3));
    logkLimitValidityHigh=polyval(fit_cp_BSCCO2212_AboveValidityHigh,logLimitValidityHigh);
    cpBSCCO2212(idxT3)=10.^logkLimitValidityHigh;
    
    cpBSCCO2212=cpBSCCO2212*density;
end