function [heat_capacity_ag] = cpAg(T)

%Calculation of specific heat capacity of Silver depending on temperature
%in the temperature range between 4K and 300K
%Input: Symbol / Description / Unit
% T / Temperature / K
%Output: Symbol / Description / Unit
% heat_cap / Specific heat capaciy/ J/(kg*K)
%Source: Jun Feng, PSFC/RR-10-7 Thermohydraulic Quenching Simulation for
%Superconducting Magnets made of YBCO HTS Tape 2010

a = [0.00201, 1.11189*10^-4, 3.70101*10^-6];
b = [-0.012, -0.04289, -0.00326];
c = [0.071, 6.05068, 1.0369];
d = [-2.66454*10^-14, -100.586, 116.90736];
for k=1:length(T)
    if T(k) <= 30
        n = 1;
    else
        if T(k) <= 125
            n = 2;
        else
            n = 3;
        end
    end
heat_capacity_ag(k) = a(n)*T(k)^3 + b(n)*T(k)^2 + c(n)*T(k) + d(n);
end
