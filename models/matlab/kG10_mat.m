function kG10 = kG10_mat(T)
% Gerard Willering, 11-04-2007
% Source: http://cryogenics.nist.gov/NewFiles/G10_CR.html
% range (K)	4-300 (EXTENDED HERE ARBITRARILY TO 500 K)
% Normal direction


    kG10=zeros(size(T));
    
    LimitValidity=500; % K
    
    idxT1=find(T<LimitValidity);
	idxT2=find(T>=LimitValidity);

    a=-4.1236;
    b=13.788;
    c=-26.068;
    d=26.272;
    e=-14.663;
    f=4.4954;
    g=-0.6905;
    h=0.0397;
    m =  0.0017;
    n =  0.0957235800137799;

    logT=log10(T(idxT1));
    logk=a+b*logT+c*logT.^2+d*logT.^3+e*logT.^4+f*logT.^5+g*logT.^6+h*logT.^7;
    kG10(idxT1)=10.^logk;

    HighlogLimitValidity=T(idxT2);
    HighlogLimitValidity=polyval([m n],HighlogLimitValidity);
    kG10(idxT2)=HighlogLimitValidity;

end