function cvHe = cvHeMass_nist_mat(T)
% % % From NIST, https://nvlpubs.nist.gov/nistpubs/Legacy/TN/nbstechnicalnote1334.pdf
% % % Temperature range: 0.8-1000 K
% % % The formula gives the thermal capacitance in [J/K/kg]

cvHe=zeros(size(T));

idxT0=find(T<=0.8);
idxT1=find(T>0.8&T<=2.167);
idxT2=find(T>2.167&T<=3);
idxT3=find(T>3&T<=4.222);
idxT4=find(T>4.222&T<=10);
idxT5=find(T>10);

% Polinomial fits
polyFit1=[241.448012111007,-95.5201593998280,0.874630242276258,7.60977238688473,1.97693963530308];
polyFit2=[-191037.978607428,401872.287717891,-337475.272209836,141422.767309501,-29577.3097166323,2473.32506486582];
polyFit3=[-2.58146202246642,3.56109372768921,2.18906733625052];
polyFit4=[-0.766746819633252,2.12000865414934,-1.94508411823656,4.08521093984522];

cvHe(idxT0) = 22.40;
cvHe(idxT1) = 10.^( polyval(polyFit1,log10(T(idxT1))) );
cvHe(idxT2) = 10.^( polyval(polyFit2,log10(T(idxT2))) );
cvHe(idxT3) = 10.^( polyval(polyFit3,log10(T(idxT3))) );
cvHe(idxT4) = 10.^( polyval(polyFit4,log10(T(idxT4))) );
cvHe(idxT5) = 3116;


end