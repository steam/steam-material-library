#include "mex.h"

#define MAX_NUM_ARGS 15

extern int eval(const char *func, int nArgs, const double **inReal, const double **inImag, int blockSize, double *outReal, double *outImag);

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {

    // Initialize variables
    char *func_name;
    mxArray **V_in_real;
    const double **inReal;
    mxArray *b_out_real, *error_out;
    double *b_real, *error;
    const mwSize *dims;
    int nArgs, rows, cols, i;

    // Check input arguments
    if (nrhs < 2 || nrhs > MAX_NUM_ARGS+1) {
        mexErrMsgIdAndTxt("MyToolbox:eval:nrhs", "Input arguments required: function name and 1 to 15 input arrays.");
    }
    nArgs = nrhs - 1;

    // Function name
    func_name = mxArrayToString(prhs[0]);
    if(func_name == NULL) 
        mexErrMsgIdAndTxt("MATLAB:revord:conversionFailed", "Could not convert input to string.");

    // Allocate space for input arrays
    V_in_real = (mxArray**) mxCalloc(MAX_NUM_ARGS, sizeof(mxArray*));
    inReal = (const double**) mxCalloc(MAX_NUM_ARGS, sizeof(double*));

    // Copy input arrays
    for (i = 0; i < nArgs; i++) {
        V_in_real[i] = mxDuplicateArray(prhs[i+1]);
        inReal[i] = mxGetPr(V_in_real[i]);
    }

    // Get dimensions of first input array:
    dims = mxGetDimensions(prhs[1]);
    rows = (int) dims[0];
    cols = (int) dims[1];

    // Check input dimensions and allocate output arrays:
    if (mxIsDouble(prhs[1]) && mxIsComplex(prhs[1]) == false) {
        // Inputs are real matrices
        b_out_real = plhs[0] = mxCreateDoubleMatrix(rows, cols, mxREAL);
        error_out = plhs[1] = mxCreateDoubleMatrix(1, 1, mxREAL);

        // Access the contents of the output arrays:
        b_real = mxGetPr(b_out_real);
        error = mxGetPr(error_out);

        error[0] = eval(func_name, nArgs, inReal, NULL, rows*cols, b_real, NULL);

        mxFree(func_name);

    } else {
        // Inputs are not real matrices or have incorrect dimensions
        mexErrMsgIdAndTxt("MyToolbox:eval:incorrectInput", "Inputs must be real matrices.");
    }
}
