from jinja2 import Environment, FileSystemLoader
import pandas as pd
import os
from pathlib import Path
import shutil
import subprocess
import sys


def compile_mex_files():
    #ver_check_proc = subprocess.run('matlab -help', capture_output=True, shell=True)
    #for line in ver_check_proc.stdout.decode('utf-8').split('\n'):
    #    if "Version" in line:
    #        print("MATLAB Version:")
    #        print(line)
    
    ret = subprocess.call(' '.join([
        'matlab',
        '-wait',
        '-logfile',
        'matlab_execution_log.txt',
        '-r',
        '"try; run(\'compile_c_to_mex.m\'); catch e; fprintf(\'%s / %s\\n\', e.identifier, e.message); exit(1); end; exit;"']),
        shell=True)
    
    if ret != 0:
        # The error code was non-zero
        print("Compilation of MEX files failed. Check the logs in 'log_compile_c_to_mex.txt'")
        print("Not building material library.")
        sys.exit(1)


def compile_STEAM_MatPro():
    path_to_matlab_template = os.path.dirname(__file__)  # os.path.join(os.path.dirname(os.getcwd()), 'compile_STEAM_MatPro')
    template_file_name = 'STEAM_MatPro_template.m'

    # Delete the output folder, if existing, and remake it
    output_folder = 'compiled_STEAM_MatPro'
    if os.path.isdir(output_folder):
        shutil.rmtree(output_folder)
    Path(output_folder).mkdir(parents=True, exist_ok=True)
    output_file_path = os.path.join(output_folder, 'STEAM_MatPro.m')

    # Read the CSV file using pandas
    # df = pd.read_excel(os.path.join(path_to_matlab_template, 'c_mat_mex_correspondance.xlsx'))
    df  = pd.read_csv(os.path.join(path_to_matlab_template, '..\\..\\functionsNames_website_csv.csv'))
    
    columns_to_convert = ['C_input_params', 'Outputs', 'MatLab_input_params']
    df[columns_to_convert] = df[columns_to_convert].astype('Int64')

    # Build the function list dictionary
    function_list = []
    for index, row in df.iterrows():
        function_name = row["C_function_prop"]
        handles = row["handle_name"]
        matlab_name = row["matlab_name"]
        C_input_params = row["C_input_params"]
        number_of_ouputs = row["Outputs"]
        MatLab_input_params = row["MatLab_input_params"]
        functions_included = row["functions_included"]
        function_list.append((function_name, handles, matlab_name, C_input_params, MatLab_input_params, number_of_ouputs, functions_included))

    # Create the dictionary to be used in the template
    template_dict = {
        'function_list': function_list
    }

    loader = FileSystemLoader(path_to_matlab_template)
    env = Environment(loader=loader, variable_start_string='<<', variable_end_string='>>',
                      trim_blocks=True, lstrip_blocks=True)
    env.globals.update(zip=zip, enumerate=enumerate, list=list, len=len)
    env.filters['type'] = lambda value: type(value).__name__

    # Write the templated .m file
    matlab_template = env.get_template(template_file_name)
    output_from_parsed_template = matlab_template.render(template_dict)
    with open(output_file_path, "w") as tf:
        tf.write(output_from_parsed_template)

    # Find functions with different numbers of input parameters
    for function_data in function_list:
        c_input_params = function_data[3]
        matlab_input_params = function_data[4]
        if c_input_params != matlab_input_params:
            print(f"Function: {function_data[0]}")
            print(f"  C_input_params: {c_input_params}")
            print(f"  MatLab_input_params: {matlab_input_params}")
            print(f"  Difference: {abs(c_input_params - matlab_input_params)}")
            print()


if __name__ == "__main__":
    print("Compiling MEX files...", flush=True)
    compile_mex_files()
    print("Done! Compiled .mex files are in 'compiled_mex'")
    print("Compiling STEAM MatPro library function...")
    compile_STEAM_MatPro()
    print("Done! Compiled STEAM_MatPro.m is in 'compiled_STEAM_MatPro'")
