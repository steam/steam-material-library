clear all
clc

%% Setup diary file
nameDiaryFile='log_compile_c_to_mex.txt';
diary(nameDiaryFile)

% % % Make sure to go to the folder where this function file is located
cd(fileparts(mfilename('fullpath')))

%% Find all functions
folder_CFUN = '..\..\models\c\general'; % folder containing the C functions to compile
list_files=dir(folder_CFUN);
list_functions={};
for fi=1:length(list_files)
    if endsWith(list_files(fi).name,'.c')==1
        list_functions{end+1}=list_files(fi).name(1:end-2);
    end
end

% Define output folder, delete it if it exists already, and add it to the Matlab path
output_folder = 'compiled_mex';
if isdir(output_folder)==1
    rmdir(output_folder, 's')
end
mkdir(output_folder)
addpath(genpath(output_folder))

% Input variable to test compiled functions
n_max_arguments=15;
N_elements=100;
for n=1:n_max_arguments
    var_input{n}=linspace(5,10,N_elements);
end
% TT=linspace(1,300,N_elements);
% BB=linspace(0,20,N_elements);
% RRR=linspace(50,300,N_elements);
% Tref=ones(size(TT))*273;

%% Compile and test all functions (it will only compile the functions that require one input)
list_functions_var=cell(n_max_arguments,0);
for fu=1:length(list_functions)
    clear out_var
    display([num2str(fu) '/' num2str(length(list_functions)) ' : ' list_functions{fu}])
    
    % Compile mex. Example:  mex -output CFUN_kSteel.mex generic_wrapper.cpp CFUN_kSteel.c
    try
        name_mex_fun = ['MEX' list_functions{fu}(5:end) '.mex'];
        str_eval = ['mex -output ' output_folder '\' list_functions{fu} '.mex generic_wrapper.cpp ' folder_CFUN '/' list_functions{fu} '.c'];
        display(['EVAL: ' str_eval]), eval(str_eval) % display the string that is being called
    catch
        warning(['Function ' list_functions{fu} ' not compiled.'])
    end
    
    % Call function. Example:  [out_var,y] = CFUN_kSteel('CFUN_kSteel',TT)
    flag_fun_worked=0;
    str_var_input = '';
    for n=1:n_max_arguments
        str_var_input = [str_var_input ',var_input{' num2str(n) '}']; % this string defines a different number of arguments to pass at each iteration of the for loop
        try
            if flag_fun_worked==0
                str_eval = ['[out_var,y] = ' list_functions{fu} '(''' list_functions{fu} '''' str_var_input ');'];
                display(['EVAL: ' str_eval]), eval(str_eval) % display the string that is being called
                % Check that the function does not return only zeros for this number of arguments
                if all(out_var == 0)
                    list_functions_var{n,end+1}=list_functions{fu};
                    flag_fun_worked=1;
                end
            end
        catch
            warning(['Function ' list_functions{fu} '.mex did not run properly and it will be deleted.'])
            delete(fullfile(output_folder, [list_functions{fu} '.mex']))
            delete(fullfile(output_folder, [list_functions{fu} '.mexw64']))
        end
    end
    
    % If the function was compiled but did not work for any of the inputs, display a warning and delete it
    if flag_fun_worked==0
        warning(['Function ' list_functions{fu} '.mex returned only zeros for all number of arguments between 1 and ' num2str(n_max_arguments) ', and it will be deleted.'])
        delete(fullfile(output_folder, [list_functions{fu} '.mex']))
        delete(fullfile(output_folder, [list_functions{fu} '.mexw64']))
    end
end

% Remove from the list of functions those already compiled and checked
for n=1:n_max_arguments
    for f=1:length(list_functions_var(n,:))
        list_functions=setdiff(list_functions, list_functions_var{n,f});
    end
end

%% Display recap
for n=1:n_max_arguments
    idx_fun = find(~cellfun(@isempty,list_functions_var(n,:)));
    n_idx_fun = length(idx_fun);
    if n_idx_fun>0
        display(['A total of ' num2str(n_idx_fun) ' functions with ' num2str(n) ' arguments were successfully compiled:'])
        for f=1:n_idx_fun
            display(char(list_functions_var(n,idx_fun(f))))
        end
    end
end
display(['A total of ' num2str(length(list_functions)) ' functions were NOT successfully compiled:'])
for fu=1:length(list_functions)
    display([list_functions{fu}])
end

diary off