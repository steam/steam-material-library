classdef STEAM_MatPro < handle
    properties
        functionSet % Set of functions to use ('m' or 'mex')
        % Function handles
        {% for function_data in function_list %}
        handle_<<function_data[1]>>
        {% endfor %}
    end
    
    methods
        % Initializing method
        function obj = STEAM_MatPro(functionSet)
            obj.functionSet = functionSet;
            obj.setFunctionSet(obj.functionSet);
        end
    
        function setFunctionSet(obj, functionSet)
            obj.functionSet = functionSet;
            switch obj.functionSet
                case 'm'
                    % Definition of all the handles to .mat functions
                    {% for function_data in function_list %}
                        obj.handle_<<function_data[1]>> = @<<function_data[2]>>;
                    {% endfor %}
            
                case 'mex'
                    % Definition of all the handles to .mex functions
                    {% for function_data in function_list %}
                    {% if function_data[1] == 'Tc_Tcs_Nb3Sn_Bordini' %}
                        obj.handle_<<function_data[1]>> = @wrapper_<<function_data[0]>>_Bordini;
                    {% elif function_data[1] == 'Tc_Tcs_Nb3Sn_Summers'%}
                        obj.handle_<<function_data[1]>> = @wrapper_<<function_data[0]>>_Summers;
                    {% else %}
                        obj.handle_<<function_data[1]>> = @wrapper_<<function_data[0]>>;
                    {% endif %}
                    {% endfor %}
                    otherwise
                      error('Invalid function set selected. Only ''m'' or ''mex'' are supported.');
                end
            end

        % Definition of all functions
        {% for function_data in function_list %}
        {% set num_args = function_data[4]|int %}
        {% set num_outputs = function_data[5]|int %}
        function [result1{% for i in range(num_outputs-1) %}{% set arg_num = i+1 %}, result<< arg_num+1 >>{% endfor %}] = <<function_data[1]>>(obj{% for i in range(num_args) %}{% set arg_num = i+1 %}, input<< arg_num >>{% endfor %})
            [result1{% for i in range(num_outputs-1) %}{% set arg_num = i+1 %}, result<< arg_num+1 >>{% endfor %}] = obj.handle_<<function_data[1]>>({% for i in range(num_args) %}{% set arg_num = i+1 %}input<< arg_num >>{% if not loop.last %}, {% endif %}{% endfor %});
            
        end
        {% endfor %}
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Helper functions
% Wrappers: This is set to avoid passing the name of the C function
{% for function_data in function_list %}
    {% if function_data[1] == 'Tc_Tcs_Nb3Sn_Bordini' %}
    function [result1, result2] = wrapper_CFUN_Tcs_Nb3Sn_v1_Bordini(input1, input2, input3, input4, input5, input6)
        Jc0 = <<function_data[6]>>('<<function_data[6]>>', 0*ones(size(input1)), input2, input3, input4, input5, input6);
            result2 = CFUN_Tcs_Nb3Sn_v1('CFUN_Tcs_Nb3Sn_v1', input1, input2, Jc0, input4, input5);
            result1 = result2 ./ (1 - min(input1./Jc0, 1));
    end
    
    {% elif function_data[1] == 'Tc_Tcs_Nb3Sn_Summers' %}
    function [result1, result2] = wrapper_CFUN_Tcs_Nb3Sn_v1_Summers(input1, input2, input3, input4, input5)
        Jc0 = <<function_data[6]>>('<<function_data[6]>>', 0*ones(size(input1)), input2, input3, input4, input5);
            result2 = CFUN_Tcs_Nb3Sn_v1('CFUN_Tcs_Nb3Sn_v1', input1, input2, Jc0, input3, input4, input5);
            result1 = result2 ./ (1 - min(input1./Jc0, 1));
    end
    {% else %}
    
    {% set num_args = function_data[3]|int %}
    {% set num_args_mat = function_data[4]|int %}
    {% set num_outputs = function_data[5]|int %}

    function [result1{% for i in range(num_outputs-1) %}{% set arg_num = i+1 %}, result<< arg_num+1 >>{% endfor %}] = wrapper_<<function_data[0]>>({% if function_data[0] in ['CFUN_Jc_Nb3Sn_Bottura_v1', 'CFUN_Jc_NbTi_Bottura_v1', 'CFUN_Jc_NbTi_Cudi_fit1_v1', 'CFUN_Jc_NbTi_Cudi_v1'] %}{% for i in range(num_args_mat) %}{% set arg_num = i+1 %}input<< arg_num >>{% if not loop.last %}, {% endif %}{% endfor %}{% else %}{% for i in range(num_args) %}{% set arg_num = i+1 %}input<< arg_num >>{% if not loop.last %}, {% endif %}{% endfor %}{% endif %}){% if function_data[5]|type != 'str' %}  
        {% if function_data[0] == 'CFUN_CvNbTi_v1' %}

        input3 = 15*ones(size(input1)); % I
        input4 = 1*ones(size(input1)); % C1
        input5 = 1*ones(size(input1)); % C2
        [result1{% for i in range(num_outputs-1) %}{% set arg_num = i+1 %}, result<< arg_num+1 >>{% endfor %}]=<<function_data[0]>>('<<function_data[0]>>' {% for i in range(function_data[3]) %}{% set arg_num = i+1 %}, input<< arg_num >>{% endfor %});
                {% elif function_data[0] == 'CFUN_rhoCu_NIST_v2' %}

        input4 = 1*ones(size(input1)); % input4 will be ignored
        [result1]=CFUN_rhoCu_NIST_v2('CFUN_rhoCu_NIST_v2' , input1, input2, input3, input4);  % input4 is ignored
        {% elif function_data[0] == 'CFUN_Jc_Nb3Sn_Bottura_v1' %}

        if isstruct(input3)==1
            Tc0  = input3.Tc0*ones(size(input1));
            Bc20  = input3.Bc20*ones(size(input1));
            CJ  = input3.CJ*ones(size(input1));
            p  = input3.p*ones(size(input1));
            q  = input3.q*ones(size(input1));
        else
            Tc0    = input3(1,:);
            Bc20   = input3(2,:);
            CJ     = input3(3,:);
            p      = input3(4,:);
            q      = input3(5,:);
        end

        if isstruct(input4)==1
            WireDiameter  = input4.wireDiameter*ones(size(input1));
            Cu_noCu = input4.Cu_noCu*ones(size(input1));
        else
            WireDiameter  = input4(1,:);
            Cu_noCu = input4(2,:);
        end

        result2=CFUN_Jc_Nb3Sn_Bottura_v1('CFUN_Jc_Nb3Sn_Bottura_v1',  input1, input2, Tc0, Bc20, CJ, p, q, WireDiameter, Cu_noCu);
        result1 = result2.* (3.14159265358979323846/4 .* WireDiameter.*WireDiameter .* (1./(1+Cu_noCu)));

        {% elif function_data[0] == 'CFUN_Jc_NbTi_Bottura_v1' %}

        if isstruct(input3)==1
            Tc0      = input3.Tc0*ones(size(input1));
            Bc20     = input3.Bc20*ones(size(input1));
            Jc_ref   = input3.Jc_ref*ones(size(input1));
            C0       = input3.C0*ones(size(input1));
            alpha    = input3.alpha*ones(size(input1));
            beta     = input3.beta*ones(size(input1));
            gamma    = input3.gamma*ones(size(input1));
        else
            Tc0      = input3(1,:);
            Bc20     = input3(2,:);
            Jc_ref   = input3(3,:);
            C0       = input3(4,:);
            alpha    = input3(5,:);
            beta     = input3(6,:);
            gamma    = input3(7,:);
        end

        if isstruct(input4)==1
            WireDiameter = input4.wireDiameter*ones(size(input1));
            Cu_noCu      = input4.Cu_noCu*ones(size(input1));
        else
            WireDiameter = input4(1,:);
            Cu_noCu      = input4(2,:);
        end
        result2=CFUN_Jc_NbTi_Bottura_v1('CFUN_Jc_NbTi_Bottura_v1', input1, input2, Tc0, Bc20, Jc_ref, C0, alpha, beta, gamma, WireDiameter, Cu_noCu);
        result1 = result2.* (3.14159265358979323846/4 .* WireDiameter.*WireDiameter .* (1./(1+Cu_noCu)));

        {% elif function_data[0] == 'CFUN_Jc_NbTi_Cudi_v1' %}

        if isstruct(input3)==1
            Tc0    = input3.Tc0*ones(size(input1));
            Bc20   = input3.Bc20*ones(size(input1));
            c1     = input3.c1*ones(size(input1));
            c2     = input3.c2*ones(size(input1));
            c3     = input3.c3*ones(size(input1));
            c4     = input3.c4*ones(size(input1));
            c5     = input3.c5*ones(size(input1));
            c6     = input3.c6*ones(size(input1));
        else
            Tc0    = input3(1,:);
            Bc20   = input3(2,:);
            c1     = input3(3,:);
            c2     = input3(4,:);
            c3     = input3(5,:);
            c4     = input3(6,:);
            c5     = input3(7,:);
            c6     = input3(8,:);
        end

        if isstruct(input4)==1
            WireDiameter = input4.wireDiameter*ones(size(input1));
            Cu_noCu      = input4.Cu_noCu*ones(size(input1));
        else
            WireDiameter = input4(1,:);
            Cu_noCu      = input4(2,:);
        end

        result2=CFUN_Jc_NbTi_Cudi_v1('CFUN_Jc_NbTi_Cudi_v1',  input1, input2, Tc0, Bc20, c1, c2, c3, c4, c5, c6, WireDiameter, Cu_noCu);
        result1 = result2.* (3.14159265358979323846/4 .* WireDiameter.*WireDiameter .* (1./(1+Cu_noCu)));
        
        {% elif function_data[0] == 'CFUN_Jc_NbTi_Cudi_fit1_v1' %}

        if isstruct(input3)==1
            Tc0   = input3.Tc0*ones(size(input1));
            Bc20  = input3.Bc20*ones(size(input1));
            C1    = input3.C1*ones(size(input1));
            C2    = input3.C2*ones(size(input1));
        else
            Tc0   = input3(1,:);
            Bc20  = input3(2,:);
            C1    = input3(3,:);
            C2    = input3(4,:);
        end

        if isstruct(input4)==1
            WireDiameter = input4.wireDiameter*ones(size(input1));
            Cu_noCu      = input4.Cu_noCu*ones(size(input1));
        else
            WireDiameter = input4(1,:);
            Cu_noCu      = input4(2,:);
        end
        result2=CFUN_Jc_NbTi_Cudi_fit1_v1('CFUN_Jc_NbTi_Cudi_fit1_v1',  input1, input2, Tc0, Bc20, C1, C2, WireDiameter, Cu_noCu);
        result1 = result2.* (3.14159265358979323846/4 .* WireDiameter.*WireDiameter .* (1./(1+Cu_noCu)));

        {% endif %}
        {% if function_data[0] == 'CFUN_kBeCu_WiedemannFranz_v1' %}

        input2=<<function_data[6]>>('<<function_data[6]>>', input1, input2);
        [result1{% for i in range(num_outputs-1) %}{% set arg_num = i+1 %}, result<< arg_num+1 >>{% endfor %}]=<<function_data[0]>>('<<function_data[0]>>' {% for i in range(function_data[3]) %}{% set arg_num = i+1 %}, input<< arg_num >>{% endfor %});
        {% elif function_data[0] == 'CFUN_kCu_v1' %}

        rho_B=CFUN_rhoCu_NIST_v2('CFUN_rhoCu_NIST_v2', input1, input2, input3);  % This assumes RRR defined between 273 K and 4 K
        rho_B0=CFUN_rhoCu_NIST_v2('CFUN_rhoCu_NIST_v2', input1, zeros(size(input1)), input3);  % This assumes RRR defined between 273 K and 4 K
        [result1]=CFUN_kCu_v1('CFUN_kCu_v1', input1, input3, rho_B0, rho_B);  % Note that input3 is the RRR
        {% elif function_data[0] == 'CFUN_kCu_Wiedemann_v1' %}

        rho_B=CFUN_rhoCu_NIST_v2('CFUN_rhoCu_NIST_v2', input1, input2, input3);  % This assumes RRR defined between 273 K and 4 K
        [result1]=CFUN_kCu_Wiedemann_v1('CFUN_kCu_Wiedemann_v1' , input1, rho_B);

        {% elif function_data[0] in ['CFUN_rhoCu_NIST_v2', 'CFUN_Jc_Nb3Sn_Bottura_v1', 'CFUN_Jc_NbTi_Bottura_v1', 'CFUN_Jc_NbTi_Cudi_fit1_v1', 'CFUN_Jc_NbTi_Cudi_v1'] %}
        {% else %}     
        [result1{% for i in range(num_outputs-1) %}{% set arg_num = i+1 %}, result<< arg_num+1 >>{% endfor %}]=<<function_data[0]>>('<<function_data[0]>>' {% for i in range(function_data[3]) %}{% set arg_num = i+1 %}, input<< arg_num >>{% endfor %});

        {% endif %}
    end
    {%endif%}
    {%endif%}
{% endfor %}




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


