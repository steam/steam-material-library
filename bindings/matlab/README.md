# How To Compile & Run C/C++ Files
## Required Software
- Python
- C/C++ Compiler
    - Windows: MinGW. Configure the PATH and add the path where the compiler is installed (typically C:\MinGW)
    - Linux: g++
- MATLAB (the MEX library must be installed. Installation steps: "Get Add-Ons"->Search for MinGW compiler->Install the "MATLAB support for MinGW-w64 C/C++ Compiler". Then Restart MATLAB.)

## Compiling Models With MATLAB
To compile the models automatically, run the `build.py` script.
This will put the results into the `mex_compilation` folder and create a wrapper class `STEAM_MatPro.m` to easily execute models from the collection.

1. Navigate to the `mex_compilation` directory
2. Create a virtual environment with `python -m venv venv`
3. Install dependencies with `pip install -r ..\requirements.txt`
4. Build with `python build.py`

To compile and individual function manually in MATLAB, use the “mex” command, for example:  
```mex -output my_mex_function.mex generic_wrapper.cpp CFUN_[STEAM Model].c```

## Running Models With MATLAB
To run the function, initialize the arguments your function uses, and call the function like any other MATLAB function. For instance:
```matlab
N=10;
TT=[linspace(1,300,N)' linspace(1,300,N)' linspace(1,300,N)' linspace(1,300,N)'];

% Call the MEX function
[out_var,y] = my_mex_function('my_c_function', TT)
```

### Additional Info on Running the Wrappers
new_test_c_compiler_to_mex.m :
There are 2 ways of running a function with 4 arguments:
1.	new_vers_wrapper_4_inputs.cpp : Has 4 nx1 matrices as inputs (4 arguments in array format).
2.	my_wrapper_1_input.cpp : Has 1 nx4 matrix as input (each row represents a variable).

> Problem: While testing the 2 functions above, the output matrix dimensions are right, but the output result is a matrix of zeros when the argument is anything else but a nx1 matrix. 
