import os
import unittest

from bindings.matlab.build import compile_STEAM_MatPro


class TestParserCsv(unittest.TestCase):

    def setUp(self) -> None:
        """
            This function is executed before each test in this class
        """
        self.current_path = os.getcwd()
        os.chdir(os.path.dirname(__file__))  # move to the directory where this file is located
        print('\nCurrent folder:          {}'.format(self.current_path))
        print('\nTest is run from folder: {}'.format(os.getcwd()))

    def tearDown(self) -> None:
        """
            This function is executed after each test in this class
        """
        os.chdir(self.current_path)  # go back to initial folder


    def test_compile_STEAM_MatPro(self, max_relative_error=1e-6):
        # arrange
        # file_name_reference = os.path.join('references', 'STEAM_MatPro_REFERENCE.m')
        file_name_expected = os.path.join('compiled_STEAM_MatPro', 'STEAM_MatPro.m')
        # If already existing, delete it
        if os.path.isfile(file_name_expected):
            os.remove(file_name_expected)

        # act
        compile_STEAM_MatPro()

        # assert
        # Check that the file STEAM_MatPro.m just generated is identical to STEAM_MatPro_REFERENCE.m
        # TODO Compare line by line output and reference files