classdef matlab_out < handle
    properties
        functionSet % Set of functions to use ('m' or 'mex')
        % Function handles
        handle_CpHeMass
        handle_CpNb3Sn
        handle_CvAg
        handle_CvAl5083
        handle_CvBeCu
        handle_CvBSCCO2212
        handle_CvCu
        handle_CvG10
        handle_cvHeMass
        handle_CvHe
        handle_CvKapton
        handle_CvNb3Sn
        handle_CvNbTi
        handle_CvSteel
        handle_CvTi
        handle_densityHe
        handle_Jc_Nb3Sn_Bordini
        handle_Jc_Nb3Sn_Summer
        handle_Jc_T_B_BSCCO2212_block20T_v2
        handle_Jc_T_B_BSCCO2212_block20T
        handle_Jc_T_B_BSCCO2212_Power_Gosh
        handle_Jc_T_B_BSCCO2212_Power_Jiang_Wesche
        handle_kAg_mat
        handle_kG10
        handle_kKapton
        handle_kSteel
        handle_n_T_B_BSCCO2212_Jiang_Wesche
        handle_rhoAgMg
        handle_rhoAg
        handle_rhoAl
        handle_rhoBeCu
        handle_rhoCu
        handle_rhoSS
        handle_kBeCu_WiedemannFranz
        handle_kCu
        handle_kCu_Wiedermann
        handle_Tc_Tcs_Nb3Sn
        handle_Jc_Nb3Sn_Bottura
        handle_Jc_NbTi_Bottura
        handle_Jc_NbTi_Cudi_fit1
        handle_Jc_NbTi_Cudi
    end
    
    methods
        % Initializing method
        function obj = matlab_out(functionSet)
            obj.functionSet = functionSet;
            obj.setFunctionSet(obj.functionSet);
        end
    
        function setFunctionSet(obj, functionSet)
            obj.functionSet = functionSet;
            switch obj.functionSet
                case 'm'
                    % Definition of all the handles to .mat functions
                        obj.handle_CpHeMass = @cpHeMass_mat;
                        obj.handle_CpNb3Sn = @cpNb3Sn_cryocomp_mat;
                        obj.handle_CvAg = @cpAg_mat;
                        obj.handle_CvAl5083 = @cpAl5083_nist;
                        obj.handle_CvBeCu = @cpBeCu_nist_mat;
                        obj.handle_CvBSCCO2212 = @cpBSCCO2212_mat;
                        obj.handle_CvCu = @cpCu_nist_mat;
                        obj.handle_CvG10 = @cpG10_nist_mat;
                        obj.handle_cvHeMass = @cvHeMass_nist_mat;
                        obj.handle_CvHe = @cpHe_mat;
                        obj.handle_CvKapton = @cpKapton_nist_mat;
                        obj.handle_CvNb3Sn = @cpNb3Sn_alternative_mat;
                        obj.handle_CvNbTi = @cpNbTi_cudi_mat;
                        obj.handle_CvSteel = @cpSS316_nist_mat;
                        obj.handle_CvTi = @cpTi_cryocomp_mat;
                        obj.handle_densityHe = @densityHe_nist_mat;
                        obj.handle_Jc_Nb3Sn_Bordini = @Jc_Nb3Sn_Bordini;
                        obj.handle_Jc_Nb3Sn_Summer = @Jc_Nb3Sn_Summer;
                        obj.handle_Jc_T_B_BSCCO2212_block20T_v2 = @Jc_T_B_BSCCO2212_block20T_v2;
                        obj.handle_Jc_T_B_BSCCO2212_block20T = @Jc_T_B_BSCCO2212_block20T;
                        obj.handle_Jc_T_B_BSCCO2212_Power_Gosh = @Jc_T_B_BSCCO2212_Power_Gosh;
                        obj.handle_Jc_T_B_BSCCO2212_Power_Jiang_Wesche = @Jc_T_B_BSCCO2212_Power_Jiang_Wesche;
                        obj.handle_kAg_mat = @kAg_mat;
                        obj.handle_kG10 = @kG10_mat;
                        obj.handle_kKapton = @kKapton_mat;
                        obj.handle_kSteel = @kSS_mat;
                        obj.handle_n_T_B_BSCCO2212_Jiang_Wesche = @n_T_B_BSCCO2212_Jiang_Wesche;
                        obj.handle_rhoAgMg = @rhoAgMg;
                        obj.handle_rhoAg = @rhoAg_mat;
                        obj.handle_rhoAl = @rhoAl_nist;
                        obj.handle_rhoBeCu = @rhoBeCu_nist_mat;
                        obj.handle_rhoCu = @rhoCu_nist;
                        obj.handle_rhoSS = @rhoSS_CERN_mat;
                        obj.handle_kBeCu_WiedemannFranz = @kBeCu_WiedemannFranz;
                        obj.handle_kCu = @kCu_nist_mat;
                        obj.handle_kCu_Wiedermann = @kCu_WiedemannFranz;
                        obj.handle_Tc_Tcs_Nb3Sn = @Tc_Tcs_Nb3Sn_Bordini_approx;
                        obj.handle_Jc_Nb3Sn_Bottura = @Jc_Nb3Sn_Bottura;
                        obj.handle_Jc_NbTi_Bottura = @Jc_NbTi_Bottura;
                        obj.handle_Jc_NbTi_Cudi_fit1 = @Jc_NbTi_Cudi_fit1;
                        obj.handle_Jc_NbTi_Cudi = @Jc_NbTi_Cudi;
            
                case 'mex'
                    % Definition of all the handles to .mex functions
                        obj.handle_CpHeMass = @wrapper_CFUN_CpHeMass_v1;
                        obj.handle_CpNb3Sn = @wrapper_CFUN_CpNb3Sn_v1;
                        obj.handle_CvAg = @wrapper_CFUN_CvAg_v1;
                        obj.handle_CvAl5083 = @wrapper_CFUN_CvAl5083_v1;
                        obj.handle_CvBeCu = @wrapper_CFUN_CvBeCu_v1;
                        obj.handle_CvBSCCO2212 = @wrapper_CFUN_CvBSCCO2212_v1;
                        obj.handle_CvCu = @wrapper_CFUN_CvCu_v1;
                        obj.handle_CvG10 = @wrapper_CFUN_CvG10_v1;
                        obj.handle_cvHeMass = @wrapper_CFUN_CvHeMass_v1;
                        obj.handle_CvHe = @wrapper_CFUN_CvHe_v1;
                        obj.handle_CvKapton = @wrapper_CFUN_CvKapton_v1;
                        obj.handle_CvNb3Sn = @wrapper_CFUN_CvNb3Sn_v1;
                        obj.handle_CvNbTi = @wrapper_CFUN_CvNbTi_v1;
                        obj.handle_CvSteel = @wrapper_CFUN_CvSteel_v1;
                        obj.handle_CvTi = @wrapper_CFUN_CvTi_v1;
                        obj.handle_densityHe = @wrapper_CFUN_densityHe_v1;
                        obj.handle_Jc_Nb3Sn_Bordini = @wrapper_CFUN_Jc_Bordini_v1;
                        obj.handle_Jc_Nb3Sn_Summer = @wrapper_CFUN_Jc_Nb3Sn_Summer_v1;
                        obj.handle_Jc_T_B_BSCCO2212_block20T_v2 = @wrapper_CFUN_Jc_T_B_BSCCO2212_block20T_new_v1;
                        obj.handle_Jc_T_B_BSCCO2212_block20T = @wrapper_CFUN_Jc_T_B_BSCCO2212_block20T_v1;
                        obj.handle_Jc_T_B_BSCCO2212_Power_Gosh = @wrapper_CFUN_Jc_T_B_BSCCO2212_Power_Gosh_v1;
                        obj.handle_Jc_T_B_BSCCO2212_Power_Jiang_Wesche = @wrapper_CFUN_Jc_T_B_BSCCO2212_Power_Jiang_Wesche_v1;
                        obj.handle_kAg_mat = @wrapper_CFUN_kAg_v1;
                        obj.handle_kG10 = @wrapper_CFUN_kG10_v1;
                        obj.handle_kKapton = @wrapper_CFUN_kKapton_v1;
                        obj.handle_kSteel = @wrapper_CFUN_kSteel_v1;
                        obj.handle_n_T_B_BSCCO2212_Jiang_Wesche = @wrapper_CFUN_n_T_B_BSCCO2212_Jiang_Wesche_v1;
                        obj.handle_rhoAgMg = @wrapper_CFUN_rhoAgMg_v1;
                        obj.handle_rhoAg = @wrapper_CFUN_rhoAg_v1;
                        obj.handle_rhoAl = @wrapper_CFUN_rhoAl_v1;
                        obj.handle_rhoBeCu = @wrapper_CFUN_rhoBeCu_v1;
                        obj.handle_rhoCu = @wrapper_CFUN_rhoCu_v1;
                        obj.handle_rhoSS = @wrapper_CFUN_rhoSS_v1;
                        obj.handle_kBeCu_WiedemannFranz = @wrapper_CFUN_kBeCu_WiedemannFranz_v1;
                        obj.handle_kCu = @wrapper_CFUN_kCu_v1;
                        obj.handle_kCu_Wiedermann = @wrapper_CFUN_kCu_Wiedemann_v1;
                        obj.handle_Tc_Tcs_Nb3Sn = @wrapper_CFUN_Tcs_Nb3Sn_v1;
                        obj.handle_Jc_Nb3Sn_Bottura = @wrapper_CFUN_Jc_Nb3Sn_Bottura_v1;
                        obj.handle_Jc_NbTi_Bottura = @wrapper_CFUN_Jc_NbTi_Bottura_v1;
                        obj.handle_Jc_NbTi_Cudi_fit1 = @wrapper_CFUN_Jc_NbTi_Cudi_fit1_v1;
                        obj.handle_Jc_NbTi_Cudi = @wrapper_CFUN_Jc_NbTi_Cudi_v1;
            
                    otherwise
                      error('Invalid function set selected. Only ''m'' or ''mex'' are supported.');
                end
            end

        % Definition of all functions
        function result = CpHeMass(obj, input1)
            result = obj.handle_CpHeMass(input1);
        end
        function result = CpNb3Sn(obj, input1, input2, input3, input4)
            result = obj.handle_CpNb3Sn(input1, input2, input3, input4);
        end
        function result = CvAg(obj, input1)
            result = obj.handle_CvAg(input1);
        end
        function result = CvAl5083(obj, input1)
            result = obj.handle_CvAl5083(input1);
        end
        function result = CvBeCu(obj, input1)
            result = obj.handle_CvBeCu(input1);
        end
        function result = CvBSCCO2212(obj, input1)
            result = obj.handle_CvBSCCO2212(input1);
        end
        function result = CvCu(obj, input1)
            result = obj.handle_CvCu(input1);
        end
        function result = CvG10(obj, input1)
            result = obj.handle_CvG10(input1);
        end
        function result = cvHeMass(obj, input1)
            result = obj.handle_cvHeMass(input1);
        end
        function result = CvHe(obj, input1)
            result = obj.handle_CvHe(input1);
        end
        function result = CvKapton(obj, input1)
            result = obj.handle_CvKapton(input1);
        end
        function result = CvNb3Sn(obj, input1, input2, input3, input4)
            result = obj.handle_CvNb3Sn(input1, input2, input3, input4);
        end
        function result = CvNbTi(obj, input1, input2)
            result = obj.handle_CvNbTi(input1, input2);
        end
        function result = CvSteel(obj, input1)
            result = obj.handle_CvSteel(input1);
        end
        function result = CvTi(obj, input1)
            result = obj.handle_CvTi(input1);
        end
        function result = densityHe(obj, input1)
            result = obj.handle_densityHe(input1);
        end
        function result = Jc_Nb3Sn_Bordini(obj, input1, input2, input3, input4, input5, input6)
            result = obj.handle_Jc_Nb3Sn_Bordini(input1, input2, input3, input4, input5, input6);
        end
        function result = Jc_Nb3Sn_Summer(obj, input1, input2, input3, input4, input5)
            result = obj.handle_Jc_Nb3Sn_Summer(input1, input2, input3, input4, input5);
        end
        function result = Jc_T_B_BSCCO2212_block20T_v2(obj, input1, input2, input3)
            result = obj.handle_Jc_T_B_BSCCO2212_block20T_v2(input1, input2, input3);
        end
        function result = Jc_T_B_BSCCO2212_block20T(obj, input1, input2, input3)
            result = obj.handle_Jc_T_B_BSCCO2212_block20T(input1, input2, input3);
        end
        function result = Jc_T_B_BSCCO2212_Power_Gosh(obj, input1, input2, input3)
            result = obj.handle_Jc_T_B_BSCCO2212_Power_Gosh(input1, input2, input3);
        end
        function result = Jc_T_B_BSCCO2212_Power_Jiang_Wesche(obj, input1, input2, input3)
            result = obj.handle_Jc_T_B_BSCCO2212_Power_Jiang_Wesche(input1, input2, input3);
        end
        function result = kAg_mat(obj, input1)
            result = obj.handle_kAg_mat(input1);
        end
        function result = kG10(obj, input1)
            result = obj.handle_kG10(input1);
        end
        function result = kKapton(obj, input1)
            result = obj.handle_kKapton(input1);
        end
        function result = kSteel(obj, input1)
            result = obj.handle_kSteel(input1);
        end
        function result = n_T_B_BSCCO2212_Jiang_Wesche(obj, input1, input2, input3)
            result = obj.handle_n_T_B_BSCCO2212_Jiang_Wesche(input1, input2, input3);
        end
        function result = rhoAgMg(obj, input1, input2)
            result = obj.handle_rhoAgMg(input1, input2);
        end
        function result = rhoAg(obj, input1)
            result = obj.handle_rhoAg(input1);
        end
        function result = rhoAl(obj, input1)
            result = obj.handle_rhoAl(input1);
        end
        function result = rhoBeCu(obj, input1, input2)
            result = obj.handle_rhoBeCu(input1, input2);
        end
        function result = rhoCu(obj, input1, input2, input3, input4)
            result = obj.handle_rhoCu(input1, input2, input3, input4);
        end
        function result = rhoSS(obj, input1)
            result = obj.handle_rhoSS(input1);
        end
        function result = kBeCu_WiedemannFranz(obj, input1, input2)
            result = obj.handle_kBeCu_WiedemannFranz(input1, input2);
        end
        function result = kCu(obj, input1, input2, input3)
            result = obj.handle_kCu(input1, input2, input3);
        end
        function result = kCu_Wiedermann(obj, input1, input2, input3, input4)
            result = obj.handle_kCu_Wiedermann(input1, input2, input3, input4);
        end
        function result = Tc_Tcs_Nb3Sn(obj, input1, input2, input3, input4, input5, input6, input7)
            result = obj.handle_Tc_Tcs_Nb3Sn(input1, input2, input3, input4, input5, input6, input7);
        end
        function result = Jc_Nb3Sn_Bottura(obj, input1, input2, input3, input4)
            result = obj.handle_Jc_Nb3Sn_Bottura(input1, input2, input3, input4);
        end
        function result = Jc_NbTi_Bottura(obj, input1, input2, input3, input4)
            result = obj.handle_Jc_NbTi_Bottura(input1, input2, input3, input4);
        end
        function result = Jc_NbTi_Cudi_fit1(obj, input1, input2, input3, input4)
            result = obj.handle_Jc_NbTi_Cudi_fit1(input1, input2, input3, input4);
        end
        function result = Jc_NbTi_Cudi(obj, input1, input2, input3, input4)
            result = obj.handle_Jc_NbTi_Cudi(input1, input2, input3, input4);
        end
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Helper functions
% Wrappers: This is set to avoid passing the name of the C function

% Functions with 1 argument
function output=wrapper_CFUN_CpHeMass_v1(input1)
    output=CFUN_CpHeMass_v1('CFUN_CpHeMass_v1' , input1);
end
function output=wrapper_CFUN_CpNb3Sn_v1(input1, input2, input3, input4)
    output=CFUN_CpNb3Sn_v1('CFUN_CpNb3Sn_v1' , input1, input2, input3, input4);
end
function output=wrapper_CFUN_CvAg_v1(input1)
    output=CFUN_CvAg_v1('CFUN_CvAg_v1' , input1);
end
function output=wrapper_CFUN_CvAl5083_v1(input1)
    output=CFUN_CvAl5083_v1('CFUN_CvAl5083_v1' , input1);
end
function output=wrapper_CFUN_CvBeCu_v1(input1)
    output=CFUN_CvBeCu_v1('CFUN_CvBeCu_v1' , input1);
end
function output=wrapper_CFUN_CvBSCCO2212_v1(input1)
    output=CFUN_CvBSCCO2212_v1('CFUN_CvBSCCO2212_v1' , input1);
end
function output=wrapper_CFUN_CvCu_v1(input1)
    output=CFUN_CvCu_v1('CFUN_CvCu_v1' , input1);
end
function output=wrapper_CFUN_CvG10_v1(input1)
    output=CFUN_CvG10_v1('CFUN_CvG10_v1' , input1);
end
function output=wrapper_CFUN_CvHeMass_v1(input1)
    output=CFUN_CvHeMass_v1('CFUN_CvHeMass_v1' , input1);
end
function output=wrapper_CFUN_CvHe_v1(input1)
    output=CFUN_CvHe_v1('CFUN_CvHe_v1' , input1);
end
function output=wrapper_CFUN_CvKapton_v1(input1)
    output=CFUN_CvKapton_v1('CFUN_CvKapton_v1' , input1);
end
function output=wrapper_CFUN_CvNb3Sn_v1(input1, input2)
    output=CFUN_CvNb3Sn_v1('CFUN_CvNb3Sn_v1' , input1, input2);
end
function output=wrapper_CFUN_CvNbTi_v1(input1, input2, input3, input4, input5)
    output=CFUN_CvNbTi_v1('CFUN_CvNbTi_v1' , input1, input2, input3, input4, input5);
end
function output=wrapper_CFUN_CvSteel_v1(input1)
    output=CFUN_CvSteel_v1('CFUN_CvSteel_v1' , input1);
end
function output=wrapper_CFUN_CvTi_v1(input1)
    output=CFUN_CvTi_v1('CFUN_CvTi_v1' , input1);
end
function output=wrapper_CFUN_densityHe_v1(input1)
    output=CFUN_densityHe_v1('CFUN_densityHe_v1' , input1);
end
function output=wrapper_CFUN_Jc_Bordini_v1(input1, input2, input3, input4, input5, input6)
    output=CFUN_Jc_Bordini_v1('CFUN_Jc_Bordini_v1' , input1, input2, input3, input4, input5, input6);
end
function output=wrapper_CFUN_Jc_Nb3Sn_Summer_v1(input1, input2, input3, input4, input5)
    output=CFUN_Jc_Nb3Sn_Summer_v1('CFUN_Jc_Nb3Sn_Summer_v1' , input1, input2, input3, input4, input5);
end
function output=wrapper_CFUN_Jc_T_B_BSCCO2212_block20T_new_v1(input1, input2, input3)
    output=CFUN_Jc_T_B_BSCCO2212_block20T_new_v1('CFUN_Jc_T_B_BSCCO2212_block20T_new_v1' , input1, input2, input3);
end
function output=wrapper_CFUN_Jc_T_B_BSCCO2212_block20T_v1(input1, input2, input3)
    output=CFUN_Jc_T_B_BSCCO2212_block20T_v1('CFUN_Jc_T_B_BSCCO2212_block20T_v1' , input1, input2, input3);
end
function output=wrapper_CFUN_Jc_T_B_BSCCO2212_Power_Gosh_v1(input1, input2, input3)
    output=CFUN_Jc_T_B_BSCCO2212_Power_Gosh_v1('CFUN_Jc_T_B_BSCCO2212_Power_Gosh_v1' , input1, input2, input3);
end
function output=wrapper_CFUN_Jc_T_B_BSCCO2212_Power_Jiang_Wesche_v1(input1, input2, input3)
    output=CFUN_Jc_T_B_BSCCO2212_Power_Jiang_Wesche_v1('CFUN_Jc_T_B_BSCCO2212_Power_Jiang_Wesche_v1' , input1, input2, input3);
end
function output=wrapper_CFUN_kAg_v1(input1)
    output=CFUN_kAg_v1('CFUN_kAg_v1' , input1);
end
function output=wrapper_CFUN_kG10_v1(input1)
    output=CFUN_kG10_v1('CFUN_kG10_v1' , input1);
end
function output=wrapper_CFUN_kKapton_v1(input1)
    output=CFUN_kKapton_v1('CFUN_kKapton_v1' , input1);
end
function output=wrapper_CFUN_kSteel_v1(input1)
    output=CFUN_kSteel_v1('CFUN_kSteel_v1' , input1);
end
function output=wrapper_CFUN_n_T_B_BSCCO2212_Jiang_Wesche_v1(input1, input2, input3)
    output=CFUN_n_T_B_BSCCO2212_Jiang_Wesche_v1('CFUN_n_T_B_BSCCO2212_Jiang_Wesche_v1' , input1, input2, input3);
end
function output=wrapper_CFUN_rhoAgMg_v1(input1, input2, input3, input4)
    output=CFUN_rhoAgMg_v1('CFUN_rhoAgMg_v1' , input1, input2, input3, input4);
end
function output=wrapper_CFUN_rhoAg_v1(input1, input2, input3, input4)
    output=CFUN_rhoAg_v1('CFUN_rhoAg_v1' , input1, input2, input3, input4);
end
function output=wrapper_CFUN_rhoAl_v1(input1)
    output=CFUN_rhoAl_v1('CFUN_rhoAl_v1' , input1);
end
function output=wrapper_CFUN_rhoBeCu_v1(input1, input2)
    output=CFUN_rhoBeCu_v1('CFUN_rhoBeCu_v1' , input1, input2);
end
function output=wrapper_CFUN_rhoCu_v1(input1, input2, input3, input4)
    output=CFUN_rhoCu_v1('CFUN_rhoCu_v1' , input1, input2, input3, input4);
end
function output=wrapper_CFUN_rhoSS_v1(input1)
    output=CFUN_rhoSS_v1('CFUN_rhoSS_v1' , input1);
end
function output=wrapper_CFUN_kBeCu_WiedemannFranz_v1(input1, input2, input3)
    input2 = CFUN_rhoBeCu_v1('CFUN_kBeCu_WiedemannFranz_v1' , input1, input2, input3);
    output=CFUN_kBeCu_WiedemannFranz_v1('CFUN_kBeCu_WiedemannFranz_v1' , input1, input2, input3);
end
function output=wrapper_CFUN_kCu_v1(input1, input2, input3, input4)
    input4 = CFUN_rhoCu_v1('CFUN_kCu_v1' , input1, input2, input3, input4);
    output=CFUN_kCu_v1('CFUN_kCu_v1' , input1, input2, input3, input4);
end
function output=wrapper_CFUN_kCu_Wiedemann_v1(input1, input2)
    input0 = CFUN_rhoCu_v1-CFUN_rhoCu_v1('CFUN_kCu_Wiedemann_v1' , input1, input2);
    output=CFUN_kCu_Wiedemann_v1('CFUN_kCu_Wiedemann_v1' , input1, input2);
end
function output=wrapper_CFUN_Tcs_Nb3Sn_v1(input1, input2, input3, input4, input5, input6)
    input3 = CFUN_Jc_Bordini_v1('CFUN_Tcs_Nb3Sn_v1' , input1, input2, input3, input4, input5, input6);
    output=CFUN_Tcs_Nb3Sn_v1('CFUN_Tcs_Nb3Sn_v1' , input1, input2, input3, input4, input5, input6);
end
function output=wrapper_CFUN_Jc_Nb3Sn_Bottura_v1(input1, input2, input3, input4, input5, input6, input7, input8, input9)
    output=CFUN_Jc_Nb3Sn_Bottura_v1('CFUN_Jc_Nb3Sn_Bottura_v1' , input1, input2, input3, input4, input5, input6, input7, input8, input9);
end
function output=wrapper_CFUN_Jc_NbTi_Bottura_v1(input1, input2, input3, input4, input5, input6, input7, input8, input9, input10, input11)
    output=CFUN_Jc_NbTi_Bottura_v1('CFUN_Jc_NbTi_Bottura_v1' , input1, input2, input3, input4, input5, input6, input7, input8, input9, input10, input11);
end
function output=wrapper_CFUN_Jc_NbTi_Cudi_fit1_v1(input1, input2, input3, input4, input5, input6, input7, input8)
    output=CFUN_Jc_NbTi_Cudi_fit1_v1('CFUN_Jc_NbTi_Cudi_fit1_v1' , input1, input2, input3, input4, input5, input6, input7, input8);
end
function output=wrapper_CFUN_Jc_NbTi_Cudi_v1(input1, input2, input3, input4, input5, input6, input7, input8, input9, input10, input11)
    output=CFUN_Jc_NbTi_Cudi_v1('CFUN_Jc_NbTi_Cudi_v1' , input1, input2, input3, input4, input5, input6, input7, input8, input9, input10, input11);
end



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

