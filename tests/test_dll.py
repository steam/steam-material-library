import  yaml
import  numpy as np
from 	bindings.python.src.steammaterials.STEAM_materials import STEAM_materials
import unittest
import pathlib as pl
import os
import pandas as pd
from   glob import glob
import shutil
import subprocess


############# Testing of the DLLs #################
# the test function name should be in the dll, if not, it will take its own name

current_directory = pl.Path(__file__).parent

class TestCaseBase(unittest.TestCase):
    def assertIsFile(self, path):
        if not pl.Path(path).resolve().is_file():
            raise AssertionError("File does not exist: %s" % str(path))


class Test_Function_Output(TestCaseBase):
    func_name = 'CFUN_rhoHast_v2'

    # def test_check_files(self):
    def test_all_names_on_csv(self):
        # Checks if all c files are on the csv list
        # If this test fails, that means that there is a rogue c material function on the loose that is not being tracked.
        # Please add this function to the list: all_material_function.csv, which you can find in the base folder.
        # It is possible that you do not want to test its output, but it still has to be on the list.

        # Loads all names from the .csv
        csv_path = pl.Path(current_directory / '../all_material_functions.csv')
        csv = pd.read_csv(csv_path)
        all_files_on_csv = sorted(csv['material_function'].values)

        # Loads all c files from the folders
        path1 = pl.Path(current_directory / '../models/c/general/')
        path2 = pl.Path(current_directory / '../models/c/legacy/')
        result = sorted([pl.Path(y).stem for x in os.walk(path1) for y in glob(os.path.join(x[0], '*.c'))] + [pl.Path(y).stem for x in os.walk(path2) for y in glob(os.path.join(x[0], '*.c'))])
        self.assertEqual(result, all_files_on_csv)
        return True

    def test_check_data(self):
        # Reads the contents of the yaml file

        # To show everything.
        self.maxDiff = None

        # NB: the library is compiled here to be able to run this test locally without depending on the CI.
        # Builds the library:
        build_path = pl.Path(current_directory / '../test_build/')
        lib_folder = pl.Path(current_directory / '../test_build/lib')
        # For each file in the library it will test against the data.

        # If the folder exists -> remove it.
        if os.path.exists(build_path):
            print('Deleting excisting build path.')
            shutil.rmtree(build_path)

        # Making a new folder.
        print('Creating new build path.')
        os.makedirs(build_path)

        # Building the dlls
        print('Building the local library.')
        subprocess.run([current_directory / 'build_test_lib.bat', build_path, current_directory.parent], check=True)

        # Loads the material_functions list.
        csv_path = pl.Path(current_directory / '../all_material_functions.csv')
        csv = pd.read_csv(csv_path)

        # Loads all c files from the folders
        path1 = pl.Path(current_directory / '../models/c/general/')
        path2 = pl.Path(current_directory / '../models/c/legacy/')
        result = sorted([pl.Path(y).stem for x in os.walk(path1) for y in glob(os.path.join(x[0], '*.c'))] + [pl.Path(y).stem for x in os.walk(path2) for y in glob(os.path.join(x[0], '*.c'))])

        for name in result:
            print('Testing: ', name)
            ref_name = STEAM_materials(name, 1, 1, material_objects_path=lib_folder).get_yaml_name()
            if ref_name != "":
                with self.subTest(msg='Check if reference file excists'):
                    path = pl.Path(current_directory / str('ref_data' + os.sep + ref_name + '.yaml'))
                    self.assertIsFile(path)
                with self.subTest(msg ='Check if the data is correct'):
                    path = pl.Path(current_directory / str('ref_data' + os.sep + ref_name + '.yaml'))
                    with open(path) as file:
                        contents = yaml.load(file, Loader=yaml.FullLoader)
                    # Sets the values in the class
                    for i in contents:
                        setattr(self, i, contents[i])

                    output_array = []
                    for i in self.input:
                        numpy2d = np.array([i]) if len(i) == 1 else np.array(i)
                        output_array.append(STEAM_materials(name, numpy2d.shape[0], 1, material_objects_path=lib_folder).evaluate(numpy2d))
                    np.testing.assert_almost_equal(output_array, np.array(self.output).reshape((len(self.output), 1)), decimal=6)
                    print('Data test for', name, 'passed successfully.')
            else:
                with self.subTest(msg='Check if the function is supposed to be not tested'):
                    test_bool = csv.loc[csv['material_function'] == name, 'testing'].values[0]
                    self.assertFalse(test_bool)
                    print('Output data for',name, 'not tested!')