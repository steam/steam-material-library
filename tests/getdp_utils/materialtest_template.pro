Group{
    dummy_region = Region[1];
}

Integration{
	{ Name Int ; // Gauss integraion scheme
		Case {
			{ Type Gauss ;
				Case {
                    { GeoElement Point; NumberOfPoints  1; }
             }
			}
		}
	}
}

Jacobian{
    { Name jac_vol; // volume Jacobian
     Case{
            { Region All; Jacobian Vol; }
     }
    }
  }

FunctionSpace{
    { Name dummy_space; Type Form0;
        BasisFunction {
            { Name dummy_space_bf; NameOfCoef dummy_space_coef; Function BF_Node;
                Support dummy_region; Entity NodesOf[ All ]; }
        }
    }
}

Formulation {
    { Name test_form; Type FemEquation;
      Quantity {
        { Name x; Type Local; NameOfSpace dummy_space; }
      }
  
      Equation {
        Integral { [ 0 * Dof{x} , {x} ] ;
        In dummy_region; Integration Int ; Jacobian jac_vol ; }
      }
    }
}

Resolution{
    { Name test_resolution;
      System {
        { Name test_sys; NameOfFormulation test_form; }
      }
  
      Operation {
      }
    }
  }

PostProcessing{
    { Name test_postPro; NameOfFormulation test_form;
        NameOfSystem test_sys;
        Quantity {
          {% for idx,zipped in enumerate(input_var_vals)%}
              { Name <<function_name>>_<<idx>>; Value {
                {% if isinstance(input_const_vals, list) and len(input_const_vals) > 0 and isinstance(input_const_vals[idx], list) and len(input_const_vals[idx]) > 0  %}
                  Term {Type Global; [ <<function_name>>[<<list(zipped)|join(', ')>>]{<<input_const_vals[idx]|join(', ')>>}]; In dummy_region; } 
                 {% else %}
                  Term {Type Global; [ <<function_name>>[<<list(zipped)|join(', ')>>]]; In dummy_region; } 
                  {% endif %}
                }
              }
          {% endfor %}
        }
    }
}

PostOperation{

    { Name test_postOp; NameOfPostProcessing test_postPro;
      Operation {
    
      Echo["", Format Table, File "<<function_name>>.txt" ];

      {% for idx,_ in enumerate(zip(input_var_vals))%}
        Print[<<function_name>>_<<idx>>, OnRegion dummy_region, Format Table,
          File "<<function_name>>.txt", SendToServer "No", AppendToExistingFile 1];

      
      {% endfor %}
      } 
    }
}