import subprocess
import gmsh
from jinja2 import Environment, FileSystemLoader
import wget
import os
import zipfile
import numpy as np
import time
from concurrent.futures import ThreadPoolExecutor, as_completed


def create_gmsh_dummy_mesh(output_folder_path, model_name_arg): 
    gmsh.initialize()
    gmsh.option.set_number('General.Verbosity', 0)
    gmsh.model.add(model_name_arg)
    pnt = gmsh.model.occ.add_point(0, 0, 0)
    gmsh.model.occ.synchronize()
    gmsh.model.add_physical_group(0, [pnt], tag = 1, name="Print Point")
    gmsh.model.occ.synchronize()
    gmsh.model.mesh.generate()
    gmsh.write(os.path.join(output_folder_path, model_name_arg + ".msh"))

def template_pro_file(pro_template_path_arg, model_name_arg, input_dict_arg, output_folder_path): 

    loader = FileSystemLoader(pro_template_path_arg)
    env = Environment(loader=loader, variable_start_string='<<', variable_end_string='>>',
                            trim_blocks=True, lstrip_blocks=True)
    env.globals.update(zip=zip, enumerate=enumerate, list=list, len=len, isinstance=isinstance)
    
    pro_template = env.get_template("materialtest_template.pro")

    output_from_parsed_template = pro_template.render(input_dict_arg)

    output_pro = os.path.join(output_folder_path, model_name_arg + ".pro")
    with open(output_pro, "w") as tf:
        tf.write(output_from_parsed_template)

    return output_pro

def read_output_file(file_path): 
    with open(file_path, 'r') as f:
        output = np.loadtxt(f)[:, 1]
    return output

def subprocess_call_getdp(cerngetdp_path, output_folder_txt, pro_file, outputs_folder_msh, model_name, input_dict):
    try:
        status = subprocess.run(
            [
                cerngetdp_path,
                pro_file,
                "-solve", "#1",
                "-pos", "#1",
                "-v", "0",
                "-msh", os.path.join(outputs_folder_msh, f"{model_name}.msh"),
                "-name", os.path.join(output_folder_txt, input_dict['c_function_name'])
            ],
            check=True
        )
        if status.returncode == 0:
            output_file_path = os.path.join(output_folder_txt, f"{input_dict['function_name']}.txt")
            return read_output_file(output_file_path)
    except Exception as e:
        print(f"Error processing {cerngetdp_path}: {e}")
        return None

def parallel_getdp_calls(cerngetdp_path_list, outputs_folder_txt_list, pro_file, outputs_folder_msh, model_name, input_dict):
    all_outputs = []

    with ThreadPoolExecutor() as executor:
        futures = [
            executor.submit(
                subprocess_call_getdp, cerngetdp_path, output_folder_txt, pro_file, outputs_folder_msh, model_name, input_dict
            )
            for cerngetdp_path, output_folder_txt in zip(cerngetdp_path_list, outputs_folder_txt_list)
        ]
        
        for future in as_completed(futures):
            result = future.result()
            if result is not None:
                all_outputs.append(result)

    return all_outputs


def call_getdp_function(input_dict, model_name, cerngetdp_path_list, pro_template_path, outputs_folder_msh, outputs_folder_pro, outputs_folder_txt_list):
    """
    Call the getdp executable and evaluate values of a GetDP function for all input values. 

    :param input_dict: dictionary with four entries
        `c_function_name`: name of the C function 
        `function_name`: name of the function in GetDP syntax
        `input_const_vals`: list of tuples, each tuple is a configuration of the constant input to be tested
        `input_var_vals`: list of tuples, each tuple is a configuration of the variable input to be tested
    :return: returns numpy array with one float output for each configuration in `input_var_vals`
    :rtype: np.array 
    """
    create_gmsh_dummy_mesh(outputs_folder_msh, model_name)

    pro_file = template_pro_file(pro_template_path, model_name, input_dict, outputs_folder_pro)

    all_outputs = []

    all_outputs = parallel_getdp_calls(
        cerngetdp_path_list,
        outputs_folder_txt_list,
        pro_file,
        outputs_folder_msh,
        model_name,
        input_dict
    )
        
    return all_outputs

