import unittest
import os
import numpy as np
import pandas as pd
import pathlib as pl
from pathlib import Path
import shutil
import wget
import zipfile
import subprocess
from glob import glob
from bindings.python.src.steammaterials.STEAM_materials import STEAM_materials
from tests.test_dll import TestCaseBase
import  yaml
from tests.getdp_utils.getdp_test_win import call_getdp_function

current_directory = pl.Path(__file__).parent
CERNGETDP_LATEST_URL = "https://cernbox.cern.ch/remote.php/dav/public-files/r6trFEwyBQvRfgx/Windows/cerngetdp_snapshot_openblas.zip"
CERNGETDP_REPO_URL = "https://gitlab.cern.ch/steam/cerngetdp/-/archive/master/cerngetdp-master.zip"

REL_TOL = 1e-10
ABS_TOL = 0

# Boolean to check if running in a GitLab runner
# only in this case will the compiled getdp test be run
is_gitlab_runner = os.getenv("CI") == "true" and os.getenv("GITLAB_CI") == "true"

class GetDP_tests(unittest.TestCase):
    @classmethod
    def setUpClass(cls):

        # some useful paths to folders and files
        cls.cerngetdp_path = current_directory / "tmp_getdp_test" / "cerngetdp_snapshot"
        cls.cerngetdp_zip = cls.cerngetdp_path / "cerngetdp_snapshot_openblas.zip"

        if is_gitlab_runner:
            cls.cerngetdp_exe_list = [cls.cerngetdp_path / "getdp.exe" , current_directory.parent / "compiled_getdp" / "getdp.exe"]
        else:
            cls.cerngetdp_exe_list = [cls.cerngetdp_path / "getdp.exe"]

        cls.cerngetdp_repo_zip = current_directory / "tmp_getdp_test" / "cerngetdp-master.zip"
        cls.cerngetdp_repo_path = current_directory / "tmp_getdp_test" / "cerngetdp-master"
        cls.cerngetdp_included_function_csv_path = cls.cerngetdp_repo_path / "steam_material_adder" / "included_function.csv"
        cls.dll_build_path = current_directory / "tmp_getdp_test" / "dll_build"
        cls.dll_path = cls.dll_build_path / "lib"
        cls.pro_files_base_path = current_directory / "tmp_getdp_test" / "getdp_outputs"
        cls.pro_template_path = current_directory / "getdp_utils"

        if is_gitlab_runner:
            cls.output_folder_txt = [cls.pro_files_base_path / "txt_snapshot", cls.pro_files_base_path / "txt_compiled"]
        else:
            cls.output_folder_txt = [cls.pro_files_base_path / "txt_snapshot"]

        for output_older in cls.output_folder_txt:
            os.makedirs(output_older, exist_ok=True)

        ## grab the latest cerngetdp snapshot
        # make temporary directory, it's okay if it already exists 
        os.makedirs(cls.cerngetdp_path, exist_ok=True)

        if cls.cerngetdp_zip.is_file():
            cls.cerngetdp_zip.unlink()
        
        zip_file = wget.download(CERNGETDP_LATEST_URL, out=str(cls.cerngetdp_zip))

        with zipfile.ZipFile(zip_file, "r") as zip_ref: 
            zip_ref.extractall(cls.cerngetdp_path)

        # check if all getdp.exe is located where needed and callable
        for exe in cls.cerngetdp_exe_list:
            subprocess.run([exe, "--version"], check=True)  

        ## download the cerngetdp repo
        os.makedirs(cls.cerngetdp_repo_path, exist_ok=True)

        if cls.cerngetdp_repo_zip.is_file():
            cls.cerngetdp_repo_zip.unlink()

        zip_file = wget.download(CERNGETDP_REPO_URL, out=str(cls.cerngetdp_repo_zip))

        with zipfile.ZipFile(zip_file, "r") as zip_ref:
            zip_ref.extractall(cls.cerngetdp_repo_path.parent)

        # make getdp outputs folder 
        os.makedirs(cls.pro_files_base_path, exist_ok=True)

        # NB: we compile the library here again, to be able to run the tests_getdp locally and without the need to have the library precompiled
        # NB: this could be optimized by relying on tests_dll to compile the library and then copy it here
        # Builds the library

        # Making a new folder.
        print('Creating new build path.')
        os.makedirs(cls.dll_build_path, exist_ok=True)

        # Building the dlls
        print('Building the local library.')

        subprocess.run([current_directory / 'build_test_lib.bat', cls.dll_build_path, current_directory.parent], check=True)


    def test_getdp_against_reference_yaml(self):
        '''
        This test compares the output of the GetDP functions with the reference data stored in the yaml files.
        It is computed with the latest snapshot from the CERNGetDP repository and with a freshly compiled version (the latter only on the runner).
        '''

        # Loads the material_functions list.
        csv_path = pl.Path(current_directory / '../all_material_functions.csv')
        csv = pd.read_csv(csv_path)

        # Loads all c files from the folders
        path1 = pl.Path(current_directory / '../models/c/general/')
        path2 = pl.Path(current_directory / '../models/c/legacy/')
        result = sorted([pl.Path(y).stem for x in os.walk(path1) for y in glob(os.path.join(x[0], '*.c'))] + [pl.Path(y).stem for x in os.walk(path2) for y in glob(os.path.join(x[0], '*.c'))])

        for name in result:
            print('Testing: ', name)
            ref_name = STEAM_materials(name, 1, 1, material_objects_path=self.dll_path).get_yaml_name()
            if ref_name != "":
                with self.subTest(msg='Check if reference file exists'):
                    path = pl.Path(current_directory / str('ref_data' + os.sep + ref_name + '.yaml'))
                    TestCaseBase.assertIsFile(self, path)
                
                path = pl.Path(current_directory / str('ref_data' + os.sep + ref_name + '.yaml'))
                with open(path) as file:
                    contents = yaml.load(file, Loader=yaml.FullLoader)
                # Sets the values in the class
                for i in contents:
                    setattr(self, i, contents[i])

                self.current_function_name = name                        
                self.update_current_getdp_settings()

                if self.current_getdp_function_name is None:
                    print('Output data for', name, 'not tested for GetDP since GetDP is not compiled with this function!')
                    assert True
                else:
                    for mapping_list, num_var_params, getdp_function_name in zip(self.current_mapping_list, self.current_num_var_params, self.current_getdp_function_name):

                        # permute input according to mapping list
                        permuted_input = [[one_input[i] for i in mapping_list] for one_input in self.input]

                        input_const = [sublist[num_var_params:] for sublist in permuted_input]
                        input_var = [sublist[:num_var_params] for sublist in permuted_input]

                        input_dict = {
                            "c_function_name": self.current_function_name,
                            "function_name": getdp_function_name,
                            "input_const_vals": input_const,
                            "input_var_vals": input_var,
                        }

                        outputs_folder_msh = self.pro_files_base_path
                        outputs_folder_pro = self.pro_files_base_path

                        getdp_outputs = call_getdp_function(input_dict, getdp_function_name, self.cerngetdp_exe_list, self.pro_template_path, outputs_folder_msh, outputs_folder_pro, self.output_folder_txt)

                        with self.subTest(msg=f"Compare GetDP snapshot and reference yaml for {name}"):
                            try:
                                np.testing.assert_allclose(getdp_outputs[0], np.array(self.output), rtol=REL_TOL, atol=ABS_TOL)
                                print('Snapshot GetDP test for', getdp_function_name, 'passed successfully.')
                            except Exception as e:
                                print(f"Error in function {name} with GetDP snapshot function {getdp_function_name}: {e}")
                                self.fail(f"Error in function {name} with GetDP snapshot function {getdp_function_name}")

                        if is_gitlab_runner:
                            with self.subTest(msg=f"Compare compiled GetDP and reference yaml for {name}"):
                                try:
                                    np.testing.assert_allclose(getdp_outputs[1], np.array(self.output), rtol=REL_TOL, atol=ABS_TOL)
                                    print('Compiled GetDP test for', getdp_function_name, 'passed successfully.')
                                except Exception as e:
                                    print(f"Error in function {name} with compiled GetDP function {getdp_function_name}: {e}")
                                    self.fail(f"Error in function {name} with compiled GetDP function {getdp_function_name}")
            else:
                with self.subTest(msg='Check if the function is supposed to be not tested'):
                    test_bool = csv.loc[csv['material_function'] == name, 'testing'].values[0]
                    self.assertFalse(test_bool)
                    print('Output data for',name, 'not tested!')

        assert True


    def update_current_getdp_settings(self):

        df = pd.read_csv(self.cerngetdp_included_function_csv_path)

        filtered_df = df[df['c_function_name'] == self.current_function_name]

        if filtered_df.empty:
            print(f"Function {self.current_function_name} not found in the included functions csv file.")
            self.current_getdp_function_name = None
            self.current_num_const_params = None
            self.current_num_var_params = None
            self.current_mapping_list = None
        else:
            self.current_getdp_function_name = filtered_df['GetDP_function_name'].values
            self.current_num_const_params = filtered_df['input_const_params'].values
            self.current_num_var_params = filtered_df['input_var_params'].values
            self.current_mapping_list = [[int(item) for item in string.split('-')] for string in filtered_df['mapping'].values]