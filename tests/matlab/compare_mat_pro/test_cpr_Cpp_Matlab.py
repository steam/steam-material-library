import unittest
import os
import numpy as np

from bindings.python.src.steammaterials import SteamMaterials


class TestBuilderLEDET(unittest.TestCase):

    def setUp(self) -> None:
        """
            This function is executed before each test in this class
        """
        self.current_path = os.getcwd()
        os.chdir(os.path.dirname(__file__))  # move to the directory where this file is located
        print(f'Current folder:          {self.current_path}')
        print(f'Test is run from folder: {os.getcwd()}')

        self.path_library_Matlab = r'C:\cernbox\steam-ledet-material-library'

    def tearDown(self) -> None:
        """
            This function is executed after each test in this class
        """
        os.chdir(self.current_path)  # go back to initial folder


    def test_cfun_rhocu_v1(self):
        # Sources:
        # [1] National Bureau of Standards
        #     Survey of Electrical Resistivity Measurements on 16 Pure Metals In the Temperature Range 0 to 273°K (1968), p.37
        #     Web: https://nvlpubs.nist.gov/nistpubs/Legacy/TN/nbstechnicalnote365.pdf
        # [2] N.J. Simon, E.S. Drexler, R.P. Reed
        #     Properties of Copper and Copper Alloys at Cryogenic Temperatures (1992), p.300
        #     Web: https://nvlpubs.nist.gov/nistpubs/Legacy/MONO/nistmonograph177.pdf
        
        # Dataset from [1], White and Woods 1959
        expected_rho = np.array([0.00287, 0.0090, 0.0247, 0.1557, 0.6377, 1.0627, 1.5527]) * 1e-8
        T = np.array([15, 30, 40, 70, 140, 200, 273])
        B = np.zeros(T.shape)
        RRR = np.ones(T.shape) * 600
        T_ref_RRR = np.ones(T.shape) * 273

        # Call material function
        param_array = np.vstack((T, B, RRR, T_ref_RRR))
        sm = SteamMaterials('CFUN_rhoCu_v1', param_array.shape[0], param_array.shape[1])
        result = sm.evaluate(param_array)
        

        relative_error = (result - expected_rho) / expected_rho
        result_str = [f'{x:.3e}' for x in result]
        print(f'Output: [{" ".join(result_str)}]')
        errors = [f'{x*100:.1f}%' for x in relative_error]
        print(f'Errors: [{" ".join(errors)}]')

        # According to [2], an accuracy of <15% is to be expected
        self.assertTrue(np.all(np.abs(relative_error) < 0.15))


    def test_bhiron1_v1_inter_extra(self):
        function_Cpp = 'BHIron2_v1'
        num_elem = 10
        x = np.linspace(0, 5000000, num_elem)
        expected_output = [0.0, 2.8354114125541225, 3.534787990907976, 4.233391566651791, 4.931798446918102, 5.629902497142767,
                           6.3283667606626715, 7.026517437764208, 7.7246266269676, 8.42275832776538]
        # make numpy array
        print(x)
        numpy2d = np.vstack((x, np.ones(num_elem), np.ones(num_elem), np.ones(num_elem)))
        sm = SteamMaterials(function_Cpp, numpy2d.shape[0], numpy2d.shape[1])

        # call with 2D numpy array and get results back
        result_Cpp = sm.evaluate(numpy2d)
        result_Cpp = list(result_Cpp)
        print(result_Cpp)
        self.assertListEqual(result_Cpp, expected_output)

    def test_bhair_v1(self):
        function_Cpp = 'BHAir_v1'
        num_elem = 10
        x = np.linspace(0, 5000000, num_elem)
        expected_output =[0.0, 0.6981317007977776, 1.3962634015955553, 2.094395102393333, 2.7925268031911106,
                          3.490658503988888, 4.188790204786666, 4.886921905584443, 5.585053606382221, 6.283185307179999]

        # make numpy array
        print(x)
        numpy2d = np.vstack((x, np.ones(num_elem), np.ones(num_elem), np.ones(num_elem)))
        sm = SteamMaterials(function_Cpp, numpy2d.shape[0], numpy2d.shape[1])

        # call with 2D numpy array and get results back
        result_Cpp = sm.evaluate(numpy2d)
        result_Cpp=list(result_Cpp)
        print(result_Cpp)
        self.assertListEqual(result_Cpp, expected_output)

    def test_cpr_one_function_1arg(self):
        """
            Compare teh output of one function with one argument
        """
        # arrange
        function_Cpp = 'CFUN_rhoCu_v1'  # function name, this one takes 4 arguments as input and returns a single float for resistivity

        # act
        T_min = 1.8
        T_max = 300
        B = 1.
        RRR = 120.0
        T_ref_RRR = 273.
        num_elem = 1000
        T = np.linspace(T_min, T_max, num_elem)

        # make numpy array
        numpy2d = np.vstack((T, np.ones(num_elem) * B, np.ones(num_elem) * RRR, np.ones(num_elem) * T_ref_RRR))

        # make dll func object

        sm = SteamMaterials(function_Cpp, numpy2d.shape[0], numpy2d.shape[1])

        # call with 2D numpy array and get results back
        result_Cpp = sm.evaluate(numpy2d)
        print(result_Cpp)

        #results_Matlab = SteamMaterials_Matlab(function_Matlab, numpy2d.shape[0], numpy2d.shape[1], self.path_library_Matlab)
        #print(results_Matlab)

        # assert
        #self.assertListEqual(list(result_Cpp), list(results_Matlab))
