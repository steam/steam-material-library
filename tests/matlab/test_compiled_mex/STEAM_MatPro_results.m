clear 
clc
parentDirectory = fileparts(pwd);

N = 1000; % number of steps
figureFormats = {'png', 'fig', 'svg'};

% addpath( genpath( fullfile(parentDirectory,'..', 'source_m') ) )
% addpath( genpath( fullfile(parentDirectory,'..', 'mex_compilation\compiled_mex') ) )
% addpath( genpath( fullfile(parentDirectory, '..', 'compiled_STEAM_MatPro') ) )
% addpath( genpath( fullfile(parentDirectory, '..', 'models', 'matlab')) )
addpath( genpath( (fullfile(fullfile(parentDirectory, '..','..','models', 'matlab'))) ))
addpath( genpath( (fullfile(fullfile(parentDirectory, '..','..','bindings', 'matlab', 'compiled_mex'))) ))
addpath( genpath( (fullfile(fullfile(parentDirectory, '..','..','bindings', 'matlab', 'compiled_STEAM_MatPro'))) ))

% Optional - Recompile the .mex files
flag_compile_c_to_mex = 0;
if flag_compile_c_to_mex==1
    compile_c_to_mex
end

filename = fullfile('..','..','..', 'functionsNames_website_csv.csv');
% fullPath = which(filename) ;

function_const_arg = 'rhoCu_nist'; %function that in MatLab takes the last input as a scalar, so the same thing should be applied in the calling of the mex.
functions_with_structs = {'Jc_Nb3Sn_Bottura', 'Jc_NbTi_Bottura', 'Jc_NbTi_Cudi_fit1', 'Jc_NbTi_Cudi'}; %functions using structs have a different format

if ~isempty(filename)    
     % Read the entire Excel sheet into a table
     data = readtable(filename, 'Delimiter', ',');
     CFUN_prop = data.x___C_function_prop;
     handles = data.handle_name;
     matlab_name = data.matlab_name;
     num_of_inputs = data.Input_parameters; 
     inputRanges = data(:, {'Range_1', 'Range_2', 'Range_3', 'Range_4', 'Range_5', 'Range_6', 'Range_7', 'Range_8', 'Range_9', 'Range_10', 'Range_11', 'Range_12'}); 
else
    error('Unable to locate the file %s', filename);
end

% Create the "outputs" folder
folderName = 'outputs';
% if exist(folderName, 'dir')
%     rmdir(folderName, 's');
% end
mkdir(folderName);
% Open a text file to save runtime measurements
txtFileName = fullfile(folderName, 'runtime_measurements.txt');
fid = fopen(txtFileName, 'w');
fprintf(fid, 'Function Name, MATLAB Runtime (seconds), MEX Runtime (seconds)\n');


% Create the "graphs" folder inside "MatLab_Mex_graphs"
MatLab_Mex_graphs = '..\..\docs\dynamic\MatLab_Mex_graphs';
% if exist(MatLab_Mex_graphs, 'dir')
%     rmdir(MatLab_Mex_graphs, 's');
% end
mkdir(MatLab_Mex_graphs);

% Create the "Only_Mex_folder" inside "MatLab_Mex_graphs"
Only_Mex_folder = fullfile(MatLab_Mex_graphs, 'Only_Mex_folder');
% if exist(Only_Mex_folder, 'dir')
%     rmdir(Only_Mex_folder, 's');
% end
mkdir(Only_Mex_folder);

% Create the "error.graphs" folder
errorFolder = 'error.graphs';
% if exist(errorFolder, 'dir')
%     rmdir(errorFolder, 's');
% end
mkdir(errorFolder);

% Initialize cell arrays to store all relative and absolute errors
allRelativeErrors = cell(1, numel(handles));
allabsoluteErrors = cell(1, numel(handles));

counter = 1;

for i = 1:numel(handles)
    % Get the current function handle
    functionToTest =  handles{i};
    CFUN_property  = CFUN_prop{i};
    MatLab_func_name  = matlab_name{i};
    num_of_inputs_func  = num_of_inputs(i);
    
    counter = counter + 1;
    try
        % Extract input ranges
        inputRange = inputRanges{i, :};
        nonEmptyCells = ~cellfun('isempty', inputRange) & ~cellfun(@(x) all(isnan(x)), inputRange);
        inputRange = inputRange(nonEmptyCells);
        
        % Evaluate the input ranges for C functions & MatLab
        inputValues = cell(1, numel(inputRange));
        inputGrid = cell(size(inputValues));
        Mat = [];
        Mex = [];
        
        
       for j = 1:numel(inputRange)
            inputString = inputRange{j}; % Get the input range
            
            % Use regular expression to match the pattern: number-number-number
            pattern = '(-?\d*\.?\d+):(-?\d*\.?\d+):(-?\d*\.?\d+)';
            matches = regexp(inputRange{j}, pattern, 'tokens', 'once');
        
            if ~isempty(matches)
                values = cellfun(@str2double, matches);
                num_of_spaces = round(values(3)-values(1)) / values(2);
                inputValues{j} = values(1):values(2):values(3);
            else
                % Extract the value after the colon and treat it as a scalar
                colonIndex = strfind(inputString, ':');
                scalarValue = str2double(inputString(colonIndex+1:end));
        
                if ~isnan(scalarValue)
                    % If scalar, repeat the value across the number of spaces
                    inputValues{j} = scalarValue;
                else
                    % For unrecognized input, provide an error message or handle as needed
                    disp([functionToTest '- Unrecognized input format.']);
                end
            end
       end
       
       handle_functionToTest = ['handle_' functionToTest];
       
       %use ngrid to iterate through all the input values
       [inputGrid{:}] = ndgrid(inputValues{:});
       
        % Convert the meshgrid to cell arrays
        inputValues2 = cellfun(@(x) x(:), inputGrid, 'UniformOutput', false);
        
        try
             % Calculate material property using 'm' functions
             functionSet='m';
             STEAM_ML = STEAM_MatPro(functionSet);
             
             if strcmp(functionToTest, 'fisc_I_Ic_n_rhom_Am_BSCCO2212')==1
                 temp_inputValues=inputValues2;
                 temp_inputValues{5}=temp_inputValues{5}(1);
                 tic;
                 Mat = STEAM_ML.(handle_functionToTest)(temp_inputValues{:});
                 matlab_runtime = toc;
                 clear temp_inputValues
             else
                 tic;
                 Mat = STEAM_ML.(handle_functionToTest)(inputValues2{:});
                 if strcmp(handle_functionToTest, ['handle_' function_const_arg])                  
                     Mat = STEAM_ML.(handle_functionToTest)(inputValues2{1:end-1}, scalarValue);
                 end
                 matlab_runtime = toc;
             end
             
             % Check if Mat is a row array
             if ~isrow(Mat)
                Mat = Mat.';
             end

             % Your existing code for formatting ranges
%              formatted_ranges = sprintf('\n | %s', strrep(ranges, "'", ""));

        
        catch 
             disp([functionToTest ' - MatLab function does not exist.']), close all
        end
        
        try
            % Switch to 'mex' function set
            functionSet='mex';
            STEAM_ML = STEAM_MatPro(functionSet);
                
            % Check if inputValues2 has any empty cells
            if any(cellfun('isempty', inputValues2))
                disp([functionToTest ' - Skipping Mex calculation due to empty cells in inputValues2.']);
                Mex = [];  % Set Mex to empty if there are empty cells
            else
                tic;
                Mex = STEAM_ML.(handle_functionToTest)(inputValues2{:});
                mex_runtime = toc;
            
                % Check if Mex is a row array
                if ~isrow(Mex)
                    Mex = Mex.';
                end
            end
        catch 
            disp([functionToTest ' - An error occurred while calculating Mex']), close all
        end
        try
            if ismember(functionToTest, functions_with_structs)
                num_input_struct_1 = num_of_inputs_func - 4;
                num_input_struct_2 = 2;
                input_struct{1} = inputValues{1};
                input_struct{2} = inputValues{2};
                input_struct{3} = struct();
                input_struct{4} = struct();

                % Extract variable names from the first row of inputRanges
                pattern = '([^:]+) *:';
                matches = regexp(inputRange, pattern, 'tokens');
                variable_names_1 = cellfun(@(x) strtrim(x{1}), matches);

                % Assign values to fields in input_struct{3}
                for i = 1:num_input_struct_1
                    input_struct{3}.(variable_names_1{i+2}) = inputValues{2 + i};
                end

                % Extract variable names from the second row of inputRanges 
                pattern = '([^:]+) *:';
                matches = regexp(inputRange, pattern, 'tokens');
                variable_names_2 = cellfun(@(x) strtrim(x{1}), matches);

                % Assign values to fields in input_struct{4}
                for j = 1:num_input_struct_2
                    input_struct{4}.(variable_names_2{j + num_input_struct_1 + 2}) = inputValues{num_input_struct_1 + 2 + j};
                end

                % Create ndgrid for the first two input structures
                [grid_T, grid_B] = ndgrid(input_struct{1}, input_struct{2});

                % Combine the grids with input_struct{3} and input_struct{4}
                inputGrid_struct = cell(size(grid_T));

                for i = 1:numel(grid_T)
                    inputGrid_struct{i} = {grid_T(i), grid_B(i), input_struct{3}, input_struct{4}};
                end

                functionSet = 'm';
                STEAM_ML = STEAM_MatPro(functionSet);
                for i = 1:numel(inputGrid_struct)
                    % Extracting the cell array for the current iteration
                    current_input = inputGrid_struct{i};

                    % Calling the STEAM_ML function with the current input
                    tic;
                    Mat(i) = STEAM_ML.(handle_functionToTest)(current_input{:});
                    matlab_runtime = toc;
                end

                functionSet = 'mex';
                STEAM_ML = STEAM_MatPro(functionSet);
                for i = 1:numel(inputGrid_struct)
                    % Extracting the cell array for the current iteration
                    current_input = inputGrid_struct{i};

                    % Calling the STEAM_ML function with the current input
                    tic;
                    Mex(i) = STEAM_ML.(handle_functionToTest)(current_input{:});
                    mex_runtime = toc;
                end
                
            end
       catch
           disp([functionToTest ' - error']), close all
       end
        % Check if Mat is not empty
    if ~isempty(Mat) && all(~isnan(Mat(:))) && any(Mat(:) ~= 0)
        %Calculate absolute error
        absoluteErrors = abs(Mat - Mex);
        absoluteErrors(isnan(absoluteErrors)) = 0;
        AbsErrorCell = num2cell(absoluteErrors);
        % Calculate relative error
        RelativeError = abs((Mat - Mex) ./ Mat);
        RelativeError(abs(Mat - Mex)<eps) = eps;
        RelErrorCell = num2cell(RelativeError);
        if isrow(RelativeError)
            RelativeError = RelativeError';
        end
        
        % Create a cell array to store the results
        results = cell(numel(Mat), 4);
        results(:, 1) = cellstr(num2str(Mat', '%.20g')); % eps
        results(:, 2) = cellstr(num2str(Mex', '%.20g'));
        results(:, 3) = RelErrorCell;
        results(:, 4) = AbsErrorCell;
        
        % Save results to CSV file in the "outputs" folder
        csvFileName = [functionToTest, '.csv'];
        csvFilePath = fullfile(folderName, csvFileName);
        cell2csv(csvFilePath, results);
        
        % Store absolute & relative error in the cell array
        allabsoluteErrors{i} = absoluteErrors;
        allRelativeErrors{i} = RelativeError;
        
        % Plot percentage error curve
%         figure;
        n_input1=length(inputGrid{1});
        n_cases=numel(RelativeError)/n_input1;
        grad_cases=fGrad_coding_minmax2(1:n_cases);
        legend_labels=cell(0);
        Figure=figure('visible','on');
        hold on,box on,grid on
        for kk=1:n_cases
            idxTemp=1+(kk-1)*n_input1:kk*n_input1;
            plot(inputValues2{1}(idxTemp), RelativeError(idxTemp), '.-', 'Color', grad_cases(kk,:)); %plot(generated_values, PercentageError, '.');
            legend_labels{kk}=[];
            try
                for in=1:num_of_inputs(i)
                    curr_value=inputValues2{in}(idxTemp(1));
                    if abs(curr_value)>1e3
                        legend_labels{kk}=[legend_labels{kk} 'i' num2str(in) ': ' num2str(inputValues2{in}(idxTemp(1)),'%0.3e') ' '];
                    else
                        legend_labels{kk}=[legend_labels{kk} 'i' num2str(in) ': ' num2str(inputValues2{in}(idxTemp(1))) ' '];
                    end
                       
                end
            catch
                disp([functionToTest ' - error']), close all
            end
        end
        legend(legend_labels, 'location','best')
%         plot(inputValues2{1}, RelativeError, '.');
        set(gca, 'XScale', 'log'); % Set logarithmic scale on x-axis
        set(gca, 'YScale', 'log'); % Set logarithmic scale on y-axis
        xlabel('Input Range 1');
        ylabel('Relative Error');
        
        title(['Relative Error Curve - ', strrep(functionToTest, '_', '\_')]);
%         legend(strrep(functionToTest, '_', '\_'));
        % Save figure as MATLAB figure in "error.graphs" folder
        %saveFigureInMultipleFormats(gcf, errorFolder, CFUN_property, figureFormats)
         for f=1:length(figureFormats)
             file_ext = figureFormats{f};
             figureFileName = [functionToTest, '.', file_ext];
%              figureFileName = [CFUN_property, '.', file_ext];
             figureFilePath = fullfile(errorFolder, figureFileName);
             saveas(gcf, figureFilePath, file_ext);
         end
%          pause(2)
        close all
        
        
    else
        disp([functionToTest ' - Mat is empty, contains NaN, or contains zeros for function: ', functionToTest]);
    end
    
    % Plot percentage error curve
    if ~isempty(Mat) && all(~isnan(Mat(:))) && any(Mat(:) ~= 0)
        n_input1=length(inputGrid{1});
        n_cases=numel(RelativeError)/n_input1;
        grad_cases=fGrad_coding_minmax2(1:n_cases);
        legend_labels=cell(0);
        Figure1=figure('visible','on');
        hold on,box on,grid on
        for kk=1:n_cases
            idxTemp=1+(kk-1)*n_input1:kk*n_input1;
            h_ToLegend(kk)=plot(inputValues2{1}(idxTemp), Mat(idxTemp), '.-', 'Color', grad_cases(kk,:));
            plot(inputValues2{1}(idxTemp), Mex(idxTemp), 'o-', 'Color', grad_cases(kk,:));%plot(generated_values, PercentageError, '.');
            legend_labels{kk}=[];
            for in=1:num_of_inputs(i)
                curr_value=inputValues2{in}(idxTemp(1));
                if abs(curr_value)>1e3
                	legend_labels{kk}=[legend_labels{kk} 'i' num2str(in) ': ' num2str(inputValues2{in}(idxTemp(1)),'%0.3e') ' '];
                else
                    legend_labels{kk}=[legend_labels{kk} 'i' num2str(in) ': ' num2str(inputValues2{in}(idxTemp(1))) ' '];
                end
            end
        end
        display(['These are the legend_labels: ' legend_labels])
        xlabel('Input Range 1');
        ylabel(strrep(functionToTest, '_', '\_'));
        
            if strcmp(MatLab_func_name,'error')==0
                title([strrep(functionToTest, '_', '\_'), ' and ', strrep(MatLab_func_name, '_', '\_')]);
            else
                title([strrep(functionToTest, '_', '\_')]);
            end
%         legend('Mex function', 'MatLab function');
        % Save figure as MATLAB figure in "error.graphs" folder
        %saveFigureInMultipleFormats(gcf, MatLab_Mex_graphs, CFUN_property, figureFormats)
         for f=1:length(figureFormats)
             file_ext = figureFormats{f};
             figureFileName = [functionToTest, '.', file_ext];
%              figureFileName = [CFUN_property,  '.', file_ext];
             figureFilePath = fullfile(MatLab_Mex_graphs, figureFileName);
             saveas(gcf, figureFilePath, file_ext);
         end
%          pause(2)
        close all
    else
        n_input1=length(inputGrid{1});
        n_cases=numel(Mex)/n_input1;
        grad_cases=fGrad_coding_minmax2(1:n_cases);
        legend_labels=cell(0);
        Figure2=figure('visible','on');
        hold on,box on,grid on
        for kk=1:n_cases
            idxTemp=1+(kk-1)*n_input1:kk*n_input1;
            plot(inputValues2{1}(idxTemp), Mex(idxTemp), 'o', 'Color', grad_cases(kk,:)); %plot(generated_values, PercentageError, '.');
            legend_labels{kk}=[];
            for in=1:num_of_inputs(i)
                curr_value=inputValues2{in}(idxTemp(1));
                if abs(curr_value)>1e3
                	legend_labels{kk}=[legend_labels{kk} 'i' num2str(in) ': ' num2str(inputValues2{in}(idxTemp(1)),'%0.3e') ' '];
                else
                    legend_labels{kk}=[legend_labels{kk} 'i' num2str(in) ': ' num2str(inputValues2{in}(idxTemp(1))) ' '];
                end
            end
        end
        legend(legend_labels, 'location','best')
        xlabel('Input Range 1');
        ylabel(strrep(functionToTest, '_', '\_'));
        if strcmp(MatLab_func_name,'error')==0
            title([strrep(functionToTest, '_', '\_'), ' and ', strrep(MatLab_func_name, '_', '\_')]);
        else
            title([strrep(functionToTest, '_', '\_')]);
        end
%         legend('Mex function');
        %saveFigureInMultipleFormats(gcf, Only_Mex_folder, CFUN_property, figureFormats)
        fprintf(fid, '%s, %f, %f\n', functionToTest, matlab_runtime, mex_runtime);

        % Save figure as MATLAB figure in "error.graphs" folder
        for f=1:length(figureFormats)
            file_ext = figureFormats{f};
            figureFileName = [functionToTest, '.', file_ext];
%             figureFileName = [CFUN_property, '.', file_ext];
            figureFilePath = fullfile(Only_Mex_folder, figureFileName);
            saveas(gcf, figureFilePath, file_ext);
        end
%         pause(2)
        close all
    end
    
    catch
        disp([functionToTest ' - Error occurred for function: ', functionToTest]); close all
        continue;
    end
end
%%
close all;
% close text file
fclose(fid);

function cell2csv(fileName, cellArray)
    %CELL2CSV Write cell array to a CSV file
    %   CELL2CSV(fileName, cellArray) writes the cell array to the specified
    %   CSV file. Each cell of the array is written as a separate element in
    %   the CSV file, with values separated by commas.

    file = fopen(fileName, 'w');
    for i = 1:size(cellArray, 1)
        for j = 1:size(cellArray, 2)
            fprintf(file, '%s', cellArray{i, j});
            if j ~= size(cellArray, 2)
                fprintf(file, ',');
            end
        end
        fprintf(file, '\n');
    end
    fclose(file);
end


function saveFigureInMultipleFormats(figHandle, directoryPath, fileName, figureFormats)
    % Create a cell array of formats
    figureFormats = {'png', 'svg', 'fig'};

    % Iterate through each format and save the figure
    for f = 1:length(figureFormats)
        fileExt = figureFormats{f};
        figureFileName = [fileName, '.', fileExt];
        figureFilePath = fullfile(directoryPath, figureFileName);

        % Save the figure based on the format
        if strcmpi(fileExt, 'png')
            % Save as PNG
            print(figHandle, figureFilePath, '-dpng');
        elseif strcmpi(fileExt, 'svg')
            % Save as SVG
            print(figHandle, figureFilePath, '-dsvg');
        elseif strcmpi(fileExt, 'fig')
            % Save as FIG
            set(gcf, 'visible', 'on');
            savefig(figHandle, figureFilePath);
            set(gcf, 'visible', 'on');
        else
            disp(['Unsupported format: ', fileExt]);
        end
    end
end
