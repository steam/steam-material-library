classdef TestCompiledMex < matlab.unittest.TestCase
    % TestCompiledMex tests the compiled .mex functions from source_C and
    % compares them to the .m functions from source_m

    properties
        N_elements=1e4; % number of values to calculate
        max_allowed_rel_diff = 1e-6; % maximum allowed relative error
        % Lists of functions to test
        functionsToTest_1arg={'cpAg','cpAl5083', 'cpBeCu', 'cpBSCCO2212','cpCuNIST','cpG10','cpHe','cpKapton','cpSS','kG10','kKapton','kSS', 'kAg'}; % list of all functions to test that require 1 argument
        functionsToTest_3arg={'kCuNIST'};
    end
    
    methods(TestMethodTeardown)
        % These fucntions are run automatically after each test
        function closeFigure(testCase)
            pause(5)  % wait 5 seconds
            close all % close all figures
        end
    end
    
    methods (Test)
        %% Test the functions requiring 1 argument within an extended temperature range
        function test_funcs_1arg_scalar(testCase)
            % Select inputs
            functionsToTest=testCase.functionsToTest_1arg; % list of all functions to test
            N_elements=testCase.N_elements; % number of values to calculate
            max_allowed_rel_diff=testCase.max_allowed_rel_diff;
            flag_plot=0;
            flag_summary_plot=1;
            
            % Get input values to test
            inputValues=getInputValues(N_elements);
            TT=mean(inputValues.T_extended_range);

            % Run tests
            n_functionsToTest=length(functionsToTest);
            output_summary=zeros(n_functionsToTest,9);
            for f=1:n_functionsToTest
                functionToTest=functionsToTest{f};
                [timed_mat, timed_mex, max_abs_err, max_rel_err, mean_abs_err, mean_rel_err, fun_out_mat, fun_out_mex] = time_cpr_fun_1arg(functionToTest, N_elements, flag_plot, TT);
                output_summary(f,:)=[timed_mat, timed_mex, max_abs_err, max_rel_err, mean_abs_err, mean_rel_err, timed_mat/N_elements, timed_mex/N_elements];
            end
            mean_abs_errs=output_summary(:,6);
            max_abs_errs =output_summary(:,4);

            % Display summary plots
            if flag_summary_plot==1
                handles_fig = displaySummaryPlots(output_summary, functionsToTest, N_elements);
            end
            
            % Assert that functions give similar output
            testCase.verifyLessThan(max(mean_abs_errs), max_allowed_rel_diff);
            testCase.verifyLessThan(max(max_abs_errs) , max_allowed_rel_diff);
        end
        
        %% Test the functions requiring 1 argument within a limited temperature range
        function test_funcs_1arg_limited_range(testCase)
            % Select inputs
            functionsToTest=testCase.functionsToTest_1arg; % list of all functions to test
            N_elements=testCase.N_elements; % number of values to calculate
            max_allowed_rel_diff=testCase.max_allowed_rel_diff;
            flag_plot=1;
            
            % Get input values to test
            inputValues=getInputValues(N_elements);
            TT=inputValues.T_limited_range;

            % Run tests
            n_functionsToTest=length(functionsToTest);
            output_summary=zeros(n_functionsToTest,8);
            for f=1:n_functionsToTest
                functionToTest=functionsToTest{f};
                [timed_mat, timed_mex, max_abs_err, max_rel_err, mean_abs_err, mean_rel_err, fun_out_mat, fun_out_mex] = time_cpr_fun_1arg(functionToTest, N_elements, flag_plot, TT);
                output_summary(f,:)=[timed_mat, timed_mex, max_abs_err, max_rel_err, mean_abs_err, mean_rel_err, timed_mat/N_elements, timed_mex/N_elements];
            end
            mean_abs_errs=output_summary(:,6);
            max_abs_errs =output_summary(:,4);

            % Display summary plots
            if flag_plot==1
                handles_fig = displaySummaryPlots(output_summary, functionsToTest, N_elements);
            end
            
            % Assert that functions give similar output
            testCase.verifyLessThan(max(mean_abs_errs), max_allowed_rel_diff);
            testCase.verifyLessThan(max(max_abs_errs) , max_allowed_rel_diff);
        end
        
        %% Test the functions requiring 1 argument within a limited temperature range, receiving a column-vector as an input
        function test_funcs_1arg_limited_range_column(testCase)
            % Select inputs
            functionsToTest=testCase.functionsToTest_1arg; % list of all functions to test
            N_elements=testCase.N_elements; % number of values to calculate
            max_allowed_rel_diff=testCase.max_allowed_rel_diff;
            flag_plot=1;
            
            % Get input values to test
            inputValues=getInputValues(N_elements);
            TT=inputValues.T_limited_range_col;

            % Run tests
            n_functionsToTest=length(functionsToTest);
            output_summary=zeros(n_functionsToTest,8);
            for f=1:n_functionsToTest
                functionToTest=functionsToTest{f};
                [timed_mat, timed_mex, max_abs_err, max_rel_err, mean_abs_err, mean_rel_err, fun_out_mat, fun_out_mex] = time_cpr_fun_1arg(functionToTest, N_elements, flag_plot, TT);
                output_summary(f,:)=[timed_mat, timed_mex, max_abs_err, max_rel_err, mean_abs_err, mean_rel_err, timed_mat/N_elements, timed_mex/N_elements];
            end
            mean_abs_errs=output_summary(:,6);
            max_abs_errs =output_summary(:,4);

            % Display summary plots
            if flag_plot==1
                handles_fig = displaySummaryPlots(output_summary, functionsToTest, N_elements);
            end
            
            % Assert that functions give similar output
            testCase.verifyLessThan(max(mean_abs_errs), max_allowed_rel_diff);
            testCase.verifyLessThan(max(max_abs_errs) , max_allowed_rel_diff);
        end
        
        %% Test the functions requiring 1 argument within an extended temperature range
        function test_funcs_1arg_extended_range(testCase)
            % Select inputs
            functionsToTest=testCase.functionsToTest_1arg; % list of all functions to test
            N_elements=testCase.N_elements; % number of values to calculate
            max_allowed_rel_diff=testCase.max_allowed_rel_diff;
            flag_plot=1;
            
            % Get input values to test
            inputValues=getInputValues(N_elements);
            TT=inputValues.T_extended_range;

            % Run tests
            n_functionsToTest=length(functionsToTest);
            output_summary=zeros(n_functionsToTest,8);
            for f=1:n_functionsToTest
                functionToTest=functionsToTest{f};
                [timed_mat, timed_mex, max_abs_err, max_rel_err, mean_abs_err, mean_rel_err, fun_out_mat, fun_out_mex] = time_cpr_fun_1arg(functionToTest, N_elements, flag_plot, TT);
                output_summary(f,:)=[timed_mat, timed_mex, max_abs_err, max_rel_err, mean_abs_err, mean_rel_err, timed_mat/N_elements, timed_mex/N_elements];
            end
            mean_abs_errs=output_summary(:,6);
            max_abs_errs =output_summary(:,4);

            % Display summary plots
            if flag_plot==1
                handles_fig = displaySummaryPlots(output_summary, functionsToTest, N_elements);
            end
            
            % Assert that functions give similar output
            testCase.verifyLessThan(max(mean_abs_errs), max_allowed_rel_diff);
            testCase.verifyLessThan(max(max_abs_errs) , max_allowed_rel_diff);
        end
        
        %% Test the functions requiring 3 arguments within an extended T, B, RRR range
        function test_funcs_3arg_extended_range(testCase)
            % Select inputs
            functionsToTest=testCase.functionsToTest_3arg; % list of all functions to test
            N_elements=testCase.N_elements; % number of values to calculate
            max_allowed_rel_diff=testCase.max_allowed_rel_diff;
            flag_plot=1;
            
            % Get input values to test
            inputValues=getInputValues(N_elements);
            TT=inputValues.T_extended_range;
            BB=inputValues.B_extended_range;
            RRR=inputValues.RRR_extended_range;

            % Run tests
            n_functionsToTest=length(functionsToTest);
            output_summary=zeros(n_functionsToTest,8);
            for f=1:n_functionsToTest
                functionToTest=functionsToTest{f};
                [timed_mat, timed_mex, max_abs_err, max_rel_err, mean_abs_err, mean_rel_err, fun_out_mat, fun_out_mex] = time_cpr_fun_3arg(functionToTest, N_elements, flag_plot, TT, BB, RRR);
                output_summary(f,:)=[timed_mat, timed_mex, max_abs_err, max_rel_err, mean_abs_err, mean_rel_err, timed_mat/N_elements, timed_mex/N_elements];
            end
            mean_abs_errs=output_summary(:,6);
            max_abs_errs =output_summary(:,4);

            % Display summary plots
            if flag_plot==1
                handles_fig = displaySummaryPlots(output_summary, functionsToTest, N_elements);
            end
            
            % Assert that functions give similar output
            testCase.verifyLessThan(max(mean_abs_errs), max_allowed_rel_diff);
            testCase.verifyLessThan(max(max_abs_errs) , max_allowed_rel_diff);
        end
        
    end

end 


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% % % Helper functions
function inputValues = getInputValues(N_elements)
    % Calculate input values for the functions to test
    inputValues.T_limited_range     = 10.^linspace(+1,+2.47712125471966,N_elements); % 10 < T < 300 K
    inputValues.T_limited_range_col = 10.^linspace(+1,+2.47712125471966,N_elements)'; % 10 < T < 300 K
    inputValues.T_extended_range    = 10.^linspace(-2,+3,N_elements); % 1 mK < T < 1000 K
    inputValues.T_zero  = 0;
    inputValues.T_negative = -10;
%     B_limited_range
    inputValues.B_extended_range    = linspace(0,20,N_elements);
%     B_negative
%     RRR_limited_range
    inputValues.RRR_extended_range    = linspace(1,500,N_elements);
%     RRR_negative    
end

function handles_fig = displaySummaryPlots(output_summary, functionsToTest, N_elements)
    % Display summary plots
    n_functionsToTest=length(functionsToTest);
    
    handles_fig(1)=figure;
    hold on,box on,grid on
    plot(1:n_functionsToTest,output_summary(:,1)*1e3,'ko-')
    plot(1:n_functionsToTest,output_summary(:,2)*1e3,'r*-')
    set(gca, 'XTick',1:n_functionsToTest, 'XTickLabel',functionsToTest)
    legend('m','mex')
    ylabel(['Time to calculate ' num2str(N_elements) ' values [ms]'])

    handles_fig(2)=figure;
    hold on,box on,grid on
    plot(1:n_functionsToTest,output_summary(:,7)*1e6,'ko-')
    plot(1:n_functionsToTest,output_summary(:,8)*1e6,'r*-')
    set(gca, 'XTick',1:n_functionsToTest, 'XTickLabel',functionsToTest)
    legend('m','mex')
    ylabel(['Time to run per calculated value [\mus]'])

    handles_fig(3)=figure;
    hold on,box on,grid on
    plot(1:n_functionsToTest,output_summary(:,4),'ko-')
    set(gca, 'XTick',1:n_functionsToTest, 'XTickLabel',functionsToTest)
    set(gca,'YScale','log')
    ylabel(['Max relative error [-]'])
    ylim([0 2])

    handles_fig(4)=figure;
    hold on,box on,grid on
    plot(1:n_functionsToTest,output_summary(:,6),'ko-')
    set(gca, 'XTick',1:n_functionsToTest, 'XTickLabel',functionsToTest)
    set(gca,'YScale','log')
    ylabel(['Mean relative error [-]'])
end