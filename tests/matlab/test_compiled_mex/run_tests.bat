@echo off

REM Change directory to the desired folder
cd ..\..\mex_compilation

REM Run the batch file "RUN_compile_c_to_mex"
call matlab -nosplash -noFigureWindows -r "try; run('compile_c_to_mex.m'); catch; end; quit"

REM Delay for 10 seconds and then display the message to continue
timeout /t 20 /nobreak >nul
echo Press Enter to continue...
pause >nul

REM Change directory back to the original folder
cd ..\tests\test_compiled_mex

REM Run the Python file "MatLab_Mex_templating.py" and log the output
REM start /wait python "MatLab_Mex_templating.py" >> output_logfile.txt
REM echo Python file execution is over

REM Run the MATLAB file "STEAM_MatPro_results_v2"
call matlab -r "run('.\STEAM_MatPro_results_v2.m'); exit;"

REM Run the MATLAB file "STEAM_MatPro_results_v2_2"
call matlab -r "run('.\STEAM_MatPro_results_v2_2.m'); exit;"

REM Run the MATLAB file "STEAM_MatPro_results_v2_3"
call matlab -r "run('.\STEAM_MatPro_results_v2_3.m'); exit;"