% mex -output CFUN_CvAg_v1.mex ..\..\mex_compilation\generic_wrapper.cpp  ..\..\Source_c\CFUN_CvAg_v1.c
% 
% TT = linspace(0,1000,1000);
% B = 5*ones(1000);
% Tc0 = 9.5*ones(1, 1000);
% Bc20 = 15*ones(1, 1000);
% CJ = 1*ones(1, 1000);
% p = 1*ones(1, 1000);
% q = 1*ones(1, 1000);
% wireDiameter = 1*ones(1, 1000);
% Cu_noCu = 1*ones(1, 1000);
% 
% 
% [out_var_mex, ~] = CFUN_CvNb3Sn_v2('CFUN_CvNb3Sn_v2', TT, B,Tc0, Bc20, CJ, p, q, wireDiameter, Cu_noCu);
% [out_var_mex, ~] = CFUN_CvAg_v1('CFUN_CvAg_v1', TT);
% hold on
% [out_var_mex, ~] = CFUN_kAg_RRR30_v1('CFUN_kAg_RRR30_v1', TT);
% 
% plot(TT, out_var_mex)

% mex -output CFUN_kCu_Wiedemann_v1.mex ..\..\mex_compilation\generic_wrapper.cpp  ..\..\Source_c\CFUN_kCu_Wiedemann_v1.c
% 
% TT = linspace(0,10,1000);
% B = 5*ones(1000);
% Tc0 = 9.5*ones(1, 1000);
% Bc20 = 15*ones(1, 1000);
% CJ = 1*ones(1, 1000);
% p = 1*ones(1, 1000);
% q = 1*ones(1, 1000);
% wireDiameter = 1*ones(1, 1000);
% Cu_noCu = 1*ones(1, 1000);
% 
% 
% [out_var_mex, ~] = CFUN_kCu_Wiedemann_v1('CFUN_kCu_Wiedemann_v1',TT,p);
% figure
% plot(TT, out_var_mex)

% parentDirectory = fileparts(pwd);
% addpath( genpath( fullfile(parentDirectory, '..\compile_STEAM_MatPro\compiled_STEAM_MatPro\') ))
% functionSet='mex'; % functionSet='m'; in case we want to use the original MatLab functions
% STEAM_ML = STEAM_MatPro(functionSet);
% fixedTc=15;
% fixedTc0=18;
% fixedTcs=12;
% 
% MP=STEAM_MatPro('mex');
% T2=ones(1,10e3)*1.9;
% for n=1:1000
%     temptemp=MP.handle_CpNb3Sn(T2,fixedTc,fixedTc0,fixedTcs)
%     if length(find(isnan(temptemp)))>0
%         display('FAULT')
%         idxFaulty=find(isnan(temptemp))
%         temptemp(idxFaulty)
%         T2(idxFaulty)
%     end
% end

%Setup material properties by assigning STEAM_MatPro function handles to current functions
% clear
% clc
% parentDirectory = fileparts(pwd);
% addpath( genpath( fullfile(parentDirectory, '..\compile_STEAM_MatPro\compiled_STEAM_MatPro\') ))
% functionSet='mex'; % functionSet='m'; in case we want to use the original MatLab functions
% STEAM_ML = STEAM_MatPro('mex');
% 
% T = linspace(0,10,1000);
% B = 5*ones(1,1000);
% Jc_Nb3Sn0 = 14.2196*ones(1,1000);
% Tc0_Nb3Sn = 18*ones(1, 1000);
% Bc20_Nb3Sn = 29*ones(1, 1000);
% 
% % fitParameters = struct('Tc0', 9.5, 'Bc20', 15, 'C1', 1, 'C2', 1);
% fitParameters = struct('Tc0', 9.5, 'Bc20', 15, 'CJ', 1, 'p', 1, 'q', 1 );
% Tc0 = 9.5*ones(1, 1000);
% Bc20 = 15*ones(1, 1000);
% CJ = 1*ones(1, 1000);
% p = 1*ones(1, 1000);
% q = 1*ones(1, 1000);
% 
% wireParameters = struct('wireDiameter', 1, 'Cu_noCu', 1);
% 
% % Define T, B & RRR_Cu
% res1 = STEAM_ML.handle_Jc_Nb3Sn_Bottura(T, B, fitParameters,  wireParameters);
% 
% STEAM_ML = STEAM_MatPro('m');
% res2 = STEAM_ML.handle_Jc_Nb3Sn_Bottura(T, B, fitParameters,  wireParameters);
% 
% figure
% plot(T, res1)
% figure;
% plot(T,res2,'o')

% clear
% clc
parentDirectory = fileparts(pwd);
addpath( genpath( fullfile(parentDirectory, '..\..\..\bindings\matlab\compiled_STEAM_MatPro\') ))
functionSet='mex'; % functionSet='m'; in case we want to use the original MatLab functions
STEAM_ML = STEAM_MatPro('mex');

T = linspace(0,1000,1000);
B = 12*ones(1,1000);
Jc_Nb3Sn0 = 14.2196*ones(1,1000);
Tc0_Nb3Sn = 18*ones(1, 1000);
Bc20_Nb3Sn = 29*ones(1, 1000);

% fitParameters = struct('Tc0', 9.5, 'Bc20', 15, 'C1', 1, 'C2', 1);
fitParameters = struct('Tc0', 9.5, 'Bc20', 15, 'CJ', 1, 'p', 1, 'q', 1 );
Tc0 = 9.5*ones(1, 1000);
Bc20 = 15*ones(1, 1000);
CJ = 273*ones(1, 1000);
p = 140*ones(1, 1000);
RRR = 1*ones(1, 1000);
% 
% wireParameters = struct('wireDiameter', 1, 'Cu_noCu', 1);
% 
% Define T, B & RRR_Cu
% res1 = STEAM_ML.handle_kCu_Wiedemann(T, B, RRR, p);
res1 = STEAM_ML.handle_kBeCu_WiedemannFranz(T, p);
% res1 = STEAM_ML.handle_rhoCu(T, B, p, CJ);
% 
STEAM_ML = STEAM_MatPro('m');
res2 = STEAM_ML.handle_kCu_Wiedemann(T, B, RRR, p);
% res2 = STEAM_ML.handle_kBeCu_WiedemannFranz(T, p);
% res2 = STEAM_ML.handle_rhoCu(T, B, p, CJ);
% 
% figure
% plot(T, res1)
% hold on
% %figure;
% plot(T,res2,'o')

% clc

%  mex -output CFUN_Tcs_Nb3Sn_v1.mex ..\..\mex_compilation\generic_wrapper.cpp  ..\..\Source_c\CFUN_Tcs_Nb3Sn_v1.c
%  mex -output CFUN_Jc_Nb3Sn_Bordini_v1.mex ..\..\mex_compilation\generic_wrapper.cpp  ..\..\Source_c\CFUN_Jc_Nb3Sn_Bordini_v1.c
 
% parentDirectory = fileparts(pwd);
% addpath( genpath( fullfile(parentDirectory, '..\compile_STEAM_MatPro\compiled_STEAM_MatPro\') ))
% functionSet='mex'; % functionSet='m'; in case we want to use the original MatLab functions
% STEAM_ML = STEAM_MatPro('mex');
% 
% T = linspace(15,300,1000);
% B = 12*ones(1,1000);
% Jc_Nb3Sn0 = 14.2196*ones(1,1000); %14.2196
% Tc0_Nb3Sn = 18*ones(1, 1000);
% Bc20_Nb3Sn = 29*ones(1, 1000);
% % Jc0 = 250*ones(1,1000);
% 
% % fitParameters = struct('Tc0', 9.5, 'Bc20', 15, 'C1', 1, 'C2', 1);
% fitParameters = struct('Tc0', 9.5, 'Bc20', 15, 'CJ', 1, 'p', 1, 'q', 1 );
% Tc0 = 9.5*ones(1, 1000);
% Bc20 = 15*ones(1, 1000);
% CJ = 273*ones(1, 1000);
% p = 140*ones(1, 1000);
% q = 1*ones(1, 1000);
% alpha = 1.01*ones(1,1000);
% 
% wireParameters = struct('wireDiameter', 1, 'Cu_noCu', 1);
% % [Jc0, ~] = CFUN_Jc_Nb3Sn_Bordini_v1('CFUN_Jc_Nb3Sn_Bordini_v1', 0*ones(1,1000), B, Jc_Nb3Sn0, Tc0_Nb3Sn, Bc20_Nb3Sn, alpha);
% % [res1,~] = CFUN_Tcs_Nb3Sn_v1('CFUN_Tcs_Nb3Sn_v1', T, B, Jc0, alpha, Tc0_Nb3Sn, Bc20_Nb3Sn);
% 
% STEAM_ML = STEAM_MatPro('mex');
% % Define T, B & RRR_Cu
% % res1 = STEAM_ML.handle_kBeCu_WiedemannFranz(T, p);
% % res1 = STEAM_ML.handle_rhoCu(T, B, p, CJ);
% [res1, res3] = STEAM_ML.handle_Tcs_Nb3Sn_v1_Bordini(T, B, Jc_Nb3Sn0, Tc0_Nb3Sn, Bc20_Nb3Sn, alpha)
% 
% STEAM_ML = STEAM_MatPro('m');
% % res2 = STEAM_ML.handle_kBeCu_WiedemannFranz(T, p);
% % res2 = STEAM_ML.handle_rhoCu(T, B, p, CJ);
% [res2, res4] = STEAM_ML.handle_Tcs_Nb3Sn_v1_Bordini(T, B, Jc_Nb3Sn0, Tc0_Nb3Sn, Bc20_Nb3Sn, alpha)
% 
% figure
% plot(T, res1)
% figure;
% plot(T,res2,'o')







