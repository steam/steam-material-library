clc 
clear
parentDirectory = fileparts(pwd);
addpath( genpath( fullfile(parentDirectory, '..\compile_STEAM_MatPro\compiled_STEAM_MatPro\') ))
figureFormats = {'png', 'fig', 'svg'};
figures_path = fullfile(parentDirectory, '..', 'docs', 'dynamic', 'MatLab_Mex_graphs');
errors_path = fullfile(parentDirectory, 'test_compiled_mex','error.graphs');
functions_with_structs = {'Jc_Nb3Sn_Bottura', 'Jc_NbTi_Bottura', 'Jc_NbTi_Cudi_fit1', 'Jc_NbTi_Cudi'};
C_funciton_names = {'CFUN_Jc_Nb3Sn_Bottura_v1', 'CFUN_Jc_NbTi_Bottura_v1', 'CFUN_Jc_NbTi_Cudi_fit1_v1', 'CFUN_Jc_NbTi_Cudi_v1'}

functionSet='mex'; % functionSet='m'; in case we want to use the original MatLab functions
STEAM_ML = STEAM_MatPro('mex');

T = linspace(0,10,1000);
B = 5*ones(1,1000);
Jc_Nb3Sn0 = 14.2196*ones(1,1000);
Tc0_Nb3Sn = 18*ones(1, 1000);
Bc20_Nb3Sn = 29*ones(1, 1000);

% fitParameters = struct('Tc0', 9.5, 'Bc20', 15, 'C1', 1, 'C2', 1);
fitParameters1 = struct('Tc0', 9.2, 'Bc20', 14.5, 'CJ', 1, 'p', 1, 'q', 1 );
fitParameters2 = struct('Tc0', 9.2, 'Bc20', 14.5,'Jc_ref', 1, 'C0', 27.04, 'alpha', 0.57, 'beta', 0.9, 'gamma', 2.32 );
fitParameters3 = struct('Tc0', 9.2, 'Bc20', 14.5, 'C1', 34404.9, 'C2', 2566.9 );
fitParameters4 = struct('Tc0', 9.2, 'Bc20', 14.5, 'c1', 6581.9, 'c2', -453.3, 'c3', 200, 'c4', 50, 'c5', 10, 'c6', 500);
Tc0 = 9.5*ones(1, 1000);
Bc20 = 15*ones(1, 1000);
CJ = 1*ones(1, 1000);
p = 1*ones(1, 1000);
q = 1*ones(1, 1000);

wireParameters = struct('wireDiameter', 0.001, 'Cu_noCu', 1);

% Define T, B & RRR_Cu
[res1, res1a] = STEAM_ML.handle_Jc_Nb3Sn_Bottura(T, B, fitParameters1,  wireParameters);
[res2, res2a] = STEAM_ML.handle_Jc_NbTi_Bottura(T, B, fitParameters2,  wireParameters);
[res3, res3a] = STEAM_ML.handle_Jc_NbTi_Cudi_fit1(T, B, fitParameters3,  wireParameters);
[res4, res4a] = STEAM_ML.handle_Jc_NbTi_Cudi(T, B, fitParameters4,  wireParameters);

STEAM_ML = STEAM_MatPro('m');
[res1b, res1c] = STEAM_ML.handle_Jc_Nb3Sn_Bottura(T, B, fitParameters1,  wireParameters);
[res2b, res2c] = STEAM_ML.handle_Jc_NbTi_Bottura(T, B, fitParameters2,  wireParameters);
[res3b, res3c] = STEAM_ML.handle_Jc_NbTi_Cudi_fit1(T, B, fitParameters3,  wireParameters);
[res4b, res4c] = STEAM_ML.handle_Jc_NbTi_Cudi(T, B, fitParameters4,  wireParameters);

% Modify the saveFigureInMultipleFormats function to accept the figure handle
for i = 1:8
    % Create figure
    fig = figure;

    % Plot data
    switch i
        case 1
            plot(T, res1a, 'or');
            hold on;
            plot(T, res1c);
            legend('mex', 'm');
            xlabel('T [K]');
            ylabel('Jc [A/m]'); 
            title('Jc - Jc\_Nb3Sn\_Bottura');
        case 2
            plot(T, res1, 'or');
            hold on;
            plot(T, res1b);
            legend('mex', 'm');
            xlabel('T [K]');
            ylabel('Ic [A]'); 
            title('Ic - Jc\_Nb3Sn\_Bottura');
        case 3
            plot(T, res2a, 'or');
            hold on;
            plot(T, res2c);
            legend('mex', 'm');
            xlabel('T [K]');
            ylabel('Jc [A/m]'); 
            title('Jc - Jc\_NbTi\_Bottura');
        case 4
            plot(T, res2, 'or');
            hold on;
            plot(T, res2b);
            legend('mex', 'm');
            xlabel('T [K]');
            ylabel('Ic [A]'); 
            title('Ic - Jc\_NbTi\_Bottura');
        case 5
            plot(T, res3a, 'or');
            hold on;
            plot(T, res3c);
            legend('mex', 'm');
            xlabel('T [K]');
            ylabel('Jc [A/m]'); 
            title('Jc - Jc\_NbTi\_Cudi\_fit1');
        case 6
            plot(T, res3, 'or');
            hold on;
            plot(T, res3b);
            legend('mex', 'm');
            xlabel('T [K]');
            ylabel('Ic [A]'); 
            title('Ic - Jc\_NbTi\_Cudi\_fit1');
        case 7
            plot(T, res4a, 'or');
            hold on;
            plot(T, res4c);
            legend('mex', 'm');
            xlabel('T [K]');
            ylabel('Jc [A/m]'); 
            title('Jc - Jc\_NbTi\_Cudi');
        case 8
            plot(T, res4, 'or');
            hold on;
            plot(T, res4b);
            legend('mex', 'm');
            xlabel('T [K]');
            ylabel('Ic [A]'); 
            title('Ic - Jc\_NbTi\_Cudi');
    end

    % Save figure in multiple formats
    saveFigureInMultipleFormats(fig, figures_path,[functions_with_structs{ceil(i/2)}], {'png', 'svg', 'fig'});
    
    % Close the figure
    close(fig);
end

%error plots
% Create and save the error graphs
for i = 1:8
    % Create figure
    fig = figure;

    % Plot data
    switch i
        case 1
            plot(T, abs((res1a-res1c)./res1a), '*');
            set(gca, 'XScale', 'log'); 
            set(gca, 'YScale', 'log');
            xlabel('T [K]');
            ylabel('relative error Jc -log'); 
            title('error for Jc - Jc\_Nb3Sn\_Bottura');
        case 2
            plot(T, abs((res1-res1b)./res1b), '*');
            set(gca, 'XScale', 'log'); 
            set(gca, 'YScale', 'log');
            xlabel('T [K]');
            ylabel('relative error Ic -log'); 
            title('error for Ic - Jc\_Nb3Sn\_Bottura');
        case 3
            plot(T, abs((res2a-res2c)./res2a), '*');
            set(gca, 'XScale', 'log'); 
            set(gca, 'YScale', 'log');
            xlabel('T [K]');
            ylabel('relative error Jc -log'); 
            title('error for Jc - Jc\_NbTi\_Bottura');
        case 4
            plot(T, abs((res2-res2b)./res2b), '*');
            set(gca, 'XScale', 'log'); 
            set(gca, 'YScale', 'log');
            xlabel('T [K]');
            ylabel('relative error Ic -log'); 
            title('error for Ic - Jc\_NbTi\_Bottura');
        case 5
            plot(T, abs((res3a-res3c)./res3a), '*');
            set(gca, 'XScale', 'log'); 
            set(gca, 'YScale', 'log');
            xlabel('T [K]');
            ylabel('relative error Jc -log'); 
            title('error for Jc - Jc\_NbTi\_Cudi\_fit1');
        case 6
            plot(T, abs((res3-res3b)./res3b), '*');
            set(gca, 'XScale', 'log'); 
            set(gca, 'YScale', 'log');
            xlabel('T [K]');
            ylabel('relative error Ic -log'); 
            title('error for Ic - Jc\_NbTi\_Cudi\_fit1');
        case 7
            plot(T, abs((res4a-res4c)./res4a), '*');
            set(gca, 'XScale', 'log'); 
            set(gca, 'YScale', 'log');
            xlabel('T [K]');
            ylabel('relative error Jc -log'); 
            title('error for Jc - Jc\_NbTi\_Cudi');
        case 8
            plot(T, abs((res4-res4b)./res4b), '*');
            set(gca, 'XScale', 'log'); 
            set(gca, 'YScale', 'log');
            xlabel('T [K]');
            ylabel('relative error Ic -log'); 
            title('error for Ic - Jc\_NbTi\_Cudi');
    end

    % Save figure in multiple formats
    % Save figure in multiple formats
    saveFigureInMultipleFormats(fig, errors_path, [functions_with_structs{ceil(i/2)}], {'png', 'svg', 'fig'});
    
    % Close the figure
    close(fig);
end

function cell2csv(fileName, cellArray)
    %CELL2CSV Write cell array to a CSV file
    %   CELL2CSV(fileName, cellArray) writes the cell array to the specified
    %   CSV file. Each cell of the array is written as a separate element in
    %   the CSV file, with values separated by commas.

    file = fopen(fileName, 'w');
    for i = 1:size(cellArray, 1)
        for j = 1:size(cellArray, 2)
            fprintf(file, '%s', cellArray{i, j});
            if j ~= size(cellArray, 2)
                fprintf(file, ',');
            end
        end
        fprintf(file, '\n');
    end
    fclose(file);
end


function saveFigureInMultipleFormats(figHandle, directoryPath, fileName, figureFormats)
    % Create a cell array of formats
    figureFormats = {'png', 'svg', 'fig'};

    % Iterate through each format and save the figure
    for f = 1:length(figureFormats)
        fileExt = figureFormats{f};
        figureFileName = [fileName, '.', fileExt];
        figureFilePath = fullfile(directoryPath, figureFileName);

        % Save the figure based on the format
        if strcmpi(fileExt, 'png')
            % Save as PNG
            print(figHandle, figureFilePath, '-dpng');
        elseif strcmpi(fileExt, 'svg')
            % Save as SVG
            print(figHandle, figureFilePath, '-dsvg');
        elseif strcmpi(fileExt, 'fig')
            % Save as FIG
            set(gcf, 'visible', 'on');
            savefig(figHandle, figureFilePath);
            set(gcf, 'visible', 'on');
        else
            % disp(['Unsupported format: ', fileExt]);
        end
    end
end
