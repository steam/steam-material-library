%% Import libraries of source .m functions and compiled .mex functions
clear all
clc
pathMaterialLibraryM  =fullfile(fileparts(fileparts(pwd)), 'source_m');
pathMaterialLibraryMex=fullfile(fileparts(fileparts(pwd)), 'mex_compilation');
addpath(genpath(pathMaterialLibraryM))
addpath(genpath(pathMaterialLibraryMex))


%% Setup diary file
nameDiaryFile='log_test_compiled_mex.txt';
diary(nameDiaryFile)

%% Delete output file, if present
fullName_passFile = fullfile([pwd], 'output', 'passed.txt');
if isfile(fullName_passFile)==1
    delete(fullName_passFile)
end

%% Run test
import matlab.unittest.TestSuite

testCase = TestCompiledMex;
res = run(testCase)

flag_all_passed=res.Passed;


%% Write output file if all tests passed
if flag_all_passed == 1
    if isdir(fileparts(fullName_passFile))==0
        mkdir(fileparts(fullName_passFile))
    end
    % Make a local file
    fid = fopen(fullName_passFile , 'w' );
    fprintf( fid, '%s\n', 'passed');
    fclose(fid);
    display('All tests passed.')
else
    warning('Some tests did not pass!')
end

diary off