# Mex - MatLab tests description

## Introduction
The `steam-material-library/models/c` ([here](https://gitlab.cern.ch/steam/steam-material-library/-/tree/master/models/c)) contains the original material functions written in C. 
Now, using the `generic_wrapper.cpp` from `steam-material-library/mex_compilation` folder we can convert C files to .mex.
In the `steam-material-library/models/matlab` folder ([here](https://gitlab.cern.ch/steam/steam-material-library/-/tree/master/models/matlab)), one can find the corresponding material functions written and  used in MatLab.

The purpose of this procedure is to compile the c functions into mex files and compare them with the MatLab ones, as far as speed and result accuracy is concerned.

## Steps to run the tests

1) Clone the Git repository `steam-material-library` ([here](https://gitlab.cern.ch/steam/steam-material-library))
2) Open powershell and run `.\tests\matlab\test_compiled_mex\run_tests.bat` **or** just double-click on the batch file.
3) Wait for the first part of the procedure to finish and then press 'enter', just as the command window indicates to continue to the next steps of the procedure.

After the procedure is finished, you will be able to see 2 folders, one named `Outputs` and the other named `error.graphs`.
The first one includes the .txt files with the results of the **MatLab** scripts in the 1<sup>st</sup> column, and the results 
of the corresponding mex scripts in the 2<sup>nd</sup> column. 
The 3<sup>d</sup> column includes the absolute error, whereas the 4<sup>th</sup> contains the percentage error. 
The percentage error plots are extracted and saved in the `error.graphs` folder, one for each material function.

Additionally, an output .txt file will appear in the same repository, named `output_logfile.txt`. It includes what is written on the
command window during the tests. Finally, one can use information from the log file named `log_compile_c_to_mex.txt`, that will be automatically created during the mex
compilation on the `steam-material-library/mex_compilation` folder. The information is about which functions were able to compile into mex and which had issues.

## Inputs for the testing functions
To change the ranges of the functions' inputs, open `mat_mex_tests.xlsx` excel file in this specific folder, and for each function
the input ranges are in a row, as the original function indicates. For the functions with the same number of input parameters in MatLab and Mex,
we use the same row of parameters, so we don't have to write them twice. For functions with different number instead, 
we should first provide the MatLab inputs and then continue with the mex/c ones. For more details on the structure of the inputs, one can search the original functions on their corresponding folders 
(Those are mentioned [here](#introduction)).
