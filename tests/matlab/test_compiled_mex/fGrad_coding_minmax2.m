function [ rgb_output ] = fGrad_coding_minmax2( input_data, external_max_input_data, external_min_input_data )
% % % Function which gives as output 3xN matrix containing the RGB color
% coding to be used in the plotting options
% E. Ravaioli, CERN, 2016

    n_points_input=length(input_data);
    grad_input=zeros(n_points_input,1);
    red_input=zeros(n_points_input,1);
    green_input=zeros(n_points_input,1);
    blue_input=zeros(n_points_input,1);

    if nargin<2
        max_input_data=max(input_data);         % normalized to maximum element
    else
        max_input_data=external_max_input_data; % normalized to a given maximum
    end
    
    if nargin<3
        min_input_data=min(input_data);         % normalized to minimum element
    else
        min_input_data=external_min_input_data; % normalized to a given minimum
    end

    for bb=1:n_points_input
        grad_input(bb)=(input_data(bb)-min_input_data)/(max_input_data-min_input_data);
        if grad_input(bb)<=1/3
            red_input(bb)=0;                   green_input(bb)=grad_input(bb)*3;             blue_input(bb)=1;
        elseif grad_input(bb)<=2/3
            red_input(bb)=(grad_input(bb)-1/3)*3;     green_input(bb)=1;                     blue_input(bb)=1-(grad_input(bb)-1/3)*3;
        else
            red_input(bb)=1;                   green_input(bb)=1-(grad_input(bb)-2/3)*3;     blue_input(bb)=0;
        end
    end
    if n_points_input==1
        rgb_output=[0 0 0];
    elseif n_points_input==2
        rgb_output=[1 0 0; 0 1 0];
    elseif n_points_input==3
        rgb_output=[1 0 0; 0 1 0; 0 0 1];
    elseif n_points_input==4
        rgb_output=[1 0 0; 0 1 0; 0 0 1; 1 0 1];
    else
        rgb_output=[red_input green_input blue_input];
    end
end
