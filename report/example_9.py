# Plotting
import matplotlib.pyplot as plt
import matplotlib

# Others
import numpy as np
import os

# Material functions: you need to install STEAM-materials, 'pip install STEAM-materials'
import 	steammaterials
from 	steammaterials.STEAM_materials import STEAM_materials

# It needs to know where the .dll files are stored. So it finds the path to it.
matpath: str = os.path.dirname(steammaterials.__file__)

cmap = matplotlib.cm.get_cmap('Spectral')

# Example with 1 input value
T = np.linspace(2.0, 550, 1000)
numpy2d = T.reshape((1, len(T)))
sm = STEAM_materials('CFUN_CvCu_v1', numpy2d.shape[0], numpy2d.shape[1], matpath)
out = sm.evaluate(numpy2d)

fig1 = plt.figure()
ax1 = fig1.add_subplot(111)
ax1.plot(T, out)
ax1.set_xlabel('Temperature, K')
ax1.set_ylabel('Volumetric Heat Capacity, J/kgm3')

plt.show()